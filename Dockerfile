FROM node:11-slim

LABEL maintainer="{aecio.santos, bowen.yu, cristian.felix, jorgehpo, s.castelo}@nyu.edu"

# Install all apt packages
# (all apt packages should be included here in lexicographic order)
RUN apt-get update && apt-get install -y \
  bash \
  git \
  python2.7 \
  python-pip \
  python3-pip \
  unzip \
  && rm -rf /var/lib/apt/lists/* \
  && pip install --upgrade pip

# Install global npm packages
RUN npm install -g pm2

# Install rulematrix dependencies
WORKDIR /ta3/rulematrix
ADD rulematrix/requirements.txt .
ADD rulematrix/install_pyfim.sh .
RUN pip3 install -r requirements.txt && \
    chmod +x install_pyfim.sh && \
    ./install_pyfim.sh
ENV PYTHONPATH /ta3/rulematrix
ENV RULEMATRIX_ROOT=/ta3/rulematrix

# Install common, and server module dependencies
# (We add here, before adding the full sources, to cache this layer
# during the docker build)

# common d3m module
WORKDIR /ta3/d3m
ADD d3m/package*.json ./
RUN npm install /ta3/d3m/
# server module
WORKDIR /ta3/server
ADD server/package*.json ./
RUN npm install /ta3/server/
# setup TA3-TA2 gRPC API
ADD server/fetch-ta3ta2api.sh .
RUN ./fetch-ta3ta2api.sh

# Add remaining source code
WORKDIR /ta3
COPY . /ta3

# Install dependencies and build common module
# (Note that the server depends on the this projects's node_modules/
# at runtime, so we have to keep it in the image)
WORKDIR /ta3/d3m
RUN npm install /ta3/d3m/ && \
    npm run build

# Install dependencies and build server module
# (Note that the server requires node_modules/ at runtime,
#  so we have to keep it in the image)
WORKDIR /ta3/server
RUN cd /ta3/server/ && npm run build

# Install dependencies and build client module
# (Note that the client does not require node_modules/ at runtime,
#  so we remove it to save space in the docker image)
WORKDIR /ta3/client
RUN cd /ta3/client/ && \
    npm install && \
    npm run build && \
    rm -r node_modules

# Move executables to system's binaries directory
RUN mv /ta3/ta3_search /usr/local/bin/ta3_search && \
  chmod +x /usr/local/bin/ta3_search

# Set default environment variables
ENV PORT 80
ENV NODE_ENV production
ENV GRPC_HOST localhost
ENV GRPC_PORT 45042

# The server runs on port 80 by default
EXPOSE 80

CMD ["ta3_search"]
