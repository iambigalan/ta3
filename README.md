# Visus

Visus is an interactive system for automatic machine learning model building and curation.

# Installation

### Datasets

To use the system, you need to first download D3M datasets avaiable at https://gitlab.datadrivendiscovery.org/d3m/datasets. Follow the instructions on that repository to clone the datasets using Git LFS.

Alternatively, you can download the datasets from https://datadrivendiscovery.org/data/seed_datasets_current/ using the following command:
```
wget -r -np -R "index.html*" -nH --header Authorization:$TOKEN https://datadrivendiscovery.org/data/seed_datasets_current/
```

where `$TOKEN` is available at: https://api-token.datadrivendiscovery.org/#!/login

### D3M Common Typings and Declarations (at /d3m)

This module contains shared code between both server and client modules. 

To install the dependencies and build this package, run:
```
npm install && npm run build
```

After this, the ``d3m/dist`` folder will contain importable shared typings and declarations common to client and server modules.
To use those: ``import * as d3m from 'd3m/dist'``.
If you change any files inside ``/d3m``, you must rebuild the dist files.
To automatically rebuild on active development, use:
 ```
npm run watch
```

### Web Client (at /)

This is a React app that runs on the browser.

Install client module dependencies:

```
npm install
```

Run in development mode:

```
npm start
```
- This enables watch and automatic rebuilds while you edit the source.
- If you need HTTP APIs served for your dev web view, you must run the dev server alongside this.

Build production files: 
```
npm run build
```
- This creates production files at `/build/`.

Local view of production files:
```
npm install -g serve
serve -s build
```

To run unit tests (which use Jest):

```
npm test
```


### Express Server (at server/)

This module is the backend server built using Express and Node.js.

Install npm dependencies:

```bash
npm install
```

Pull the `ta3ta2-api` locally and prepare its google protobuf dependencies:
```bash
./fetch-ta3ta2api.sh
```

To use RuleMatrix:
```bash
cd ../rulematrix/
# install dependencies in the `requirements.txt` through pip  (at rulematrix/):
pip install -r requirements.txt
# Install the pyfim package  (at rulematrix/):
bash install_pyfim.sh
# export the `PYTHONPATH` and `RULEMATRIX_ROOT` to include the local rulematrix module (at server/)
cd ../server/
export PYTHONPATH=$HOME/workspace/ta3/rulematrix
export RULEMATRIX_ROOT=$HOME/workspace/ta3/rulematrix
```
Build the server module (this creates the server files at `/server/dist/`):
```bash
npm run build
```

Create an ``.env`` file with the fields defined in ``server/.env.template``:
```bash
cp server/.env.template server/.env
# then, edit server/.env with necessities of your dev environment
```

Generate the system configuration file (``config.local.json``) using (make sure to replace `{dataset_name}` with the dataset you want to use, and use your local machine paths):
```
python docker-compose/change_dataset.py {dataset_name} /Users/user/workspace/d3m/D3MData/ /Users/user/workspace/d3m/output/
```

Run the dev server (you must build the server before starting it):  
```
npm start
```


Alternatively, to automatically rebuild and run when the source code changes, use:
```bash
npm run watch
```

To execute the tests (Jest), use:

```
npm test
```


### Backend TA2 Server

The TA3 system depends on TA2 systems that implement the common [gRPC TA3TA2-API](https://gitlab.com/datadrivendiscovery/ta3ta2-api). To run a TA2 server you can either run a full-fledged TA2 system or run the TA2 stubs (mock API implementation) during development.

To run a full TA2 system, you can use the docker-compose file available at `docker-compose/docker-compose-ta2-dev.yml`:

```bash
# Create a config.local.json file configured with your local machine paths
python docker-compose/change_dataset.p {dataset_name} /Users/user/workspace/d3m/D3MData/ /Users/user/workspace/d3m/output/
# Start docker-compose with environment variables configured with your local machine paths
D3MOUTPUTDIR=/Users/user/workspace/d3m/D3MData/ \
D3MOUTPUTDIR=/Users/user/workspace/d3m/output/ \
docker-compose -f docker-compose-ta2-dev.yml up
```

To run the TA2 stubs for development, follow instructions on the [ta2ta3-stubs](https://gitlab.com/ViDA-NYU/d3m/ta2ta3-stubs) project.

### Build & Run Using Docker

Build the docker image:

```
docker build -t ta3 .
```

Make sure the ``server/ta3-ta2api`` and ``docker-compose/config.docker.json`` exist:

```
python docker-compose/change_dataset.py {dataset_name}
bash server/fetch-ta3ta2api.sh
```

Make sure that the folder ``../D3MData`` (relative to the ta3 repository) is a directory that contains the d3m dataset folders.
``../D3MData`` will be mounted by docker.
If ``../D3MData`` is a symlink, the linked directory must be accessible (shared) by docker.

Finally, start the docker containers using:

```
docker-compose -f docker-compose/docker-compose.yml up
```
The TA3 system will be available at [``http://localhost:80``](http://localhost:80).

Here is a sequence of commands that start the project's docker-compose from scratch:

```bash
# Clone and enter the app directory
git clone git@gitlab.com:ViDA-NYU/d3m/ta3.git ta3
cd ta3/

# Build the docker images named "ta3"
docker build . -t ta3

# Make sure that ../D3MData contains d3m datasets and is a directory accessible by docker.
# The output should contain dataset folders like:
#   26_radon_seed
#   185_baseball
#   ...
ls ../D3MData

# Fetch the ta3ta2-api
bash server/fetch-ta3ta2api.sh

# Generate a config file that will be mounted in docker.
# Note that unlike your local development environment, here the "/d3m" paths must not be changed.
# This is because docker mounts those resources to these fixed locations.
python docker-compose/change_dataset.py {dataset_name}

# Start docker containers
docker-compose -f docker-compose/docker-compose.yml up

# Finally, access TA3 at http://localhost.
```
