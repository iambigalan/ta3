import { PerformanceMetric } from './ta2';
import { TaskType, TaskSubType, MetricValue } from './problems';

export const metricNamesMapping: { [key: number]: string } = {
  [PerformanceMetric.METRIC_UNDEFINED]: 'Undefined',
  [PerformanceMetric.ACCURACY]: 'Accuracy',
  [PerformanceMetric.PRECISION]: 'Precision',
  [PerformanceMetric.RECALL]: 'Recall',
  [PerformanceMetric.F1]: 'F1',
  [PerformanceMetric.F1_MACRO]: 'F1 Macro',
  [PerformanceMetric.F1_MICRO]: 'F1 Micro',
  [PerformanceMetric.ROC_AUC]: 'ROC AUC',
  [PerformanceMetric.ROC_AUC_MACRO]: 'ROC AUC Macro',
  [PerformanceMetric.ROC_AUC_MICRO]: 'ROC AUC Micro',
  [PerformanceMetric.MEAN_SQUARED_ERROR]: 'Mean Squared Error',
  [PerformanceMetric.ROOT_MEAN_SQUARED_ERROR]: 'Root Mean Squared Error',
  [PerformanceMetric.MEAN_ABSOLUTE_ERROR]: 'Mean Absolute Error',
  [PerformanceMetric.R_SQUARED]: 'R Squared',
  [PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION]: 'Normalized Mutual Information',
  [PerformanceMetric.JACCARD_SIMILARITY_SCORE]: 'Jaccard Similarity Score',
  [PerformanceMetric.PRECISION_AT_TOP_K]: 'Precision at top K',
  [PerformanceMetric.OBJECT_DETECTION_AVERAGE_PRECISION]: 'Object Detection Average Precision',
  [PerformanceMetric.HAMMING_LOSS]: 'Hamming Loss',
  [PerformanceMetric.RANK]: 'Rank',
  [PerformanceMetric.LOSS]: 'Loss',
};

/**
 * List of metrics that should be regarged as error metrics,
 * i.e., the smaller the score value, the better the score.
 */
const errorMetrics = [
  PerformanceMetric.MEAN_SQUARED_ERROR,
  PerformanceMetric.ROOT_MEAN_SQUARED_ERROR,
  PerformanceMetric.MEAN_ABSOLUTE_ERROR,
  PerformanceMetric.HAMMING_LOSS,
];

export const isErrorMetric = (metric: PerformanceMetric) => {
  return errorMetrics.findIndex((m) => m === metric) >= 0;
}

/**
 * Checks if the lowerbound of the metric is zero.
 * An error metric, and most of the scores have zero lowerbounds.
 * The only exception right now is R-Squared.
 */
export const isZeroMinMetric = (metric: PerformanceMetric) => {
  return metric !== PerformanceMetric.R_SQUARED;
};

/**
 * Checks if a metric has a value between zero and one.
 */
const zeroOneMetrics = [
  PerformanceMetric.ACCURACY,
  PerformanceMetric.PRECISION,
  PerformanceMetric.RECALL,
  PerformanceMetric.F1,
  PerformanceMetric.F1_MACRO,
  PerformanceMetric.F1_MICRO,
  PerformanceMetric.ROC_AUC,
  PerformanceMetric.ROC_AUC_MACRO,
  PerformanceMetric.ROC_AUC_MICRO,
  PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION,
  PerformanceMetric.JACCARD_SIMILARITY_SCORE,
  PerformanceMetric.PRECISION_AT_TOP_K,
  PerformanceMetric.OBJECT_DETECTION_AVERAGE_PRECISION,
]

export const isZeroOneMetric = (metric: PerformanceMetric) => {
  return zeroOneMetrics.findIndex(m => m == metric) >= 0;
};

const ACC = { metric: MetricValue.accuracy, label: 'Accuracy' };
const PREC = { metric: MetricValue.precision, label: 'Precision' };
const REC = { metric: MetricValue.recall, label: 'Recall' };
const F1 = { metric: MetricValue.f1, label: 'F1' };
const F1_MICRO = { metric: MetricValue.f1Micro, label: 'Micro F1' };
const F1_MACRO = { metric: MetricValue.f1Macro, label: 'Macro F1' };
const MSE = { metric: MetricValue.meanSquaredError, label: 'Mean Squared Error' };
const RMSE = { metric: MetricValue.rootMeanSquaredError, label: 'Root Mean Squared Error' };
const MAE = { metric: MetricValue.meanAbsoluteError, label: 'Mean Absolute Error' };
const R2 = { metric: MetricValue.rSquared, label: 'R-Squared' };
const JACCARD = { metric: MetricValue.jaccardSimilarityScore, label: 'Jaccard Similarity' };
const NMI = { metric: MetricValue.normalizedMutualInformation, label: 'Normalized Mutual Information' };
const OBJ_DET_AP = { metric: MetricValue.objectDetectionAP, label: 'Average Precision' };

// TODO: Indentify which problem types use HammingLoss
// const HAMMING = { metric: MetricValue.hammingLoss, label: 'Hamming Loss' };

// ROC metrics are not yet supported
// const ROC_AUC = { metric: d3m.MetricValue.rocAuc, label: 'ROC AUC' };
// const ROC_AUC_MICRO = { metric: d3m.MetricValue.rocAucMicro, label: 'ROC AUC Micro' };
// const ROC_AUC_MACRO = { metric: d3m.MetricValue.rocAucMacro, label: 'ROC AUC Macro' };

export const getMetricsForTask = (taskType: TaskType, taskSubType?: TaskSubType) => {

  if (taskType === TaskType.classification) {
    const commonClassificationMetrics = [ACC];
    if (taskSubType === TaskSubType.binary) {
      // TODO: add ROC_AUC
      return [
        ...commonClassificationMetrics,
        PREC,
        REC,
        F1,
      ];
    } else if (taskSubType === TaskSubType.multiClass) {
      return [
        ...commonClassificationMetrics,
        F1_MICRO,
        F1_MACRO,
        // TODO: Add ROC_AUC_* when they become supported
        // ROC_AUC_MICRO,
        // ROC_AUC_MACRO,
      ];
    } else if (taskSubType === TaskSubType.multiLabel) {
      return [
        ...commonClassificationMetrics,
        F1_MICRO,
        F1_MACRO,
        // TODO: Add Hamming Loss when it becomes supported
        // HAMMING,
        // TODO: Add ROC_AUC_* when they become supported
        // ROC_AUC_MICRO,
        // ROC_AUC_MACRO,
      ];
    }
    return commonClassificationMetrics;
  } else if (taskType === TaskType.regression)  {
    return [
      MSE,
      RMSE,
      MAE,
      R2,
    ];
  } else if (taskType === TaskType.linkPrediction)  {
    return [
      ACC,
      JACCARD,
    ];
  } else if (taskType === TaskType.vertexNomination)  {
    // TODO: return Mean Reciprocal Rank when it becomes available in TA3TA2 API
    // return [
    //   MRR,
    // ];
    return [];
  } else if (taskType === TaskType.vertexClassification)  {
    return [
      ACC,
      F1_MICRO,
      F1_MACRO,
    ];
  }
  else if (taskType === TaskType.communityDetection)  {
    return [
      NMI,
    ];
  } else if (taskType === TaskType.graphMatching)  {
    return [
      ACC,
      JACCARD,
    ];
  } else if (taskType === TaskType.timeSeriesForecasting)  {
    return [
      MSE,
      RMSE,
      MAE,
      R2,
    ];
  } else if (taskType === TaskType.collaborativeFiltering)  {
    return [
      MSE,
      RMSE,
      MAE,
      R2,
    ];
  } else if (taskType === TaskType.objectDetection)  {
    return [
      OBJ_DET_AP,
    ];
  } else if (taskType === TaskType.semiSupervisedClassification)  {
    return [
      ACC,
      F1_MICRO,
      F1_MACRO,
    ];
  } else if (taskType === TaskType.semiSupervisedRegression)  {
    return [
      MSE,
      RMSE,
      MAE,
      R2,
    ];
  } else {
    console.warn('Requested metrics for unsuported task type.');
    return [];
  }
}
