// D3M datasets schema
// https://gitlab.datadrivendiscovery.org/MIT-LL/d3m_data_supply/blob/shared/schemas/datasetSchema.json

export interface Dataset {
  about: DatasetAbout;
  dataResources: DataResource[];
  qualities: Quality<{}>[]
}

export interface Dict<T = unknown> {
  [key: string]: T
}

export enum QualityValueType {
  dict = 'dict',
  boolean = 'boolean',
  integer = 'integer',
  real = 'real',
  string = 'string'
}
export interface Quality<Q = unknown> {
  qualName: string;
  qualValue: Dict<Q> | string | number | boolean;
  qualValueType: QualityValueType
}

export interface AugmentQualityValue {
  augmentation_type: string,
  nb_rows_after: number,
  nb_rows_before: number,
  new_columns: string[],
  removed_columns: string[],
}

export interface DatasetAbout {
  datasetID: string;
  datasetName: string;
  datasetURI?: string;
  description?: string;
  citation?: string;
  publicationDate?: string;
  humanSubjectsResearch?: boolean;
  license: string;
  source?: string;
  sourceURI?: string;
  approximateSize?: string;
  applicationDomain?: string;
  datasetVersion: string;
  datasetSchemaVersion: string;
  redacted: boolean;
  digest?: string;
}

export interface DataResource {
  resID: string;
  resPath: string;
  resType: ResourceType;
  resFormat: string[];
  isCollection: boolean;
  columns?: Column[]; // only for resType "table" or "timeseries"
}

export interface Column {
  colIndex: number;
  colName: string;
  colDescription?: string;
  colType: ColumnType;
  role: ColumnRole[];
  refersTo?: DataResourceLink;
}

export interface DataResourceLink {
  resID: string;
  resObject: ResourceLinkObject | ResourceLinkColumnIndex | ResourceLinkColumnName;
}

export interface ResourceLinkColumnIndex {
  columnIndex: number;
}

export interface ResourceLinkColumnName {
  columnName: string;
}

export enum ResourceLinkObject {
  item = 'item',
  node = 'node',
  nodeAttribute = 'nodeAttribute',
  edge = 'edge',
  edgeAttribute = 'edgeAttribute',
}
export enum ResourceType {
  image = 'image',
  video = 'video',
  audio = 'audio',
  speech = 'speech',
  text = 'text',
  graph = 'graph',
  edgeList = 'edgeList',
  table = 'table',
  timeseries = 'timeseries',
  raw = 'raw',
}

export enum ColumnType {
  boolean = 'boolean',
  integer = 'integer',
  real = 'real',
  string = 'string',
  categorical = 'categorical',
  dateTime = 'dateTime',
  realVector = 'realVector', // This is stored as a string in CSV files. It's represented as "val1,val2,vl3,val4,...".
  json = 'json',
  geojson = 'geojson',
}

export enum ColumnRole {
  index = 'index',
  key = 'key',
  attribute = 'attribute',
  suggestedTarget = 'suggestedTarget',
  timeIndicator = 'timeIndicator',
  locationIndicator = 'locationIndicator',
  boundaryIndicator = 'boundaryIndicator',
  instanceWeight = 'instanceWeight',
  boundingBox = 'boundingBox',
  augmented = 'augmented',
}
