// D3M problem schema
// https://gitlab.datadrivendiscovery.org/MIT-LL/d3m_data_supply/blob/shared/schemas/problemSchema.json

export interface Problem {
  inputs: Inputs;
  expectedOutputs: ExpectedOutputs;
  about: ProblemAbout;
  dataAugmentation?: DataAugmentation[];
}

export enum TaskType {
  classification = 'classification',
  regression = 'regression',
  clustering = 'clustering',
  linkPrediction = 'linkPrediction',
  vertexNomination = 'vertexNomination',
  vertexClassification = 'vertexClassification',
  communityDetection = 'communityDetection',
  graphMatching = 'graphMatching',
  timeSeriesForecasting = 'timeSeriesForecasting',
  collaborativeFiltering = 'collaborativeFiltering',
  objectDetection = 'objectDetection',
  semiSupervisedClassification = 'semiSupervisedClassification',
  semiSupervisedRegression = 'semiSupervisedRegression',
}

export enum TaskSubType {
  binary = 'binary',
  multiClass = 'multiClass',
  multiLabel = 'multiLabel',
  univariate = 'univariate',
  multivariate = 'multivariate',
  overlapping = 'overlapping',
  nonOverlapping = 'nonOverlapping',
}

// Based on https://datadrivendiscovery.org/wiki/display/work/Taxonomy+of+problems and
// https://datadrivendiscovery.org/wiki/display/work/Matrix+of+metrics
export const  problemType = {
  'classification': ['binary', 'multiClass', 'multiLabel'],
  'regression': ['univariate', 'multivariate'],
  'communityDetection': ['overlapping', 'nonOverlapping'],
  'clustering': [],
  'linkPrediction': [],
  'vertexNomination': [],
  'vertexClassification': ['binary', 'multiClass', 'multiLabel'],
  'graphMatching': [],
  'timeSeriesForecasting': [],
  'collaborativeFiltering': [],
  'objectDetection': [],
  'semiSupervisedClassification': ['binary', 'multiClass', 'multiLabel'],
  'semiSupervisedRegression': ['univariate', 'multivariate'],
};

export interface ProblemAbout {
  problemID: string;
  problemName: string;
  problemDescription?: string;
  problemURI?: string;
  taskType: TaskType;
  taskSubType?: TaskSubType;
  problemVersion: string;
  problemSchemaVersion: string;
}

export interface Inputs {
  data: InputsData[];
  dataSplits?: DataSplits;
  performanceMetrics: Metrics[];
}

export interface InputsData {
  datasetID: string;
  targets: InputsTarget[];
}

export interface Feature  {
  resID: string;
  colIndex: number;
  colName: string;
}

export interface InputsTarget extends Feature {
  targetIndex: number;
  resID: string;
  colIndex: number;
  colName: string;
  numClusters?: number;
}

export enum SplitMethod {
  holdOut = 'holdOut',
  kFold = 'kFold',
}

export interface DataSplits {
  method: SplitMethod;
  testSize?: number;
  numFolds: number;
  stratified: boolean;
  numRepeats: number;
  randomSeed?: number;
  splitsFile: string;
}

export interface Metrics {
  metric: MetricValue;
  applicabilityToTarget?: ApplicabilityToTarget;
  K?: number;
  posLabel?: string;
}

export enum ApplicabilityToTarget {
  singleTarget = 'singleTarget',
  allTargets = 'allTargets',
}

export enum MetricValue {
  accuracy = 'accuracy',
  precision = 'precision',
  recall = 'recall',
  f1 = 'f1',
  f1Micro = 'f1Micro',
  f1Macro = 'f1Macro',
  rocAuc = 'rocAuc',
  rocAucMicro = 'rocAucMicro',
  rocAucMacro = 'rocAucMacro',
  meanSquaredError = 'meanSquaredError',
  rootMeanSquaredError = 'rootMeanSquaredError',
  rootMeanSquaredErrorAvg = 'rootMeanSquaredErrorAvg',
  meanAbsoluteError = 'meanAbsoluteError',
  rSquared = 'rSquared',
  normalizedMutualInformation = 'normalizedMutualInformation',
  jaccardSimilarityScore = 'jaccardSimilarityScore',
  precisionAtTopK = 'precisionAtTopK',
  objectDetectionAP = 'objectDetectionAP',
  hammingLoss = 'hammingLoss',
  rank = 'rank',
  loss = 'loss',
}

export interface ExpectedOutputs {
  predictionsFile: string;
  scoresFile?: string;
}

export interface DataAugmentation {
  domain?: string[];
  keywords?: string[];
}