import * as ta2 from './ta2';
import * as problems from './problems';
import * as metrics from './metrics';

const PerformanceMetric = ta2.PerformanceMetric;
const MetricValue = problems.MetricValue;

export const PerformanceMetricEnumTranslator = {
  [MetricValue.accuracy.toString()]: PerformanceMetric.ACCURACY,
  [MetricValue.f1.toString()]: PerformanceMetric.F1,
  [MetricValue.f1Macro.toString()]: PerformanceMetric.F1_MACRO,
  [MetricValue.f1Micro.toString()]: PerformanceMetric.F1_MICRO,
  [MetricValue.jaccardSimilarityScore.toString()]: PerformanceMetric.JACCARD_SIMILARITY_SCORE,
  [MetricValue.meanAbsoluteError.toString()]: PerformanceMetric.MEAN_ABSOLUTE_ERROR,
  [MetricValue.meanSquaredError.toString()]: PerformanceMetric.MEAN_SQUARED_ERROR,
  [MetricValue.normalizedMutualInformation.toString()]: PerformanceMetric.NORMALIZED_MUTUAL_INFORMATION,
  [MetricValue.objectDetectionAP.toString()]: PerformanceMetric.OBJECT_DETECTION_AVERAGE_PRECISION,
  [MetricValue.precision.toString()]: PerformanceMetric.PRECISION,
  [MetricValue.precisionAtTopK.toString()]: PerformanceMetric.PRECISION_AT_TOP_K,
  [MetricValue.rSquared.toString()]: PerformanceMetric.R_SQUARED,
  [MetricValue.recall.toString()]: PerformanceMetric.RECALL,
  [MetricValue.rocAuc.toString()]: PerformanceMetric.ROC_AUC,
  [MetricValue.rocAucMacro.toString()]: PerformanceMetric.ROC_AUC_MACRO,
  [MetricValue.rocAucMicro.toString()]: PerformanceMetric.ROC_AUC_MICRO,
  [MetricValue.rootMeanSquaredError.toString()]: PerformanceMetric.ROOT_MEAN_SQUARED_ERROR,
  [MetricValue.hammingLoss.toString()]: PerformanceMetric.HAMMING_LOSS,
  [MetricValue.rank.toString()]: PerformanceMetric.RANK,
  [MetricValue.loss.toString()]: PerformanceMetric.LOSS,
};

export function metricName(metricValue: problems.MetricValue): string {
  return ta2MetricName(PerformanceMetricEnumTranslator[metricValue]);
}

export function ta2MetricName(ta2Metric: ta2.PerformanceMetric): string {
  return metrics.metricNamesMapping[ta2Metric];
}

const TaskType = problems.TaskType;
const TA2TaskType = ta2.TaskType;

export const TranslateTaskType = {
  [TaskType.classification]: TA2TaskType.CLASSIFICATION,
  [TaskType.clustering]: TA2TaskType.CLUSTERING,
  [TaskType.collaborativeFiltering]: TA2TaskType.COLLABORATIVE_FILTERING,
  [TaskType.communityDetection]: TA2TaskType.COMMUNITY_DETECTION,
  [TaskType.graphMatching]: TA2TaskType.GRAPH_MATCHING,
  [TaskType.linkPrediction]: TA2TaskType.LINK_PREDICTION,
  [TaskType.objectDetection]: TA2TaskType.OBJECT_DETECTION,
  [TaskType.regression]: TA2TaskType.REGRESSION,
  [TaskType.timeSeriesForecasting]: TA2TaskType.TIME_SERIES_FORECASTING,
  [TaskType.vertexNomination]: TA2TaskType.VERTEX_NOMINATION,
  [TaskType.vertexClassification]: TA2TaskType.VERTEX_CLASSIFICATION,
  [TaskType.semiSupervisedClassification]: TA2TaskType.SEMISUPERVISED_CLASSIFICATION,
  [TaskType.semiSupervisedRegression]: TA2TaskType.SEMISUPERVISED_REGRESSION,
};

const TaskSubType = problems.TaskSubType;
const TA2TaskSubtype = ta2.TaskSubtype;
export const TranslateTaskSubType = {
  [TaskSubType.binary]: TA2TaskSubtype.BINARY,
  [TaskSubType.multiClass]: TA2TaskSubtype.MULTICLASS,
  [TaskSubType.multiLabel]: TA2TaskSubtype.MULTILABEL,
  [TaskSubType.multivariate]: TA2TaskSubtype.MULTIVARIATE,
  [TaskSubType.nonOverlapping]: TA2TaskSubtype.NONOVERLAPPING,
  [TaskSubType.overlapping]: TA2TaskSubtype.OVERLAPPING,
  [TaskSubType.univariate]: TA2TaskSubtype.UNIVARIATE,
};

export function toTA2PerformanceMetric(p: problems.Metrics): ta2.ProblemPerformanceMetric {
  // FIXME: Some metrics have required parameters that should either be read
  // from the problemDoc schema or supplied by the user, e.g., posLabel is
  // required by accuracy, recall, and F1.
  // For details, see:
  // https://gitlab.datadrivendiscovery.org/MIT-LL/d3m_data_supply/issues/138
  if (p.metric === problems.MetricValue.precision ||
      p.metric === problems.MetricValue.recall ||
      p.metric === problems.MetricValue.f1) {
      p.posLabel = '1'; // F1 metric requires pos_label argument
  }

  return {
    k: p.K,
    metric: p.metric ? PerformanceMetricEnumTranslator[p.metric] : PerformanceMetric.METRIC_UNDEFINED,
    pos_label: p.posLabel,
  };
}

export function toTA2PropblemInput(input: problems.InputsData): ta2.ProblemInput {
  return {
    dataset_id: input.datasetID,
    targets: input.targets.map(
      (t): ta2.ProblemTarget => {
        return {
          clusters_number: t.numClusters,
          column_index: t.colIndex,
          column_name: t.colName,
          resource_id: t.resID,
          target_index: t.targetIndex,
        };
      },
    ),
  };
}

export function toTA2ProblemDescription(problem: problems.Problem): ta2.ProblemDescription {

  const performanceMetrics = problem.inputs.performanceMetrics.map(toTA2PerformanceMetric);
  const { problemID, problemName, taskType, taskSubType, problemDescription, problemVersion } = problem.about;
  const inputs = problem.inputs.data;
  if (!taskType) {
    console.warn('Invalid taskType provided: ' + taskType);
  }

  return {
    problem: {
      performance_metrics: performanceMetrics,
      task_type: taskType ? TranslateTaskType[taskType] : TA2TaskType.TASK_TYPE_UNDEFINED, // taskType should never be undefined at this point
      task_subtype: taskSubType ? TranslateTaskSubType[taskSubType] : TA2TaskSubtype.NONE, // taskSubType should never be undefined
    },
    inputs: inputs && inputs.map(toTA2PropblemInput),
    id: problemID,
    name: problemName,
    version: problemVersion,
    description: problemDescription,
    // digest: // NOTE: Not sure what to do here. It doesn't seem to be a mandatory field, neither we can compute it easily.
    data_augmentation: problem.dataAugmentation && problem.dataAugmentation,
  };
}
