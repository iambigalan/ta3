import { Column } from './datasets';
import { Problem, Metrics } from './problems';
import { PipelineDescription } from './ta2';
import { metrics } from '.';

export interface ClientConfig {
  env_vars: {
    DATAMART_URL: string;
  }
}

export enum NumericType {
  integer = 'integer',
  real = 'real',
}

export enum TimeUnits {
  Minutes = 1,
  Hours = 60,
}

export interface SolutionSearchRequest {
  problem: Problem;
  timeLimit: number;
  priority: number;
  maxSolutions?: number;
}

export type PrimitiveValue = string | number | Date;
export interface ColumnValueStat {
  name: PrimitiveValue;
  count: number;
}

export interface ColumnProfile {
  column: Column;
  values: ColumnValueStat[];
  isText: boolean;
  stats?: {
    min: number;
    max: number;
    mean: number;
    stdDev: number;
  };
}

export interface ResourceFileData {
  name: string;
  content: string;
  src: string;
}

export interface TimeSeriesData {
  name: string;
  minValue: number;
  maxValue: number;
  seriesKey: string;
  dataKey: string;
  data: Array<{ [anyKey: string]: number } | { [anyKey: number]: number }>;
}

export type DataForResource = ColumnProfile[] | ResourceFileData[];

export enum DescribeSolutionStatus {
  REQUESTED,
  ERRORED,
  COMPLETED,
}

export interface DescribeSolutionProgress {
  status: DescribeSolutionStatus
  modelName?: string;
  pipeline?: PipelineDescription;
}

export interface SolutionScore {
  metric: string;
  score: number;
}

export enum ScoringStatus {
  REQUESTED,
  ERRORED,
  COMPLETED,
}

export interface SolutionScoreProgress {
  status: ScoringStatus;
  scores?: {
    [metric: string]: SolutionScore,
  };
}

export interface Solution {
  id: string;
  name: string;
  internal_score: number;
  scores?: SolutionScoreProgress;
  description?: DescribeSolutionProgress;
}

export interface SimplifiedProblem {
  searchParams: {
    maxSolution: number;
    maxTime: number;
    timeUnit: string;
    evaluationMetrics: { [metric: string]: boolean };
  };
}

export interface SolutionSearchProgress {
  state: string; // stringified version of ProgressState enum
  status: string; // TA2 custom status string
  percentage: number; // [0, 1] percentage based on ticks
}

/**
 * Formatted solution search update sent to TA3 client.
 */
export interface SolutionSearchUpdate {
  progress?: SolutionSearchProgress;
  solution?: Solution;
}

/**
 * Solution search status maintained locally in the TA3.
 */
export interface SolutionSearchState {
  searchID: string;
  params: SolutionSearchRequest;
  progress: SolutionSearchProgress;
  solutions: Solution[]; // maintained list of solutions on TA3 client
}

export interface ProblemDefinition {
  target?: string;
  type?: string;
  subType?: string;
  maxSolutions?: string,
  maxTime?: string,
  maxTimeUnit?: TimeUnits.Minutes,
  metrics?: Metrics[],
}
