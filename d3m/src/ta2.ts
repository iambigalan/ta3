/**
 * @fileoverview Provides type definitions for the protobuf messages
 * defined in the TA2-TA3 gRPC API.
 */

export enum StatusCode {
  UNKNOWN = 0,
  OK = 1,
  CANCELLED = 2,
  SESSION_UNKNOWN = 3,
  SESSION_ENDED = 4,
  SESSION_EXPIRED = 5,
  INVALID_ARGUMENT = 6,
  RESOURCE_EXHAUSTED = 7,
  UNAVAILABLE = 8,
  FAILED_PRECONDITION = 9,
  OUT_OF_RANGE = 10,
  UNIMPLEMENTED = 11,
  INTERNAL = 12,
  ABORTED = 13,
}

export enum ProgressState {
  PROGRESS_UNKNOWN = 0,
  // The process has been scheduled but is pending execution.
  PENDING = 1,
  // The process is currently running. There can be multiple messages with this state
  // (while the process is running).
  RUNNING = 2,
  // The process completed and final results are available.
  COMPLETED = 3,
  // The process failed.
  ERRORED = 4,
}

export const ProgressStateNameMap = {
  0: 'PROGRESS_UNKNOWN',
  1: 'PENDING',
  2: 'RUNNING',
  3: 'COMPLETED',
  4: 'ERRORED',
};

export interface Progress {
  state: ProgressState;
  status: string;
}

export enum Rpc {
  // Relay
  HELLO = 'Hello',
  SEARCH_SOLUTIONS = 'SearchSolutions',
  GET_SEARCH_SOLUTIONS_RESULTS = 'GetSearchSolutionsResults',
  STOP_SEARCH_SOLUTIONS = 'StopSearchSolutions',
  END_SEARCH_SOLUTIONS = 'EndSearchSolutions',
  SCORE_SOLUTION = 'ScoreSolution',
  DESCRIBE_SOLUTION = 'DescribeSolution',
  GET_SCORE_SOLUTION_RESULTS = 'GetScoreSolutionResults',
  FIT_SOLUTION = 'FitSolution',
  GET_FIT_SOLUTION_RESULTS = 'GetFitSolutionResults',
  PRODUCE_SOLUTION = 'ProduceSolution',
  GET_PRODUCE_SOLUTION_RESULTS = 'GetProduceSolutionResults',
  SOLUTION_EXPORT = 'SolutionExport',
  UPDATE_PROBLEM = 'UpdateProblem',
  LIST_PRIMITIVE = 'ListPrimitives',
}

export enum OutputType {
  OUTPUT_TYPE_UNDEFINED = 0,
}

export interface Status {
  code: StatusCode;
  details: string;
}

export interface Response {
  status: Status;
}

export interface HelloResponse {
  // Some string identifying the name and version of the TA2 system.
  user_agent: string;
  // Shall be set to "protocol_version" above.
  version: string;
  // List of value types that a TA3 system can use to communicate values to a TA2 system.
  // The order is important as a TA3 system should try value types in order until one works
  // out, or an error will be returned instead of the value.
  allowed_value_types: ValueType[];
  // List of API extensions that a TA2 supports.
  supported_extensions: Array<string>;
}

export interface SearchSolutionsRequest {
  user_agent: string;
  version: string;
  time_bound_search: number;
  priority: number;
  allowed_value_types: ValueType[];
  problem: ProblemDescription;
  template?: PipelineDescription;
  inputs: Value[];
  // It can be used to specify anticipated limit on time to one pass of the pipeline run
  time_bound_run: number;
  rank_solutions_limit?: number;
}

export interface SearchSolutionsResponse {
  search_id: string;
}

export interface GetSearchSolutionsResultsRequest {
  search_id: string;
}

export interface GetSearchSolutionsResultsResponse {
  progress: Progress;
  done_ticks: number;
  all_ticks: number;
  solution_id: string;
  internal_score: number;
  scores: SolutionSearchScore[];
}

// COMPUTE SCORE
export interface ScoreSolutionRequest {
  solution_id: string;
  inputs: Value[];
  performance_metrics: ProblemPerformanceMetric[];
  users?: SolutionRunUser[]; // optional
  configuration?: ScoringConfiguration;
}

export interface ScoreSolutionResponse {
  request_id: string;
}

export interface GetScoreSolutionResultsRequest {
  request_id: string;
}

export interface GetScoreSolutionResultsResponse {
  progress: Progress;
  scores: Score[];
}
// END

export interface SolutionSearchScore {
  scoring_configuration: ScoringConfiguration;
  scores: Score[];
}

export interface ScoringConfiguration {
  method: EvaluationMethod;
  folds?: number;
  train_test_ratio?: number;
  shuffle?: boolean;
  random_seed?: number;
  stratified?: boolean;
}

export interface StopSearchSolutionsResponse {}

export interface StopSearchSolutionsRequest {
    search_id: string;
}

export interface DescribeSolutionRequest {
  solution_id: string;
}

export interface DescribeSolutionResponse {
  pipeline: PipelineDescription;
}
export enum EvaluationMethod {
  // Default value. Not to be used.
  EVALUATION_METHOD_UNDEFINED = 0,
  // Evaluation methods supported by TA2s (there are other entries in the proto
  // file that are not supported by TA2)
  HOLDOUT = 1,
  K_FOLD = 2,
}

export interface Score {
  metric: ProblemPerformanceMetric;
  fold: number;
  value: Value;
  random_seed?: number;
  normalized?: Value; // optional
}

export interface SolutionDescriptionContainerArgument {
  argument: "container",
  container: {
    data: string
  }
}

export interface PipelineDescription {
  // The original protobuf message PipelineDescription (from pipelines.proto)
  // contains more fields than the ones that are included here (e.g., id, 
  // description, context, etc). Such fields should be added here if necessary.
  inputs: PipelineDescriptionInput[]
  outputs: PipelineDescriptionOutput[]
  steps: PipelineDescriptionStep[];
}

export interface PipelineDescriptionInput {
  name: string;
}

export interface PipelineDescriptionOutput {
  name: string;
  data: string;
}

export interface PipelineDescriptionStep {
  // This is a `oneof` message and  there are other fields 
  // in the original message that are not included here
  primitive: PrimitivePipelineDescriptionStep;
}

export interface PrimitivePipelineDescriptionStep {
  primitive: Primitive;
  arguments: {
    [aug: string]: SolutionDescriptionContainerArgument,
  };
  outputs: { id: string }[];
}

export interface Primitive {
  id: string;
  name: string;
  python_path: string;
}

// User associated with the run of the solution.
export interface SolutionRunUser {
  // A UUID of the user. It does not have to map to any real ID, just that it is possible
  // to connect multiple solution actions by the same user together, if necessary.
  id: string;
  // Was this run because solution was choosen by this user.
  chosen: boolean;
  // Textual reason provided by the user why the run was choosen by this user.
  reason: string;
}

export interface StepProgress {
  progress: Progress;
  // If step is a sub-pipeline, then this list contains progress for each step in the
  // sub-pipeline, in order.
  // List can be incomplete while the process is in progress. Systems can provide
  // steps only at the end (when "progress" equals COMPLETED) and not during running.
  // tslint:disable-next-line:no-any
  steps: any[];
}

export interface FitSolutionResponse {
  request_id: string;
}

export interface FitSolutionRequest {
  solution_id: string;
  inputs: Value;
  // List of data references of step outputs which should be exposed to the TA3 system.
  // If you want to expose outputs of the whole pipeline (e.g., predictions themselves),
  // list them here as well. These can be recursive data references like
  // "steps.1.steps.4.produce" to point to an output inside a sub-pipeline.
  // Systems only have to support exposing final outputs and can return "ValueError" for
  // intermediate values.
  expose_outputs?: string[];
  // Which value types should be used for exposing outputs. If not provided, the allowed
  // value types list from hello call is used instead.
  // The order is important as TA2 system will try value types in order until one works out,
  // or an error will be returned instead of the value. An error exposing a value does not
  // stop the overall process.
  expose_value_types: ValueType;
  // Any users associated with this call itself. Optional.
  users?: SolutionRunUser;
}

// Get the latest state of fitting the solution and start receiving any further
// new updates to the state as well.
export interface GetFitSolutionResultsRequest {
  request_id: string;
}

export interface GetFitSolutionResultsResponse {
  // Overall process progress.
  progress: Progress;
  // The list contains progress for each step in the pipeline, in order.
  // List can be incomplete while the process is in progress. Systems can provide
  // steps only at the end (when "progress" equals COMPLETED) and not during running.
  steps: StepProgress [];
  // A mapping between data references of step outputs and values.
  exposed_outputs: {[key: string]: Value};

  // The fitted solution ID, once progress = COMPLETED.
  fitted_solution_id: string ;
}

export interface SolutionExportRequest {
  solution_id: string;
  rank: number;
}

export interface SolutionExportResponse {
  // nothing to add here
}

// Get the latest state of producing the fitted solution and start receiving any further
// new updates to the state as well.
export interface GetProduceSolutionResultsRequest {
  request_id: string;
}

export interface GetProduceSolutionResultsResponse {
  // Overall process progress.
  progress: Progress;
  // The list contains progress for each step in the pipeline, in order.
  // List can be incomplete while the process is in progress. Systems can provide
  // steps only at the end (when "progress" equals COMPLETED) and not during running.
  steps: StepProgress[];
  // A mapping between data references of step outputs and values.
  exposed_outputs: { [key: string]: Value };
}

export interface ProduceSolutionRequest {
  fitted_solution_id: string;
  inputs: Value[];
  expose_outputs: Array<string>;
  expose_value_types: ValueType[];
  users?: SolutionRunUser[]; // optional
}

export interface ProduceSolutionResponse {
  request_id: string;
}

/********** problem.proto **********/

export interface ProblemDescription {
  problem: Problem;
  inputs: ProblemInput[];
  id: string;
  version: string;
  name: string;
  description?: string;
  digest?: string;
  data_augmentation?: DataAugmentation[];
  other_names?: string;
}

export interface Problem {
  task_type: TaskType;
  task_subtype?: TaskSubtype;
  performance_metrics: ProblemPerformanceMetric[];
}

export interface DataAugmentation {
  domain?: string[];
  keywords?: string[];
}

export enum TaskType {
  TASK_TYPE_UNDEFINED = 0,             // TaskType not yet declared. Shall NOT be sent to TA2.
  CLASSIFICATION = 1,
  REGRESSION = 2,
  CLUSTERING = 3,
  LINK_PREDICTION = 4,
  VERTEX_NOMINATION = 5,
  VERTEX_CLASSIFICATION = 6,
  COMMUNITY_DETECTION = 7,
  GRAPH_MATCHING = 8,
  TIME_SERIES_FORECASTING = 9,
  COLLABORATIVE_FILTERING = 10,
  OBJECT_DETECTION = 11,
  SEMISUPERVISED_CLASSIFICATION = 12,
  SEMISUPERVISED_REGRESSION = 13,
}

export enum TaskSubtype {
  TASK_SUBTYPE_UNDEFINED = 0,          // TaskSubtype not yet declared. Shall NOT be sent to TA2.
  NONE = 1,                            // No secondary task is applicable for this problem
  BINARY = 2,
  MULTICLASS = 3,
  MULTILABEL = 4,
  UNIVARIATE = 5,
  MULTIVARIATE = 6,
  OVERLAPPING = 7,
  NONOVERLAPPING = 8,
}

export interface ProblemPerformanceMetric {
  metric: PerformanceMetric;
  k?: number;
  pos_label?: string;
}

export enum PerformanceMetric {
  METRIC_UNDEFINED = 0,
  ACCURACY = 1,
  PRECISION = 2,
  RECALL = 3,
  F1 = 4,
  F1_MICRO = 5,
  F1_MACRO = 6,
  ROC_AUC = 7,
  ROC_AUC_MICRO = 8,
  ROC_AUC_MACRO = 9,
  MEAN_SQUARED_ERROR = 10,
  ROOT_MEAN_SQUARED_ERROR = 11,
  MEAN_ABSOLUTE_ERROR = 12,
  R_SQUARED = 13,
  NORMALIZED_MUTUAL_INFORMATION = 14,
  JACCARD_SIMILARITY_SCORE = 15,
  PRECISION_AT_TOP_K = 17,
  OBJECT_DETECTION_AVERAGE_PRECISION = 18,
  HAMMING_LOSS = 19,
  RANK = 99,
  LOSS = 100,
}

export interface ProblemPrivilegedData {
  privileged_data_index: number;
  resource_id: string;
  column_index: number;
  column_name: string;
}

export interface ProblemInput {
  dataset_id: string;
  targets: ProblemTarget[];
  privileged_data?: ProblemPrivilegedData;
}

export interface ProblemTarget {
  target_index: number;
  resource_id: string;
  column_index: number;
  column_name: string;
  clusters_number?: number;
}

/********** value.proto **********/

export enum ValueType {
  VALUE_TYPE_UNDEFINED,
  RAW,
  DATASET_URI,
  CSV_URI,
  PICKLE_URI,
  PICKLE_BLOB,
  PLASMA_ID,
  LARGE_RAW,
  LARGE_PICKLE_BLOB,
}

export interface Value {
  // TODO: Is this the best/right way to represent protobuf 'oneof' constructs?
  error?: ValueError;
  raw?: ValueRaw;
  dataset_uri?: string;
  csv_uri?: string;
  pickle_uri?: string;
  pickle_blob?: Buffer;
  plasma_id?: Buffer;
}

export interface ValueError {
  message: string;
}

export interface ValueRaw {
  null?: NullValue;
  double?: number;
  int64?: number;
  bool?: boolean;
  string?: string;
  list?: ValueList;
  bytes?: Buffer;
  dict?: ValueDict;
}

export enum NullValue {
  NULL_VALUE,
}

export interface ValueList {
  items: ValueRaw[];
}

export interface ValueDict {
  items: Map<string, ValueRaw>;
}
