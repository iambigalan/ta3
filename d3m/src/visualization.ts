// NOTE(bowen): The original repo of 'rule-matrix-js' has d3 typing errors.
// Here we are using our forked repo.
import { RuleModel, Stream, Support, SupportMat } from 'rule-matrix-js/dist/models';
import { Column } from './datasets';

export type DataPointValue = string | number | boolean | number[];

export interface ConfusionMatrixData {
  conf: { [index: string]: { [index: string]: number } };
  classDefs: string[];
}

export interface ConfusionScatterplotData {
  points: [number, number][];
}

export interface RuleMatrixData {
  continue: boolean;
  error: boolean | string;
  model?: RuleModel; // required from model.json
  streams?: Stream[]; // required from stream.json
  support?: Support | SupportMat; // required from support_mat.json
}

export interface RuleMatrixError {
  error: string;
}

export interface RuleMatrixStart {
  start: boolean;
}

export interface RuleMatrixConfig {
  resId: string;
  featureIndices: number[];
  targetIndices: number[];
}

export interface PartialPlotData {
  baseStats: {
    mean: number;
    std: number;
  };
  partials: {
    column: Column;
    histogram: {
      value: DataPointValue,
      mean: number,
      std: number,
      count: number,
    }[];
    values: {
      value: DataPointValue,
      dataRange: [number, number],
      mean: number;
      std: number;
    }[];
  }[];
}
