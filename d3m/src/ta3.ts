import { Problem } from './problems';

/**
 * @fileoverview Provides socket message definitions for TA3-only communications between TA3 client and server.
 */

export enum Rpc {
  GET_SEARCH_RESULTS = 'ta3/getSearchResults',
  EVALUATE_SOLUTION = "ta3/evaluateSolution",
}

export interface GetSearchResultsRequest {
  searchID: string;
  problem: Problem;
}

export interface EvaluateSolutionRequest {
  solutionID: string;
  problem: Problem;
}

export interface EvaluateSolutionResponse {
  fittedSolutionID: string;
  error: boolean;
}
