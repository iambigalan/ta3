export interface DatamartRequest {
  query?: string;
  datasetID: string;
}

export interface DatamartResponse {
  results: DatamartResult[];
}

export interface DatamartResult {
  id: string;
  score: number;
  join_columns: [string, string][];
  augmentation?: {
    type: string;
    left_columns: number[][];
    left_columns_names: string[][];
    right_columns: number[][];
    right_columns_names: string[][];
  }
  metadata: {
    filename: string;
    name: string;
    description: string;
    size: number;
    nb_rows: number;
    columns: {
      name: string;
      structural_type: string;
      semantic_types: string[];
    }[];
  };
}

export interface DatamartAugmentRequest {
  result: DatamartResult;
  datasetID: string;
}
