#!/bin/bash
WAIT_TIMEOUT_SECS=${WAIT_TIMEOUT_SECS:-300}
TA3_URL=${TA3_URL:="http://localhost"}
start_secs=`date +%s`
printf "Trying to reach server at \"${TA3_URL}\"..."
until $(curl --output /dev/null --silent --head --fail ${TA3_URL}); do
    seconds_since_start=$(( `date '+%s'` - $start_secs ))
    if [ ${seconds_since_start} -gt ${WAIT_TIMEOUT_SECS} ];then
      printf "\nServer failed to responder server within  $WAIT_TIMEOUT_SECS.\n"
      exit 1
    fi
    printf '.'
    sleep 1
done
seconds_since_start=$(( `date '+%s'` - $start_secs ))
printf "\nServer responded in ${seconds_since_start} seconds.\n"
