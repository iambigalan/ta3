
const PIPELINE_SEARCH_TIME_MINUTES = 5;
const PIPELINE_SEARCH_TIMEOUT_MS = PIPELINE_SEARCH_TIME_MINUTES*60*1000 * 2;
const EVALUATE_PIPELINE_TIMEOUT_MS = 1*1000;
const DEFAULT_TIMEOUT_MS = 15*1000;

describe('Build Model for Classification Dataset', function() {

  it('Hello Visus!', function() {

    // This address is prefixed with the `baseUrl` variable from cypress.json file.
    // It can also be changed via the `CYPRESS_baseUrl` environment variable.
    cy.visit('/')
    cy.contains('Visus')

    // Click on "Use Visus" button to open select dataset page
    cy.get('[data-cy=use-visus]')
      .click()

    // Select a dataset
    cy.get('[data-cy="datasetID"]')
      .contains('185_baseball_dataset')
      .should('be.visible')
      .click()

    // Select task: create new problem
    cy.get('[data-cy="btn-define-problem"]')
      .click()

    // Select problem variables: target
    cy.get('[type="radio"]', {timeout: DEFAULT_TIMEOUT_MS})
      .check('learningData:Hall_of_Fame')
      .should('be.visible')
      .should('have.value', 'learningData:Hall_of_Fame')

    cy.get('[data-cy="next"]')
      .should('be.visible')
      .click()

    // Select problem variables: type
    cy.get('[type="radio"]', {timeout: DEFAULT_TIMEOUT_MS})
      .check('classification')
      .should('be.visible')
      .should('have.value', 'classification')

    cy.get('[data-cy="next"]')
      .should('be.visible')
      .click()

    // Select problem variables: sub-type
    cy.get('[type="radio"]', {timeout: DEFAULT_TIMEOUT_MS})
      .check('multiClass')
      .should('be.visible')
      .should('have.value', 'multiClass')

    cy.get('[data-cy="done"]')
      .should('be.visible')
      .click()

    // Go to configure search page
    cy.get('button.btn')
      .contains('Configure Search')
      .click()

    // Set search parameters

    // TODO: select a search timeout (there is some bug that prevents typing the correct number)
    cy.get('input#settings-max-time-value')
      .type('{selectall}{backspace}{end}' + PIPELINE_SEARCH_TIME_MINUTES.toString())
      .should('have.value', PIPELINE_SEARCH_TIME_MINUTES.toString())

    cy.get('#settings-max-time-unit')
      .select('1') // minutes
      .wait(1000)
      .should('have.value', '1')

    cy.get('input#chck-bx-metric-f1Macro')
      .check()

    // Start the solution search
    cy.get('button.btn')
      .contains('Start Solutions Search')
      .click()

    // cy.wait(PIPELINE_SEARCH_TIMEOUT_MS)

    // Make sure a solution was created
    cy.contains('div.rt-td', '#001', {timeout: PIPELINE_SEARCH_TIMEOUT_MS})

    // Go to explain solutions page
    cy.get('button.btn')
      .contains('Explain Solutions')
      .click()

    // Select first solution
    cy.get('div.rt-td')
      .contains('#001')
      .click()

    cy.wait(EVALUATE_PIPELINE_TIMEOUT_MS)

    // TODO: Test visualizations.
    // cy.get('div.card-body div.row div.col-lg-4 > div > div')
    //   .contains('Select A Visualization')
    //   .click()

    // cy.get('div')
    //   .contains('Confusion Matrix')
    //   .click()

    // cy.wait(1000)

    // cy.get('h5')
    //   .contains('Confusion Matrix')

    cy.get('div.rt-td button')
      .first()
      .click()

    cy.get('button.btn')
      .contains('Export Favorite Solutions')
      .click()

    cy.get('div.alert', {timeout: DEFAULT_TIMEOUT_MS})
      .contains('Successfully exported 1 solution')
  })
})