.PHONY: d3m server client clean

all: d3m server client

d3m:
	cd d3m/ && npm install && npm run build && cd -

server:
	cd server/ && npm install && npm run build && cd -

client:
	cd client/ && npm install && npm run build && cd -

clean:
	rm -rf server/node_modules
	rm -rf client/node_modules
	rm -rf node_modules