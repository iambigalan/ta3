import * as path from 'path';
import * as dotenv from 'dotenv';
import * as fs from 'fs';
import { normalizeUrl } from './utils';

dotenv.config({ path: process.env.NODE_ENV === 'test' ? '.env.test' : '.env' });

export const NODE_ENV = process.env.NODE_ENV || 'development';

export const D3MPROBLEMPATH = process.env.D3MPROBLEMPATH;
export const D3M_DATA_PATH = process.env.D3M_DATA_PATH || process.env.D3MINPUTDIR || '/input';
export const OUTPUT_DIR = process.env.OUTPUT_DIR || process.env.D3MOUTPUTDIR || '/output';
export const PORT = process.env.PORT || '3000';

export const ENABLE_RECORDER = process.env.ENABLE_RECORDER;

export const GRPC_PORT = process.env.GRPC_PORT || '';
export const GRPC_HOST = process.env.GRPC_HOST;
export const GRPC_CONN_URL = GRPC_HOST + ':' + GRPC_PORT;
/**
 * __dirname points to the javascript executable folder ta3/server/dist.
 * The ta3ta2-api folder's parent is ta3/server.
 */
export const GRPC_PROTO_PATH: string = path.join(__dirname, '../ta3ta2-api');

export const CONFIG_JSON_PATH = process.env.CONFIG_JSON_PATH;

export const PROTO_PATH = path.join(GRPC_PROTO_PATH, 'core.proto');

export const RULEMATRIX_ROOT = process.env.RULEMATRIX_ROOT || '/ta3/rulematrix';

export const DATAMART_URL =
  normalizeUrl(process.env.DATAMART_URL) ||
  normalizeUrl(process.env.DATAMART_NYU_URL) ||
  normalizeUrl(process.env.DATAMART_URL_NYU) ||
  'https://datamart.d3m.vida-nyu.org';

export const DATASETS_INFO = path.join(OUTPUT_DIR, 'datasets_info');
export const DATAMART_MARK = '__TA3DMKTAUG__';

const DEFAULT_ALLOW_ORIGINS = NODE_ENV === 'development' ? 'http://localhost:5000' : '';

export const ALLOW_ORIGINS = (process.env.ALLOW_ORIGINS || DEFAULT_ALLOW_ORIGINS).split(';');

export const checkResources = (): boolean => {
  let passCheck = true;
  if (!PROTO_PATH || !fs.existsSync(PROTO_PATH)) {
    console.error(
      `Environment variable $PROTO_PATH=["${PROTO_PATH}"] is not set or the proto file does not exist.`,
      'Did you run fetch-ta3ta2api.sh?',
    );
    passCheck = false;
  }
  if (!OUTPUT_DIR || !fs.existsSync(OUTPUT_DIR)) {
    console.error(
      `Environment variable $OUTPUT_DIR=["${OUTPUT_DIR}"] is not set or the output directory does not exist.`,
    );
    passCheck = false;
  }
  const outputDirStats = fs.lstatSync(OUTPUT_DIR);
  if (!outputDirStats.isDirectory()) {
    console.error(
      `Environment variable $OUTPUT_DIR=["${OUTPUT_DIR}"] points to a file, not a directory.`,
    );
    passCheck = false;
  }
  return passCheck;
};

/**
 * Returns the config JSON.
 */
export const configJSON = () => {
  if (!CONFIG_JSON_PATH) {
    console.warn('ENV CONFIG_JSON_PATH not set.');
    return undefined;
  }
  const strConfigJson = fs.readFileSync(CONFIG_JSON_PATH, 'utf8');
  return JSON.parse(strConfigJson);
};

export const tempStorageRoot = () => {
  const config = configJSON();
  return (config && config.temp_storage_root) || OUTPUT_DIR + '/storage';
};
