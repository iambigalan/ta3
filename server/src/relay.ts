import * as path from 'path';
import * as WebSocket from 'ws';
import { Server } from 'http';
// Environment variables for GRPC
import { GRPC_CONN_URL, GRPC_PROTO_PATH } from './env';
import { processWS } from './rest/index';
import { circularStringify, toDebugString } from './utils';
// import * as protoLoader from '@grpc/proto-loader';

const grpc = require('grpc');

export interface GrpcMessage {
  rid: string;
  fname: string;
  object: object;
}

interface SocketError extends Error {
  code: string;
}

export let createRelayServer = (wspath: string, server: Server): WebSocket.Server => {
  // Websocket defs
  const wss = new WebSocket.Server(
    {
      server: server,
      path: wspath,
    },
  );

  // Loading proto files
  const core = grpc.load(path.join(GRPC_PROTO_PATH, 'core.proto'));

  //
  // TODO: Following is the code to load the proto file using the new
  // @grpc/proto-loader library, which is supposed to be new right way
  // to load the .proto files, but apparently doesn't work with our core.proto
  //
  // const options = {
  //   keepCase: true,
  //   longs: String,
  //   enums: String,
  //   defaults: true,
  //   oneofs: true,
  //   includeDirs: [GRPC_PROTO_PATH]
  // };
  // const CORE_PROTO_PATH = path.join(GRPC_PROTO_PATH, 'core.proto');
  // const packageDefinition = protoLoader.loadSync(CORE_PROTO_PATH, options);
  // const core = grpc.loadPackageDefinition(packageDefinition);

  // Creating the GRPC Connections
  // TODO(aecio): Following 'connections' variable is an array because
  // of previous API versions that had multiple 'modules'. The array can
  // can be removed in the new version.
  const connections: Array<{ stub: any, module: any }> = []; // tslint:disable-line no-any
  connections.push({
    stub: new core.Core(GRPC_CONN_URL, grpc.credentials.createInsecure()),
    module: core,
  });

  // Websocket relay server
  wss.on('connection', (ws: WebSocket) => {
    ws.on('message', (strMessage: string) => {
      const message: GrpcMessage = JSON.parse(strMessage);
      const parameter: object = message.object;

      // In the new TA3 server, when the gRPC message has custom prefix,
      // it is handled using TA3 server handlers rather than simply relayed to TA2.
      // To define how to handle the gRPC message, create a WS resolver in wsSolutionResolvers (rest/solutions.ts).
      //
      // NOTE(bowen): When there are multiple types of resolvers, shall we define them in separate files and merge them?
      if (message.fname.startsWith('ta3/')) {
        console.log('TA3 gRPC message', message.fname, toDebugString(message.object));
        processWS(ws, message);
        return;
      }

      console.log('relay message', message.fname, message.object);
      const fname: string = message.fname[0].toLowerCase() + message.fname.substr(1);

      connections.forEach(conn => {
        const stub = conn.stub;
        if (!(fname in stub)) {
          return;
        }
        // call method
        const responseClass = conn.module[conn.stub[fname].responseType.name];
        if (conn.stub[fname].responseStream === true) {
          // streaming response
          const call = conn.stub[fname](parameter);
          call.on('data', (response: object) => {
            try {
              response = new responseClass(response);
              ws.send(circularStringify({rid: message.rid, object: response}));
            } catch (error) {
              console.error(error);
            }
          });
        } else {
          // single message response
          conn.stub[fname](parameter, (err: Error, response: object) => {
            if (err) {
              console.error('The gRPC server returned an error.', err);
            }
            try {
              response = new responseClass(response);
              console.log(circularStringify({rid: message.rid, object: response}));
              ws.send(circularStringify({rid: message.rid, object: response}));
            } catch (err) {
              console.log(err);
            }
          });
        }
      });
    });

    ws.on('error', (err: SocketError) => {
      switch (err.code) {
        case 'ECONNRESET':
          console.log('TA3 resets socket connection');
          break;
        default:
          console.error('Unknown socket error');
      }
    });

    ws.on('close', () => {
      console.log('socket closed');
    });
  });
  return wss;
};
