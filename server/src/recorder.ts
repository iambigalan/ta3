import { Request, Router } from 'express';
import { OUTPUT_DIR } from './env';
import * as pathlib from 'path';
import * as fs from 'fs';
import * as d3m from 'd3m/dist';
import { mkdirIfAbsent } from './utils';
import { ascending } from 'd3-array';
import { WSRequest } from './rest';
import { RegressionPartialPlotsParams } from './rest/visualization/index';

const RECORD_SESSION_NAME = 'demo-expo';
function getAbsPath(path: string) {
  return pathlib.join(OUTPUT_DIR,  'records', RECORD_SESSION_NAME, path);
}

function partsOfProblem(problem: d3m.Problem, includeMetircs: Boolean = false, includeTarget = false) {
  const datasetID = problem.inputs.data[0].datasetID;
  const problemID = problem.about.problemID;
  const result = [datasetID, problemID];
  if (includeTarget) {
    const target  = getStringForTarget(problem);
    result.push(target);
  }
  if (includeMetircs) {
    const metrics = getMetricsString(problem);
    result.push(metrics);
  }
  return [datasetID, problemID];
}

function recordJSON(obj: Object, path: string) {
  const absPath = getAbsPath(path);
  const dirname = pathlib.dirname(absPath);
  mkdirIfAbsent(dirname);
  console.log('Recording at', absPath);
  fs.writeFile(absPath, JSON.stringify(obj, undefined, 4), () => {});
}

function readJSON<T>(path: string) {
  const absPath = getAbsPath(path);
  if (fs.existsSync(absPath)) {
    return JSON.parse(fs.readFileSync(absPath, { encoding: 'utf-8'})) as T;
  } return undefined;
}

export class Recorder<T> {

  record: (req: Request, data: T) => Promise<T>;
  retrieve: (req: Request) => Promise<T>;

  constructor(
    record: (req: Request, data: T) => Promise<T>,
    retrieve: (req: Request) =>  Promise<T> | undefined) {
    this.record = record;
    this.retrieve = retrieve;
  }

  static recorderFor<T, B>(fileName: string | ((data: B) => string)): Recorder<T> {
    return new Recorder<T>(
      async (req, data) => {
        let path = undefined;
        if (typeof fileName === 'string') {
          path = fileName;
        }
        else {
          path = (fileName as (data: B) => string)(req.body);
        }
        recordJSON(data, path);
        return data;
      },
      async (req) => {
        let path = undefined;
        if (typeof fileName === 'string') {
          path = fileName;
        }
        else {
          path = (fileName as (data: B) => string)(req.body);
        }
        return readJSON(path);
      },
    );
  }
}

type KeyGetter<T> = (params: T) => string;

enum TapeStatus {
  RECORDING,
  RECORDED,
}

interface TapeState<Req, Res> {
  params: Req;
  data: Res[];
}

export class Tape<Req, Res> {

  data: TapeState<Req, Res>;
  name: string;
  params: Req;
  status: TapeStatus;
  getKey: KeyGetter<Req>;

  constructor(name: string, getKey: KeyGetter<Req>, params: Req) {
    this.getKey = getKey;
    this.name = name;
    this.params = params;
    this.data = this.getData();
    if (this.data) {
      this.status = TapeStatus.RECORDED;
      console.log(`Loaded tape [${name}] on RECORDED with ${this.data.data.length} responses.`);
    } else {
      this.data = {
        params: params,
        data: [],
      };
      this.status = TapeStatus.RECORDING;
      this.write();
      console.log(`Initialized empty tape [${name}] on RECORDING mode.`);
    }
  }

  getFilePath(key: string) {
    return `tape/${this.name}/${key}.json`;
  }

  getData() {
    const key = this.getKey(this.params);
    return readJSON<TapeState<Req, Res>>(this.getFilePath(key));
  }

  write() {
    const key = this.getKey(this.params);
    recordJSON(this.data, this.getFilePath(key));
  }

  delay() {
    return 1000;
  }

  add(data: Res) {
    this.data.data.push(data);
    this.write();
  }

  asyncIterator(): Promise<Res>[] {
    let timeout = 0;
    return this.data.data.map(d => {
      timeout += this.delay();
      return new Promise( (resolve) => {
        setTimeout(() => resolve(d), timeout);
      });
    });
  }
}

/**
 * Recorders ==================================================================
 */

function keyForSearchSolution(request: d3m.SolutionSearchRequest) {
  const datasetID = request.problem.inputs.data[0].datasetID;
  const problemID = request.problem.about.problemID;
  return `${datasetID}/${problemID}/searchSolution.json`;
}

function getMetricsString(problem: d3m.Problem) {
  return problem.inputs.performanceMetrics
    .map(m => m.metric)
    .sort(ascending)
    .join('-');
}

function getStringForTarget(problem: d3m.Problem) {
  return problem.inputs.data[0].targets[0].colName;
}

interface SearchSolutionRecord {
  datasetID: string;
  problemID: string;
  metrics: string;
  target: string;
  response: d3m.ta2.SearchSolutionsResponse;
}

function createRecordForSearchSolution(request: d3m.SolutionSearchRequest, response?: d3m.ta2.SearchSolutionsResponse): SearchSolutionRecord {
  const datasetID = request.problem.inputs.data[0].datasetID;
  const problemID = request.problem.about.problemID;
  const metrics = getMetricsString(request.problem);
  const target  = getStringForTarget(request.problem);
  return {datasetID, problemID, metrics, target, response };
}

function isSameSolutionRecord(r1: SearchSolutionRecord, r2: SearchSolutionRecord) {
  return r1.datasetID === r2.datasetID
    && r1.metrics === r2.metrics
    && r1.problemID === r2.problemID
    && r1.target === r2.target;
}

const searchSolutions = new Recorder<d3m.ta2.SearchSolutionsResponse>( // It records the id for a given dataset
  async (req, data) => {
    const key = keyForSearchSolution(req.body);
    const saved = (readJSON(key) || []) as SearchSolutionRecord[];
    saved.push(createRecordForSearchSolution(req.body, data));
    recordJSON(saved, key);
    return data;
  },
  async (req) => {
    const key = keyForSearchSolution(req.body);
    const saved = (readJSON(key) || []) as SearchSolutionRecord[];
    const record = createRecordForSearchSolution(req.body);
    const found = saved.find(r => isSameSolutionRecord(r, record));
    if (found) {
      return found.response;
    }
    return undefined;
  },
);

const getSearchResultsTape = (params: d3m.ta3.GetSearchResultsRequest) =>  {
  const tape = new Tape<
  d3m.ta3.GetSearchResultsRequest, d3m.SolutionSearchUpdate>(
    'getSearchResults',
    (p)  => `${params.problem.inputs.data[0].datasetID}/${params.searchID}`,
    params,
  );
  tape.delay = () => Math.random() * 1000;
  return tape;
};

const evaluateSolutionTape = (params: d3m.ta3.EvaluateSolutionRequest) =>  {
  const tape = new Tape<
  d3m.ta3.EvaluateSolutionRequest, d3m.ta3.EvaluateSolutionResponse>(
    'evaluateSolution',
    (p)  => `${params.problem.inputs.data[0].datasetID}/${params.solutionID}`,
    params,
  );
  tape.delay = () => Math.random() * 1000;
  return tape;
};

const ruleMatrix = new Recorder<d3m.RuleMatrixData>(
  async (req, data) => {
    const { fittedSolutionID } = req.body;
    const key = `/visualization/rule-matrix/${fittedSolutionID}.json`;
    console.log('=> recording rule matrix', data);
    if (data.continue === false) {
    recordJSON(data, key);
    }
    return data;
  },
  async req => {
    const { fittedSolutionID } = req.body;
    const key = `/visualization/rule-matrix/${fittedSolutionID}.json`;
    const saved = readJSON(key);
    if (saved) {
    return saved as d3m.RuleMatrixData;
    }
    return undefined;
  },
  );

/**
 * End Recorders ==================================================================
 */

const recorders: { [recorder: string]: Recorder<unknown> } = {
  '/rest/datasets': Recorder.recorderFor('datasets.json'),
  '/rest/solutions/_searchSolutions': searchSolutions,
  '/rest/visualization/_regressionPartialPlots': Recorder
    .recorderFor<{}, RegressionPartialPlotsParams>(d => `/explanations/_regressionPartialPlots/${d.fittedSolutionID}.json`),
  '/rest/visualization/confusion-matrix': Recorder
    .recorderFor<{}, { fittedSolutionID: string }>(d => `/visualization/confusion-matrix/${d.fittedSolutionID}.json`),
  '/rest/visualization/confusion-scatterplot': Recorder
    .recorderFor<{}, { fittedSolutionID: string }>(d => `/visualization/confusion-scatterplot/${d.fittedSolutionID}.json`),
    '/rest/visualization/rule-matrix': ruleMatrix,
};

type Resolvers = {
  [resolver: string]: (params: unknown, request: WSRequest) => void;
};

type TapeBuilder<Req, Res> = (params: Req) => Tape<Req, Res>;

function wrapResolver<Req, Res extends Object>(
    resolvers: Resolvers,
    resolver: string,
    tapeBuilder: TapeBuilder<Req, Res>): Resolvers {

  const _resolver = resolvers[resolver];
  const newResolver = async (params: Req, req: WSRequest) => {
    console.log('== Resolving ==', params);
    const tape = tapeBuilder(params);
    if (tape.status === TapeStatus.RECORDED) {
      console.log(`Re-playing recorded tape: ${tape.name} with ${tape.data.data.length} responses`);
      for await (const update of tape.asyncIterator()) {
        console.log('Sending response from tape:', update);
        if (update) {
          req.send(update);
        }
      }
    } else {
      const _send = req.send.bind(req);
      req.send = (result: Res) => {
        console.log('Sending back response and recorded it on tape:', params);
        tape.add(result);
        _send(result);
      };
      console.log('Calling original resolver');
      _resolver(params, req);
    }
  };
  return { ...resolvers, [resolver]: newResolver };
}

export function wrapResolvers(resolvers: Resolvers) {
  resolvers = wrapResolver(resolvers, 'getSearchResults', getSearchResultsTape);
  resolvers = wrapResolver(resolvers, 'evaluateSolution', evaluateSolutionTape);
  return resolvers;
}

export function addRecorders(app: Router) {
  app.use('*', async (req, res, next) => {
    const recorder = recorders[req.originalUrl];
    if ((req.method === 'GET' || req.method === 'POST') && recorder) {
      const data = await recorder.retrieve(req);
      if (!data) {
        const sendJson = res.json.bind(res);
        res.json = function(body) {
          recorders[req.originalUrl].record(req, body);
          return sendJson(body);
        };
        next();
      } else {
        console.log('Using recorder data for', req.originalUrl);
        res.json(data);
      }
    } else {
      next();
    }
  });
}
