import * as errorHandler from 'errorhandler';
import * as http from 'http';
import { createRelayServer } from './relay';
import { NODE_ENV, checkResources } from './env';

if (!checkResources()) {
  // Critical resources are missing, cannot run server.
  process.exit(1);
}

const server = http.createServer();
const app = require('./app');

// Error Handler. Provides full stack - remove for production
if (NODE_ENV === 'development') {
  app.use(errorHandler());
}

// Register express app on the server
server.on('request', app);

// Register web socket path on the server
createRelayServer('/ws', server);

// Start Express/WebSocket server
server.listen(app.get('port'), () => {
    console.log('  App is running at http://localhost:%d in %s mode',
                app.get('port'), app.get('env'));
    console.log('  Web socket is running at http://localhost:%d/ws in %s mode',
                app.get('port'), app.get('env'));
    console.log('  Press CTRL-C to stop\n');
});

export = server;
