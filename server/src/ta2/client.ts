import * as path from 'path';
import * as NodeCache from 'node-cache';
import { cloneDeep } from 'lodash';
import * as d3m from 'd3m/dist';
import { toDebugString, circularStringify } from '../utils';
import { withCache } from '../cache';
// Environment variables for GRPC
import { GRPC_CONN_URL, GRPC_PROTO_PATH } from '../env';
import { Value } from 'd3m/dist/ta2';
import { usageLogger } from '../usage-log';
// import * as protoLoader from '@grpc/proto-loader';

const grpc = require('grpc');
const cache = new NodeCache();

export enum StreamStatus {
  UPDATE = 'UPDATE',
  END = 'END',
  ERROR = 'ERROR',
}

export interface StreamUpdate {
  status: StreamStatus;
  data: object;
  rawData?: object;
}

export function fixMapFields(orig, parsed: Object) {
  for (const key of Object.keys(parsed)) {
    const value = parsed[key];
    if (value && orig[key]) {
      if (value.field && value.keyElem && value.valueElem && value.map) {
        parsed[key] = orig[key];
      } else if (Array.isArray(value)) {
        parsed[key] = value.map((v, idx) => fixMapFields(orig[key][idx], v));
      }
      else if (typeof value === 'object') {
        parsed[key] = fixMapFields(orig[key], value);
      }
    }
  }
  return parsed;
}

function getProvidedValue(inputs: Value) {
  const values = Object.keys(inputs).filter(k => inputs[k]);
  if (values.length > 1) {
    throw 'More than one value is not supported';
  }
  return values[0];
}

export class TA2Client {

  private _connection;

  constructor() { }

  get connection() {
    if (!this._connection) {
      // const packageDefinition = protoLoader.loadSync(path.join(GRPC_PROTO_PATH, 'core.proto'), { keepCase: true });
      console.log('\nConnecting to TA2 GRPC API at', GRPC_CONN_URL);
      const core = grpc.load(path.join(GRPC_PROTO_PATH, 'core.proto'));
      const stub = new core.Core(GRPC_CONN_URL, grpc.credentials.createInsecure());
      this._connection = { core, stub };
    }
    return this._connection;
  }

  async execMethod(method: string, request: Object, rawResponse?: boolean) {
    const { stub, core } = this.connection;
    const responseClass = core[stub[method].responseType.name];
    return new Promise((resolve, reject) => {
      console.log(`\n> Calling gRPC method ${method}() with params:\n`, toDebugString(request));
      stub[method](request, (error, response) => {
        usageLogger.logGrpcApi(method);
        if (error) {
          // We reject the the promise when the execution fails.
          // Client should always catch the error using the try/catch
          // syntax or the then()/catch() Promise functions.
          reject(error);
        }
        if (!rawResponse) {
          response = new responseClass(response);
        }
        console.log(`\n> got gRPC response from ${method}():\n`, toDebugString(response));
        resolve(response);
      });
    });
  }

  async execStreamMethod(method: string, request: Object, onUpdate: (update: StreamUpdate) => void) {
    const { stub, core } = this.connection;
    const responseClass = core[stub[method].responseType.name];
    console.log(`\n> Calling gRPC stream method ${method}() with params:\n`, toDebugString(request));
    const call = stub[method](request);
    call.on('data', (update: object) => {
      console.log(`\n> got gRPC stream data from ${method}():\n`, toDebugString(update));
      try {
        const originalUpdate = cloneDeep(update);
        update = new responseClass(update);

        // This is a hack that prevents grpc Map being converted to a proto Map
        // class instance that loses its original (key, values).
        for (const key in update) {
          const value = update[key];
          if (value.field && value.keyElem && value.valueElem && value.map) {
            update[key] = originalUpdate[key];
          }
        }
        onUpdate({
          status: StreamStatus.UPDATE,
          data: update,
          rawData: originalUpdate,
        });
      } catch (error) {
        console.error(error);
        onUpdate({
          status: StreamStatus.ERROR,
          data: error,
        });
      }
    });
    call.on('end', (update: object) => {
      onUpdate({
        status: StreamStatus.END,
        data: update,
      });
    });
    call.on('error', (update: object) => {
      onUpdate({
        status: StreamStatus.ERROR,
        data: update,
      });
    });
  }

  async searchSolutions(request: d3m.ta2.SearchSolutionsRequest) {
    return this.execMethod(d3m.ta2.Rpc.SEARCH_SOLUTIONS, request) as Promise<d3m.ta2.SearchSolutionsResponse>;
  }

  async describeSolution(request: d3m.ta2.DescribeSolutionRequest) {
    const cacheKey = `describeSolution_${request.solution_id}`;
    let currValue: d3m.ta2.DescribeSolutionResponse = cache.get(cacheKey);
    if (!currValue) {
      currValue = await (this.execMethod(d3m.ta2.Rpc.DESCRIBE_SOLUTION, request, true) as Promise<d3m.ta2.DescribeSolutionResponse>);

      cache.set(cacheKey, currValue);
    }
    return currValue;
  }

  async getSearchSolutionsResults(request: d3m.ta2.GetSearchSolutionsResultsRequest, onUpdate: (update:
    d3m.ta2.GetSearchSolutionsResultsResponse,
    raw: object) => void) {
    return new Promise((resolve, reject) => {
      this.execStreamMethod(d3m.ta2.Rpc.GET_SEARCH_SOLUTIONS_RESULTS, request, update => {
        if (update.status === StreamStatus.UPDATE) {
          onUpdate(update.data as d3m.ta2.GetSearchSolutionsResultsResponse, update.rawData);
        } else if (update.status === StreamStatus.END) {
          resolve();
        }
      });
    });
  }

  async scoreSolution(request: d3m.ta2.ScoreSolutionRequest) {
    return this.execMethod(d3m.ta2.Rpc.SCORE_SOLUTION, request) as Promise<d3m.ta2.ScoreSolutionResponse>;
  }

  async getScoreSolutionsResults(request: d3m.ta2.GetScoreSolutionResultsRequest, onUpdate: (update: d3m.ta2.GetScoreSolutionResultsResponse) => void) {
    return new Promise((resolve, reject) => {
      this.execStreamMethod(d3m.ta2.Rpc.GET_SCORE_SOLUTION_RESULTS, request, update => {
        if (update.status === StreamStatus.UPDATE) {
          if (update.rawData) {
            update.rawData['progress']['state'] = d3m.ta2.ProgressState[update.rawData['progress']['state']];
            onUpdate(update.rawData as d3m.ta2.GetScoreSolutionResultsResponse);
          }
        } else if (update.status === StreamStatus.END) {
          resolve();
        }
      });
    });
  }

  async fitSolutionRequest(request: d3m.ta2.FitSolutionRequest) {
    const { solution_id, inputs } = request;
    const cacheKey = `ta2/client/fitSolutionRequest/${solution_id}/${inputs[getProvidedValue(inputs)]}`;
    return withCache(cacheKey, () => this.execMethod(d3m.ta2.Rpc.FIT_SOLUTION, request) as Promise<d3m.ta2.FitSolutionResponse>);
  }

  async getFitSolutionResults(request: d3m.ta2.GetFitSolutionResultsRequest, onUpdate?: (update: d3m.ta2.GetFitSolutionResultsResponse) => void) {
    const cacheKey = `ta2/client/getFitSolutionResults/${request.request_id}`;
    return withCache(cacheKey, () => new Promise((resolve, reject) => {
      this.execStreamMethod(d3m.ta2.Rpc.GET_FIT_SOLUTION_RESULTS, request, update => {
        if (update.status === StreamStatus.UPDATE) {
          if (onUpdate) {
            onUpdate(update.data as d3m.ta2.GetFitSolutionResultsResponse);
          }
        } else if (update.status === StreamStatus.END) {
          resolve(update.data);
        }
      });
    }));
  }

  async produceSolutionRequest(request: d3m.ta2.ProduceSolutionRequest) {
    const { inputs, fitted_solution_id } = request;
    const cacheKey = `ta2/client/produceSolutionRequest/${fitted_solution_id}/${inputs[0][getProvidedValue(inputs[0])]}`;
    return withCache(cacheKey, () => this.execMethod(d3m.ta2.Rpc.PRODUCE_SOLUTION, request) as Promise<d3m.ta2.ProduceSolutionResponse>);
  }

  async stopSolutionSearch(request: d3m.ta2.StopSearchSolutionsRequest) {
    return this.execMethod(d3m.ta2.Rpc.STOP_SEARCH_SOLUTIONS, request) as Promise<d3m.ta2.StopSearchSolutionsResponse>;
  }

  async solutionExportRequest(request: d3m.ta2.SolutionExportRequest) {
    return this.execMethod(d3m.ta2.Rpc.SOLUTION_EXPORT, request) as Promise<d3m.ta2.SolutionExportResponse>;
  }
  async getProduceSolutionResults(request: d3m.ta2.GetProduceSolutionResultsRequest, onUpdate: (update?: d3m.ta2.GetProduceSolutionResultsResponse) => void) {
    const cacheKey = `ta2/client/getProduceSolutionResults/${request.request_id}`;
    return withCache(cacheKey, () => new Promise((resolve, reject) => {
      this.execStreamMethod(d3m.ta2.Rpc.GET_PRODUCE_SOLUTION_RESULTS, request, update => {
        if (update.status === StreamStatus.UPDATE) {
          if (onUpdate) {
            onUpdate(update.data as d3m.ta2.GetFitSolutionResultsResponse);
          }
        } else if (update.status === StreamStatus.END) {
          resolve(update.data);
        }
      });
    }));
  }
}

export default new TA2Client();
