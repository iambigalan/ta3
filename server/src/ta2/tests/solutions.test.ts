import 'jest';
import config from '../../data/config';
import { DataSource } from '../../data/DataSource';
import { Dataset } from '../../data/Dataset';
import { Problem } from '../../data/Problem';
import { startSolutionSearch, getSearchResults } from '../solutions';
import getDataSourceConfig from '../../data/config';

function init() {
  const store = new DataSource(getDataSourceConfig());
  return store;
}

describe.skip('TA2 - Interaction', () => {
  beforeEach(() => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 15000;
  });

  it('Start a pipeline search', async () => {
    const store = init();
    const datasetId = '185_baseball';
    const datasetInfo: Dataset = await store.getDatasetById(datasetId);
    const problems: Problem[] = await datasetInfo.getProblems();
    const searchId = await startSolutionSearch({
      problem: problems[0].toJSON(),
      priority: 0,
      timeLimit: 0,
    });
    expect(searchId).toHaveProperty('search_id');
  });

  it('Start a pipeline search, and get updates', async () => {
    const store = init();
    const datasetId = '185_baseball';
    const datasetInfo: Dataset = await store.getDatasetById(datasetId);
    const problems: Problem[] = await datasetInfo.getProblems();
    const searchId = await startSolutionSearch({
      problem: problems[0].toJSON(),
      priority: 0,
      timeLimit: 0,
    });

    return new Promise((resolve, reject) => {
      getSearchResults(searchId.search_id, (update) => {
        if (update.progress.state.toUpperCase() === 'COMPLETED') {
          expect(update.progress.percentage).toEqual(1);
          resolve();
        }
      });
    });
  });
});
