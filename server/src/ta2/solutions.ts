import client from './client';
import { toTA2ProblemDescription } from 'd3m/dist/translate';
import * as d3m from 'd3m/dist';
import { getDataSource } from '../data/config';
import { flatten } from 'lodash';
import * as json2csv from 'json2csv';
import * as fs from 'fs';
import * as path from 'path';
import { mkDirByPathSync } from '../api/utils';
import { getProblemsDirectory } from '../data/config';
import { SeqIdCreator } from '../utils';
import { TEST_SPLIT_PROPORTION } from '../data/Dataset';

const USER_AGENT = 'TA3-NYU';
const VERSION = '2019.7.9';

const userProblems: Array<{
  problem_id: string;
  system: string;
  meaningful: string;
}> = [];

export async function startSolutionSearch(
  params: d3m.SolutionSearchRequest,
): Promise<d3m.ta2.SearchSolutionsResponse> {

  const problemDescription = toTA2ProblemDescription(params.problem);
  const dataSource = getDataSource();
  const inputs: d3m.ta2.Value[] = [];
  for (const input of problemDescription.inputs) {
    const dataset = dataSource.getDatasetById(input.dataset_id);
    inputs.push({
      dataset_uri: await dataset.getTrainingDocURI(input.targets[0].column_name.toString(),
                                                   params.problem.about.taskType.toString()),
    });
  }
  const searchRequest: d3m.ta2.SearchSolutionsRequest = {
    user_agent: USER_AGENT,
    version: VERSION,
    allowed_value_types: [d3m.ta2.ValueType.RAW],
    problem: problemDescription,
    time_bound_search: params.timeLimit,
    priority: params.priority,
    inputs: inputs,
    time_bound_run: 0,
  };

  saveProblem(params.problem, searchRequest);

  return client.searchSolutions(searchRequest);
}

export async function saveProblem(
  problem: d3m.Problem, searchSolutionCall: d3m.ta2.SearchSolutionsRequest,
): Promise<string> {

  const problemSchema = problem;
  const pathProblemSchema = getProblemsDirectory();
  const problemId = problemSchema.about.problemID;
  const meaningful = 'yes'; // req.body.meaningful; // it could be 'no' if the user did not select the problem as a meaningful problem.
  const system = 'user'; // req.body.system; // it could be 'auto' if the user did not select the problem as a meaningful problem.
  userProblems.push({
    problem_id: problemId,
    system: system,
    meaningful: meaningful,
  });
  const csv = json2csv.parse(userProblems);
  const csvOutput = path.join(pathProblemSchema, 'labels.csv');
  mkDirByPathSync(path.join(pathProblemSchema, problemId));
  if (fs.existsSync(csvOutput)) {
    fs.unlinkSync(csvOutput);
  }
  fs.writeFileSync(csvOutput, csv, { encoding: 'utf8', flag: 'w' });
  const schemaOutputFilename = path.join(pathProblemSchema, problemId, 'problem_schema.json');
  fs.writeFile(schemaOutputFilename, JSON.stringify(problemSchema), () => {});
  const callOutputFilename = path.join(pathProblemSchema, problemId,  'ss_api.json');
  fs.writeFile(callOutputFilename, JSON.stringify(searchSolutionCall), () => {});

  return 'Ok';
}

export async function describeSolution(solutionID: string) {
  return client.describeSolution({
    solution_id: solutionID,
  });
}

export function getSearchResults(
  searchID: string,
  problem: d3m.Problem,
  onUpdate: (update: d3m.SolutionSearchUpdate) => void,
) {
  const seqIdCreator = new SeqIdCreator();
  return client.getSearchSolutionsResults(
    {
      search_id: searchID,
    },
    async (data: d3m.ta2.GetSearchSolutionsResultsResponse, raw) => {

      // Send progress update
      onUpdate({
        progress: {
          percentage: data.done_ticks / (data.all_ticks || 1), // avoid NaN on divide-by-zero
          state: d3m.ta2.ProgressStateNameMap[data.progress.state],
          status: data.progress.status,
        },
      });

      // When there is an solution_id, we return it and trigger
      // the loading of the pipeline description and the scores
      if (data.solution_id) {

        const name: string = seqIdCreator.getOrCreateSeqId(data.solution_id, 3);

        const solution: d3m.Solution = {
          id: data.solution_id,
          internal_score: data.internal_score,
          name: name,
        };
        // Send a partial solution to browser so that it can be displayed immediately
        onUpdate({ solution });

        // Dispatch async loading of solution description (for pipeline steps graph)
        try {
          const description: d3m.DescribeSolutionProgress = await loadSolutionDescription(data.solution_id);
          onUpdate({
            solution: {
              ...solution,
              description,
            },
          });
        } catch {
          onUpdate({
            solution: {
              ...solution,
              description: {
                status: d3m.DescribeSolutionStatus.ERRORED,
              },
            },
          });
        }
        const problemDescription = toTA2ProblemDescription(problem);
        const scoreValues = tryToReadScoresFromSolution(raw, problemDescription);
        if (scoreValues) {
          onUpdate({
            solution: {
              ...solution,
              scores: {
                status: d3m.ScoringStatus.COMPLETED,
                scores: scoreValues,
              },
            },
          });
        } else {
          // Dispatch async computation of scores
          try {
            const scores: d3m.SolutionScoreProgress = await scoreSolution(data.solution_id, problemDescription);
            onUpdate({
              solution: {
                ...solution,
                scores,
              },
            });
          } catch {
            onUpdate({
              solution: {
                ...solution,
                scores: {
                  status: d3m.ScoringStatus.ERRORED,
                },
              },
            });
          }
        }
      }
    },
  );
}

/**
 * Tries to read the metrics requested in the problem description from the
 * solution search result. If all metrics requested were returned, then they
 * are returned, otherwise, no metric is returned.
 */
function tryToReadScoresFromSolution(
  raw: object,
  problemDescription: d3m.ta2.ProblemDescription,
) {
  try {
    const scoreValues: d3m.SolutionScore[] = flatten(
      raw['scores'].map((s) =>
        s['scores'].map((d) => (
          {
            metric: d.metric.metric,
            score: d.value.raw.double,
          } as d3m.SolutionScore
        )),
      ),
    );

    const scores: {
        [metric: string]: d3m.SolutionScore;
    } = {};
    const requestedMetrics = problemDescription.problem.performance_metrics;
    const metricNames: string[] = requestedMetrics.map(m => d3m.ta2.PerformanceMetric[m.metric]);
    metricNames.forEach(name => {
      const i = scoreValues.findIndex(v => v.metric === name);
      if (i >= 0) {
        scores[name] = scoreValues[i];
      }
    });

    const receivedMetrics: number = Object.keys(scores).length;
    console.log(`Received ${receivedMetrics} out of ${requestedMetrics.length} ` +
      `requested metrics in search solution response.`);
    if (Object.keys(scores).length === requestedMetrics.length) {
      return scores;
    } else {
      return undefined;
    }
  } catch {
    return undefined;
  }
}

async function loadSolutionDescription(solutionId: string): Promise<d3m.DescribeSolutionProgress> {
  try {
    const solutionDetail = await client.describeSolution({
      solution_id: solutionId,
    });
    const pipeline = solutionDetail.pipeline;
    const modelName = findMainPrimitiveName(pipeline);
    return {
      status: d3m.DescribeSolutionStatus.COMPLETED,
      modelName: modelName,
      pipeline: pipeline,
    };
  } catch {
    console.error(`Couldn't get details of solution ${solutionId}`);
    return {
      status: d3m.DescribeSolutionStatus.ERRORED,
    };
  }
}

function findMainPrimitiveName(pipeline: d3m.ta2.PipelineDescription) {
  const prefixes = [
    'd3m.primitives.classification',
    'd3m.primitives.regression',
    'd3m.primitives.object_detection',
    'd3m.primitives.graph_matching',
    'd3m.primitives.clustering',
    'd3m.primitives.time_series_forecasting',
    'd3m.primitives.time_series_classification',
    'd3m.primitives.vertex_nomination',
    'd3m.primitives.semisupervised_classification',
    'd3m.primitives.link_prediction',
  ];
  for (const step of pipeline.steps) {
    for (const prefix of prefixes) {
      console.log({step, prefix, python_path: step.primitive.primitive.python_path, name: step.primitive.primitive.name});
      if (step.primitive.primitive.python_path.startsWith(prefix)) {
        return step.primitive.primitive.name.split('.').pop();
      }
    }
  }
  return undefined;
}

export async function scoreSolution(
  solutionID: string,
  problemDescription: d3m.ta2.ProblemDescription,
): Promise<d3m.SolutionScoreProgress> {

  const dataSource = getDataSource();
  const inputs: d3m.ta2.Value[] = await Promise.all(problemDescription.inputs.map(
    async (input): Promise<d3m.ta2.Value> => ({
      dataset_uri: await dataSource.getDatasetById(input.dataset_id).getTestingDocURI(
                         input.targets[0].column_name.toString(), problemDescription.problem.task_type),
    })),
  );

  const scoreRequest: d3m.ta2.ScoreSolutionRequest = {
    solution_id: solutionID,
    inputs: inputs,
    performance_metrics: problemDescription.problem.performance_metrics,
    configuration: {
      method: d3m.ta2.EvaluationMethod.HOLDOUT,
      random_seed: 1,
      train_test_ratio: 1.0 - TEST_SPLIT_PROPORTION,
      shuffle: false,
    } as d3m.ta2.ScoringConfiguration,
  };

  try {
    const scoreSolutionResponse = await client.scoreSolution(scoreRequest);
    return await getScoreSolutionResults(solutionID, scoreSolutionResponse);
  } catch {
    return  {
      status: d3m.ScoringStatus.ERRORED,
    };
  }
}

export async function getScoreSolutionResults(
  solutionID: string,
  scoreSolutionResponse: d3m.ta2.ScoreSolutionResponse,
) {
  return new Promise<d3m.SolutionScoreProgress>(resolve => {

    const returnedScores: {[metric: string]: d3m.SolutionScore} = {};

    client.getScoreSolutionsResults(
      { request_id: scoreSolutionResponse.request_id },
      update => {

        // we first accumulate all received scores locally
        if (update.scores && update.scores.length > 0) {
          update.scores.forEach((s: d3m.ta2.Score) => {
            const metric = s.metric.metric.toString();
            returnedScores[metric] = {
              metric: metric,
              score: s.value.raw.double,
            };
          });
        }

        // when the progress is completed, resolve all scores
        if (update.progress.state === d3m.ta2.ProgressState.COMPLETED) {
          // return scores in the same order that they were returned from TA2
          resolve({
            status: d3m.ScoringStatus.COMPLETED,
            scores: returnedScores,
          });
        }

        // if scoring errored, resolve with errored state
        if (update.progress.state === d3m.ta2.ProgressState.ERRORED) {
          console.error(`Score solution for solutionID=[${solutionID}] and ` +
            `request_id=[${scoreSolutionResponse.request_id}] errored.`);
          resolve({
            status: d3m.ScoringStatus.ERRORED,
          });
        }
      },
    );
  });
}

export async function fitSolution(
  req: {
    solutionID: string,
    problem: d3m.Problem,
    inputs?: { dataset_uri: string },
}): Promise<d3m.ta2.FitSolutionResponse> {
  const { solutionID, problem } = req;
  const dataSource = getDataSource();
  const request: d3m.ta2.FitSolutionRequest = {
    solution_id: solutionID,
    inputs: req.inputs || {
      dataset_uri: await dataSource.getDatasetById(problem.inputs.data[0].datasetID).getTrainingDocURI(
        problem.inputs.data[0].targets[0].colName.toString(), problem.about.taskType),
    },
    expose_outputs: [],
    expose_value_types: d3m.ta2.ValueType.CSV_URI,
  };
  return client.fitSolutionRequest(request);
}

export async function getFitSolutionResults(
  requestID: string,
  onUpdate: (update: d3m.ta2.GetFitSolutionResultsResponse) => void,
) {
  return client.getFitSolutionResults({ request_id: requestID }, onUpdate);
}

/**
 * Gets the final fitted solution ID given a FitSolution request.
 */
export async function getFitSolutionFittedSolutionID(requestID: string) {
  return new Promise<{ fittedSolutionID: string }>(resolve => {
    getFitSolutionResults(
      requestID,
      async (fitUpdate: d3m.ta2.GetFitSolutionResultsResponse) => {
        // Wait for FitSolution to complete
        if (fitUpdate.progress.state === d3m.ta2.ProgressState.COMPLETED) {
          const fittedSolutionID = fitUpdate.fitted_solution_id;
          resolve({ fittedSolutionID });
        }
        if (fitUpdate.progress.state === d3m.ta2.ProgressState.ERRORED) {
          // TODO: Implement a better error handling
          console.error(`Fit solution results for requestID=[${requestID}] errored.`);
        }
      },
    );
  });
}

/**
 * Produces the solution using the given inputs.
 */
export async function produceSolution(req: {
  fittedSolutionID: string;
  inputs: Array<{ dataset_uri: string }>;
}): Promise<d3m.ta2.ProduceSolutionResponse> {
  const { fittedSolutionID, inputs } = req;
  const request: d3m.ta2.ProduceSolutionRequest = {
    fitted_solution_id: fittedSolutionID,
    inputs: await Promise.all(inputs),
    expose_outputs: ['outputs.0'],
    expose_value_types: [d3m.ta2.ValueType.CSV_URI],
  };
  return client.produceSolutionRequest(request);
}

export function stopSearchSolutions(
  searchID,
): Promise<d3m.ta2.StopSearchSolutionsResponse> {
  return client.stopSolutionSearch({
    search_id: searchID,
  });
}

export async function getProduceSolutionResults(
  requestID: string,
  onUpdate: (update: d3m.ta2.GetProduceSolutionResultsResponse) => void,
) {
  return client.getProduceSolutionResults({ request_id: requestID }, onUpdate);
}

/**
 * Returns the final produced CSV URI for a ProduceSolution request.
 */
export async function getProduceSolutionCsvUri(requestID: string) {
  return new Promise<string>(resolve => {
    getProduceSolutionResults(requestID, update => {
      if (update.progress.state === d3m.ta2.ProgressState.COMPLETED) {
        resolve(update.exposed_outputs['outputs.0'].csv_uri);
      }
    });
  });
}

export async function solutionExport(solutionID: string, rank: number) {
  return client.solutionExportRequest({
    solution_id: solutionID,
    rank,
  } as d3m.ta2.SolutionExportRequest);
}
