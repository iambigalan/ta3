import fetch from 'node-fetch';
import * as path from 'path';
import * as yauzl from 'yauzl';
import { DATAMART_URL, DATASETS_INFO, DATAMART_MARK } from '../env';
import * as fs from 'fs';
import * as FormData from 'form-data';
import * as d3m from 'd3m/dist';
import { Dataset } from '../data/Dataset';
import { DatamartResult } from 'd3m/dist';
import { mkdirIfAbsent, toDebugString } from '../utils';
import { camelCase } from 'lodash';
import { ascending } from 'd3-array';

export async function search(dataset: Dataset, query: string) {
  const formData = new FormData();

  const resource = dataset.getMainResource().getPath();
  const data = fs.readFileSync(resource, { encoding: 'utf-8' });
  formData.append('data', data, resource);
  formData.append(
    'query',
    JSON.stringify({
      keywords: query ? [query] : [],
      variables: [],
    }),
  );

  const dataSearchEndpoint = `${DATAMART_URL}/search`;
  console.log(`> Issuing query to Datamart at ${dataSearchEndpoint}`);
  const response = await fetch(dataSearchEndpoint, {
    method: 'POST',
    body: formData,
  });

  console.log(`> Datamart /search HTTP response status code  : ${response.status}`);

  if (response.status !== 200) {
    throw 'Status ' + response.status;
  }

  return response.json();
}

export async function augment(dataset: Dataset, hit: DatamartResult) {
  console.log(`> Augmenting dataset "${dataset.datasetID}"` +
    ' with search result:\n', toDebugString(hit));

  const resource = dataset.getMainResource().getPath();
  const data = fs.readFileSync(resource, { encoding: 'utf-8' });

  // Prepare datamart augmentation request
  const formData = new FormData();
  formData.append('data', data, resource);
  formData.append('task', JSON.stringify(hit));
  const response = await fetch(DATAMART_URL + '/augment', {
    method: 'POST',
    body: formData,
    timeout: 30 * 60 * 1000, // milliseconds
  });

  console.log('> Datamart /augment HTTP response status code: ',
    response.status);

  const buffer = await response.buffer();
  const isAugmented = dataset.datasetID.indexOf(DATAMART_MARK) > -1;
  const parts = dataset.datasetID.split(DATAMART_MARK);
  let previous = parts.length > 1 ? parts[1].split('__AND__') : [];
  const sourceDatasetId = parts[0];
  previous.push(camelCase(hit.metadata.name));
  const augmentationID = previous.join('__AND__');
  previous = previous.sort(ascending);
  const datasetID = sourceDatasetId + `${DATAMART_MARK}${augmentationID}`;

  const datasetAugmentedFolder = path.join(DATASETS_INFO, sourceDatasetId, 'augmented');
  mkdirIfAbsent(datasetAugmentedFolder);

  const newDatasetFolder = path.join(datasetAugmentedFolder, augmentationID);
  mkdirIfAbsent(newDatasetFolder);
  fs.writeFileSync(path.join(newDatasetFolder, 'source_hit.json'), JSON.stringify(hit));

  const files: {
    datasetID: string,
  } = await new Promise((resolve, reject) => {
    yauzl.fromBuffer(buffer, (error: Error, zipFile: yauzl.ZipFile) => {
      if (error) {
        reject(error);
      }

      zipFile.on('entry', function(entry) {
        if (/\//.test(entry.fileName)) {
          const subPath = entry.fileName.substring(0, entry.fileName.lastIndexOf('/') + 1);
          mkdirIfAbsent(path.join(newDatasetFolder, subPath));
        }
        zipFile.openReadStream(entry, function(err, readStream) {
          if (err) {
            throw err;
          }
          if (entry.fileName === 'datasetDoc.json') {
            const buffers = [];
            readStream.on('data', (d) => {
              buffers.push(d);
            });
            readStream.on('end', () => {
              const mainResourceId = dataset.getMainResource().resourceID;
              const datasetDoc: d3m.Dataset = JSON.parse(Buffer.concat(buffers).toString('utf8'));
              datasetDoc.about = {...dataset.about};
              datasetDoc.about.datasetID = datasetID;
              datasetDoc.about.description += `\n **Augmented With:** ${hit.metadata.name}`;
              datasetDoc.dataResources.find(r => r.resPath.indexOf('learningData') > -1)!.resID = mainResourceId;
              const quality = datasetDoc.qualities[0];
              datasetDoc.qualities = dataset.qualities || [];
              quality.qualValue['hit'] = hit;
              quality.qualValue['sourceDatasetID'] = sourceDatasetId;
              quality.qualValue['previousAugmentations'] = datasetDoc.qualities.filter(q => q.qualName === 'augmentation_info');
              datasetDoc.qualities = datasetDoc.qualities.filter(q => q.qualName !== 'augmentation_info');
              datasetDoc.qualities.push(quality);
              fs.writeFileSync(path.join(newDatasetFolder, 'datasetDoc.json'), JSON.stringify(datasetDoc, undefined, 4));
            });
          } else {
            readStream.pipe(fs.createWriteStream(path.join(newDatasetFolder, entry.fileName)));
          }
        });
      });
      zipFile.on('end', () => {
        try {
          resolve({
            datasetID: datasetID,
          });
        } catch (err) {
          console.log('Failed to resolve unzip promise.', err);
          reject(err);
        }
      });
    });
  });

  console.log('> Done with augmentation. New datasetID:', files.datasetID);
  return files;
}
