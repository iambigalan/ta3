import * as fs from 'fs';
import * as path from 'path';

export function deleteFolderRecursive(deletePath: string) {
  if (fs.existsSync(deletePath)) {
    fs.readdirSync(deletePath).forEach(function(file: string, index: number) {
      const curPath = deletePath + '/' + file;
      if (fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(deletePath);
  }
}

export function isDirectory(filePath: string): boolean {
  // fs.realpathSync resolves symlinks to directories
  return fs.existsSync(filePath) && fs.lstatSync(fs.realpathSync(filePath)).isDirectory();
}

export function isFile(filePath: string): boolean {
  return fs.existsSync(filePath) && fs.lstatSync(filePath).isFile();
}

export function mkDirByPathSync(targetDir: string, {isRelativeToScript = false} = {}) {
  const sep = path.sep;
  const initDir = path.isAbsolute(targetDir) ? sep : '';
  const baseDir = isRelativeToScript ? __dirname : '.';
  targetDir.split(sep).reduce(
    (parentDir: string, childDir: string) => {
      const curDir = path.resolve(baseDir, parentDir, childDir);
      if (fs.existsSync(curDir)) {
        return curDir;
      }
      try {
        fs.mkdirSync(curDir);
      } catch (err) {
        console.warn('Directory exists');
      }
      return curDir;
    }
    , initDir);
}

export function formatAsFileURI(filePath: string) {
  let uri: string;
  if (!filePath.startsWith('file://')) {
    uri = 'file://' + filePath;
  } else {
    uri = filePath;
  }
  return uri;
}
