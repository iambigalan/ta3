// TODO: prepare to remove this api file

import { Response, Request, NextFunction } from 'express';
import * as fs from 'fs';
import * as path from 'path';
import * as fsapi from '../rest/loader';
import { getConfigJson } from '../data/config';

interface Dataset {
  id: string;
  size: string;
  description: string;
  nColumns: number;
  datasetdocUri: string;
}

/*
  Function that returns the available datasets to the client.
  Environment variable D3M_DATA_PATH has the root to the datasets folder
*/
export let getDatatasetList = (req: Request, res: Response, next: NextFunction) => {
  fsapi.walk((err: NodeJS.ErrnoException, fnames: string[]) => {
    const ret: Dataset[] = [];
    fnames.forEach(fname => {
      const datasetDocPath = path.join(fname, fname + '_dataset', 'datasetDoc.json');
      const problemDocPath = path.join(fname, fname + '_problem', 'problemDoc.json');
      if (fsapi.hasDoc(datasetDocPath)) {
        const datasetDoc = fsapi.readDatasetDoc(datasetDocPath);
        const size = datasetDoc.about.approximateSize;
        const id = fname;
        let description = '';
        if (fsapi.hasDoc(problemDocPath)) {
          const problemDoc = fsapi.readProblemDoc(problemDocPath);
          if (problemDoc.about.problemDescription) {
            description = problemDoc.about.problemDescription;
          }
        }
        const datasetdocUri = datasetDocPath;
        let nColumns = 0;
        for (let i = 0; i < datasetDoc.dataResources.length; ++i) {
          if (datasetDoc.dataResources[i].columns) {
            nColumns += datasetDoc.dataResources[i].columns.length - 1;
          }
        }
        ret.push({ id, size, description, nColumns, datasetdocUri });
      }
    });
    res.json(ret);
  });
};

export let getDatasetResources = (req: Request, res: Response, next: NextFunction) => {
  const datasetId = req.body.datasetId;
  const datasetDocPath = path.join(datasetId, datasetId + '_dataset', 'datasetDoc.json');
  if (fsapi.hasDoc(datasetDocPath)) {
    const datasetDoc = fsapi.readDatasetDoc(datasetDocPath);
    res.json(datasetDoc.dataResources);
  }
};

export let getDataInfoFromConfig = (req: Request, res: Response, next: NextFunction) => {
  const config = getConfigJson();
  let datasetSchema = undefined, problemSchema = undefined;

  console.warn(config);
  if (fs.existsSync(config.dataset_schema)) {
    datasetSchema = JSON.parse(fs.readFileSync(config.dataset_schema, 'utf8'));
  }
  if (fs.existsSync(config.problem_schema)) {
    problemSchema = JSON.parse(fs.readFileSync(config.problem_schema, 'utf8'));
  }
  const data = {
    dataset_schema_path: config.dataset_schema,
    dataset_schema: datasetSchema,
    problem_schema: problemSchema,
    training_data_root: config.training_data_root,
    problem_root: config.problem_root,
    pipeline_logs_root: config.pipeline_logs_root,
    executables_root: config.executables_root,
    user_problems_root: config.user_problems_root,
    temp_storage_root: config.temp_storage_root,
    timeout: config.timeout,
    cpus: config.cpus,
    ram: config.ram,
  };
  res.json(data);
};
