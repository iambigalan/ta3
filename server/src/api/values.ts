// TODO: prepare to remove this api file

import { Response, Request, NextFunction } from 'express';
import { Table, getBase, readTableFromResource, readTableFromResourceTimeSerie } from '../rest/loader';
import * as d3m from 'd3m/dist';

import * as fs from 'fs';
import * as path from 'path';

interface Feature {
  dist: { [index: string]: number };
  vals: string[];
  mean?: number;
  stddev?: number;
  isCat: boolean;
}

interface TimeSerieObject {
  tableObject: { [index: string]: Table };
}

const allFilesSync = (dir: string, fileList: string[] = []) => {
  fs.readdirSync(dir).forEach(file => {
    const filePath = path.join(dir, file);
    fileList.push(file);
  });
  return fileList;
};

export let getValues = (req: Request, res: Response, next: NextFunction) => {
  const doc: string = req.body.doc;
  if (!doc) {
    res.json({ features: [] });
    return;
  }
  const base = getBase(doc);
  const drs: d3m.DataResource[] = req.body.drs;
  const allFeatures: { [index: number]: Feature[] } = {};
  Promise.all<void>(drs.map((dr, ix) => {
    if (!dr.resPath) {
      return new Promise<void>((resolve) => resolve());
    }
    const dirCSV = dr.resPath;
    if (dirCSV.includes('.csv')) {
      return readTableFromResource(dr, base).then((table) => {
        allFeatures[ix] = computeFeatures(table);
      });
    }
  })).then(() => {
    const ret = [];
    for (let ix = 0; ix < Object.keys(allFeatures).length; ix += 1) {
      ret.push(allFeatures[Object.keys(allFeatures)[ix]]);
    }
    res.json({ features: ret });
  }).catch((err) => {
    res.status(500).send('error');
    console.error(err.stack, err);
  });
};

export let getTimeSeriesDatasets = (req: Request, res: Response, next: NextFunction) => {
  const doc: string = req.body.doc;
  if (!doc) {
    res.json({ timeSeriesFeatures: [] });
    return;
  }
  const base = getBase(doc);
  const dataResources: d3m.DataResource[] = req.body.drs;
  const allTimeSeries: ({ [index: string]: Table })[] = [];

  const allTablePromises = [];
  dataResources.forEach((dataResource: d3m.DataResource) => {
    if (!dataResource.resPath) {
      return new Promise<void>((resolve) => resolve());
    }
    const dirCSV = dataResource.resPath;
    // Handling timeseries.
    if (!dirCSV.includes('.csv')) {
      const baseTimeSeries = base + '/' + dataResource.resPath;
      const listFiles = allFilesSync(baseTimeSeries);
      listFiles.forEach(filename => {
        const fileDir = baseTimeSeries + filename;
        allTablePromises.push(readTableFromResourceTimeSerie(dataResource, fileDir).then((table) => {
          const tableObject: {
            [index: string]: Table;
          } = {};
          tableObject[filename] = table;
          allTimeSeries.push(tableObject);
        }));
      });
    }
  });
  Promise.all<void>(allTablePromises).then(() => {
    const ret = [];
    for (let ix = 0; ix < allTimeSeries.length; ix += 1) {
      ret.push(allTimeSeries[ix]);
    }
    res.json({ timeSeriesFeatures: ret });
  }).catch((err) => {
    res.status(500).send('error');
    console.error(err.stack, err);
  });
};

function computeFeatures(table: Table): Feature[] {
  const features = table.mapColumn((col, values) => {
    const isCat = (col.colType === 'categorical' || col.colType === 'string' || col.colType === 'dateTime');
    const dist: { [index: string]: number } = {};
    let vals: string[] = undefined;
    if (isCat) {
      values.forEach((v) => {
        if (!(v in dist)) {
          dist[v] = 0;
        }
        dist[v] += 1;
      });
      vals = Object.keys(dist);
      vals.sort((a, b) => -(dist[a] - dist[b]));
    } else {
      const [ min, max ] = findMinAndMaxValues(values);
      vals = [];
      if (Number.isFinite(min) && Number.isFinite(max)) {
        const buckets: number[] = [];
        const stride = Math.max(1e-5, (max - min) / 20.0);
        let cur = min;
        while (cur <= max) {
          vals.push(`[${cur}; ${cur + stride})`);
          buckets.push(cur);
          cur += stride;
        }
        values.forEach((v) => {
          let bix = 0;
          while (bix < buckets.length && +v >= buckets[bix]) {
            bix += 1;
          }
          bix -= 1;
          if (bix >= 0) {
            const b = vals[bix];
            if (!(b in dist)) {
              dist[b] = 0;
            }
            dist[b] += 1;
          }
        });
      }
    }
    const out: Feature = {
      isCat, dist, vals,
    };
    if (!isCat) {
      const { mean, stddev } = computeMeanAndStdDeviation(values);
      out.mean = mean;
      out.stddev = stddev;
    }
    return out;
  });
  return features;
}

function findMinAndMaxValues(values: (string | number)[]): number[] {
  const [min, max] = values.reduce((p, v) => {
    if (!Number.isFinite(+v)) {
      return p;
    }
    return [Math.min(p[0], +v), Math.max(p[1], +v)];
  }, [Number.POSITIVE_INFINITY, Number.NEGATIVE_INFINITY]);
  return [min, max];
}

function computeMeanAndStdDeviation(values: (string | number)[]) {
  const stats = values.reduce((p, v) => {
    const vNumber = +v;
    return [
      p[0] + vNumber, // sum of values
      p[1] + vNumber * vNumber, // sum of squared values
      p[2] + 1, // count of values
    ];
  }, [0, 0, 0]);
  const mean = stats[0] / stats[2];
  const stddev = Math.sqrt(stats[1] / stats[2] - mean * mean);
  return {mean, stddev};
}
