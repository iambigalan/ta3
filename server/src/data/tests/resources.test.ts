import 'jest';
import { getDataSourceConfig } from '../config';
import { DataSource } from '../DataSource';

const baseDataset = '185_baseball_dataset';

async function getDataset(datasetID: string = baseDataset) {
  const store = new DataSource(getDataSourceConfig());
  const dataset = await store.getDatasetById(datasetID);
  console.log(dataset.datasetDocPath);

  return dataset;
}

describe("Read datasets' info", () => {

  it('load resource info', async () => {
    const dataset = await getDataset();
    const resource = dataset.getResource('0');
    const resourceInfo = resource.toJSON();
    expect(resourceInfo).toMatchObject({ resType: 'table', resID: '0' });
  });

  it('load a profiler for a column', async () => {
    const dataset = await getDataset();
    const resource = dataset.getResource('0');
    const columnProfilers = await resource.getColumnsProfiles();
    expect(columnProfilers).toHaveLength(resource.config.columns.length);
    expect(columnProfilers[2].column).toMatchObject({
      colName: 'Number_seasons',
    });
    expect(columnProfilers[2].values[0]).toMatchObject({
      count: 243,
      name: 10,
    });
  });
});

describe('Sample Loading', () => {

  it('should load samples from table', async () => {
    const dataset = await getDataset();
    const resource = dataset.getResource('0');
    const sampleSize = 5;
    const samples = await resource.getSample({size: 5 });
    expect(samples).toHaveLength(sampleSize);
    expect(samples[0].toJSON()).toMatchObject({ Player: 'HANK_AARON' });
  });

  it('should load samples from text', async () => {
    const dataset = await getDataset('30_personae_dataset');
    const resource = dataset.getResource('0');
    const sampleSize = 5;
    const samples = await resource.getSample({size: 5 });
    expect(samples).toHaveLength(sampleSize);
  });

});
