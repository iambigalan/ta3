import 'jest';
import { getDataSourceConfig } from '../config';
import { DataSource } from '../DataSource';
import { Dataset } from '../Dataset';
import { Problem } from '../Problem';

function init() {
  const store = new DataSource(getDataSourceConfig());
  return store;
}

describe("Read datasets' info", () => {
  it('should find all datasets', async () => {
    const store = init();
    const datasetIds = [
      '185_baseball_dataset',
      '185_bl_dataset_TEST',
      '185_bl_dataset_TEST',
      '185_bl_dataset_TRAIN',
      '30_personae_dataset',
      '30_pe_dataset_TEST',
      '30_pe_dataset_TEST',
      '30_pe_dataset_TRAIN',
      'LL1_736_stock_market_dataset',
      'LL1_736_stock_dataset_TEST',
      'LL1_736_stock_dataset_TEST',
      'LL1_736_stock_dataset_TRAIN',
    ].sort();
    const datasetList: Dataset[] = await store.getDatasets();
    const datasetList2: Dataset[] = await store.getDatasets();
    expect(datasetList).toBeDefined();
    expect(datasetList).toHaveLength(12);
    expect(datasetList.map(d => d.datasetID).sort()).toEqual(datasetIds);
    expect(datasetList2.map(d => d.datasetID).sort()).toEqual(datasetIds);
  });

  it('should return a dataset by id', async () => {
    const store = init();
    const datasetId = '185_baseball_dataset';
    const datasetInfo: Dataset = await store.getDatasetById(datasetId);
    expect(datasetInfo.about.datasetName).toBe('baseball');
    expect(datasetInfo).toMatchSnapshot('baseball_dataset');
  });

  it('should load the problems', async () => {
    const store = init();
    const datasetId = '185_baseball_dataset';
    const problems: Problem[] = await store.getProblemsForDatasetId(datasetId);
    expect(problems).toHaveLength(1);
    const firstProblemJSON = problems[0].toJSON();
    expect(firstProblemJSON).toMatchObject(
      {
        about: { problemID: '185_baseball_problem' },
      },
    );
    expect(firstProblemJSON).toMatchSnapshot('baseball_dataset_problems');
  });
});
