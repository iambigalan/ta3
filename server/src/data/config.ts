import * as fs from 'fs';
import * as path from 'path';
import { DataSourceConfig, DataSource } from './DataSource';
import { D3M_DATA_PATH, D3MPROBLEMPATH, CONFIG_JSON_PATH, OUTPUT_DIR } from '../env';

export const getDataSourceConfig = (): DataSourceConfig => {
  return {
    datasetsPath: path.resolve(D3M_DATA_PATH),
    problemPath: D3MPROBLEMPATH,
  };
};

/*
 * A singleton DataSource instance to avoid re-scaning the data directory on
 * every request.
 */
const DATA_SOURCE = new DataSource(getDataSourceConfig());

/**
 * Returns a data source instance over the current data source configuration.
 */
export const getDataSource = (): DataSource => {
  return DATA_SOURCE;
};

export interface ConfigJson {
  temp_storage_root: string;
  pipeline_logs_root: string;
  executables_root: string;
  dataset_schema?: string;
  problem_schema?: string;
  training_data_root?: string;
  problem_root?: string;
  user_problems_root?: string;
  timeout?: string; // "10"
  cpus?: string; // "2"
  ram?: string; // "5Gi"
}

/**
 * Reads the problem_config Json file and returns its parsed Json object.
 */
export const getConfigJson = (): ConfigJson => {
  if (CONFIG_JSON_PATH && fs.existsSync(CONFIG_JSON_PATH)) {
    const file = fs.readFileSync(CONFIG_JSON_PATH, 'utf8');
    return JSON.parse(file) as ConfigJson;
  }
  else {
    const config = {
      temp_storage_root: `${OUTPUT_DIR}/storage`,
      pipeline_logs_root: `${OUTPUT_DIR}/logs`,
      executables_root: `${OUTPUT_DIR}/executables`,
      user_problems_root: `${OUTPUT_DIR}/user_problem_root`,
      // dataset_schema: `${D3M_DATA_PATH}/185_baseball/185_baseball_dataset/datasetDoc.json`,
      // problem_schema: `${D3M_DATA_PATH}/185_baseball/185_baseball_problem/problemDoc.json`,
      // training_data_root: `${D3M_DATA_PATH}/185_baseball/185_baseball_dataset`,
      // problem_root: `${D3M_DATA_PATH}/185_baseball/185_baseball_problem`,
      timeout: '10',
      cpus: '2',
      ram: '5Gi',
    };
    console.log(
      'Environment variable CONFIG_JSON_PATH is not set. Using generated default values.\n',
      config,
    );
    return config;
  }
};

export function getProblemsDirectory(): string {
  return path.join(OUTPUT_DIR, 'problems');
}

export function getResourcesPath(): string {
  return getConfigJson().training_data_root;
}
