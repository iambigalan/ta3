import * as path from 'path';
import * as fs from 'fs';
import * as d3m from 'd3m/dist';
import { Dataset } from './Dataset';
import { formatAsFileURI, isDirectory } from '../api/utils';
import { DATASETS_INFO, DATAMART_MARK } from '../env';
import { Problem } from './Problem';
import { fileExists } from '../utils';

export interface DataSourceConfig {
  datasetsPath: string;
  problemPath: string;
}

export class DataSource {

  private datasets: Dataset[];
  private problems: Problem[];
  private datasetsIdx: { [datasetID: string]: Dataset };

  constructor(config: DataSourceConfig) {
    if (config.problemPath) {
      if (fileExists(config.problemPath)) {
        const problem = Problem.readFromProblemDocFile(config.problemPath);
        this.datasets = this
          .findAllDatasets(config.datasetsPath)
          .filter(d => d.about.datasetID === problem.getInputDatasetID());
        this.problems = [ problem ];
      } else {
        console.error(`File ${config.problemPath} does not exist. `);
      }
    } else {
      this.datasets = this.findAllDatasets(config.datasetsPath);
      this.problems = this.findAllProblems(config.datasetsPath);
    }
    this.datasetsIdx = {};
    this.datasets.forEach((d: Dataset) => this.datasetsIdx[d.datasetID] = d);
    console.log(`Found ${this.datasets.length} dataset(s) and ` +
      `${this.problems.length} problem(s) at ${config.datasetsPath}`);
  }

  /**
   * Recursively walks the given directory path searching for datasetDoc.json
   * files and returns an array of Dataset objects.
   *
   * @param rootPath - a path to the directory to be searched.
   */
  private findAllDatasets(rootPath: string): Dataset[] {
    const allFiles = this.findAllFilesByName(rootPath, 'datasetDoc.json');
    return allFiles.map(f => Dataset.readFromDatasetDocFile(f));
  }

  /**
   * Recursively walks the given directory path searching for problemDoc.json
   * files and returns an array of Problem objects.
   *
   * @param rootPath - a path to the directory to be searched.
   */
  private findAllProblems(rootPath: string): Problem[] {
    const allFiles = this.findAllFilesByName(rootPath, 'problemDoc.json');
    return allFiles.map(f => Problem.readFromProblemDocFile(f));
  }

  /**
   * Recursively walks the given directory path searching for files
   * with the given name and returns an array of string file paths.
   *
   * @param rootPath - a path to the directory to be searched.
   * @param filename - the name of the files
   */
  private findAllFilesByName(rootPath: string, filename: string): string[] {
    // for all paths that are a directory, searches for files recursively
    const allFiles: string[] = [];
    if (isDirectory(rootPath)) {
      const childPaths = fs.readdirSync(rootPath);
      if (childPaths && childPaths.length > 0) {
        for (let i = 0; i < childPaths.length; i++) {
          const childPath = path.join(rootPath, childPaths[i]);
          if (isDirectory(childPath)) {
            const files = this.findAllFilesByName(childPath, filename);
            if (files && files.length > 0) {
              files.forEach(f => allFiles.push(f));
            }
          }
        }
      }
    }
    const filePath: string = path.join(rootPath, filename);
    if (fs.existsSync(filePath)) {
      allFiles.push(filePath);
    }
    return allFiles;
  }

  /**
   * Produces an path for the given datasetID (relative to the given base path
   * or to the default system configured data path).
   */
  getDatasetDocPath(datasetID: string): string {
    const dataset =  this.getDatasetById(datasetID);
    return dataset.datasetDocPath;
  }

  /**
   * Produces an file URI (which starts with file://) for the given datasetID.
   */
  getDatasetDocURI(datasetID: string): string {
    const datasetDocPath = this.getDatasetDocPath(datasetID);
    return formatAsFileURI(datasetDocPath);
  }

  getDatasets(): Dataset[] {
    return this.datasets;
  }

  getProblems(): Problem[] {
    return this.problems;
  }

  getDatasetById(id: string): Dataset {
    if (this.datasetsIdx[id] === undefined) {
      const parts = id.split(DATAMART_MARK);
      if (parts.length > -1) {
        const datasetDocPath: string = path.join(DATASETS_INFO, parts[0] , 'augmented', parts[1], 'datasetDoc.json');
        console.log('Loading from', datasetDocPath);
        if (fs.existsSync(datasetDocPath)) {
          return Dataset.readFromDatasetDocFile(datasetDocPath);
        }
      }
    }
    return this.datasetsIdx[id];
  }

  async getProblemsForDatasetId(datasetId: string): Promise<Problem[]> {
    const dataset = this.getDatasetById(datasetId);

    let problemFiles = [];
    if (dataset.problemPath) {
      problemFiles = [dataset.problemPath];
    } else {
      problemFiles = fs
        .readdirSync(dataset.path)
        .map(p => path.join(dataset.path, p, 'problemDoc.json'))
        .filter(p => fileExists(p));
    }

    const problems = await Promise.all(
      problemFiles.map(
        p =>
          new Promise<d3m.Problem>((resolve, reject) =>
            fs.readFile(p, 'utf-8', (err, f) =>
              err ? reject(err) : resolve(JSON.parse(f)),
            ),
          ),
      ),
    );
    return problems.map(p => new Problem(p));
  }

}
