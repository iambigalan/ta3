import { Resource } from '../Resource';
import { DataPoint } from '../DataPoint';

export abstract class DataLoader {
  abstract stream();
  abstract writeData(data: DataPoint[]);
}
