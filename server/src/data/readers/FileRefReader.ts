import { DataLoader } from './DataLoader';
import { DataPoint } from '../DataPoint';
import * as fs from 'fs';
import * as path from 'path';
import { Column } from '../Column';
import * as d3m from 'd3m/dist';

export class FileRefReader extends DataLoader {
  path: string;
  basePath: string;
  columns: Column[];
  constructor(config: { path: string, basePath: string }) {
    super();
    this.path = config.path;
    this.basePath = config.basePath,
    this.columns = [
        new Column({
            colIndex: 0,
            colName: 'name',
            role: [],
            colType: d3m.ColumnType.categorical,
            colDescription: 'File Name',
        }),
        new Column({
            colIndex: 1,
            colName: 'src',
            role: [],
            colType: d3m.ColumnType.string,
            colDescription: 'file relative path',
        }),
    ];
  }
  writeData(data: DataPoint[]) {
    throw 'Not Implemented';
  }
  *stream() {
    const files = fs.readdirSync(this.path);
    for (const file of files) {
        const src = `${this.basePath}/${file}`;
        yield new DataPoint([file, src], this.columns);
    }
  }
}
