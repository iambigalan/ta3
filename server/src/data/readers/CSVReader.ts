import { DataLoader } from './DataLoader';
import { DataPoint, DataPointValue } from '../DataPoint';
import * as csvParse from 'csv-parse';
import * as fs from 'fs';
import { Transform } from 'stream';
import * as d3m from 'd3m/dist';
import { Column } from '../Column';
import * as stringify from 'csv-stringify';
import { mkdirIfAbsent } from '../../utils';
class RowParser extends Transform {
  loader: CSVReader;
  readHeader: boolean;
  constructor(loader: CSVReader) {
    super({ objectMode: true });
    this.loader = loader;
  }
  async _transform(data, encoding, callback) {
    if (!this.readHeader) {
      this.readHeader = true;
    } else {
      const parsedValues = this.loader.columns.map(column => {
        const columnType = column.config.colType;
        let value: DataPointValue = data[column.config.colIndex];

        if (d3m.NumericType[columnType]) {
          value = +value;
        }
        if (columnType === d3m.ColumnType.realVector) {
          value = (value as string).split(',').map(v => +v);
        }
        return value;
      });
      this.push(new DataPoint(parsedValues, this.loader.columns));
    }
    callback();
  }
}

export class CSVReader extends DataLoader {
  path: string;
  columns: Column[];
  constructor(config: { path: string; columns: Column[] }) {
    super();
    this.path = config.path;
    this.columns = config.columns;
  }
  writeData(data: DataPoint[]) {
    mkdirIfAbsent(this.path);
    const writer = fs.createWriteStream(this.path);
    stringify(data.map(v => v.values), {
      header: true,
      columns: this.columns.map(c => c.colName),
    }).pipe(writer);
  }
  stream() {
    const parser = csvParse({
      auto_parse: false,
    });
    const self = this;
    return fs
      .createReadStream(this.path)
      .pipe(parser)
      .pipe(new RowParser(this));
  }
}
