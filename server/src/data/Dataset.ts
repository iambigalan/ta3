import * as fs from 'fs';
import * as path from 'path';
import * as d3m from 'd3m/dist';
import { Resource } from './Resource';
import { cloneDeep } from 'lodash';
import { DataPoint } from './DataPoint';
import { mkdirIfAbsent } from '../utils';
import { OUTPUT_DIR } from '../env';
import { formatAsFileURI } from '../api/utils';
import { readLearningDataTable } from 'src/rest/loader';

const SPLITS_DATASETS_PATH = path.join(OUTPUT_DIR, 'dataset_splits');
export const TEST_SPLIT_PROPORTION = 0.25;

export class Dataset {

  datasetID: string;
  path: string;
  datasetDocPath: string;
  datasetName: string;
  problemPath: string;
  private _config: d3m.Dataset;
  private _dataResources: Resource[];

  constructor(config: { id: string; path: string; problemPath?: string }) {
    this.datasetID = config.id;
    this.datasetDocPath = config.path;
    this.path = path.join(config.path, '..', '..');
    this.problemPath = config.problemPath;
  }

  static readFromDatasetDocFile(filePath: string) {
    const datasetDoc = JSON.parse(
      fs.readFileSync(filePath, 'utf8'),
    ) as d3m.Dataset;
    return new Dataset({id: datasetDoc.about.datasetID, path: filePath});
  }

  get about() {
    return this.config.about;
  }

  get qualities() {
    return this.config.qualities;
  }

  get dataResources() {
    if (!this._dataResources) {
      this._dataResources = this.config.dataResources.map(r => new Resource(r, this));
    }
    return this._dataResources;
  }

  get config() {
    if (!this._config) {
      this._config = JSON.parse(
        fs.readFileSync(this.datasetDocPath, 'utf8'),
      ) as d3m.Dataset;
    }
    return this._config;
  }

  set config(value) {
    this._config = value;
  }

  async getTrainingDocURI(target, taskType) {
    const trainingPath = this.getTrainingPath();
    const datasetDoc = path.join(trainingPath, 'datasetDoc.json');
    if (fs.existsSync(datasetDoc)) {
      return datasetDoc;
    }
    const {training} = await this.generateSplits(target, taskType);
    return formatAsFileURI(training);
  }

  async getTestingDocURI(target, taskType) {
    const testingPath = this.getTestingPath();
    const datasetDoc = path.join(testingPath, 'datasetDoc.json');
    if (fs.existsSync(datasetDoc)) {
      return datasetDoc;
    }
    const {testing} = await this.generateSplits(target, taskType);
    return formatAsFileURI(testing);
  }

  getTrainingPath() {
    return path.join(SPLITS_DATASETS_PATH, this.datasetID, 'TRAIN');
  }

  getTestingPath() {
    return path.join(SPLITS_DATASETS_PATH, this.datasetID, 'TEST');
  }

  getStratifiedSamplingData(data: DataPoint[], indexTarget: number) {
    const trainingData = [];
    const testingData = [];
    const _ = require('lodash');
    const groups = {};
    data.forEach( (d, idx) => {
      const label = d.values[indexTarget].toString();
      (groups[label] = groups[label] || []).push(idx);
    });
    Object.keys(groups).forEach(label => {
      const shuffledGroupIndex: number[] = _.shuffle(groups[label]);
      const testingCountGroup = Math.ceil(shuffledGroupIndex.length * TEST_SPLIT_PROPORTION);
      for (let i = 0; i < testingCountGroup; i++) {
        testingData.push(data[shuffledGroupIndex[i]]);
      }
      for (let i = testingCountGroup; i < shuffledGroupIndex.length; i++) {
        trainingData.push(data[shuffledGroupIndex[i]]);
      }
    });
    return {trainingData, testingData};
  }

  async generateSplits(target: string, taskType: string) {
    // This only works for datasets with one table resource.
    if (this.dataResources.length > 1 || !(this.dataResources[0].config.resType === d3m.ResourceType.table)) {
      return {
        training: this.datasetDocPath,
        testing: this.datasetDocPath,
      };
    }

    // Load original data
    const mainResource = this.getMainResource();
    const data = [...await mainResource.loadData()];

    let testingData = [];
    let trainingData = [];
    if (taskType.toLowerCase() === 'classification') {
      console.warn('Stratified Sampling...');
      // Stratified sampling
      const targetIndex = data[0].columns.findIndex(c => c.colName === target);
      const sample = this.getStratifiedSamplingData(data, targetIndex);
      testingData = sample.testingData;
      trainingData = sample.trainingData;

    } else {
      console.warn('Random Sampling...');
      // Randomly split data into train/test using SPLIT_RATIO
      const testingCount = Math.ceil(data.length * TEST_SPLIT_PROPORTION);
      while (testingData.length < testingCount) {
        const randomIndex = Math.floor(Math.random() * data.length);
        const selected = data.splice(randomIndex, 1)[0];
        testingData.push(selected);
      }
      trainingData = data;
    }
    console.log('Splitting data:', testingData.length, 'for testing and', trainingData.length, 'for training.');

    // Copy training data datasetDoc.json to its path
    const trainingPath = this.getTrainingPath();
    const trainingDataset = this.copyTo(trainingPath);
    trainingDataset
      .clearResources()
      .addResourceFromDataPoints(trainingData, mainResource);
    trainingDataset.save();

    // Copy test data datasetDoc.json to its path
    const testingPath = this.getTestingPath();
    const testDataset = this.copyTo(testingPath);
    testDataset
      .clearResources()
      .addResourceFromDataPoints(testingData, mainResource);
    testDataset.save();

    return { training: trainingDataset.datasetDocPath, testing: testDataset.datasetDocPath };
  }

  copyTo(basePath: string) {
    const dataset = new Dataset({
      id: this.datasetID,
      path: path.join(basePath, this.datasetID, this.datasetID + '_dataset', 'datasetDoc.json'),
      problemPath: this.problemPath,
    });

    dataset.config = cloneDeep(this.config);
    return dataset;
  }

  clearResources() {
    this._dataResources = undefined;
    this.config.dataResources = [];
    return this;
  }

  addResource(resource: Resource) {
    this._dataResources = this._dataResources || [];
    this._dataResources.push(resource);
    this.config.dataResources.push(resource.toJSON());
  }

  addResourceFromDataPoints(dataPoints: DataPoint[], original: Resource) {
    const resource = Resource.fromDataPoints(dataPoints, original, this);
    this.addResource(resource);
    return this;
  }

  setResources(resources: Resource[]) {
    this.config.dataResources = resources.map(r => r.toJSON());
    this._dataResources = resources;
  }

  getResource(resourceId: string) {
    return this.dataResources.find(r => r.resourceID === resourceId);
  }

  getMainResource() {
    return this.getResource('learningData') || this.dataResources.find(resource =>
      resource.getPath().endsWith('learningData.csv'),
    );
  }

  save() {
    mkdirIfAbsent(path.dirname(this.datasetDocPath));
    fs.writeFileSync(this.datasetDocPath, JSON.stringify(this.config, undefined, 4));
    for (const resource of this.dataResources) {
      resource.saveData();
    }
  }

  toJSON(
    template: { about: boolean; dataResources: boolean; problems: boolean, qualities: boolean } = {
      about: true,
      qualities: true,
      dataResources: true,
      problems: false,
    },
  ) {
    const result = {};
    for (const key in template) {
      if (template[key]) {
        result[key] = this[key];
      }
    }
    return result;
  }
}
