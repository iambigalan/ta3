import { clone, cloneDeep, each } from 'lodash';
import { histogram } from 'd3-array';
import * as d3m from 'd3m/dist';
import { DataPoint, DataPointValue } from './DataPoint';
import * as path from 'path';
import { Dataset } from './Dataset';
import { Column } from './Column';
import { DataLoader } from './readers/DataLoader';
import { CSVReader } from './readers/CSVReader';
import { TXTReader } from './readers/TXTReader';
import { FileRefReader } from './readers/FileRefReader';
import { ResourceType } from 'd3m/dist';
import { Dictionary } from './preprocessing/TextProcessing';

export enum SampleMethod {
  TOP,
  RANDOM,
  BOTTOM,
}

const TEXT_NUM_TOKENS = 5;
const DEFAULT_NUM_BINS = 20;
const DEFAULT_TICK_PRECISION = 6;
const numericTypes = new Set(['integer', 'real']);

/**
 * Removes small floating point error and keep the numeric value at a fixed precision.
 * This is to ensure that chart ticks are nicely displayed.
 */
const removeSmallError = (v: number): number => {
  return +v.toFixed(DEFAULT_TICK_PRECISION);
};

export type PrimitiveValue = string | number | Date;
export interface ColumnProfile {
  column: Column;
  values: {
    name: PrimitiveValue;
    count: number;
  }[];
  valuesIndex?: { [name: string]: number };
  binSize: number;
  isText?: boolean;
  stats: {
    count: number;
    distinctValueCount?: number;
    sum?: number;
    min?: number;
    max?: number;
    mean?: number;
    stdDev?: number;
  };
}

export class Resource {
  resourceID: string;
  config: d3m.DataResource;
  private _reader: DataLoader;
  dataset: Dataset;
  data: DataPoint[];

  constructor(config: d3m.DataResource, dataset: Dataset) {
    this.resourceID = config.resID;
    this.config = config;
    this.dataset = dataset;
  }

  static fromDataPoints(data: DataPoint[], original: Resource, dataset: Dataset) {
    const dataResource = cloneDeep(original.config);
    dataResource.columns = data[0].columns;
    const resource =  new Resource(dataResource, dataset);
    resource.data = data;
    return resource;
  }

  static getStandAloneResource(
    dataPath: string,
    {columns, format, isCollection, resourceType}:
    {columns?: d3m.Column[], isCollection?: boolean, format?: string, resourceType?: d3m.ResourceType} = {}) {
    if (format === undefined) {
      if (dataPath.endsWith('.csv')) {
        format = 'text/csv';
        isCollection = false;
        resourceType = d3m.ResourceType.table;
        if (!columns) {
          throw 'Columns are required for CSV';
        }
      } else {
        throw 'Could not identify the format of the resource';
      }
    }
    return new Resource({
      columns: columns,
      isCollection: isCollection,
      resFormat: [format],
      resID: 'STANDALONE',
      resPath: dataPath,
      resType: resourceType,
    }, undefined);
  }

  saveData() {
    const reader = this.getReader();
    reader.writeData(this.data);
  }

  streamData(): DataPoint[] {
    const reader = this.getReader();
    return reader.stream();
  }

  async getSample(params: { size?: number; start?: number, method?: SampleMethod }) {
    const { size, method, start } = params;
    const samples: DataPoint[] = [];
    if (start) {
      const data = [...await this.loadData()];
      return data.slice(start, size + start);
    } else if (!method) {
      for await (const d of this.streamData()) {
        samples.push(d);
        if (size && size <= samples.length) {
          break;
        }
      }
    } else if (method === SampleMethod.RANDOM) {
      const data = [...await this.loadData()];
      if (data.length < size) {
        return data;
      }
      for (let i = 0; i <= size; i++) {
        const randomIndex = Math.floor(Math.random() * data.length);
        const selected = data.splice(randomIndex, 1)[0];
        samples.push(selected);
      }
    }
    return samples;
  }

  async loadData() {
    this.data = [];
    for await (const d of this.streamData()) {
      this.data.push(d);
    }
    return this.data;
  }

  /**
   * Returns the base path of this resource. The returned path is resolved
   * based on the datasetDoc.json path and `resPath` path value from
   * datasetDoc.json.
   */
  getPath() {
    if (!this.dataset) {
      return this.config.resPath;
    }
    return path.join(this.dataset.datasetDocPath, '..', this.config.resPath);
  }

  async getSize() {
    const data = await this.loadData();
    return data.length;
  }

  async summarizeTextData(texts: DataPoint[]): Promise<[number, {name: PrimitiveValue; count: number; }[]]>  {
    const dictionary = new Dictionary();
    for await (const d of texts) {
      dictionary.addDocument(d.getValue('content').toString());
    }
    const topTerms = dictionary.getTopTokens();
    return [dictionary.numDocuments, topTerms.map(t => ({
      name: t.token,
      count: t.docFrequency,
    }))];
  }

  async getColumnsProfiles(): Promise<ColumnProfile[]> {
    const numBins = DEFAULT_NUM_BINS;
    if (!this.config.columns) {
      if (this.config.resType === ResourceType.text) {
        const data = await this.getSample({size: 1000});
        const [numDocuments, summary] = await this.summarizeTextData(data);
        const ColumnConfig: d3m.Column = {
          colIndex: 0,
          colName: 'Text',
          colDescription: '',
          colType: d3m.ColumnType.string,
          role: [d3m.ColumnRole.attribute],
        };
        const columnProfile: ColumnProfile = {
          column: new Column(ColumnConfig),
          values: summary,
          binSize: -1,
          stats: {
            count: numDocuments,
          },
        };
        columnProfile.values = summary;
        return [columnProfile];
      }
    }
    const columnsStats: { [name: string]: ColumnProfile } = this.config.columns.reduce((prev, c) => {
      prev[c.colIndex] = {
        column: c,
        values: [],
        valuesIndex: {},
        stats: {
          count: 0,
          distinctValueCount: 0,
          sum: 0,
          mean: 0,
          stdDev: 0,
          min: Infinity,
          max: -Infinity,
        },
      };
      return prev;
    }, {});

    const dictionaries: {[name: string]: Dictionary} = {};
    // Compute initial stats
    for await (const record of this.streamData()) {
      record.columns.forEach((column, idx) => {
        let value = record.values[idx];
        const colIndex = column.config.colIndex;
        const columnInfo = columnsStats[colIndex];
        const stats = columnInfo.stats;
        if (numericTypes.has(column.config.colType)) {
          value = +value;
          if (!isNaN(value)) {
            stats.min = Math.min(stats.min, value);
            stats.max = Math.max(stats.max, value);
            stats.sum += value;
          }
        } else if (column.config.colType === d3m.ColumnType.dateTime) {
          value = new Date(value as string).getTime();
          if (!isNaN(value)) {
            stats.min = Math.min(stats.min, value);
            stats.max = Math.max(stats.max, value);
            stats.sum += value;
          }
        } else if (column.config.colType === d3m.ColumnType.string) {
          let dictionary: Dictionary;
          if (dictionaries[colIndex]) {
            dictionary = dictionaries[colIndex];
          } else {
            dictionary = new Dictionary();
            dictionaries[colIndex] = dictionary;
          }
          dictionary.addDocument(value.toString());
        } else {
          let info = columnInfo.valuesIndex[value.toString()] || 0;
          info += 1;
          columnInfo.valuesIndex[value.toString()] = info;
        }
        stats.count += 1;
      });
    }

    each(columnsStats, column => {
      if (numericTypes.has(column.column.colType) || column.column.colType === d3m.ColumnType.dateTime) {
        const stats = column.stats;
        // Use d3 histogram to generate human-readable bins.
        const bins = histogram()
          .domain([stats.min, stats.max])
          .thresholds(numBins)([]); // pass an empty array just to generate the bin coordinates
        if (!bins.length) {
          console.warn('empty numeric column');
          column.values = [];
          column.binSize = 0;
          stats.mean = NaN;
          return;
        }

        // Avoid using the first or last bin to estimate bin size.
        column.binSize = bins.length >= 3 ? bins[2].x0 - bins[1].x0 : bins[0].x1 - bins[0].x0;
        column.binSize = removeSmallError(column.binSize);

        // The first and last bin may have uneven size.
        // We must make them even otherwise recharts bars may not work properly (bar width extremely thin).
        bins[0].x0 = bins[0].x1 - column.binSize;
        bins[bins.length - 1].x1 = bins[bins.length - 1].x0 + column.binSize;
        column.values = bins.map(b => ({ name: removeSmallError(b.x0), count: 0 }));
        stats.mean = stats.sum / stats.count;
      }
    });

    for await (const record of this.streamData()) {
      record.columns.forEach((column, idx) => {
        const value = column.config.colType === d3m.ColumnType.dateTime ?
          new Date(record.values[idx].toString()).getTime() : record.values[idx];
        const colIndex = column.config.colIndex;
        const stats = columnsStats[colIndex];
        stats.stats.stdDev += Math.pow(+value - stats.stats.mean, 2);
        if (numericTypes.has(column.config.colType) || column.config.colType === d3m.ColumnType.dateTime) {
          const binIndex = this.findBinIndex(value, stats.values);
          if (binIndex >= 0) {
            stats.values[binIndex].count++;
          }
        }
      });
    }
    return Object.keys(columnsStats).map(k => {
      const column = columnsStats[k];
      if (numericTypes.has(column.column.colType) || column.column.colType === d3m.ColumnType.dateTime) {
        column.stats.stdDev = Math.sqrt(column.stats.stdDev / column.stats.count);
      } else if (dictionaries[k]) {
        let values = [];
        if (dictionaries[k].maxDocLength > TEXT_NUM_TOKENS) {
          values = dictionaries[k].getTopTokens();
          column.isText = true;
        } else {
          values = dictionaries[k].getTokenList();
        }

        column.values = values.map(t => ({
          name: t.token,
          count: t.docFrequency,
        }));
        column.stats.distinctValueCount = dictionaries[k].numDocuments;
      } else {
        column.values = Object.keys(column.valuesIndex).map(ck => ({
          name: ck,
          count: column.valuesIndex[ck],
        }));
        column.stats.distinctValueCount = column.values.length;
      }
      delete column.valuesIndex;
      return column;
    });
  }

  /**
   * Executes a binary search to find index of the bin which the given
   * value belongs.
   */
  findBinIndex(
    value: DataPointValue,
    bins: {name: PrimitiveValue, count: number}[],
  ): number {
    let li = 0;
    let ri = bins.length - 1;
    while (li <= ri) {
      const mi = (li + ri) >> 1;
      if (bins[mi].name <= value) {
        li = mi + 1;
      } else {
        ri = mi - 1;
      }
    }
    return ri;
  }

  getColumnByName(name: string) {
    if (!this.config.columns) {
      return undefined;
    }
    const columnIndex = this.config.columns.findIndex(c => c.colName === name);
    if (columnIndex === -1) {
      return undefined;
    }
    return { index: columnIndex, column: this.config.columns[columnIndex]};
  }

  getReader() {
    if (!this._reader) {
      const formats = new Set(this.config.resFormat);
      if (formats.has('text/csv') && this.config.resType === 'table') {
        this._reader = new CSVReader({
          path: this.getPath(),
          columns: this.config.columns.map(
            c =>
              new Column({
                colIndex: +c.colIndex,
                colName: c.colName,
                role: c.role,
                colType: d3m.ColumnType[c.colType],
                colDescription: c.colDescription,
              }),
          ),
        });
      } else if (formats.has('text/plain')) {
        this._reader = new TXTReader({
          path: this.getPath(),
        });
      } else {
        const basePath = `${this.dataset.datasetID}/resources/${this.config.resID}/file`;
        this._reader = new FileRefReader({
          path: this.getPath(),
          basePath,
        });
      }
    }
    return this._reader;
  }

  toJSON() {
    return clone(this.config);
  }

}
