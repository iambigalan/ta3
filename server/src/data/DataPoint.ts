import { Column } from './Column';

export type DataPointValue = string | number | boolean | number[];

export class DataPoint {
  values: Array<DataPointValue>;
  columns: Column[];

  constructor(values: Array<DataPointValue>, columns: Column[]) {
    this.columns = columns;
    this.values = values;
  }

  static getColumnValues(dataPoins: DataPoint[], colIdx: number): DataPointValue[] {
    return dataPoins.map(d => d.values[colIdx]);
  }

  getValue(column: string) {
    const columnIdx = this.columns.findIndex(c => c.colName === column);
    return this.values[columnIdx];
  }

  copy() {
    return new DataPoint([...this.values], this.columns);
  }

  toJSON(): { [name: string]: DataPointValue } {
    return this.columns.reduce((result, column, idx) => {
      const value = this.values[idx];
      result[column.config.colName] = value;
      return result;
    }, {});
  }
}
