import * as d3m from 'd3m/dist';
import { ColumnType, ColumnRole } from 'd3m/dist';

export function d3mIndexColumn(idx: number = 0): d3m.Column {
  return {
    colIndex: idx,
    colName: 'd3mIndex',
    colType: ColumnType.integer,
    role: [ColumnRole.index],
  };
}

export class Column implements d3m.Column {
  colIndex: number;
  colName: string;
  colDescription?: string;
  colType: d3m.ColumnType;
  role: d3m.ColumnRole[];
  refersTo?: d3m.DataResourceLink;
  config: d3m.Column;

  constructor(config: d3m.Column) {
    this.config = config;
    Object.assign(this, config);
  }

  toJSON() {
    return this.config;
  }
}
