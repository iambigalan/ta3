import * as fs from 'fs';
import * as path from 'path';
import * as stringify from 'csv-stringify/lib/sync';
import { OUTPUT_DIR } from './env';
import { mkdirIfAbsent } from './utils';

const LOG_PATH = path.join(OUTPUT_DIR, './usage-logs.log');

enum UsageLogType {
  TA23API = 'TA23API',
  SYSTEM = 'SYSTEM',
  DATAMART = 'DATAMART',
}

enum ActivityType {
  DATA_PREPARATION = 'DATA_PREPARATION',
  PROBLEM_DEFINITION = 'PROBLEM_DEFINITION',
  MODEL_SELECTION = 'MODEL_SELECTION',
}

enum ActivityTypeL2 {
  LAUNCH_TA3 = 'LAUNCH_TA3',
  DATA_OPEN = 'DATA_OPEN',
  DATA_EXPLORE = 'DATA_EXPLORE',
  DATA_AUGMENT = 'DATA_AUGMENT',
  DATA_DOWNLOAD = 'DATA_DOWNLOAD',
  DATA_SEARCH = 'DATA_SEARCH',
  DATA_TRANSFORM = 'DATA_TRANSFORM',
  PROBLEM_SPECIFICATION = 'PROBLEM_SPECIFICATION',
  MODEL_SEARCH = 'MODEL_SEARCH',
  MODEL_SUMMARIZATION = 'MODEL_SUMMARIZATION',
  MODEL_COMPARISON = 'MODEL_COMPARISON',
  MODEL_EXPLANATION = 'MODEL_EXPLANATION',
  MODEL_EXPORT = 'MODEL_EXPORT',
}

export enum UserEvent {
  // Data Preparation
  LIST_DATASETS = 'LIST_DATASETS',
  LIST_PROBLEMS = 'LIST_PROBLEMS',
  VIEW_RESOURCE_STATS = 'VIEW_RESOURCE_STATS',
  GET_DATA_DESCRIPTION = 'GET_DATA_DESCRIPTION',
  DATAMART_SEARCH = 'DATAMART_SEARCH',
  DATAMART_AUGMENT = 'DATAMART_AUGMENT',
  // Problem Definition
  SELECT_TARGET = 'SELECT_TARGET',
  SELECT_PROBLEM_TYPE = 'SELECT_PROBLEM_TYPE',
  SELECT_PROBLEM_SUBTYPE = 'SELECT_PROBLEM_SUBTYPE',
  // Model Selection
  VIEW_CONFUSION_MATRIX = 'VIEW_CONFUSION_MATRIX',
  VIEW_CONFUSION_SCATTER_PLOT = 'VIEW_CONFUSION_SCATTER_PLOT',
  VIEW_RULE_MATRIX = 'VIEW_RULE_MATRIX',
  VIEW_PARTIAL_DEPENDENCE_PLOTS = 'VIEW_PARTIAL_DEPENDENCE_PLOTS',
  // Export
  RANK_SOLUTION = 'RANK_SOLUTION',
}

const eventMapping: {
  [event: string]: {
    type: string,
    L1: string,
    L2: string,
  },
} = {};

// Data Preparation
eventMapping[UserEvent.LIST_DATASETS] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.DATA_PREPARATION,
  L2: ActivityTypeL2.DATA_OPEN,
};
eventMapping[UserEvent.LIST_PROBLEMS] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.DATA_PREPARATION,
  L2: ActivityTypeL2.DATA_OPEN,
};
eventMapping[UserEvent.GET_DATA_DESCRIPTION] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.DATA_PREPARATION,
  L2: ActivityTypeL2.DATA_OPEN,
};
eventMapping[UserEvent.VIEW_RESOURCE_STATS] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.DATA_PREPARATION,
  L2: ActivityTypeL2.DATA_EXPLORE,
};
eventMapping[UserEvent.DATAMART_SEARCH] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.DATA_PREPARATION,
  L2: ActivityTypeL2.DATA_SEARCH,
};
eventMapping[UserEvent.DATAMART_AUGMENT] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.DATA_PREPARATION,
  L2: ActivityTypeL2.DATA_AUGMENT,
};
// Problem Definition
eventMapping[UserEvent.SELECT_TARGET] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.PROBLEM_DEFINITION,
  L2: ActivityTypeL2.PROBLEM_SPECIFICATION,
};
eventMapping[UserEvent.SELECT_PROBLEM_TYPE] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.PROBLEM_DEFINITION,
  L2: ActivityTypeL2.PROBLEM_SPECIFICATION,
};
eventMapping[UserEvent.SELECT_PROBLEM_SUBTYPE] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.PROBLEM_DEFINITION,
  L2: ActivityTypeL2.PROBLEM_SPECIFICATION,
};
// Model Selection
eventMapping[UserEvent.VIEW_CONFUSION_MATRIX] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping[UserEvent.VIEW_CONFUSION_SCATTER_PLOT] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping[UserEvent.VIEW_PARTIAL_DEPENDENCE_PLOTS] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping[UserEvent.RANK_SOLUTION] = {
  type: UsageLogType.SYSTEM,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPORT,
};

// System Activity
eventMapping['SearchSolutions'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_SEARCH,
};
eventMapping['GetSearchSolutionsResults'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_SEARCH,
};
eventMapping['DescribeSolution'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_SUMMARIZATION,
};
eventMapping['ScoreSolution'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_SUMMARIZATION,
};
eventMapping['GetScoreSolutionResults'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_SUMMARIZATION,
};
eventMapping['FitSolution'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping['GetFitSolutionResults'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping['ProduceSolution'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping['GetProduceSolutionResults'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPLANATION,
};
eventMapping['SolutionExport'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.MODEL_EXPORT,
};
eventMapping['Hello'] = {
  type: UsageLogType.TA23API,
  L1: ActivityType.MODEL_SELECTION,
  L2: ActivityTypeL2.LAUNCH_TA3,
};

class UsageLogger {

  logFile: fs.WriteStream;

  constructor(file: string) {
    mkdirIfAbsent(LOG_PATH);
    this.logFile = fs.createWriteStream(file);
    // Write column headers
    this.logFile.write('timestamp,feature_id,type,activity_l1,activity_l2,other\n');
  }

  logEvent(featureName: string, metadata?: object) {
    this.log(UsageLogType.SYSTEM, featureName, metadata);
  }

  logGrpcApi(method: string, metadata?: object) {
    this.log(UsageLogType.TA23API, method, metadata);
  }

  log(type: UsageLogType, featureId: string, metadata?: object) {
    const line = stringify([[
      new Date().toISOString(),
      featureId,
      type.toString(),
      eventMapping[featureId] ? eventMapping[featureId].L1 : 'OTHER_L1',
      eventMapping[featureId] ? eventMapping[featureId].L2 : 'OTHER_L2',
      JSON.stringify(metadata ? metadata : {}),
    ]]);
    this.logFile.write(line);
    process.nextTick(() => this.logFile.uncork()); // flush buffer
  }

}

export const usageLogger = new UsageLogger(LOG_PATH);
