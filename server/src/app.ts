import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as logger from 'morgan';
import * as path from 'path';
import * as child_process from 'child_process';
import restRoutes from './rest';
import { Response, Request, NextFunction } from 'express';
import { getProblemsDirectory, getResourcesPath } from './data/config';
import { deleteFolderRecursive } from './api/utils';

import { PORT, ALLOW_ORIGINS } from './env';
// Create Express server
const app = express();

// Allow CORS
app.use((req, res, next) => {
  const origin = req.get('Origin');
  if (origin && ALLOW_ORIGINS.indexOf(origin) !== -1) {
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Credentials', 'true');
    const requestHeaders = req.get('Access-Control-Request-Headers');
    if (requestHeaders) {
      res.header('Access-Control-Allow-Headers', requestHeaders);
    }
    const requestMethods = req.get('Access-Control-Request-Method');
    if (requestMethods) {
      res.header('Access-Control-Allow-Methods', requestMethods);
    }
  }
  next();
});

// Express configuration
app.set('port', PORT);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// API routes
app.use('/rest', restRoutes);
// app.post('/api/values', api.getValues);
// app.get('/api/datasets', api.getDatatasetList);
// app.post('/api/dataResources', api.getDatasetResources);
// app.get('/api/dataInfoFromConfig', api.getDataInfoFromConfig);
// app.post('/api/readTSData', api.getTimeSeriesDatasets);

app.get('/api/exit', (req: Request, res: Response, next: NextFunction) => {
  res.json({});
  child_process.spawn('pm2', ['stop', 'server']);
});
app.get('/api/delete-old-outputs', (req: Request, res: Response, next: NextFunction) => {
  const outputProblemsDirectory = getProblemsDirectory();
  deleteFolderRecursive(outputProblemsDirectory);
  console.warn('deleted');
  res.json({ ok: true });
});
//
const resourcesStorePath = getResourcesPath();
if (resourcesStorePath) {
  app.use(express.static(resourcesStorePath));
  app.use('/data', express.static(resourcesStorePath));
}

// Serve the react webs from react build folder
app.use('/', express.static(path.join(__dirname, '../../client/build')));

app.get('*', (req: Request, res: Response) => {
  console.log('sending', path.join(__dirname, '../../client/build/index.html'));
  res.sendFile(path.join(__dirname, '../../client/build/index.html'));
});

module.exports = app;
