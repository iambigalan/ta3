import { getDataSource } from '../data/config';
import datasetsRoutes from './datasets';
import solutionRoutes, { wsSolutionResolvers } from './solutions';
import visualizationRoutes from './visualization/index';
import datamartRoutes from './datamart';
import logsRoutes from './logs';
import * as WebSocket from 'ws';
import { Router } from 'express';
import { DataSource } from '../data/DataSource';
import { Dataset } from '../data/Dataset';
import * as ta2 from '../ta2';
import { GrpcMessage } from '../relay';
import { addRecorders, wrapResolvers } from '../recorder';
import { ENABLE_RECORDER, DATAMART_URL } from '../env';

export interface RequestContext {
  dataSource?: DataSource;
  dataset?: Dataset;
  ta2?: typeof ta2;
}

export interface WSRequest {
  context: RequestContext;
  send: (message: object) => void;
  done: () => void;
}

const routes = Router();
const dataSource = getDataSource();

routes.use((req, res, next) => {
  req.context = req.context || {};
  req.context.dataSource = dataSource;
  req.context.ta2 = ta2;
  next();
});

let resolvers: {
  [resolver: string]: (params: unknown, request: WSRequest) => void;
} = Object.assign({}, wsSolutionResolvers);

// This enables recording and caching of the API calls
// for faster response times during system demonstrations
if (ENABLE_RECORDER && ENABLE_RECORDER === 'true') {
  console.log('===============================================');
  console.log('Enabling recording of API calls. To disable it,');
  console.log('unset the ENABLE_RECORDER environment variable.');
  console.log('===============================================');
  addRecorders(routes);
  resolvers = wrapResolvers(resolvers);
}

const configRoute = Router();
configRoute.get('/', async (req, res, next) => {
  res.json({
    env_vars: {
      DATAMART_URL: DATAMART_URL,
    },
  });
});
routes.use('/log', logsRoutes);
routes.use('/clientConfig', configRoute);
routes.use('/datasets', datasetsRoutes);
routes.use('/solutions', solutionRoutes);
routes.use('/visualization', visualizationRoutes);
routes.use('/datamart', datamartRoutes);

/**
 * Removes the TA3 grpc message prefix from fname.
 */
const removeMessageNamePrefix = (fname: string): string => {
  return fname.match(/^.*\/(.*)/)[1];
};

export function processWS(ws: WebSocket, message: GrpcMessage) {
  const command = removeMessageNamePrefix(message.fname);
  if (command in resolvers) {
    const context: RequestContext = {
      dataSource,
      ta2,
    };
    resolvers[command](message.object, {
      context,
      done: () => {},
      send: (msg) => ws.send(JSON.stringify({
        rid: message.rid,
        object: msg,
      })),
    });
  }
}

export default routes;
