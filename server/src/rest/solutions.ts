import { Router } from 'express';
import { WSRequest } from './index';
import * as d3m from 'd3m/dist';
import { getFitSolutionResults, getProduceSolutionResults } from '../ta2';
import { evaluateResult, fittedSolutions, cacheEvaluateResult, clearCachedEvaluateResult } from './store';
import { formatAsFileURI } from '../api/utils';
import { getDataSource } from '../data/config';
import { circularStringify } from '../utils';

const solutions = Router();

// Returns resource stats;
solutions.post('/_searchSolutions', async (req, res) => {
  const result = await req.context.ta2.startSolutionSearch(req.body as d3m.SolutionSearchRequest);
  res.json(result);
});

solutions.post('/describeSolution', async (req, res) => {
  const result = await req.context.ta2.describeSolution(req.body.solutionID);
  res.json(JSON.parse(circularStringify(result)));
});

/**
 * Calls FitSolution and ProduceSolution to prepare for TA3 solution evaluation.
 * The method returns the fitted solution ID, which should be used as a key to request visualization data.
 */
const evaluateSolution = async (params: d3m.ta3.EvaluateSolutionRequest, req: WSRequest) => {
  const { solutionID } = params;
  if (solutionID in fittedSolutions) {
    const fittedSolutionID = fittedSolutions[solutionID];
    console.log(`solution ${solutionID} already evaluated with fitted solution ${fittedSolutionID}`);
    return req.send({ fittedSolutionID, error: false });
  }
  const fitRequest = await req.context.ta2.fitSolution(params);
  const fitRequestID = fitRequest.request_id;
  getFitSolutionResults(fitRequestID, async (fitUpdate: d3m.ta2.GetFitSolutionResultsResponse) => {
    // Wait for FitSolution to complete
    if (fitUpdate.progress.state === d3m.ta2.ProgressState.COMPLETED) {
      const fittedSolutionID = fitUpdate.fitted_solution_id;
      // Use the fitted solution to produce solution.
      const inputs = await Promise.all(params.problem.inputs.data.map(async d => ({
        dataset_uri: await getDataSource().getDatasetById(d.datasetID).getTestingDocURI(
          d.targets[0].colName.toString(), params.problem.about.taskType.toString()),
      })));
      const produceRequest = await req.context.ta2.produceSolution({
        fittedSolutionID,
        inputs: inputs,
      });
      const produceRequestID = produceRequest.request_id;

      getProduceSolutionResults(produceRequestID, (produceUpdate: d3m.ta2.GetProduceSolutionResultsResponse) => {
        if (produceUpdate.progress.state === d3m.ta2.ProgressState.COMPLETED) {
          const csvUri = formatAsFileURI(produceUpdate.exposed_outputs['outputs.0'].csv_uri);
          console.log('Produced solution CSV:', csvUri);
          evaluateResult[fittedSolutionID] = {
            problem: params.problem,
            predictionCsvUri: csvUri,
            solutionID,
          };
          fittedSolutions[solutionID] = fittedSolutionID;
          req.send({ fittedSolutionID, error: false});
        } else if (fitUpdate.progress.state === d3m.ta2.ProgressState.ERRORED) {
            req.send({
              error: true,
              fitRequestID,
            });
          }
        });
      } else if (fitUpdate.progress.state === d3m.ta2.ProgressState.ERRORED) {
        req.send({
          error: true,
          fitRequestID,
        });
      }
  });
};

solutions.post('/_stopSearchSolutions', async (req, res) => {
  const result = await req.context.ta2.stopSearchSolutions(req.body.searchID);
  res.json(result);
});

solutions.post('/_exportSolution', async (req, res) => {
  const { solutionID, rank } = req.body;
  return res.json(await req.context.ta2.solutionExport(solutionID, rank));
});

solutions.post('/cacheEvaluations', (req, res) => {
  cacheEvaluateResult();
  res.json(true);
});

solutions.post('/clearCachedEvaluations', (req, res) => {
  clearCachedEvaluateResult();
  res.json(true);
});

const getSearchResults = (params: d3m.ta3.GetSearchResultsRequest, req: WSRequest) => {
  req.context.ta2.getSearchResults(params.searchID, params.problem, update => {
    req.send(update);
  });
};

export const wsSolutionResolvers = {
  getSearchResults: getSearchResults,
  evaluateSolution: evaluateSolution,
};

export default solutions;
