import { Router } from 'express';
import * as dm from '../api/datamart';
import * as FormData from 'form-data';
import { usageLogger, UserEvent } from '../usage-log';

const datamart = Router();

datamart.post('/augment', async (req, res) => {
  usageLogger.logEvent(UserEvent.DATAMART_AUGMENT);
  const dataset = await req.context.dataSource.getDatasetById(req.body.datasetID);
  const hit = req.body.result;
  const searchResult = await dm.augment(dataset, hit);
  res.json(searchResult);
});

datamart.post('/search', async (req, res) => {
  usageLogger.logEvent(UserEvent.DATAMART_SEARCH, {query: req.body.query, data: true});
  const dataset = await req.context.dataSource.getDatasetById(
    req.body.datasetID,
  );
  const query = req.body.query;
  const searchResult = await dm.search(dataset, query).catch(err => {
    console.log('Datamart Error', err);
  });
  res.json(searchResult);
});

export default datamart;
