import { Router } from 'express';
import resources from './resources';
import { usageLogger, UserEvent } from '../usage-log';

const datasets = Router();

// get resources info and data
datasets.use('/:datasetId/resources', async (req, res, next) => {
  req.context.dataset = await req.context.dataSource.getDatasetById(req.params.datasetId);
  resources(req, res, next);
});

// Returns the problems for a dataset
datasets.get('/:datasetId/problems', async (req, res) => {
  usageLogger.logEvent(UserEvent.LIST_PROBLEMS);
  const problems = await req.context.dataSource.getProblemsForDatasetId(req.params.datasetId);
  res.json(problems.map(p => p.toJSON()));
});

// Returns a dataset description
datasets.get('/:datasetId', async (req, res) => {
  usageLogger.logEvent(UserEvent.GET_DATA_DESCRIPTION);
  const dataset = await req.context.dataSource.getDatasetById(req.params.datasetId);
  res.json(dataset.toJSON());
});

// Returns a list of datasets
datasets.get('/', async (req, res) => {
  usageLogger.logEvent(UserEvent.LIST_DATASETS);
  const datasetList = await req.context.dataSource.getDatasets();
  res.json(datasetList.map(d => d.toJSON()));
});

export default datasets;
