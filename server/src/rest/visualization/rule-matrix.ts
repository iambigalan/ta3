import { Response } from 'express';
import { RuleModel, Stream, SupportMat } from 'rule-matrix-js/dist/models';
import * as fs from 'fs-extra';
import * as path from 'path';
import * as d3m from 'd3m/dist';
import { RULEMATRIX_ROOT, tempStorageRoot } from '../../env';

import { spawn } from 'child_process';
import { formatAsFileURI } from '../../api/utils';
import { mkdirIfAbsent, removeFileURLPrefix } from '../../utils';
import { getDataSource } from '../../data/config';
import { evaluateResult } from '../store';
import { getProduceSolutionResults, produceSolution } from '../../ta2';

const RULE_MATRIX_ERROR = { error: 'err' };
const RULE_MATRIX_START = { start: true };
const SAMPLE_DATASET_FOLDER_NAME = 'sampleDatasetFolder';

export const ruleMatrixResults: { [id: string]: d3m.RuleMatrixData | d3m.RuleMatrixStart | d3m.RuleMatrixError } = {};

const readJSON = (filePath: string): object => {
  return JSON.parse(fs.readFileSync(filePath, 'utf-8').toString());
};

const compute = (fittedSolutionID: string, problem: d3m.Problem, ruleMatrixConfig: d3m.RuleMatrixConfig[], predictionCsvUri: string) => {
  const datasetID = problem.inputs.data[0].datasetID;
  const id = `${fittedSolutionID}:${datasetID}`;
  const dataSource = getDataSource();
  try {
    ruleMatrixResults[id] = RULE_MATRIX_START;
    // Generates training data for rulematrix
    const tempFolder = path.resolve(tempStorageRoot(), fittedSolutionID);
    mkdirIfAbsent(tempFolder);
    const sampleDatasetFolder = path.join(tempFolder, SAMPLE_DATASET_FOLDER_NAME);
    mkdirIfAbsent(sampleDatasetFolder);
    const tableFolder = path.join(sampleDatasetFolder, 'tables');
    mkdirIfAbsent(tableFolder);

    const rulesPath = path.join(tempFolder, 'rule_list.mdl');
    const samplePath = path.join(tableFolder, 'learningData.csv');
    const datasetDocPath = dataSource.getDatasetDocPath(datasetID);
    fs.copySync(datasetDocPath, path.join(sampleDatasetFolder, 'datasetDoc.json'));
    const callSampleParameters = {
      rulesPath,
      samplePath,
      datasetPath: path.resolve(datasetDocPath, '..'), // This must be the directory that contains 'datasetDoc.json'
      problemDataset: ruleMatrixConfig, // NOTE(bowen): python code expects this to be named "problemDataset"
    };

    const callSampleParametersFile = path.join(tempFolder, 'callSample.json');
    fs.writeFile(callSampleParametersFile, JSON.stringify(callSampleParameters), err => {
      if (err) {
        console.error(`rule matrix error:`, err);
        return;
      }

      console.log('callSampleParameters', callSampleParameters);
      console.log('callSampleParametersFile', callSampleParametersFile);

      const ruriOriginal = predictionCsvUri;
      if (!ruriOriginal.startsWith('file://')) {
        ruleMatrixResults[id] = RULE_MATRIX_ERROR;
        console.error('unsupported protocol');
        return;
      }
      // Do sampling
      const sampleRulematrix = spawn('python3', ['-u',
        RULEMATRIX_ROOT + '/rulematrix/sample.py',
        '--config', callSampleParametersFile]);
      sampleRulematrix.on('error', function (code) {
        ruleMatrixResults[id] = RULE_MATRIX_ERROR;
        console.error('code', code);
      });
      sampleRulematrix.stdout.on('data', function(data) {
        console.log(data.toString());
      });
      sampleRulematrix.stderr.on('data', function(data) {
        console.warn(data.toString());
      });
      sampleRulematrix.on('close', async function(code) {
        if (code !== 0) {
          ruleMatrixResults[id] = { error: 'err' };
          console.error('code', code);
          return;
        }
        console.log('datasetDoc.json URI', formatAsFileURI(path.join(sampleDatasetFolder, 'datasetDoc.json')));
        // Classify generated data
        const produceRequest = await produceSolution({
          fittedSolutionID,
          inputs: [{
            dataset_uri: formatAsFileURI(path.join(sampleDatasetFolder, 'datasetDoc.json')),
          }],
        });
        const produceRequestID = produceRequest.request_id;
        getProduceSolutionResults(produceRequestID, (produceUpdate: d3m.ta2.GetProduceSolutionResultsResponse) => {
          if (produceUpdate.progress.state === d3m.ta2.ProgressState.ERRORED) {
            ruleMatrixResults[id] = RULE_MATRIX_ERROR;
            return;
          }
          if (produceUpdate.progress.state === d3m.ta2.ProgressState.COMPLETED) {
            const csvUri = produceUpdate.exposed_outputs['outputs.0'].csv_uri;
            console.log('Produced rule matrix solution CSV:', csvUri);
            evaluateResult[fittedSolutionID].ruleMatrixPredictionCsvUri = csvUri;
            const ruri = csvUri;

            console.log(RULEMATRIX_ROOT + '/rulematrix/prepare.py',
              removeFileURLPrefix(ruriOriginal),
              removeFileURLPrefix(ruri),
              '--config', callSampleParametersFile, '-v');

            console.log('RuleMatrix spawning prepare.py script.');
            const rulesRulematrix = spawn('python3',
              ['-u', RULEMATRIX_ROOT + '/rulematrix/prepare.py',
                removeFileURLPrefix(ruriOriginal),
                removeFileURLPrefix(ruri),
                '--config', callSampleParametersFile, '-v']);

            // Capture stdout and stderr and print on Node's log
            rulesRulematrix.stdout.on('data', function (data) {
              console.log(data.toString());
            });
            rulesRulematrix.stderr.on('data', function (data) {
              console.warn(data.toString());
            });

            // Listen to error code and register error status
            rulesRulematrix.on('error', function (code0) {
              console.log('RuleMatrix prepare.py returned error: ', code0);
              ruleMatrixResults[id] = RULE_MATRIX_ERROR;
              console.error('code', code0);
            });
            rulesRulematrix.on('close', function (code0) {
              console.log('RuleMatrix prepare.py closed with code ', code0);
              if (code0 !== 0) {
                ruleMatrixResults[id] = RULE_MATRIX_ERROR;
                console.error('code', code0);
                return;
              }
              ruleMatrixResults[id] = {
                model: readJSON(RULEMATRIX_ROOT + '/tmp/model.json') as RuleModel,
                streams: readJSON(RULEMATRIX_ROOT + '/tmp/stream.json') as Stream[],
                support: readJSON(RULEMATRIX_ROOT + '/tmp/support_mat.json') as SupportMat,
                continue: false,
                error: false,
              };
            }).on('error', (err0: Error) => {
              ruleMatrixResults[id] = RULE_MATRIX_ERROR;
              console.error(err0.stack, err0);
            });
          }
        });
      });
    });
  } catch (err) {
    ruleMatrixResults[id] = RULE_MATRIX_ERROR;
    console.error(err.stack, err);
  }
};

export const computeRuleMatrix = (fittedSolutionID: string, ruleMatrixConfig: d3m.RuleMatrixConfig[],
  problem: d3m.Problem, predictionCsvUri: string, res: Response) => {
  const datasetID = problem.inputs.data[0].datasetID;
  const id = `${fittedSolutionID}:${datasetID}`;
  if (!(id in ruleMatrixResults)) {
    res.json({
      continue: true,
      error: false,
    });
    compute(fittedSolutionID, problem, ruleMatrixConfig, predictionCsvUri);
    return;
  }
  const result: d3m.RuleMatrixData | d3m.RuleMatrixStart | d3m.RuleMatrixError = ruleMatrixResults[id];
  if ('start' in result) {
    res.json({
      continue: true,
      error: false,
      model: undefined,
      streams: undefined,
      support: undefined,
    });
    return;
  }
  const errorResult = result as d3m.RuleMatrixError;
  if ('error' in errorResult && errorResult.error) {
    res.json({
      continue: false,
      error: true,
      model: undefined,
      streams: undefined,
      support: undefined,
    });
    return;
  }
  res.json(result);
};
