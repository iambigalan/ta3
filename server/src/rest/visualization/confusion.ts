import { Response } from 'express';
import { readLearningDataTable, readPredTable } from '../loader';
import * as d3m from 'd3m/dist';
import { getDataSource } from '../../data/config';

export const computeConfusionMatrix = async (problem: d3m.Problem, predictionURI: string, res: Response) => {
  const inputData = problem.inputs.data[0];
  const datasetDocPath = getDataSource().getDatasetDocPath(inputData.datasetID);
  const target = inputData.targets[0].colName;
  try {
    const table = await readLearningDataTable(datasetDocPath);
    const predTable = await readPredTable(predictionURI, table, target);
    const [ tIxs, pIxs ] = table.intersectIndices(predTable);
    const labels = table.getValues(table.getLabelColumn(target), tIxs);
    const preds = predTable.getValues(predTable.getLabelColumn(target), pIxs);
    const conf: { [index: string]: { [index: string]: number } } = {};
    labels.forEach((l, ix) => {
      const p = preds[ix];
      if (!(p in conf)) {
        conf[p] = {};
      }
      if (!(l in conf)) {
        conf[l] = {};
      }
      if (!(l in conf[p])) {
        conf[p][l] = 0;
      }
      conf[p][l] += 1;
    });
    const classDefs = Object.keys(conf);
    classDefs.forEach((p) => {
      classDefs.forEach((l) => {
        if (!(p in conf)) {
          conf[p] = {};
        }
        if (!(l in conf[p])) {
          conf[p][l] = 0;
        }
      });
    });
    res.json({ conf, classDefs });
  } catch (err) {
    res.status(500).send('error');
    console.error(err.stack, err);
  }
};

export const computeConfusionScatterplot = async (problem: d3m.Problem, predictionURI: string, res: Response) => {
  const inputData = problem.inputs.data[0];
  const datasetDocPath = getDataSource().getDatasetDocPath(inputData.datasetID);
  const target = inputData.targets[0].colName;
  try {
    const table = await readLearningDataTable(datasetDocPath);
    const predTable = await readPredTable(predictionURI, table, target);
    const [ tIxs, pIxs ] = table.intersectIndices(predTable);
    const labels = table.getValues(table.getLabelColumn(target), tIxs);
    const preds = predTable.getValues(predTable.getLabelColumn(target), pIxs);
    const points = preds.map((v, ix) => [ v, labels[ix] ]);
    res.json({ points });
  } catch (err) {
    res.status(500).send('error');
    console.error(err.stack, err);
  }
};
