import * as _ from 'lodash';
import * as env from '../../env';
import * as path from 'path';
import * as d3m from 'd3m/dist';
import { removeFileURLPrefix } from '../../utils';
import { DataSource } from '../../data/DataSource';
import { Problem } from '../../data/Problem';
import * as stats from 'd3-array';
import { Resource, SampleMethod } from '../../data/Resource';
import { produceSolution, getProduceSolutionCsvUri, fitSolution, getFitSolutionFittedSolutionID } from '../../ta2';
import { d3mIndexColumn } from '../../data/Column';
import { DataPoint, DataPointValue } from '../../data/DataPoint';
import { flatMapDepth, flatten } from 'lodash';
import { formatAsFileURI } from '../../api/utils';

const PARTIAL_PLOTS_DATA_PATH = path.resolve(path.join(env.OUTPUT_DIR, 'ta3', 'tmp_data', 'partial_plots_data'));

export const partialPlotsResults: { [fittedSolutionID: string]: d3m.PartialPlotData } = {};

function getColumnsRanges(sample: DataPoint[]) {
  const columns = sample[0].columns;
  return columns.map((c, idx) => {
    switch (c.colType) {
      case d3m.ColumnType.integer:
      case d3m.ColumnType.real:
        return stats.extent(sample, d => +(d as DataPoint).values[idx]);
      case d3m.ColumnType.dateTime:
        const r = stats.extent(sample.map(v => Date.parse(v.values[idx].toString())), v => v);
        return r;
      default:
        return undefined;
    }
  });
}

function shouldIgnoreColumn(column: d3m.Column, ignoreColumns?) {
  if (ignoreColumns && ignoreColumns.has(column.colName)) {
    return true;
  }
  switch (column.colType) {
    case d3m.ColumnType.integer:
    case d3m.ColumnType.real:
    case d3m.ColumnType.dateTime:
      return false;
    default:
      return true;
  }
}

function generateTestDataPoints(
  sample: DataPoint[],
  numBins: number,
  targetIdx: number,
  ignoreColumns?: Set<string>,
) {
  const columns = sample[0].columns;
  const ranges = getColumnsRanges(sample);
  const d3mColumnIndex = columns.findIndex(c => c.colName === 'd3mIndex');
  const sampleSize = sample.length;
  const testValues = columns.map((c, idx) => {
    if (shouldIgnoreColumn(c, ignoreColumns)) {
      return undefined;
    }
    const range = ranges[idx];
    const span = range[1] - range[0];
    const step = span / (numBins - 1);
    let values = _.range(0, numBins).map(m => range[0] + m * step);
    if (c.colType === d3m.ColumnType.integer || c.colType === d3m.ColumnType.dateTime) {
      values = values.map(v => Math.floor(v));
    }
    if (c.colType === d3m.ColumnType.real) {
      return values.map(v => +v.toFixed(2));
    }
    return values;
  });
  const dataSplit: Array<{
    column: d3m.Column,
    histogram: Array<{
      value: DataPointValue,
      mean: number,
      std: number,
      count: number,
    }>,
    values: Array<{
      value: DataPointValue,
      dataRange: [number, number],
    }>,
  }> = [];
  let splitCount = 0;
  const testSamples = columns.map((c, idx) => {
    if (shouldIgnoreColumn(c, ignoreColumns)) {
      return undefined;
    }

    const values = testValues[idx];
    const histogram = stats.histogram<DataPoint, number>()
      .thresholds(values)
      .value((v) => c.colType === d3m.ColumnType.dateTime
        ? Date.parse(v.values[idx].toString())
        : v.values[idx] as number)(sample)
      .map(b => b.map(v => v.values[targetIdx] as number));

    const info = {
      column: c,
      histogram: histogram.map((b, bidx) => ({
        value: values[bidx],
        mean: stats.mean(b),
        std: stats.deviation(b),
        count: b.length,
      })),
      values: [],
    };

    const result = values.map(v => {
        splitCount++;
        info.values.push({value: v, dataRange: [sampleSize * splitCount, sampleSize * (splitCount + 1) - 1]});
        return sample.map(d => {
          d = d.copy();
          if (c.colType === d3m.ColumnType.dateTime) {
            d.values[idx] = new Date(v).toISOString().substring(0, 10);
          } else {
            d.values[idx] = v;
          }
          return d;
        });
      },
    );
    dataSplit.push(info);
    return result;
  });

  const selectedSample: DataPoint[] = flatMapDepth(testSamples.filter(v => v), undefined, 2);
  return {
    splitInfo: { dataSplit, baseDataRange: [0, sampleSize - 1] },
    testSample: sample.concat(selectedSample).map((s, idx) => {
      s = s.copy();
      s.values[d3mColumnIndex] = idx;
      return s;
    }),
  };
}

async function createTestDataset(problem: Problem) {
  const numBins = 10;
  const dataset = problem.dataset;
  const resource = dataset.getMainResource();
  const sample = await resource.getSample({ size: 300, method: SampleMethod.RANDOM});

  console.log('Using', sample.length, 'Datapoints for partialPlots');

  const {testSample, splitInfo} = generateTestDataPoints(sample, numBins, problem.target.colIndex, new Set(['d3mIndex', problem.target.colName]));
  const testDataset = dataset
    .copyTo(PARTIAL_PLOTS_DATA_PATH)
    .clearResources()
    .addResourceFromDataPoints(testSample, resource);

  testDataset.save();
  return {testDataset, splitInfo};
}

export async function computeRegressionPartialPlots(
  fittedSolutionID: string,
  problemConfig: d3m.Problem,
  dataSource: DataSource,
) {
  if (fittedSolutionID in partialPlotsResults) {
    return partialPlotsResults[fittedSolutionID];
  }

  const problem = new Problem(problemConfig, dataSource);
  const dataset = problem.dataset;
  const targetColumn = dataset.getResource(problem.target.resID).config.columns[problem.target.colIndex];
  const {testDataset, splitInfo: { dataSplit, baseDataRange }} = await createTestDataset(problem);

  const produceSolutionRequest = await produceSolution({
    fittedSolutionID,
    inputs: [ { dataset_uri: formatAsFileURI(testDataset.datasetDocPath) } ],
  });
  const producedCsvUri = await getProduceSolutionCsvUri(produceSolutionRequest.request_id);
  const evaluatedSolution = removeFileURLPrefix(producedCsvUri);

  const predictionResource = Resource.getStandAloneResource(
    evaluatedSolution,
    { columns: [{...d3mIndexColumn(), colIndex: 0}, {...targetColumn, colIndex: 1}] },
  );
  const data = await predictionResource.loadData();
  const targetPredictions = DataPoint.getColumnValues(data, 1) as Array<number>;
  const baseData = targetPredictions.slice(baseDataRange[0], baseDataRange[1] + 1);

  const result = {
    baseStats: { mean: stats.mean(baseData), std: stats.deviation(baseData) },
    partials: dataSplit
      .map(c => ({...c, values: c.values
        .map(v => {
          const dataValues = targetPredictions.slice(v.dataRange[0], v.dataRange[1] + 1);
          return ({...v, mean: stats.mean(dataValues), std: stats.deviation(dataValues)});
        })})),
  };
  partialPlotsResults[fittedSolutionID] = result;
  return result;
}
