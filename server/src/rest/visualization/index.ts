import { Router } from 'express';
import { evaluateResult } from '../store';
import { computeConfusionMatrix, computeConfusionScatterplot } from './confusion';
import { computeRuleMatrix } from './rule-matrix';
import * as d3m from 'd3m/dist';
import { computeRegressionPartialPlots } from './partialPlots';
import { usageLogger, UserEvent } from '../../usage-log';

const visualization = Router();

/**
 * Middleware that makes sure that the requested fittedSolutionID is valid.
 */
visualization.post('*', async (req, res, next) => {
  const { fittedSolutionID } = req.body as { fittedSolutionID: string };
  if (!(fittedSolutionID in evaluateResult)) {
    return res.status(400).send(`"${fittedSolutionID}" is not a valid fitted solution ID`);
  }
  next();
});

/**
 * Requests computation of confusion matrix for a given evaluated solution (classification only).
 */
visualization.post('/confusion-matrix', async (req, res) => {
  usageLogger.logEvent(UserEvent.VIEW_CONFUSION_MATRIX);
  const { fittedSolutionID } = req.body as { fittedSolutionID: string };
  const result = evaluateResult[fittedSolutionID];
  const { problem, predictionCsvUri } = result;
  computeConfusionMatrix(
    problem,
    predictionCsvUri,
    res,
  );
});

/**
 * Requests computation of confusion scatterplot for a given evaluated solution (regression only).
 */
visualization.post('/confusion-scatterplot', async (req, res) => {
  usageLogger.logEvent(UserEvent.VIEW_CONFUSION_SCATTER_PLOT);
  const { fittedSolutionID } = req.body as { fittedSolutionID: string };
  const result = evaluateResult[fittedSolutionID];
  const { problem, predictionCsvUri } = result;
  computeConfusionScatterplot(
    problem,
    predictionCsvUri,
    res,
  );
});

/**
 * Requests computation of rule matrix for an evaluated solution.
 */
visualization.post('/rule-matrix', async (req, res) => {
  usageLogger.logEvent(UserEvent.VIEW_RULE_MATRIX);
  const { fittedSolutionID, ruleMatrixConfig } = req.body as { fittedSolutionID: string, ruleMatrixConfig: d3m.RuleMatrixConfig[] };
  const result = evaluateResult[fittedSolutionID];
  computeRuleMatrix(
    fittedSolutionID,
    ruleMatrixConfig,
    result.problem,
    result.predictionCsvUri,
    res,
  );
});

export interface RegressionPartialPlotsParams {
  fittedSolutionID: string;
  problem: d3m.Problem;
}

visualization.post('/_regressionPartialPlots', async (req, res) => {
  usageLogger.logEvent(UserEvent.VIEW_PARTIAL_DEPENDENCE_PLOTS);
  const params: RegressionPartialPlotsParams = req.body;
  const result = await computeRegressionPartialPlots(params.fittedSolutionID, params.problem, req.context.dataSource);
  res.json(result);
});

export default visualization;
