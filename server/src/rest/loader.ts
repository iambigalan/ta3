import * as fs from 'fs';
import * as path from 'path';
import * as csvParse from 'csv-parse';
import * as d3m from 'd3m/dist';

// TODO: no module should read from env directly except for lowest level method such as data/config.ts.
// We should replace the data reading here by those in data/*.ts.
import { D3M_DATA_PATH } from '../env';

export class Table {
  rows: (number | string)[][];
  columns: d3m.Column[];
  columnLookup: { [ index: string ]: number };
  roleCache: { [ index: string ]: number[] };

  constructor(rows: (number | string)[][], columns: d3m.Column[]) {
    this.rows = rows;
    this.columns = columns;
    this.columnLookup = {};
    this.columns.forEach((v, ix) => {
      this.columnLookup[v.colName] = ix;
    });
    this.roleCache = {};
  }

  getColumnType(ix: number): d3m.ColumnType {
    return this.columns[ix].colType;
  }

  forEachColumn(cb: (col: d3m.Column, values: (string | number)[]) => void, ixs?: number[]) {
    this.columns.forEach((col, ix) => {
      cb(col, this.getValues(ix, ixs));
    });
  }

  mapColumn<T>(cb: (col: d3m.Column, values: (string | number)[]) => T, ixs?: number[]): T[] {
    return this.columns.map((col, ix) => {
      return cb(col, this.getValues(ix, ixs));
    });
  }

  mapColumnByName<T>(cb: (col: d3m.Column, values: (string | number)[]) => T, ixs?: number[]): { [index: string]: T } {
    const res: { [index: string]: T } = {};
    this.columns.forEach((col, ix) => {
      res[col.colName] = cb(col, this.getValues(ix, ixs));
    });
    return res;
  }

  getFeatureIndexByName(name: string): number {
    if (!(name in this.columnLookup)) {
      throw new Error('unknown name: ' + name);
    }
    return this.columnLookup[name];
  }

  getRoleColumns(role: string): number[] {
    if (!(role in this.roleCache)) {
      this.roleCache[role] = this.columns.filter((v) => {
        return v.role.indexOf(role as d3m.ColumnRole) >= 0;
      }).map((v, ix) => {
        return v.colIndex;
      });
    }
    return this.roleCache[role];
  }

  getRowIndexColumn(): number {
    const res = this.getRoleColumns('index');
    if (res.length !== 1) {
      throw new Error('no or multiple index columns');
    }
    return res[0];
  }

  getAttributeColumns(): number[] {
    return this.getRoleColumns('attribute');
  }

  getLabelColumn(name?: string): number {
    if (name !== undefined) {
      return this.getFeatureIndexByName(name);
    }
    const res = this.getRoleColumns('suggestedTarget');
    if (res.length !== 1) {
      throw new Error('no or multiple suggestedTarget columns');
    }
    return res[0];
  }

  getValues(col: number, ixs?: number[]): (number | string)[] {
    if (ixs) {
      return ixs.map((ix) => this.rows[ix][col]);
    }
    return this.rows.map((arr) => arr[col]);
  }

  getShape(): [ number, number ] {
    return [ this.rows.length, this.columns.length ];
  }

  intersectIndices(other: Table): [ number[], number[] ] {
    const ixs = this.getValues(this.getRowIndexColumn());
    const otherIxs = other.getValues(other.getRowIndexColumn());
    const lookup: { [index: string]: number } = {};
    ixs.forEach((v, ix) => {
      lookup[v] = ix;
    });
    const otherLookup: { [index: string]: number } = {};
    otherIxs.forEach((v, ix) => {
      otherLookup[v] = ix;
    });
    const isect = Object.keys(lookup).filter((v) => v in otherLookup);
    isect.sort();
    return [ isect.map((v) => lookup[v]), isect.map((v) => otherLookup[v]) ];
  }
} // Table

const getFilename = (uri: string, base?: string): string => {
  if (uri.startsWith('file://')) {
    return uri.slice('file://'.length);
  }
  if (uri.startsWith('/')) {
    return uri;
  }
  if (!base) {
    base = D3M_DATA_PATH;
  }
  return path.join(base, uri);
};

export const getBase = (uri: string): string => {
  return path.dirname(getFilename(uri));
};

export const hasDoc = (uri: string, base?: string): boolean => {
  return fs.existsSync(getFilename(uri, base));
};

export const walk = (cb: (err: NodeJS.ErrnoException, fnames: string[]) => void, base?: string) => {
  fs.readdir(base ? base : D3M_DATA_PATH, cb);
};

export const readDatasetDoc = (uri: string): d3m.Dataset => {
  return JSON.parse(fs.readFileSync(getFilename(uri), 'utf8'));
};

export const readProblemDoc = (uri: string): d3m.Problem => {
  return JSON.parse(fs.readFileSync(getFilename(uri), 'utf8'));
};

export const readTableFromResource = async (
      dr: d3m.DataResource,
      base?: string,
      relaxed?: boolean,
    ): Promise<Table> => {
  return new Promise<Table>((resolve, _) => {

    const resource = getFilename(dr.resPath, base);

    const columns: d3m.Column[] = dr.columns;
    columns.sort((a: d3m.Column, b: d3m.Column) => {
      return +a.colIndex - +b.colIndex;
    }); // make sure indices are in order
    const colLookup: { [ index: string ]: number } = {};
    columns.forEach((c, ix) => {
      colLookup[c.colName] = ix;
    });

    const parser = csvParse({
      auto_parse: false,
    }, (err, data: string[][]) => {
      if (err) {
        throw err;
      }

      const header = data.shift();
      const perm = relaxed ? header.map((v, ix) => {
        return ix;
      }) : header.map((v) => {
        return colLookup[v];
      });

      const rows = data.map((arr): (string | number)[] => {
        return perm.map((ix) => arr[ix]).map((v, ix): (string | number) => {
          if (columns[ix].colType === d3m.ColumnType.integer ||
            columns[ix].colType === d3m.ColumnType.real) {
            return +v;
          }
          return v;
        });
      });
      resolve(new Table(rows, columns));
    });
    if (!fs.existsSync(resource)) {
      throw new Error(`resource ${resource} does not exist`);
    }
    fs.createReadStream(resource).pipe(parser);
  });
};

export const readTableFromResourceTimeSerie = async (
      dr: d3m.DataResource,
      base?: string,
      relaxed?: boolean,
    ): Promise<Table> => {
  return new Promise<Table>((resolve, _) => {
    const resource = base;
    const columns: d3m.Column[] = dr.columns;
    columns.sort((a: d3m.Column, b: d3m.Column) => {
      return +a.colIndex - +b.colIndex;
    }); // make sure indices are in order
    const colLookup: { [ index: string ]: number } = {};
    columns.forEach((c, ix) => {
      colLookup[c.colName] = ix;
    });
    const parser = csvParse({
      auto_parse: false,
    }, (err, data: string[][]) => {
      if (err) {
        throw err;
      }
      const header = data.shift();
      const perm = relaxed ? header.map((v, ix) => {
        return ix;
      }) : header.map((v) => {
        return colLookup[v];
      });
      const rows = data.map((arr): (string | number)[] => {
        return perm.map((ix) => arr[ix]).map((v, ix): (string | number) => {
          if (columns[ix].colType === d3m.ColumnType.integer ||
            columns[ix].colType === d3m.ColumnType.real) {
            return +v;
          }
          return v;
        });
      });
      resolve(new Table(rows, columns));
    });
    fs.createReadStream(resource).pipe(parser);
  });
};

/**
 * Reads a CSV table from the given dataset located at the given data resource index.
 *
 * @param uriDoc an URI to the datasetDoc.json file
 * @param resourceIndex the index of the data resource to be read.g
 */
export const readTable = async (uriDoc: string, resourceIndex: number): Promise<Table> => {
  const doc = readDatasetDoc(uriDoc);
  if (doc.dataResources[resourceIndex].resFormat.indexOf('text/csv') !== -1) {
    return await readTableFromResource(doc.dataResources[resourceIndex], path.dirname(getFilename(uriDoc)));
  } else {
    throw new Error('DataResource ' + resourceIndex + ' from ' + uriDoc + ' is not CSV file.');
  }
};

/**
 * Find and read learningData CSV table from the given dataset.
 * @param uriDoc an URI to the datasetDoc.json file
 */
export const readLearningDataTable = async (uriDoc: string): Promise<Table> => {
  const doc = readDatasetDoc(uriDoc);
  let index = 0;
  //  Handling multiple resources.
  //  Find the index position of the learningData resource.
  for (const resource of doc.dataResources) {
    if (resource.resType === 'table' && resource.resPath.endsWith('learningData.csv')) {
      break;
    }
    index = index + 1;
  }
  return await readTable(uriDoc, index);
};

export const readPredTable = async (uri: string, table: Table, target: string): Promise<Table> => {
  return await readTableFromResource({
    resID: '0',
    resPath: uri,
    resType: d3m.ResourceType.table,
    resFormat: [],
    isCollection: false,
    columns: [
      {
        colIndex: 0,
        colName: 'd3mIndex',
        colType: d3m.ColumnType.integer,
        role: [
          d3m.ColumnRole.index,
        ],
      },
      {
        colIndex: 1,
        colName: target,
        colType: table.getColumnType(table.getLabelColumn(target)),
        role: [
          d3m.ColumnRole.suggestedTarget,
        ],
      },
    ],
  }, getBase(uri), true);
};
