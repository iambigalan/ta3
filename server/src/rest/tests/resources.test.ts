import * as express from 'express';
import * as request from 'supertest';
import resources from '../../rest/resources';
import { DataSource } from '../../data/DataSource';
import { getDataSourceConfig } from '../../data/config';

function init() {
  const app = express();
  const datasource = new DataSource(getDataSourceConfig());
  app.use(async (req, res, next) => {
    req.context = {
      datasource: datasource,
      dataset: await datasource.getDatasetById('185_baseball_dataset'),
    };
    next();
  });
  app.use(resources);
  return app;
}

describe('Resources GET /', function() {
  it('it should return resources info', function() {
    const app = init();
    return request(app)
      .get('/0')
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        return expect(response.body).toMatchObject({
          resPath: 'tables/learningData.csv',
        });
      });
  });

  it('it should return resource distributions', function() {
    const app = init();
    return request(app)
      .get('/0/stats')
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        expect(response.body[2].column).toMatchObject({
          colName: 'Number_seasons',
        });
        return expect(response.body[2].values[0]).toMatchObject({
          count: 243,
          name: 10,
        });
      });
  });
});
