import * as express from 'express';
import * as request from 'supertest';
import * as ta2 from '../../ta2';
import * as bodyParser from 'body-parser';
import solutions from '../solutions';
import { getDataSourceConfig } from '../../data/config';
import { DataSource } from '../../data/DataSource';

function init() {
  const app = express();
  app.use((req, res, next) => {
    req.context = { ta2 };
    next();
  });
  app.use(bodyParser.json());
  app.use(solutions);
  return app;
}

describe('Solutions POST /', function() {
  it.skip('starts the search', async function() {
    const app = init();
    const dataSource = new DataSource(getDataSourceConfig());
    const problems = await dataSource.getProblemsForDatasetId('185_baseball_dataset');
    const problemInfo = problems[0].toJSON();
    return request(app)
      .post('/_searchSolutions')
      .send({
        problem: problemInfo,
        priority: 1,
        timeLimit: 20,
      })
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        return expect(response.body).toHaveProperty('search_id');
      });
  });
});
