import { Router } from 'express';
import { usageLogger, UserEvent } from '../usage-log';

const logsRouter = Router();

logsRouter.post('/', async (req, res) => {
  console.log('body: ', req.body);
  console.log('params: ', req.params);
  const feature = UserEvent[req.body.feature];
  if (feature) {
    usageLogger.logEvent(feature, req.body.metadata);
    res.json({ success: true});
  } else {
    console.log('> Received log request without valid feature.');
    res.json({ success: false});
  }
});

export default logsRouter;
