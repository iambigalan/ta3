import { RequestContext } from 'src/rest';

declare global {
  namespace Express {
    interface Request {
      context: RequestContext;
    }
  }
}
