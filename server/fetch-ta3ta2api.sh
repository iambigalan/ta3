#!/bin/bash

fetch_script_dir=`dirname $0`
api_dir=$fetch_script_dir/ta3ta2-api

env="osx"
uname_out=`uname -s`
case $uname_out in
  Linux*) env="linux";;
esac

[ -e $api_dir ] && rm -r $api_dir

if [ ! -e $api_dir ]; then
  git clone https://gitlab.com/datadrivendiscovery/ta3ta2-api $api_dir
  # copy the google protobuf protos to local locations
  curl -OL https://github.com/google/protobuf/releases/download/v3.3.0/protoc-3.3.0-$env-x86_64.zip
  unzip protoc-3.3.0-$env-x86_64.zip -d protoc3
  mv protoc3/include/google $api_dir/
  rm protoc-3.3.0*.zip
  rm -r protoc3

  cd $api_dir
  git checkout v2019.7.9
  cd -
  rm -rf $api_dir/.git
fi
