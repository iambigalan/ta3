const rewireReactHotLoader = require('react-app-rewire-hot-loader');
process.env.PORT = process.env.PORT || 5000
process.env.REACT_APP_API_HOST = process.env.REACT_APP_API_HOST || 'localhost'
process.env.REACT_APP_API_PORT = process.env.REACT_APP_API_PORT || 3000

/* config-overrides.js */
module.exports = function override(config, env) {
  config = rewireReactHotLoader(config, env);
  return config;
}
