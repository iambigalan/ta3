import * as React from 'react';
import { BASE_PATH } from './config';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
} from 'react-router-dom';
import { hot } from 'react-hot-loader';
import { Layout } from './components/Layout/Layout';
import SelectDatasetView from './components/SelectDatasetView/SelectDatasetView';
import SelectTaskView from './components/SelectTaskView';
import DefineProblemView from './components/DefineProblemView/DefineProblemView';
import ExplainSolutionsView from './components/ExplainSolutionsView/ExplainSolutionsView';
import ExploreSolutionsView from './components/ExploreSolutionsView/ExploreSolutionsView';
import ErrorBoundary from './components/ErrorBoundary';
import Loading from './components/Loading/Loading';
import styled from 'styled-components';

// Global styles
import 'react-table/react-table.css';
import { HomePageView } from './components/HomePage/HomePageView';

const NotFoundView = () => (
  <div>
    <h1>Page Not Found! :(</h1>
    <p>The page you are looking for is not here.</p>
    <p>
      Head back to the initial page to{' '}
      <Link to="/">select a dataset</Link>.
    </p>
  </div>
);

const PageCenteredDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin-top: -1em;
`;

class App extends React.Component {
  public render() {
    return (
      <React.Suspense fallback={
        <PageCenteredDiv style={{ height: window.innerHeight }}>
          <Loading />
        </PageCenteredDiv>
      }>
      <ErrorBoundary>
        <Router basename={BASE_PATH}>
          <Switch>
            <Route
              exact={true}
              path="/"
              component={() => (
                <HomePageView />
              )}
            />
            <Route
              exact={true}
              path="/select-dataset"
              component={() => (
                <Layout>
                  <SelectDatasetView />
                </Layout>
              )}
            />
            <Route
              exact={true}
              path="/select-task"
              component={() => (
                <Layout>
                  <SelectTaskView />
                </Layout>
              )}
            />
            <Route
              exact={true}
              path="/define-problem"
              component={() => (
                <Layout>
                  <DefineProblemView />
                </Layout>
              )}
            />
            <Route
              exact={true}
              path="/explore-solutions"
              component={() => (
                <Layout>
                  <ExploreSolutionsView />
                </Layout>
              )}
            />
            <Route
              exact={true}
              path="/explain-solutions"
              component={() => (
                <Layout>
                  <ExplainSolutionsView />
                </Layout>
              )}
            />
            <Route component={NotFoundView} />
          </Switch>
        </Router>
      </ErrorBoundary>
      </React.Suspense>
    );
  }
}

export default hot(module)(App);
