export class FetchError extends Error {
  originalError: Error;
  message: string;
  constructor(err: Error, message: string) {
    super();
    this.originalError = err;
    this.message = message;
  }
}

/**
 * Represents an object that may eventually be loaded. The load may fail,
 * in which case, the error object will be available.
 */
export default class Loadable<T> {
  error?: FetchError;
  private _value?: T;

  constructor(loadedValue: T | undefined, error?: FetchError) {
    this._value = loadedValue;
    this.error = error;
  }

  get hasValue() {
    return this._value !== undefined;
  }

  get value(): T {
    if (this.error) {
      throw this.error;
    }
    if (!this.hasValue) {
      throw new Error('attempted to read undefined value');
    }
    return this._value as T;
  }

  static fromJSON(value: {
    value?: unknown;
    error?: FetchError;
  }) {
    let originalError: Error;
    let error: FetchError;

    if (value.error) {
      originalError = new Error();
      if (value.error.originalError) {
        Object.assign(originalError, value.error.originalError);
      }
      error = new FetchError(originalError, value.error.message);
      return new Loadable<unknown>(value.value, error);
    }
    return new Loadable<unknown>(value.value, undefined);
  }
}
