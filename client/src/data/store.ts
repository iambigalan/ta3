import { DataStore, StoreState } from './DataStore';

const storeInitialState: StoreState = {
  favoriteSolutions: [],
  removedSolutions: [],
  orderedSolutions: [],
  isDoneDefineProblem: false,
  problemDefinition: {},
  selectedConfigDefineProblem: '1',
  defaultProblemDefinition: {},
};

export const store = new DataStore(storeInitialState);
export const connectPropsToDatastore = store.connectPropsToDatastore;
export const downloadState = store.downloadState;
export const loadStateFromFile = store.loadStateFromFile;
// For debuging purposes ------------------------------
declare global {
  interface Window {
    __TA3STORE__: DataStore;
  }
}
store.load();
store.persistOnChange = true;
window.__TA3STORE__ = store;
