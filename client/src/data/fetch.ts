import { API_URL, BASE_PATH_URL } from '../config';

interface FetchInit {
  method: string;
  headers: { [index: string]: string };
  body?: string;
  credentials: 'include';
}

// NOTE(joschi): this function accomplishes the following things:
// - determine whether to send a POST or GET (if `post === null`)
// - parsing the JSON result
// - checking the return status
export function fetchJSON(
  url: string,
  args?: object | null,
  post?: object | null,
  customBase?: string,
  external?: boolean,
) {
  let argstring = '';
  if (args) {
    Object.keys(args).forEach((k, ix) => {
      const kk = encodeURIComponent(k);
      const ak = args[k] !== undefined ? encodeURIComponent(args[k]) : '';
      argstring += `${ix === 0 ? '?' : '&'}${kk}=${ak}`;
    });
  }
  const init: FetchInit = {
    method: post ? 'POST' : 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    credentials: 'include',
  };
  if (post) {
    const content = JSON.stringify(post, null, '');
    init.headers['Content-Length'] = `${content.length}`;
    init.body = content;
  }
  const path = external ? url : `${customBase ? `${BASE_PATH_URL}/${customBase}` : API_URL}${url}${argstring}`;
  return fetch(path, init).then(data => {
    if (data.status !== 200 || !data.ok) {
      throw new Error(`server returned ${data.status}${data.ok ? ' ok' : ''}`);
    }
    const ct = data.headers.get('content-type');
    if (ct && ct.includes('application/json')) {
      return data.json();
    }
    throw new TypeError('response not JSON encoded');
  });
}
