import * as _ from 'lodash';
import * as api from './Api';
import { API_URL } from '../config';
import { flatten, sortBy } from 'lodash';
import * as d3m from 'd3m/dist';
import { DataStoreBase } from './DataStoreBase';
import { produce } from 'immer';
import Loadable from './Loadable';

const uuidv1 = require('uuid/v1');
const { suspense, cachedSuspense } = api;
enum TimeUnits {
  Minutes = 1,
  Hours = 60,
}
export interface StoreState {
  datasets?: Loadable<d3m.Dataset[]>;
  clientConfig?: Loadable<d3m.ClientConfig>;
  selectedDataset?: d3m.Dataset;
  problems?: Loadable<d3m.Problem[]>;
  selectedProblem?: d3m.Problem;
  solutionSearchState?: d3m.SolutionSearchState;
  favoriteSolutions: d3m.Solution[];
  removedSolutions: d3m.Solution[];
  orderedSolutions: d3m.Solution[]; // ordered by user ranking
  isDoneDefineProblem: boolean;
  selectedConfigDefineProblem: string;
  problemDefinition: d3m.ProblemDefinition;
  defaultProblemDefinition?: d3m.ProblemDefinition;
}

export class DataStore extends DataStoreBase<StoreState> {

  cache: Cache;

  constructor(initialState: StoreState) {
    super(initialState);
    this.ignoreKeysOnPersist = [];
    this.loadableKeys = new Set(['datasets', 'problems']);
  }

  get clientConfig() {
    return cachedSuspense<d3m.ClientConfig>({
      cacheID: 'client-config',
      loader: () => api.clientConfig(),
    }).value;
  }

  get selectedDataset() {
    return this.state.selectedDataset;
  }

  get datasets() {
    return suspense<d3m.Dataset[]>({
      currentValue: this.state.datasets,
      loader: () => api.getDatasets(),
      onLoad: (datasets: Loadable<d3m.Dataset[]>) => this.setState({ datasets }),
      info: { field: 'datasets' },
    });
  }

  get selectedProblem() {
    return this.state.selectedProblem;
  }

  get problemTarget() {
    if (!this.selectedProblem) {
      return undefined;
    }
    return this.selectedProblem.inputs.data[0].targets[0];
  }

  get problems(): Loadable<d3m.Problem[]> {
    if (this.selectedDataset) {
      return suspense<d3m.Problem[]>({
        currentValue: this.state.problems,
        loader: () => api.getProblems(this.selectedDataset!.about.datasetID),
        onLoad: (problems: Loadable<d3m.Problem[]>) => this.setState({ problems }),
      });
    } else {
      return new Loadable([]);
    }
  }

  get datasetFeatureNames() {
    if (this.selectedDataset) {
      const fieldsList = this.selectedDataset.dataResources.map(r =>
        r.columns ? r.columns.map(c => `r${r.resID}:${c.colName}`) : [],
      );
      return sortBy(flatten(fieldsList));
    } else {
      return [];
    }
  }

  get datasetFeatures() {
    if (this.selectedDataset) {
      const fieldsList: d3m.Feature[][] = this.selectedDataset.dataResources.map(r =>
        r.columns
          ? r.columns.filter(c => c.colName !== 'd3mIndex')
          .map(c => ({
              resID: r.resID,
              colIndex: c.colIndex,
              colName: c.colName,
            }))
          : [],
      );
      return sortBy(flatten(fieldsList), ['resID', 'colName']);
    } else {
      return [];
    }
  }

  get solutionSearchStatus() {
    return this.state.solutionSearchState;
  }

  /**
   * Retrieves a user-pruned solution list.
   */
  get solutions(): d3m.Solution[] {
    const searchState = this.state.solutionSearchState;
    const problem = this.state.selectedProblem;
    if (!searchState || !problem || !searchState.solutions) {
      // It is possible that solutions are accessed at the client side while solution search is being fired.
      return [];
    }
    // apply user pruning
    return searchState.solutions.filter(solution => this.state.removedSolutions.indexOf(solution) === -1);
  }

  /**
   * Gets a list of favorite solutions.
   */
  get favoriteSolutions(): d3m.Solution[] {
    return this.solutions.filter(solution => this.isFavoriteSolution(solution.id));
  }

  /**
   * Gets a list of (unremoved) solutions ordered by user ranking.
   */
  get rankedSolutions(): d3m.Solution[] {
    const solutions = this.solutions;
    solutions.sort((sol1, sol2) =>
      this.state.orderedSolutions.indexOf(sol1) - this.state.orderedSolutions.indexOf(sol2));
    return solutions;
  }

  isFavoriteSolution = (id: string) => {
    return this.state.favoriteSolutions.find(solution => solution.id === id) !== undefined;
  }

  getURLToFile = (filePath: string) => {
    return `${API_URL}/datasets/${filePath}`;
  }

  selectDataset = (dataset: d3m.Dataset) => {
    this.setState({
      selectedDataset: dataset,
      problems: undefined,
    });
  };

  selectProblem = (problem: d3m.Problem) => {
    this.setState({
      selectedProblem: JSON.parse(JSON.stringify(problem)),
    });
  };

  setProblemMetrics = (metrics: d3m.Metrics[]) => {
    const problemDefinition = produce(this.state.problemDefinition!, draft => {
      draft.metrics = metrics;
    });
    if (this.state.selectedConfigDefineProblem === '1') { // Updating metrics values through 'default config' option
      if (Object.keys(this.state.defaultProblemDefinition!).length === 0 ) {
        this.setState({ // Setting up 'defaultProblemDefinition' for the fisrt time
          defaultProblemDefinition: problemDefinition,
          problemDefinition: problemDefinition,
        });
      } else {
        // 'problemDefinition' must be to update with the saved metrics in 'defaultProblemDefinition' config,
        // when 'default config' mode is selected, because it could have changed in 'advance configuration' mode
        this.setState({
          problemDefinition: produce(this.state.problemDefinition!, draft => {
                                      draft.metrics = this.state.defaultProblemDefinition!.metrics;
                                    }),
        });
      }
    } else { // Updating metrics values through 'advance configuration' option
      this.setState({
        problemDefinition: problemDefinition,
      });
    }
  }

  partialPlot = (solutionID: string) => {
    if (!this.selectedProblem) {
      return undefined;
    }
    return cachedSuspense<d3m.PartialPlotData>({
      cacheID: `partialPlot-${solutionID}`,
      loader: () => {
        return api.getPartialPlots(solutionID, this.selectedProblem!);
      },
    }).value;
  };

  setProblemInfo = (info: Partial<d3m.SimplifiedProblem>) => {
    const problem = produce(this.state.selectedProblem!, draft => {
      if (info.searchParams) {
        if (info.searchParams.evaluationMetrics) {
          draft.inputs!.performanceMetrics =
            Object.keys(info.searchParams.evaluationMetrics)
            .map(k => ({ metric: k as d3m.MetricValue }));
        }
      }
    });
    this.setState({
      selectedProblem: problem,
    });
  }

  dataForResource = (resource: d3m.DataResource): d3m.DataForResource | undefined => {
    const datasetId = this.selectedDataset!.about.datasetID; // FIX THIS
    const resourceDataID = `@data-${datasetId}-${resource.resID}`;
    switch (resource.resType) {
      case d3m.ResourceType.timeseries:
        return cachedSuspense<d3m.ResourceFileData[]>({
          cacheID: resourceDataID,
          loader: () => api.getFilesSample(datasetId!, resource.resID),
        }).value;
      case d3m.ResourceType.table:
        return cachedSuspense<d3m.ColumnProfile[]>({
          cacheID: resourceDataID,
          loader: () => api.getColumnsStats(datasetId!, resource.resID),
        }).value;
      default:
        return cachedSuspense<d3m.ResourceFileData[]>({
          cacheID: resourceDataID,
          loader: () => api.getFilesSample(datasetId!, resource.resID),
        }).value;
    }
  }

  getRawData(dataset: d3m.Dataset, resource: d3m.DataResource, size?: number, start?: number) {
    const datasetId = dataset!.about.datasetID;
    const resourceDataID = `@data-${datasetId}-${resource.resID}-samples`;
    return cachedSuspense<d3m.ResourceFileData[]>({
      cacheID: resourceDataID,
      loader: () => api.getFilesSample(datasetId!, resource.resID, size, start),
    }).value;
  }

  getColumnsStats(resource: d3m.DataResource) {
    const datasetId = this.selectedDataset!.about.datasetID;
    const resourceDataID = `@data-${datasetId}-${resource.resID}-summary`;
    return cachedSuspense<d3m.ColumnProfile[]>({
      cacheID: resourceDataID,
      loader: () => api.getColumnsStats(datasetId!, resource.resID),
    }).value;
  }

  getResourceSamples(resource: d3m.DataResource) {
    const datasetId = this.selectedDataset!.about.datasetID;
    const resourceDataID = `@data-${datasetId}-${resource.resID}-samples`;
    return cachedSuspense<d3m.ResourceFileData[]>({
      cacheID: resourceDataID,
      loader: () => api.getFilesSample(datasetId!, resource.resID),
    }).value;
  }

  resourceNumRows = (resource: d3m.DataResource): number => {
    const datasetId = this.selectedDataset!.about.datasetID;
    const resourceDataID = `@resourceNumRows-${datasetId}-${resource.resID}`;
    return cachedSuspense<number>({
      cacheID: resourceDataID,
      loader: () => api.getNumRows(datasetId!, resource.resID),
    }).value;
  }

  setInitialNewProblem = () => {
    const initialDP: d3m.ProblemDefinition = {
      target: '',
      type: '',
      subType: '',
      maxSolutions: '',
      maxTime: '60',
      maxTimeUnit: TimeUnits.Minutes,
    };
    this.setState({
      problemDefinition: initialDP,
      defaultProblemDefinition: {}, // Update problem definition
      selectedConfigDefineProblem: '1',
    });
  }

  setExistProblem = (problem: d3m.Problem) => {
    const metrics = new Set(problem.inputs.performanceMetrics.map(m => m.metric));
    const selectedMetrics = Array.from(metrics).map(m => ({metric: d3m.MetricValue[m]}));
    const existedProblem: d3m.ProblemDefinition = {
      target: this.getFeatureID(problem.inputs.data[0].targets[0]),
      type: problem.about.taskType,
      subType: problem.about.taskSubType,
      maxSolutions: '',
      maxTime: '60',
      maxTimeUnit: TimeUnits.Minutes,
      metrics: selectedMetrics,
    };
    this.setState({
      problemDefinition: existedProblem,
      defaultProblemDefinition: existedProblem, // Update problem definition
      selectedProblem: JSON.parse(JSON.stringify(problem)),
      selectedConfigDefineProblem: '1',
    });
  }

  getFeatureID(c: d3m.Feature): string {
    return `${c.resID}:${c.colName}`;
  }

  createNewProblem = (problem: d3m.ProblemDefinition) => {
    if (!this.selectedDataset) {
      throw new Error('Dataset not selected');
    }
    const targetFeature = this.datasetFeatures.find(c => this.getFeatureID(c) === problem.target)!;
    const target: d3m.InputsTarget = Object.assign({targetIndex: 0}, targetFeature);
    const datasetID = this.selectedDataset.about.datasetID;
    const metrics = problem.metrics ? problem.metrics : [];
    const newProblem: d3m.Problem = {
      about: {
        problemID: this.selectedDataset.about.datasetID + '_' + uuidv1(),
        problemName: this.selectedDataset.about.datasetID + '_' + uuidv1(),
        problemDescription: 'New_Problem_' + this.selectedDataset.about.datasetID + '_' + uuidv1() + '. '
                            + 'You defined a ' + problem.type + ' ' + problem.subType + ' problem.',
        taskType: problem.type as d3m.TaskType,
        taskSubType: problem.subType as d3m.TaskSubType,
        problemVersion: '1.0',
        problemSchemaVersion: '3.3.1',
      },
      inputs: {
        performanceMetrics: metrics,
        data: [
          {
            datasetID: datasetID,
            targets: [
              target,
            ],
          },
        ],
      },
      expectedOutputs: {
        predictionsFile: 'predictions.csv',
      },
    };
    this.selectProblem(newProblem);
  };

  getSearchResults = (searchId: string, problem: d3m.Problem) => {
    api.getResultsForSearch(searchId, problem, data => {
      const newStatus = produce(this.state.solutionSearchState!, draft => {
        if (data.solution && data.solution.id) {
          // create or update solution in store
          let index = draft.solutions.findIndex(p => p.id === data.solution!.id);
          if (index >= 0) {
            draft.solutions[index] = {...draft.solutions[index], ...data.solution};
          } else {
            // Note: solution.id could be empty string when "Solution doesn't work"
            draft.solutions.push(data.solution);
            index = draft.solutions.length;
          }
          // set the description for given solution
          if (data.solution.description) {
            draft.solutions[index].description = data.solution.description;
          }
          // set scores for given solution
          if (data.solution.scores) {
            draft.solutions[index].scores = data.solution.scores;
          }
        }
        if (data.progress) {
          // We update the progress only when the state is not terminal to avoid
          // rewriting it (e.g., when a RUNNING state arrives after a COMPLETED state)
          if (draft.progress.state.toUpperCase() !==
              d3m.ta2.ProgressStateNameMap[d3m.ta2.ProgressState.COMPLETED]) {
            draft.progress = data.progress;
          }
        }
      });
      this.setState({
        solutionSearchState: newStatus,
      });
    });
  }

  searchSolutions = async (params: d3m.SolutionSearchRequest) => {
    // TODO: Check if there is a search already running, and
    // confirm if the user wants to stop it.

    // Set initial state of search
    const solutionSearchState: d3m.SolutionSearchState = {
      searchID: '',
      params,
      solutions: [],
      progress: {
        percentage: 0,
        state: d3m.ta2.ProgressStateNameMap[d3m.ta2.ProgressState.PROGRESS_UNKNOWN],
        status: 'Initializing...',
      } as d3m.SolutionSearchProgress,
    };
    this.setState({
      solutionSearchState: solutionSearchState,
      // clear user settings
      favoriteSolutions: [],
      removedSolutions: [],
    });

    const setFailureState = () => {
      const failedStateProgress = {
        percentage: 100,
        state: d3m.ta2.ProgressStateNameMap[d3m.ta2.ProgressState.ERRORED],
        status: 'Search for solutions failed.',
      };
      solutionSearchState.progress = failedStateProgress;
      this.setState({
        solutionSearchState: solutionSearchState,
      });
    };

    // Send request to start solution search
    api
      .startPipelinesSearch(params as d3m.SolutionSearchRequest)
      .then((searchSolutionResponse: d3m.ta2.SearchSolutionsResponse) => {
        if (searchSolutionResponse && searchSolutionResponse.search_id) {
          solutionSearchState.searchID = searchSolutionResponse.search_id;
          solutionSearchState.progress = {
            percentage: 100,
            state: d3m.ta2.ProgressStateNameMap[d3m.ta2.ProgressState.PROGRESS_UNKNOWN],
            status: 'Requesting search solution results...',
          };
          this.setState({
            solutionSearchState: solutionSearchState,
          });
          // Request the search solution results
          this.getSearchResults(searchSolutionResponse.search_id, params.problem as d3m.Problem);
        } else {
          // Did not return a valid search_id
          setFailureState();
        }
      })
      .catch(() => {
        // Search solutions request failed
        setFailureState();
      });
  };

  stopSearch = async (searchID?: string) => {
    if (this.state.solutionSearchState && this.state.solutionSearchState.searchID) {
      searchID = searchID || this.state.solutionSearchState.searchID;
      api.stopSearch(searchID);
    }
  }

  exportSolutions = async (solutions: d3m.Solution[], callback?: (message: string, done?: boolean) => void) => {
    for (const solution of solutions) {
      if (callback) {
        callback(`Exporting solution ${solution.id}`);
      }
      await api.exportSolution(solution.id, this.getSolutionRank(solution.id));
      if (callback) {
        callback(`Solution exported ${solution.id}`);
      }
    }
    if (callback) {
      callback(`Successfully exported ${solutions.length} solution${solutions.length === 1 ? '' : 's'}`, true);
    }
  }

  toggleFavoriteSolution = (solutionId: string) => {
    const solution = this.state.favoriteSolutions.find(sol => sol.id === solutionId);
    if (solution) { // remove
      this.setState({ favoriteSolutions: _.pull(this.state.favoriteSolutions, solution) });
    } else { // add
      const newSolution = (this.state.solutionSearchState as d3m.SolutionSearchState).solutions
        .find(sol => sol.id === solutionId) as d3m.Solution;
      this.setState({ favoriteSolutions: this.state.favoriteSolutions.concat([newSolution]) });
    }
  }

  removeSolution = (solutionId: string) => {
    this.setState({
      removedSolutions: this.state.removedSolutions.concat([this.getSolutionById(solutionId)]),
    });
  }

  rankUpSolution = (solutionID: string) => {
    const ranked = this.rankedSolutions.concat();
    const solution = this.getSolutionById(solutionID);
    const index = ranked.indexOf(solution);
    if (index === 0) {
      return; // already ranked first
    }
    const swappedSolution = ranked[index - 1];
    ranked[index - 1] = solution;
    ranked[index] = swappedSolution;
    this.setState({ orderedSolutions: ranked });
  }

  rankDownSolution = (solutionID: string) => {
    const ranked = this.rankedSolutions.concat();
    const solution = this.getSolutionById(solutionID);
    const index = ranked.indexOf(solution);
    if (index === ranked.length - 1) {
      return; // already ranked last
    }
    const swappedSolution = ranked[index + 1];
    ranked[index + 1] = solution;
    ranked[index] = swappedSolution;
    this.setState({ orderedSolutions: ranked });
  }

  /**
   * Returns the 1-based rank of the ranked solutions.
   * If a solution has no rank (e.g. a removed solution), return -1.
   */
  getSolutionRank = (solutionID: string): number => {
    const solution = this.getSolutionById(solutionID);
    const rank = this.rankedSolutions.indexOf(solution);
    return rank === -1 ? -1 : rank + 1;
  }

  /**
   * Resets all solutions to the originally received solution list.
   */
  resetSolutions = () => {
    this.setState({ removedSolutions: [] });
  }

  /**
   * Check whether define problem step was completed or not.
   */
  setDefineProblemStatus = (isDone: boolean) => {
    this.setState({
      isDoneDefineProblem: isDone,
    });
  };

  get getDefineProblemStatus(): boolean {
    return this.state.isDoneDefineProblem;
  }

  /**
   * Selected configuration for searching (Default or Advanced).
   */
  setSelectedConfig = (selectedConfig: string) => {
    if (selectedConfig === '1') {
      this.setState({
        selectedConfigDefineProblem: selectedConfig,
        problemDefinition: this.state.defaultProblemDefinition,
      });
    } else {
      this.setState({
        selectedConfigDefineProblem: selectedConfig,
      });
    }
  };

  get getSelectedConfig(): string {
    return this.state.selectedConfigDefineProblem;
  }

  /**
   * Check whether define problem step was completed or not.
   */
  setProblemDefinition = (defineProblem: d3m.ProblemDefinition) => {
    this.setState({
      problemDefinition: defineProblem,
    });
  };

  get problemDefinition(): d3m.ProblemDefinition {
    return this.state.problemDefinition;
  }

  setDefaultProblemDefinition = () => {
    const availableMetrics = this.state.problemDefinition.type &&
    d3m.metrics.getMetricsForTask(this.state.problemDefinition.type as d3m.TaskType,
                                  this.state.problemDefinition.subType as d3m.TaskSubType);
    if (this.state.problemDefinition.type && availableMetrics) {
      let metrics = (this.state.problemDefinition.metrics && this.state.problemDefinition.metrics.length > 0) ?
                      // Using an existing setting to define a problem
                      new Set(this.state.problemDefinition.metrics.map(m => m.metric))
                      :
                      // Defing a problem from scratch. Setting the fisrt metric as the default metric
                      new Set([availableMetrics[0].metric]);
      this.setProblemMetrics( // update field 'metrics' of the d3m problem
            Array.from(metrics).map(m => ({metric: d3m.MetricValue[m]})),
      );
    }
  }

  get getDefaultProblemDefinition(): d3m.ProblemDefinition {
    return this.state.defaultProblemDefinition!;
  }

  changeDataset = (datasetID: string) => {
    api.getDataset(datasetID).then(dataset => {
      if (this.state.selectedProblem) {
        // update datasetID references to new dataset
        const newProblem = produce(this.state.selectedProblem, draft => {
          if (draft.inputs && draft.inputs.data) {
            for (let i = 0; i < draft.inputs.data.length; i++) {
              draft.inputs.data[i].datasetID = datasetID;
            }
          }
        });
        this.selectProblem(newProblem);
        console.log(
          `> Problem schema updates with new datasetID=${datasetID}.`,
          newProblem,
        );
      }
      this.setState({ selectedDataset: dataset });
    });
  }

  private getSolutionById = (id: string): d3m.Solution => {
    const searchStatus = this.state.solutionSearchState;
    if (!searchStatus) {
      throw new Error('attempted to retrieve solutions when there was no pipeline search');
    }
    const solution = searchStatus.solutions.find(sol => sol.id === id);
    if (!solution) {
      throw new Error(`solution ${id} cannot be found`);
    }
    return solution;
  }

}
