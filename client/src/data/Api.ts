import Cache from 'node-cache';
import * as socket from './socket/socket';
import { fetchJSON } from './fetch';
import * as d3m from 'd3m/dist';
import Loadable, { FetchError } from './Loadable';
export type Promisable<T> = T | Promise<T>;

// Store current running searches
const runningSearches = {};

const cache = new Cache({ stdTTL: 600, errorOnMissing: false });

export interface CachedSuspenseContext<T> {
  cacheID: string;
  loader: (() => Promise<T>);
}

export function cachedSuspense<T>(ctx: CachedSuspenseContext<T>): Loadable<T> {
  return suspense<T>({
    currentValue: cache.get(ctx.cacheID),
    loader: ctx.loader,
    onLoad: (data: Loadable<T>) => cache.set(ctx.cacheID, data),
    info: { field: ctx.cacheID },
  });
}

export interface SuspenseContext<T> {
  currentValue: Loadable<T> | undefined;
  loader: (() => Promise<T>);
  onLoad: ((value: Loadable<T>) => void);
  info?: { field?: string };
}

export function suspense<T>(ctx: SuspenseContext<T>): Loadable<T> {
  if (ctx.currentValue) {
    if (ctx.currentValue instanceof Error) {
      throw ctx.currentValue;
    }
    if (ctx.currentValue instanceof Promise) {
      throw ctx.currentValue;
    }
    return ctx.currentValue;
  } else {
    throw ctx.loader()
      .then(d => ctx.onLoad(new Loadable(d)))
      .catch((err: Error) => {
        let msg = `Failed to fetch data${ctx.info && ctx.info.field ? ` for ${ctx.info.field}` : ''}`;
        ctx.onLoad(new Loadable<T>(undefined, new FetchError(err, msg)));
      });
  }
}

export function getDatasets(): Promise<d3m.Dataset[]> {
  return fetchJSON(`/datasets`).then(r => {
    return r;
  });
}

export function getProblems(datasetId: string): Promise<d3m.Problem[]> {
  return fetchJSON(`/datasets/${datasetId}/problems`);
}

export function getDataset(datasetId: string): Promise<d3m.Dataset> {
  return fetchJSON(`/datasets/${datasetId}`);
}

export function getColumnsStats(datasetId: string, resId: string): Promise<d3m.ColumnProfile[]> {
  return fetchJSON(`/datasets/${datasetId}/resources/${resId}/stats`);
}

export function getNumRows(datasetId: string, resId: string): Promise<number> {
  return fetchJSON(`/datasets/${datasetId}/resources/${resId}/numRows`);
}

export function getFilesSample(datasetId: string, resId: string, size: number = 10, start?: number):
  Promise<d3m.ResourceFileData[]> {
  return fetchJSON(`/datasets/${datasetId}/resources/${resId}/sample`, undefined, {
    size, start,
  });
}

export function startPipelinesSearch(params: d3m.SolutionSearchRequest): Promise<d3m.ta2.SearchSolutionsResponse> {
  return fetchJSON(`/solutions/_searchSolutions`, undefined, params);
}

export function stopSearch(searchID: string) {
  return fetchJSON(`/solutions/_stopSearchSolutions`, undefined, { searchID });
}

export function getResultsForSearch(searchID: string, problem: d3m.Problem,
                                    onUpdate: (update: d3m.SolutionSearchUpdate) => void) {
  if (runningSearches[searchID]) {
    return;
  }

  runningSearches[searchID] = true;
  socket.sendMessage(
    d3m.ta3.Rpc.GET_SEARCH_RESULTS,
    {
      searchID,
      problem,
    },
    onUpdate,
  );
}

export function exportSolution(solutionID: string, rank: number) {
  return fetchJSON(`/solutions/_exportSolution`, undefined, { solutionID, rank });
}

/**
 * Stores the currently running or finished evaluations.
 * Maps the solution id to its evaluation promise.
 */
const evaluations: { [solutionID: string ]: Promise<{ fittedSolutionID: string, error: boolean }>  } = {};

/**
 * Requests client-initiated solution evaluation.
 * This will call FitSolution and ProduceSolution on the server side.
 * The server will perform TA3-only train/test split, fit the solution using the train data,
 * and produce solution on the test data.
 */
export function evaluateSolution(solutionID: string, selectedProblem: d3m.Problem):
  Promise<{ fittedSolutionID: string, error: boolean}> {
  if (solutionID in evaluations) {
    return evaluations[solutionID];
  }

  return new Promise((resolve, reject) => {
    const params: d3m.ta3.EvaluateSolutionRequest = {
      solutionID: solutionID,
      problem: selectedProblem,
    };
    const onUpdate = (res: d3m.ta3.EvaluateSolutionResponse) => {
      if (res.error) {
        reject(res);
      } else {
        resolve(res);
      }
    };
    socket.sendMessage(d3m.ta3.Rpc.EVALUATE_SOLUTION, params, onUpdate);
  });
}

export function getConfusionMatrix(fittedSolutionID: string): Promise<d3m.ConfusionMatrixData> {
  return fetchJSON('/visualization/confusion-matrix', undefined, { fittedSolutionID });
}

export function getConfusionScatterplot(fittedSolutionID: string): Promise<d3m.ConfusionScatterplotData> {
  return fetchJSON('/visualization/confusion-scatterplot', undefined, { fittedSolutionID });
}

export function getRuleMatrix(fittedSolutionID: string, ruleMatrixConfig: d3m.RuleMatrixConfig[]):
  Promise<d3m.RuleMatrixData> {
  return fetchJSON('/visualization/rule-matrix', undefined, { fittedSolutionID, ruleMatrixConfig });
}

export function getPartialPlots(fittedSolutionID: string, problem: d3m.Problem): Promise<d3m.PartialPlotData> {
  return fetchJSON(`/visualization/_regressionPartialPlots`, undefined, { fittedSolutionID, problem });
}

/**
 * Requests server to cache evaluated solutions, e.g. their predicion file URIs.
 */
export function cacheSolutionEvaluations(): Promise<void> {
  return fetchJSON('/solutions/cacheEvaluations', undefined, {});
}

/**
 * Requests server to clear the cache for evaluated solutions.
 */
export function clearCachedSolutionEvaluations(): Promise<void> {
  return fetchJSON('/solutions/clearCachedEvaluations', undefined, {});
}

/**
 * Datamart
 */
export function searchDatamart(request: d3m.DatamartRequest): Promise<d3m.DatamartResponse> {
  return fetchJSON('/datamart/search', undefined, request);
}

export function augmentData(request: d3m.DatamartAugmentRequest): Promise<{ datasetID: string }> {
  return fetchJSON('/datamart/augment', undefined, request);
}

/**
 * Returns configurations for the client application.
 */
export function clientConfig() {
  return fetchJSON('/clientConfig', undefined, undefined);
}

export function log(feature: string, metadata?: object) {
  return fetchJSON('/log', undefined, {
    feature: feature,
    metadata: metadata,
  });
}
