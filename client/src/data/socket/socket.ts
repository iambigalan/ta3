/**
 * @fileoverview Provides a WebSocket singleton that connects to TA2 (via the relay server).
 *
 * Usage example:
 *   import * as ws from './socket';
 *   ws.sendMessage(...);
 */
import { SOCKET_URL } from '../../config';
import * as d3m from 'd3m/dist';
import WebSocket from 'ws';
import ReconnectingWebSocket from 'reconnecting-websocket';

export interface MessageResponse {
  rid: string;
  object: {
    response_info: d3m.ta2.Response;
  };
}

interface Message {
  fname: d3m.ta2.Rpc | d3m.ta3.Rpc;
  object: object;
  callback: Function;
}

interface MessageInfo {
  rid: string;
  fname: d3m.ta2.Rpc | d3m.ta3.Rpc;
  callback: Function;
}

const randomString = (len: number): string => {
  return Math.random()
    .toString(36)
    .substr(2, len);
};

/**
 * Socket messages waiting for a response.
 */
const expecting: { [index: string]: MessageInfo } = {};

// Creates a web socket connection and wait for it to be ready.
// ReconnectingWebSocket manages connections and automatically
// and tries to reconnect when the connection is lost.
// ReconnectingWebSocket also maintains internally a queue of
// messages to be sent after the connection is restored.
const options = {
  WebSocket: WebSocket, // custom WebSocket constructor
};
export const ws = new ReconnectingWebSocket(SOCKET_URL, [], options);

ws.onopen = () => {
  console.log('WebSocket connected to ' + SOCKET_URL);
};

ws.onmessage = (reply: { data: unknown }) => {
  const data: MessageResponse = JSON.parse(reply.data as string);

  const rid: string = data.rid;
  const res: { response_info: d3m.ta2.Response } = data.object;
  if (!(rid in expecting)) {
    console.error(rid, 'not expected but received');
  }

  const responseInfo = res.response_info;
  if (
    responseInfo &&
    responseInfo.status &&
    (responseInfo.status.code === d3m.ta2.StatusCode.UNAVAILABLE ||
      responseInfo.status.code === d3m.ta2.StatusCode.UNIMPLEMENTED)
  ) {
    console.warn('TA2 does not support ' + expecting[rid].fname);
    return;
  }

  const callback = expecting[rid].callback;
  if (callback) {
    callback(res);
  }
};

export const sendMessage = (
  fname: d3m.ta2.Rpc | d3m.ta3.Rpc,
  object: object,
  callback: Function,
): void => {

  const msg: Message = { fname, object, callback };
  const rid: string = randomString(6);
  ws.send(
    JSON.stringify({
      // Request id is used to distinguish responses of different messages.
      rid: rid,
      fname: msg.fname,
      object: msg.object,
    }),
  );
  expecting[rid] = {
    rid: rid,
    fname: msg.fname,
    callback: msg.callback,
  };
};
