import * as d3m from 'd3m/dist';
import { format } from 'd3';

export function arrayReplace<T>(arr: Array<T>, idx: number, value: T) {
  return [...arr.slice(0, idx), value, ...arr.slice(idx + 1)];
}

export function isFeatureEqual(a: d3m.Feature, b: d3m.Feature) {
  return a.colName === b.colName && a.resID === b.resID;
}

export const isNumericColumnType = (colType: d3m.ColumnType): boolean => {
  return colType === d3m.ColumnType.integer || colType === d3m.ColumnType.real;
};

export function downloadObj(obj: object) {
  const dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(obj, undefined, 4));
  const dlAnchorElem = document.createElement('a');
  dlAnchorElem.setAttribute('href', dataStr);
  dlAnchorElem.setAttribute('download', 'state.json');
  dlAnchorElem.click();
}

export function loadFile(onLoad: (data: string) => void) {
  const input = document.createElement('input');
  input.type = 'file';
  input.accept = 'application/json';
  input.addEventListener('change', (evt) => {
    const files = input.files;
    const reader = new FileReader();
    if (files) {
      const f = files[0];
      reader.addEventListener('load', () => {
        if (reader.result) {
          try {
            onLoad(reader.result.toString());
          } catch (err) {
            alert('Error loading file');
            console.error('Error loading file');
          }
        }
      });
      reader.readAsText(f);
    }
  });
  input.click();
}

export function shallowEqual(a: Object, b: Object) {
  if (a === b) {
    return true;
  }

  const aKeys = Object.keys(a);
  const bKeys = Object.keys(b);
  if (bKeys.length !== aKeys.length) {
    return false;
  }

  for (let i = 0; i < aKeys.length; i++) {
    var key = aKeys[i];
    if (a[key] !== b[key]) {
      return false;
    }
  }
  return true;
}

const LARGE_NUMBER_THRESHOLD = 1e6;

/**
 * Formats a value to make it more readable.
 * Integers are printed without decimal point.
 * Other numbers are formatted to have a reasonable amount of decimal places (depending on the size of the number).
 */
export const formatValue = (v: number | string): string => {
  if (Number.isNaN(+v)) {
    return `${v}`;
  }
  if (+v < 0) {
    return `-${formatValue(-v)}`;
  }
  if (+v >= LARGE_NUMBER_THRESHOLD) {
    return format('.3e')(+v);
  }
  if (Math.floor(+v) === +v) {
    return `${+v}`;
  }
  if (+v < 0.1) {
    return `${(+v).toFixed(3)}`;
  }
  if (+v <= 100) {
    return `${Math.round(+v * 1000.0) / 1000.0}`;
  }
  return `${Math.round(+v)}`;
};

/**
 * Returns a copy of the string with the first letter capitalized.
 * If the string has delimitors '_' or ' ' (space) they will be removed.
 *   'abc' -> 'Abc'
 *   'abc def' -> 'Abc Def'
 *   'ABC_DEF' -> 'Abc Def'
 */
export const capitalize = (s: string): string => {
  const tokens = s.split(/[,_]/g);
  for (let i = 0; i < tokens.length; i++) {
    const t = tokens[i].toLowerCase();
    tokens[i] = t.charAt(0).toUpperCase() + t.substr(1);
  }
  return tokens.join(' ');
};

/**
 * Normalizes a string and only keep the letters and digits in it.
 * This is for string comparison.
 */
export const normalize = (s: string): string => {
  const matched = s.match(/[a-zA-Z0-9]+/g);
  return matched !== null ? matched.join('') : '';
};
