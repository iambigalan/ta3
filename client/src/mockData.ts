import * as d3m from 'd3m/dist';

export const metrics = ['TA2-Score', 'Precision', 'Recall', 'F1 Score'];

export const solutionResults = [
  {
    solutionId: '1',
    metrics: [0.7812, 0.7234, 0.7335],
  },
  {
    solutionId: '2',
    metrics: [0.7812, 0.7234, 0.7335],
  },
  {
    solutionId: '3',
    metrics: [0.7812, 0.7234, 0.7335],
  },
  {
    solutionId: '4',
    metrics: [0.7812, 0.7234, 0.7335],
  },
  {
    solutionId: '5',
    metrics: [0.7812, 0.7234, 0.7335],
  },
];

export const selectedSolutions = [
  {
    solutionId: '1',
    metrics: [0.7812, 0.7234, 0.7335],
  },
  {
    solutionId: '2',
    metrics: [0.7812, 0.7234, 0.7335],
  },
  {
    solutionId: '3',
    metrics: [0.7812, 0.7234, 0.7335],
  },
  {
    solutionId: '4',
    metrics: [0.7812, 0.7234, 0.7335],
  },
];

export const tableColumnData = {
  values: ['1', '3', '10', '15', '20', '25', '30', '40'],
  distribution: {
    1: 40,
    3: 25,
    10: 23.5,
    15: 23.5,
    20: 21.5,
    25: 21.5,
    30: 20,
    40: 10,
  },
  stats: {
    mean: 21.25,
    stdDev: 1.0,
    min: 10,
    max: 40,
  },
};

const manualTimeSeriesData = [
  {
    name: 'series year 1',
    minValue: 10,
    maxValue: 105,
    seriesKey: 'year',
    dataKey: 'value',
    data: [
      { year: 1930, value: 27 },
      { year: 1940, value: 18 },
      { year: 1950, value: 33 },
      { year: 1960, value: 42 },
      { year: 1970, value: 45 },
      { year: 1980, value: 50 },
      { year: 1990, value: 61 },
      { year: 2000, value: 72 },
      { year: 2010, value: 105 },
    ],
  },
  {
    name: 'series year 2',
    minValue: 15,
    maxValue: 90,
    seriesKey: 'year',
    dataKey: 'value',
    data: [
      { year: 1930, value: 27 },
      { year: 1940, value: 15 },
      { year: 1950, value: 33 },
      { year: 1960, value: 42 },
      { year: 1970, value: 45 },
      { year: 1980, value: 50 },
      { year: 1990, value: 61 },
      { year: 2000, value: 72 },
      { year: 2010, value: 88 },
    ],
  },
  {
    name: 'series date',
    minValue: 40,
    maxValue: 60,
    seriesKey: 'date',
    dataKey: 'close',
    data: [
      { date: '2014-01-02', close: 45.285 },
      { date: '2014-01-03', close: 45.56399999999999 },
      { date: '2014-01-06', close: 43.9 },
      { date: '2014-01-07', close: 43.988 },
      { date: '2014-01-08', close: 43.873999999999995 },
      { date: '2014-10-28', close: 54.798 },
      { date: '2014-10-29', close: 53.943000000000005 },
      { date: '2014-10-30', close: 55.02 },
      { date: '2014-10-31', close: 57.071999999999996 },
    ],
  },
];

const randomSeriesData = new Array(10).fill(0).map((_, index) => ({
  name: 'random ' + (index + 1),
  minValue: 0,
  maxValue: 1,
  seriesKey: 'i',
  dataKey: 'v',
  data: new Array(100)
    .fill(0)
    .map((__, dIndex) => ({ i: dIndex, v: Math.random() })),
}));

export const timeSeriesData = ([] as any[]) // tslint:disable-line no-any
  .concat(manualTimeSeriesData)
  .concat(randomSeriesData);

/**
 * Generates a random text of length "len" with spaces and words of an average length of 5 :-)
 */
const genText = (len: number) => {
  return String.fromCharCode.apply(
    undefined,
    new Array(len)
      .fill(0)
      .map(() => (Math.floor(Math.random() * 6) || -64) + 96),
  );
};

export const textData = new Array(10).fill(0).map((_, index) => ({
  name: index + 1 + '.txt',
  content: '',
  text: genText(1000),
}));

export const imageData: d3m.ResourceFileData[] = new Array(10)
  .fill(0)
  .map((_, index) => ({
    name: index + 1 + '.jpg',
    content: '',
    src: 'https://wagingnonviolence.org/wp-content/uploads/2011/05/cute-cat.jpg',
  }));

export const audioData: d3m.ResourceFileData[] = new Array(10)
  .fill(0)
  .map((_, index) => ({
    name: index + 1 + '.wav',
    content: '',
    src:
      'http://ia902606.us.archive.org/35/items/shortpoetry_047_librivox/song_cjrg_teasdale_64kb.mp3',
  }));

export const videoData = new Array(10).fill(0).map((_, index) => ({
  name: index + 1 + '.webm',
  content: '',
  src:
    'https://upload.wikimedia.org/wikipedia/commons/transcoded/9/96/' +
    'Curiosity%27s_Seven_Minutes_of_Terror.ogv/Curiosity%27s_Seven_Minutes_of_Terror.ogv.480p.vp9.webm',
}));

export const confusionMatrixData: d3m.ConfusionMatrixData = {
  conf: {
    a: { a: 215, b: 0, c: 2, d: 0, e: 5, f: 2 },
    b: { a: 0, b: 135, c: 34, d: 0, e: 2, f: 40 },
    c: { a: 0, b: 16, c: 368, d: 1, e: 0, f: 12 },
    d: { a: 1, b: 0, c: 2, d: 458, e: 0, f: 0 },
    e: { a: 3, b: 0, c: 1, d: 20, e: 183, f: 30 },
    f: { a: 0, b: 36, c: 12, d: 0, e: 8, f: 414 },
  },
  classDefs: ['a', 'b', 'c', 'd', 'e', 'f'],
};

export const confusionScatterplotData: d3m.ConfusionScatterplotData = {
  points: [[3, 2], [4, 3], [5, 5], [6, 7], [8, 8], [10, 8.5], [12, 10]] as [
    number,
    number
  ][],
};

export const searchHits = [
  {
    id: 'datamart.socrata.data-cityofnewyork-us.w9ei-idxz',
    title: 'New York City Locations Providing Seasonal Flu Vaccinations',
    description:
      'Location and facility information for places in New York City providing seasonal flu vaccinations.',
    size: '324.8 kB',
  },
  {
    id: 'datamart.socrata.data-cityofnewyork-us.7hi3-kaps',
    title: 'Staff Injuries - Class A Injuries',
    description:
      'Serious injury to staff as a result of inmate assault on staff (uniform staff only).',
    size: '5.5 kB',
  },
  {
    id: 'datamart.socrata.data-cityofnewyork-us.s4kf-3yrf',
    title: 'LinkNYC Locations',
    description:
      'LinkNYC is replacing the City’s outdated public telephones with a network of kiosks that provide fre...',
    size: '429.0 kB',
  },
  {
    id: 'WikiQA: A Challenge Dataset for Open-Domain Question Answering',
    title:
      'WikiQA dataset is a publicly available set of question and sentence (QS) pairs, collected and annota...',
    description: 'datamart.d3m.32_wikiqa',
    size: '520.3 kB',
  },
  {
    id: 'datamart.socrata.data-cityofnewyork-us.evjd-dqpz',
    title: 'NYCHA Development Data Book',
    description:
      'Contains the main body of the “Development Data Book” as of January 1, 2016. The Development Data Bo...',
    size: '112.3 kB',
  },
  {
    id: 'datamart.socrata.data-cityofnewyork-us.gsr2-xq9e',
    title: 'Approved Link NYC Kiosk Locations',
    description:
      'This dataset lists locations where a LinkNYC kiosk has been approved but is not yet installed. It i...',
    size: '51.6 kB',
  },
  {
    id: 'datamart.socrata.data-cityofnewyork-us.xp25-gxux',
    title: 'LinkNYC New Site Permit Applications',
    description:
      'LinkNYC is replacing the City’s outdated public telephones with a network of kiosks that provide fre...',
    size: '64.9 kB',
  },
  {
    id: 'datamart.socrata.data-cityofnewyork-us.wjtn-s4z7',
    title: 'Clear Channel Sign List - Times Square',
    description: 'Clear Channel Spectacolor Sign Locations in Times Square',
    size: '16.0 kB',
  },
  {
    id: 'datamart.socrata.data-cityofnewyork-us.iwdd-99mu',
    title: 'Construction Pipeline',
    description:
      'Pursuant to 312(a) of the City Charter, as amended by Local Law 63 of 2011, the Mayor\'s Office is r...',
    size: '101.6 kB',
  },
];

export const datamartWetherQuery = {
  results: [
    {
      id: 'datamart.url.a3943fd7892d5d219012f889327c6661',
      score: 13.037019,
      discoverer: 'datamart.url',
      metadata: {
        name: 'Newyork Weather Data around Airport 2016-18',
        description:
          'This data contains weather information for NY city\
       around LaGuardia Airport from 2016 to 2018; weath...',
        size: 1523693,
        nb_rows: 24624,
        columns: [
          {
            name: 'DATE',
            structural_type: 'http://schema.org/Text',
            semantic_types: ['http://schema.org/DateTime'],
            mean: 1495931400,
            stddev: 25590011.431395352,
            coverage: [
              {
                range: {
                  gte: 1451610000,
                  lte: 1540252800,
                },
              },
            ],
          },
          {
            name: 'HOURLYSKYCONDITIONS',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'HOURLYDRYBULBTEMPC',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 14.666224009096823,
            stddev: 9.973788193915643,
            coverage: [
              {
                range: {
                  gte: -17.2,
                  lte: 37.8,
                },
              },
            ],
          },
          {
            name: 'HOURLYRelativeHumidity',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 60.70849577647823,
            stddev: 18.42048051096981,
            coverage: [
              {
                range: {
                  gte: 11,
                  lte: 100,
                },
              },
            ],
          },
          {
            name: 'HOURLYWindSpeed',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 10.68859649122807,
            stddev: 5.539675475162907,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 41,
                },
              },
            ],
          },
          {
            name: 'HOURLYWindDirection',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'HOURLYStationPressure',
            structural_type: 'http://schema.org/Float',
            semantic_types: [
              'https://metadata.datadrivendiscovery.org/types/PhoneNumber',
            ],
            mean: 29.90760315139694,
            stddev: 0.24584097919742368,
            coverage: [
              {
                range: {
                  gte: 28.89,
                  lte: 30.81,
                },
              },
            ],
          },
        ],
        materialize: {
          identifier: 'datamart.url',
          direct_url:
            'https://drive.google.com/uc?export=download&id=1jRwzZwEGMICE3n6-nwmVxMD2c0QCHad4',
        },
        date: '2019-01-22T01:54:58.281183Z',
      },
      join_columns: [],
      union_columns: [],
    },
    {
      id: 'datamart.upload.a031bc4968cb4838967e4709e63a0ddc',
      score: 12.841263,
      discoverer: 'datamart.upload',
      metadata: {
        filename: 'weather.csv',
        name: 'NYC Weather Data 2010-2018',
        description:
          'This dataset contains daily weather information (rain precipitation, visibility, and temperature) fo...',
        size: 155635,
        nb_rows: 3014,
        columns: [
          {
            name: 'time',
            structural_type: 'http://schema.org/Text',
            semantic_types: [
              'http://schema.org/DateTime',
              'https://metadata.datadrivendiscovery.org/types/PhoneNumber',
            ],
            mean: 1392465600,
            stddev: 75173772.19216819,
            coverage: [
              {
                range: {
                  gte: 1262304000,
                  lte: 1522627200,
                },
              },
            ],
          },
          {
            name: 'Amt[PrecipHourly1]',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 0.3664799320016243,
            stddev: 0.8568352594999706,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 8.958064516129031,
                },
              },
              {
                range: {
                  gte: 12.713636363636367,
                  lte: 12.713636363636367,
                },
              },
            ],
          },
          {
            name: 'Temp[Temp]',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 12.776484275218623,
            stddev: 9.351401369497479,
            coverage: [
              {
                range: {
                  gte: -13.081249999999997,
                  lte: -12.590624999999998,
                },
              },
              {
                range: {
                  gte: -11.553125000000005,
                  lte: 31.25625,
                },
              },
              {
                range: {
                  gte: 32.290625000000006,
                  lte: 32.290625000000006,
                },
              },
            ],
          },
          {
            name: 'Visby[Visibility]',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 14564.427670064131,
            stddev: 2760.711876629015,
            coverage: [
              {
                range: {
                  gte: 1111.6545454545455,
                  lte: 1111.6545454545455,
                },
              },
              {
                range: {
                  gte: 2054.5,
                  lte: 16270.9375,
                },
              },
            ],
          },
        ],
        materialize: {
          identifier: 'datamart.upload',
        },
        date: '2019-01-22T20:26:14.096225Z',
      },
      join_columns: [],
      union_columns: [],
    },
    {
      id: 'datamart.socrata.data-cityofnewyork-us.yhdx-itry',
      score: 7.339852,
      discoverer: 'datamart.socrata',
      metadata: {
        name: 'Overhead Electronic Signs',
        description:
          'Electronic Signs are used to display dynamic messages that advise drivers of street closures, delays...',
        size: 20554,
        nb_rows: 134,
        columns: [
          {
            name: 'VMS',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'Main Roadway',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'Direction',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'Cross Street',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'Borough',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'Type',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'Owner',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'Latitude',
            structural_type: 'http://schema.org/Float',
            semantic_types: ['http://schema.org/latitude'],
            mean: 40.73108425470999,
            stddev: 0.07588345920525343,
          },
          {
            name: 'Longitude',
            structural_type: 'http://schema.org/Float',
            semantic_types: ['http://schema.org/longitude'],
            mean: -550400.9244567757,
            stddev: 6346680.494876826,
          },
          {
            name: 'Location 1',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
        ],
        spatial_coverage: [
          {
            lat: 'Latitude',
            lon: 'Longitude',
            ranges: [
              {
                range: {
                  type: 'envelope',
                  coordinates: [
                    [-74.0170771, 40.8694240428753],
                    [-73.7143546, 40.6451286],
                  ],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [
                    [-74.2130327224731, 40.6308743544543],
                    [-74.0765404701233, 40.5512111581172],
                  ],
                },
              },
            ],
          },
        ],
        materialize: {
          socrata_id: 'yhdx-itry',
          socrata_domain: 'data.cityofnewyork.us',
          socrata_updated: '2018-09-10T19:21:44.000Z',
          direct_url:
            'https://data.cityofnewyork.us/api/views/yhdx-itry/rows.csv?accessType=DOWNLOAD',
          identifier: 'datamart.socrata',
          date: '2019-01-17T19:47:00.781996Z',
        },
        date: '2019-01-18T07:10:14.232571Z',
      },
      join_columns: [],
      union_columns: [],
    },
    {
      id: 'datamart.url.0a41288b3f9256e9906062a5fd75169a',
      score: 4.649092,
      discoverer: 'datamart.url',
      metadata: {
        name: 'Earthquake Data of Turkey in 1910-2017',
        description:
          'The data covers up all the recorded earthquakes in the latitudes between 25 - 50; longitudes 15 - 60...',
        size: 2211652,
        nb_rows: 24007,
        columns: [
          {
            name: 'id',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'date',
            structural_type: 'http://schema.org/Text',
            semantic_types: [
              'http://schema.org/DateTime',
              'https://metadata.datadrivendiscovery.org/types/PhoneNumber',
            ],
            mean: 690523674.4616153,
            stddev: 642816976.79487,
            coverage: [
              {
                range: {
                  gte: -1864339200,
                  lte: -1864339200,
                },
              },
              {
                range: {
                  gte: -1855958400,
                  lte: -1846972800,
                },
              },
              {
                range: {
                  gte: -1840320000,
                  lte: -1836518400,
                },
              },
              {
                range: {
                  gte: -1823731200,
                  lte: -1803513600,
                },
              },
              {
                range: {
                  gte: -1796515200,
                  lte: -1796515200,
                },
              },
              {
                range: {
                  gte: -1789344000,
                  lte: -1719273600,
                },
              },
              {
                range: {
                  gte: -1713139200,
                  lte: -1544054400,
                },
              },
              {
                range: {
                  gte: -1537488000,
                  lte: -1521763200,
                },
              },
              {
                range: {
                  gte: -1512086400,
                  lte: -1485475200,
                },
              },
              {
                range: {
                  gte: -1477353600,
                  lte: -1397433600,
                },
              },
              {
                range: {
                  gte: -1389744000,
                  lte: -1227657600,
                },
              },
              {
                range: {
                  gte: -1221350400,
                  lte: -1206144000,
                },
              },
              {
                range: {
                  gte: -1199923200,
                  lte: -1130371200,
                },
              },
              {
                range: {
                  gte: -1121472000,
                  lte: -1119312000,
                },
              },
              {
                range: {
                  gte: -1110067200,
                  lte: -1094169600,
                },
              },
              {
                range: {
                  gte: -1087948800,
                  lte: -1078185600,
                },
              },
              {
                range: {
                  gte: -1071360000,
                  lte: -1019260800,
                },
              },
              {
                range: {
                  gte: -1012003200,
                  lte: -967766400,
                },
              },
              {
                range: {
                  gte: -960681600,
                  lte: -880588800,
                },
              },
              {
                range: {
                  gte: -872553600,
                  lte: -851212800,
                },
              },
              {
                range: {
                  gte: -844819200,
                  lte: -795398400,
                },
              },
              {
                range: {
                  gte: -787708800,
                  lte: -782179200,
                },
              },
              {
                range: {
                  gte: -775353600,
                  lte: -252201600,
                },
              },
              {
                range: {
                  gte: -245203200,
                  lte: 1501459200,
                },
              },
            ],
          },
          {
            name: 'time',
            structural_type: 'http://schema.org/Text',
            semantic_types: ['http://schema.org/DateTime'],
            mean: 1547771400.3229058,
            stddev: 1043.3604454218018,
            coverage: [
              {
                range: {
                  gte: 1547769600,
                  lte: 1547773200,
                },
              },
            ],
          },
          {
            name: 'lat',
            structural_type: 'http://schema.org/Float',
            semantic_types: ['http://schema.org/latitude'],
            mean: 37.929474319990106,
            stddev: 2.2055594615099885,
          },
          {
            name: 'long',
            structural_type: 'http://schema.org/Float',
            semantic_types: ['http://schema.org/longitude'],
            mean: 30.77322905819139,
            stddev: 6.584459017197077,
          },
          {
            name: 'country',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'city',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'area',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'direction',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'dist',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 1.3307368684133793,
            stddev: 3.2778383911601194,
            coverage: [
              {
                range: {
                  gte: 0.1,
                  lte: 81.6,
                },
              },
              {
                range: {
                  gte: 89.5,
                  lte: 95.4,
                },
              },
            ],
          },
          {
            name: 'depth',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 18.49177323280712,
            stddev: 23.218069179391854,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 208,
                },
              },
              {
                range: {
                  gte: 225,
                  lte: 225,
                },
              },
            ],
          },
          {
            name: 'xm',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 4.056037822301977,
            stddev: 0.5740729990965191,
            coverage: [
              {
                range: {
                  gte: 3.5,
                  lte: 7.9,
                },
              },
            ],
          },
          {
            name: 'md',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 1.9123463989669087,
            stddev: 2.059737269896423,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.4,
                },
              },
            ],
          },
          {
            name: 'richter',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 2.196825925771579,
            stddev: 2.0813738783450964,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.2,
                },
              },
            ],
          },
          {
            name: 'mw',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 0.9334069229807896,
            stddev: 1.6877939363970755,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.7,
                },
              },
            ],
          },
          {
            name: 'ms',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 0.677677344107974,
            stddev: 1.6756731498756767,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.9,
                },
              },
            ],
          },
          {
            name: 'mb',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 1.6905610863498015,
            stddev: 2.146063470085694,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.1,
                },
              },
            ],
          },
        ],
        spatial_coverage: [
          {
            lat: 'lat',
            lon: 'long',
            ranges: [
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.73, 37.33], [35.06, 35.87]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36.77, 37.6], [37.34, 36.97]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.53, 34.76], [33.27, 34.11]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.97, 40.75], [29.28, 40.58]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.2, 40.3], [29.44, 40.07]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.6, 41], [31.71, 40.41]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.7, 38.34], [31.1, 37.87]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[31.15, 38.74], [31.3, 38.5]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[31.7, 38.83], [31.94, 38.34]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.44, 34.52], [26.61, 34.32]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.99, 34.43], [26.4, 34.16]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.58, 34.79], [26.71, 34.65]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[21.82, 38.66], [22.5, 37.42]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.96, 39.2], [29.19, 39.05]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.25, 41.84], [44.5, 40.8]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.99, 39.01], [43.74, 38.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.43, 40.07], [41.01, 39.78]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23, 35.67], [24.6, 34.32]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.91, 38.52], [39.37, 38.24]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.5, 38.18], [42.74, 37.97]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.34, 37.62], [42.79, 37.01]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.94, 46.3], [28.6, 43.41]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.53, 39.21], [27.01, 38.36]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.4, 35.63], [28.07, 34.98]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24, 39.25], [24.63, 38.86]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.13, 40.08], [24.65, 39.71]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.93, 40.07], [24.1, 39.91]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[46.37, 43.32], [47.4, 42.78]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.42, 38.18], [38.61, 37.99]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.03, 39.16], [44.61, 38.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.63, 38.62], [45.16, 38.37]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.26, 36.76], [26.1, 36.39]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.59, 36.17], [25.15, 35.86]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.64, 35.42], [27.24, 34.84]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.63, 37.93], [27.01, 37.58]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.5, 35.14], [34.06, 34.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.3, 37.25], [28.43, 36.77]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[41.36, 40.76], [42.49, 39.78]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.34, 35.68], [26.6, 35.3]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.73, 40.12], [33.11, 39.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.64, 39.74], [33.3, 39.13]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.85, 37.56], [27, 37.3]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23, 40.88], [23.66, 40.46]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.41, 37.11], [36.15, 36.82]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.64, 36.31], [36.36, 35.82]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.18, 41.23], [34.07, 40.66]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[22.99, 38.37], [23.47, 38.05]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.27, 38.91], [23.85, 38.5]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.52, 37.71], [30.17, 37.25]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.79, 38.42], [42.98, 38.15]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.2, 35.65], [27.33, 35.46]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.25, 36.43], [31.8, 35.18]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.59, 39.54], [28.53, 38.66]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27, 40.43], [27.51, 39.98]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.47, 36.85], [26.67, 36.64]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.23, 35.6], [32.81, 35.13]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[22.91, 37.2], [23.52, 36.06]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[31.95, 34.92], [32.66, 33.82]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.95, 35.14], [46.32, 34.83]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.06, 40.96], [42.4, 40.77]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.89, 36.41], [28.09, 36.19]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.71, 40.2], [25.01, 40]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.92, 36.48], [27.32, 35.97]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.73, 33.67], [47.4, 32.83]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[47.42, 33.44], [48, 32.32]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.48, 38.33], [26.84, 38.06]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.04, 41.26], [32.3, 41.09]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.2, 41.89], [32.67, 41.36]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.13, 34.96], [26.44, 34.63]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.54, 39.13], [30.03, 38.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.21, 39.01], [29.44, 38.94]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.84, 34.87], [25.16, 34.53]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.6, 34.93], [24.87, 34.53]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.26, 37.67], [38.8, 37.27]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.76, 36.75], [27.22, 36.42]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.72, 38.38], [38.9, 38.17]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.67, 35.06], [25.97, 34.84]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.97, 39.39], [29.32, 39.2]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.34, 39.24], [29.55, 39.08]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.9, 39.6], [26.23, 39.37]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.27, 39.54], [26.43, 39.38]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.62, 42.8], [45.6, 42.28]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.9, 38.36], [30.3, 37.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.14, 36.35], [28.31, 36.21]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.62, 42.77], [26.6, 41.5]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.83, 36.68], [28.03, 36.43]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.62, 36.41], [27.77, 36.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.5, 38.28], [29.66, 37.39]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.65, 39.79], [44.4, 39.27]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.67, 40.59], [34.11, 39.85]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.56, 40.53], [26.31, 40.22]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.02, 40.37], [25.4, 40.18]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.1, 38.34], [27.22, 38.25]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.1, 38.7], [27.33, 38.57]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.37, 38.87], [27.68, 38.55]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.35, 34.3], [25.82, 33.96]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.97, 34.48], [25.31, 33.96]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.59, 34.54], [25.74, 34.36]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[37.32, 37.76], [37.57, 37.55]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.64, 40.95], [28.19, 40.79]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.33, 35.62], [24.56, 35.3]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.52, 39.67], [28.84, 39.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.72, 39.32], [28.83, 39.19]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[20.05, 40.34], [21.51, 39.33]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.32, 43.31], [45.9, 42.88]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.36, 39.3], [36.32, 38.73]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.05, 37.07], [29.31, 36.93]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.84, 36.56], [29.14, 36.33]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.58, 39.51], [41.13, 39.23]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.9, 37.3], [30.46, 36.89]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.81, 37.31], [31.2, 36.73]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.38, 40.53], [26.5, 40.39]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26, 40.92], [26.25, 40.86]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.73, 39], [25.1, 38.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.53, 37.89], [44.45, 37.16]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36, 37.64], [36.42, 37.05]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.8, 42.89], [44.14, 41.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.34, 37.11], [29.6, 36.98]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.4, 39.12], [32.79, 38.76]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.25, 36.75], [27.42, 36.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[46, 39.5], [46.31, 38.91]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.33, 39.39], [23.94, 38.99]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.28, 40.85], [27.66, 40.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.99, 38.54], [25.32, 38.26]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.4, 38.78], [25.55, 38.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.32, 38.63], [25.53, 38.43]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.25, 35.58], [28.72, 34.65]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23, 42.05], [23.5, 41.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.56, 37.6], [35.86, 37.43]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.17, 40.09], [30.7, 39.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23, 39.4], [23.28, 39.17]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.38, 40], [23.59, 39.88]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.72, 40.88], [33.17, 40.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[41.25, 39.38], [41.89, 39.02]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.47, 37.14], [28.65, 37.04]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.89, 40.52], [29.21, 40.38]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[34.56, 34.08], [35.65, 32.76]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.49, 36.88], [41.69, 36]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.9, 35.17], [29.14, 34.76]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.38, 35.98], [30.04, 35.72]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.26, 39.87], [25.72, 39.45]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.13, 39.48], [25.81, 39.04]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.79, 43.61], [41.58, 41.93]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[37.75, 39.1], [38.24, 38.57]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.4, 36.9], [24.63, 36.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.99, 43.55], [46.32, 43.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.23, 38.54], [43.39, 38.45]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.29, 40.46], [33.45, 40.26]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.42, 43.4], [28.1, 42.61]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.26, 39.16], [40.56, 38.91]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.13, 39.47], [40.34, 39.14]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.15, 37.96], [27.43, 37.8]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.47, 37.98], [27.7, 37.77]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.61, 44.91], [34.99, 44]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.4, 38.74], [40.9, 38.33]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.78, 38.76], [31.08, 38.59]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[22.69, 38.83], [23, 38.7]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.7, 39.41], [25.05, 39.25]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.8, 38.89], [40.26, 38.64]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.5, 38.66], [39.74, 38.5]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[34.62, 40.76], [34.99, 40.46]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.2, 35.85], [27.3, 35.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.28, 34.65], [45.73, 34.28]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32, 37.98], [32.72, 37.57]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.6, 42.99], [36.55, 42.07]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.15, 39.17], [42.72, 38.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.15, 40.93], [35.55, 40.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.77, 40.81], [36.1, 40.47]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.62, 39.95], [42.9, 39.57]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.13, 37.01], [43.49, 36.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.06, 38.56], [23.22, 38.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.71, 38.55], [43.91, 38.46]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36.47, 40.89], [37.4, 40.15]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.86, 37.11], [24.2, 36.8]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.16, 35.02], [25.54, 34.78]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.9, 38.24], [27.05, 38.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36.4, 39.56], [36.86, 39.3]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.57, 37.24], [45.17, 36.73]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.4, 35.88], [27.51, 35.69]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.43, 39.89], [39.67, 39.69]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.66, 39.66], [39.98, 39.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.76, 40.38], [28.06, 40.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.9, 41.94], [46.73, 41.43]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.14, 35.43], [26.44, 35]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[46.34, 38.59], [47.23, 37.84]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.48, 36], [45.97, 35.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.64, 40.03], [39.89, 39.94]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.53, 39.79], [39, 39.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.8, 37.14], [28.95, 36.96]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.7, 37.18], [28.8, 37.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.2, 44.34], [39.93, 43.32]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.6, 39.13], [28.76, 39.03]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.1, 38.76], [44.44, 38.53]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36.76, 44.91], [38.01, 44.45]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[20, 38.83], [20.96, 37.7]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.5, 36.64], [28.79, 36.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.36, 34.47], [25.5, 34.34]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.89, 35.87], [24.1, 35.67]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.67, 36.13], [29.72, 36.01]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.34, 36.06], [27.47, 35.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.98, 36.1], [29.16, 35.98]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.22, 36.63], [28.32, 36.45]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.01, 40.16], [24.09, 40.07]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.68, 39.68], [23.81, 39.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.53, 38.35], [34.39, 37.73]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[41, 39.1], [41.21, 38.97]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.81, 39.73], [27.94, 39.62]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[37.76, 38.06], [38, 37.91]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[34.25, 41.28], [34.44, 41.02]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.99, 36.82], [30.13, 36.61]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.82, 36.23], [28.92, 36.1]],
                },
              },
            ],
          },
        ],
        materialize: {
          identifier: 'datamart.url',
          direct_url:
            'https://drive.google.com/uc?export=download&id=1XnUPt196hg7_4KFMvMCIrDMxDheSH7jM',
        },
        date: '2019-01-18T21:18:20.123783Z',
      },
      join_columns: [],
      union_columns: [],
    },
    {
      id: 'datamart.upload.437d8904c08f41b587dd12f890cdc75a',
      score: 4.5041327,
      discoverer: 'datamart.upload',
      metadata: {
        date: '2019-01-17T19:30:34.296771Z',
        description:
          'The data covers up all the recorded earthquakes in the latitudes between 25 - 50; longitudes 15 - 60...',
        filename: 'earthquake.csv',
        name: 'Earthquake Data of Turkey in 1910-2017',
        nb_rows: 24007,
        size: 2211652,
        columns: [
          {
            name: 'id',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'date',
            structural_type: 'http://schema.org/Text',
            semantic_types: [
              'http://schema.org/DateTime',
              'https://metadata.datadrivendiscovery.org/types/PhoneNumber',
            ],
            mean: 690523674.4616153,
            stddev: 642816976.79487,
            coverage: [
              {
                range: {
                  gte: -1864339200,
                  lte: -1864339200,
                },
              },
              {
                range: {
                  gte: -1855958400,
                  lte: -1846972800,
                },
              },
              {
                range: {
                  gte: -1840320000,
                  lte: -1836518400,
                },
              },
              {
                range: {
                  gte: -1823731200,
                  lte: -1803513600,
                },
              },
              {
                range: {
                  gte: -1796515200,
                  lte: -1796515200,
                },
              },
              {
                range: {
                  gte: -1789344000,
                  lte: -1719273600,
                },
              },
              {
                range: {
                  gte: -1713139200,
                  lte: -1544054400,
                },
              },
              {
                range: {
                  gte: -1537488000,
                  lte: -1521763200,
                },
              },
              {
                range: {
                  gte: -1512086400,
                  lte: -1485475200,
                },
              },
              {
                range: {
                  gte: -1477353600,
                  lte: -1397433600,
                },
              },
              {
                range: {
                  gte: -1389744000,
                  lte: -1227657600,
                },
              },
              {
                range: {
                  gte: -1221350400,
                  lte: -1206144000,
                },
              },
              {
                range: {
                  gte: -1199923200,
                  lte: -1130371200,
                },
              },
              {
                range: {
                  gte: -1121472000,
                  lte: -1119312000,
                },
              },
              {
                range: {
                  gte: -1110067200,
                  lte: -1094169600,
                },
              },
              {
                range: {
                  gte: -1087948800,
                  lte: -1078185600,
                },
              },
              {
                range: {
                  gte: -1071360000,
                  lte: -1019260800,
                },
              },
              {
                range: {
                  gte: -1012003200,
                  lte: -967766400,
                },
              },
              {
                range: {
                  gte: -960681600,
                  lte: -880588800,
                },
              },
              {
                range: {
                  gte: -872553600,
                  lte: -851212800,
                },
              },
              {
                range: {
                  gte: -844819200,
                  lte: -795398400,
                },
              },
              {
                range: {
                  gte: -787708800,
                  lte: -782179200,
                },
              },
              {
                range: {
                  gte: -775353600,
                  lte: -252201600,
                },
              },
              {
                range: {
                  gte: -245203200,
                  lte: 1501459200,
                },
              },
            ],
          },
          {
            name: 'time',
            structural_type: 'http://schema.org/Text',
            semantic_types: ['http://schema.org/DateTime'],
            mean: 1547685000.3282375,
            stddev: 1043.2868134301443,
            coverage: [
              {
                range: {
                  gte: 1547683200,
                  lte: 1547686800,
                },
              },
            ],
          },
          {
            name: 'lat',
            structural_type: 'http://schema.org/Float',
            semantic_types: ['http://schema.org/latitude'],
            mean: 37.929474319990106,
            stddev: 2.2055594615099885,
          },
          {
            name: 'long',
            structural_type: 'http://schema.org/Float',
            semantic_types: ['http://schema.org/longitude'],
            mean: 30.77322905819139,
            stddev: 6.584459017197077,
          },
          {
            name: 'country',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'city',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'area',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'direction',
            structural_type: 'http://schema.org/Text',
            semantic_types: [],
          },
          {
            name: 'dist',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 1.3307368684133793,
            stddev: 3.2778383911601194,
            coverage: [
              {
                range: {
                  gte: 0.1,
                  lte: 81.6,
                },
              },
              {
                range: {
                  gte: 89.5,
                  lte: 95.4,
                },
              },
            ],
          },
          {
            name: 'depth',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 18.49177323280712,
            stddev: 23.218069179391854,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 208,
                },
              },
              {
                range: {
                  gte: 225,
                  lte: 225,
                },
              },
            ],
          },
          {
            name: 'xm',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 4.056037822301977,
            stddev: 0.5740729990965191,
            coverage: [
              {
                range: {
                  gte: 3.5,
                  lte: 7.9,
                },
              },
            ],
          },
          {
            name: 'md',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 1.9123463989669087,
            stddev: 2.059737269896423,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.4,
                },
              },
            ],
          },
          {
            name: 'richter',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 2.196825925771579,
            stddev: 2.0813738783450964,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.2,
                },
              },
            ],
          },
          {
            name: 'mw',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 0.9334069229807896,
            stddev: 1.6877939363970755,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.7,
                },
              },
            ],
          },
          {
            name: 'ms',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 0.677677344107974,
            stddev: 1.6756731498756767,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.9,
                },
              },
            ],
          },
          {
            name: 'mb',
            structural_type: 'http://schema.org/Float',
            semantic_types: [],
            mean: 1.6905610863498015,
            stddev: 2.146063470085694,
            coverage: [
              {
                range: {
                  gte: 0,
                  lte: 7.1,
                },
              },
            ],
          },
        ],
        spatial_coverage: [
          {
            lat: 'lat',
            lon: 'long',
            ranges: [
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.73, 37.33], [35.06, 35.87]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36.77, 37.6], [37.34, 36.97]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.53, 34.76], [33.27, 34.11]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.97, 40.75], [29.28, 40.58]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.2, 40.3], [29.44, 40.07]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.6, 41], [31.71, 40.41]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.7, 38.34], [31.1, 37.87]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[31.15, 38.74], [31.3, 38.5]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[31.7, 38.83], [31.94, 38.34]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.44, 34.52], [26.61, 34.32]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.99, 34.43], [26.4, 34.16]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.58, 34.79], [26.71, 34.65]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[21.82, 38.66], [22.5, 37.42]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.96, 39.2], [29.19, 39.05]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.25, 41.84], [44.5, 40.8]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.99, 39.01], [43.74, 38.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.43, 40.07], [41.01, 39.78]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23, 35.67], [24.6, 34.32]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.91, 38.52], [39.37, 38.24]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.5, 38.18], [42.74, 37.97]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.34, 37.62], [42.79, 37.01]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.94, 46.3], [28.6, 43.41]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.53, 39.21], [27.01, 38.36]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.4, 35.63], [28.07, 34.98]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24, 39.25], [24.63, 38.86]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.13, 40.08], [24.65, 39.71]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.93, 40.07], [24.1, 39.91]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[46.37, 43.32], [47.4, 42.78]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.42, 38.18], [38.61, 37.99]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.03, 39.16], [44.61, 38.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.63, 38.62], [45.16, 38.37]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.26, 36.76], [26.1, 36.39]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.59, 36.17], [25.15, 35.86]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.64, 35.42], [27.24, 34.84]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.63, 37.93], [27.01, 37.58]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.5, 35.14], [34.06, 34.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.3, 37.25], [28.43, 36.77]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[41.36, 40.76], [42.49, 39.78]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.34, 35.68], [26.6, 35.3]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.73, 40.12], [33.11, 39.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.64, 39.74], [33.3, 39.13]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.85, 37.56], [27, 37.3]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23, 40.88], [23.66, 40.46]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.41, 37.11], [36.15, 36.82]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.64, 36.31], [36.36, 35.82]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.18, 41.23], [34.07, 40.66]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[22.99, 38.37], [23.47, 38.05]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.27, 38.91], [23.85, 38.5]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.52, 37.71], [30.17, 37.25]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.79, 38.42], [42.98, 38.15]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.2, 35.65], [27.33, 35.46]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.25, 36.43], [31.8, 35.18]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.59, 39.54], [28.53, 38.66]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27, 40.43], [27.51, 39.98]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.47, 36.85], [26.67, 36.64]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.23, 35.6], [32.81, 35.13]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[22.91, 37.2], [23.52, 36.06]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[31.95, 34.92], [32.66, 33.82]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.95, 35.14], [46.32, 34.83]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.06, 40.96], [42.4, 40.77]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.89, 36.41], [28.09, 36.19]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.71, 40.2], [25.01, 40]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.92, 36.48], [27.32, 35.97]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.73, 33.67], [47.4, 32.83]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[47.42, 33.44], [48, 32.32]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.48, 38.33], [26.84, 38.06]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.04, 41.26], [32.3, 41.09]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.2, 41.89], [32.67, 41.36]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.13, 34.96], [26.44, 34.63]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.54, 39.13], [30.03, 38.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.21, 39.01], [29.44, 38.94]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.84, 34.87], [25.16, 34.53]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.6, 34.93], [24.87, 34.53]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.26, 37.67], [38.8, 37.27]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.76, 36.75], [27.22, 36.42]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.72, 38.38], [38.9, 38.17]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.67, 35.06], [25.97, 34.84]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.97, 39.39], [29.32, 39.2]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.34, 39.24], [29.55, 39.08]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.9, 39.6], [26.23, 39.37]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.27, 39.54], [26.43, 39.38]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.62, 42.8], [45.6, 42.28]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.9, 38.36], [30.3, 37.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.14, 36.35], [28.31, 36.21]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.62, 42.77], [26.6, 41.5]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.83, 36.68], [28.03, 36.43]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.62, 36.41], [27.77, 36.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.5, 38.28], [29.66, 37.39]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.65, 39.79], [44.4, 39.27]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.67, 40.59], [34.11, 39.85]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.56, 40.53], [26.31, 40.22]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.02, 40.37], [25.4, 40.18]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.1, 38.34], [27.22, 38.25]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.1, 38.7], [27.33, 38.57]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.37, 38.87], [27.68, 38.55]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.35, 34.3], [25.82, 33.96]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.97, 34.48], [25.31, 33.96]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.59, 34.54], [25.74, 34.36]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[37.32, 37.76], [37.57, 37.55]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.64, 40.95], [28.19, 40.79]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.33, 35.62], [24.56, 35.3]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.52, 39.67], [28.84, 39.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.72, 39.32], [28.83, 39.19]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[20.05, 40.34], [21.51, 39.33]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.32, 43.31], [45.9, 42.88]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.36, 39.3], [36.32, 38.73]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.05, 37.07], [29.31, 36.93]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.84, 36.56], [29.14, 36.33]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.58, 39.51], [41.13, 39.23]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.9, 37.3], [30.46, 36.89]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.81, 37.31], [31.2, 36.73]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.38, 40.53], [26.5, 40.39]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26, 40.92], [26.25, 40.86]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.73, 39], [25.1, 38.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.53, 37.89], [44.45, 37.16]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36, 37.64], [36.42, 37.05]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.8, 42.89], [44.14, 41.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.34, 37.11], [29.6, 36.98]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.4, 39.12], [32.79, 38.76]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.25, 36.75], [27.42, 36.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[46, 39.5], [46.31, 38.91]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.33, 39.39], [23.94, 38.99]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.28, 40.85], [27.66, 40.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.99, 38.54], [25.32, 38.26]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.4, 38.78], [25.55, 38.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.32, 38.63], [25.53, 38.43]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.25, 35.58], [28.72, 34.65]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23, 42.05], [23.5, 41.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.56, 37.6], [35.86, 37.43]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.17, 40.09], [30.7, 39.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23, 39.4], [23.28, 39.17]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.38, 40], [23.59, 39.88]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32.72, 40.88], [33.17, 40.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[41.25, 39.38], [41.89, 39.02]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.47, 37.14], [28.65, 37.04]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.89, 40.52], [29.21, 40.38]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[34.56, 34.08], [35.65, 32.76]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.49, 36.88], [41.69, 36]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.9, 35.17], [29.14, 34.76]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.38, 35.98], [30.04, 35.72]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.26, 39.87], [25.72, 39.45]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.13, 39.48], [25.81, 39.04]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.79, 43.61], [41.58, 41.93]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[37.75, 39.1], [38.24, 38.57]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.4, 36.9], [24.63, 36.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.99, 43.55], [46.32, 43.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.23, 38.54], [43.39, 38.45]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.29, 40.46], [33.45, 40.26]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.42, 43.4], [28.1, 42.61]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.26, 39.16], [40.56, 38.91]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.13, 39.47], [40.34, 39.14]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.15, 37.96], [27.43, 37.8]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.47, 37.98], [27.7, 37.77]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.61, 44.91], [34.99, 44]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[40.4, 38.74], [40.9, 38.33]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[30.78, 38.76], [31.08, 38.59]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[22.69, 38.83], [23, 38.7]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.7, 39.41], [25.05, 39.25]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.8, 38.89], [40.26, 38.64]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.5, 38.66], [39.74, 38.5]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[34.62, 40.76], [34.99, 40.46]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.2, 35.85], [27.3, 35.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.28, 34.65], [45.73, 34.28]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[32, 37.98], [32.72, 37.57]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.6, 42.99], [36.55, 42.07]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.15, 39.17], [42.72, 38.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.15, 40.93], [35.55, 40.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[35.77, 40.81], [36.1, 40.47]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[42.62, 39.95], [42.9, 39.57]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.13, 37.01], [43.49, 36.68]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.06, 38.56], [23.22, 38.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[43.71, 38.55], [43.91, 38.46]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36.47, 40.89], [37.4, 40.15]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.86, 37.11], [24.2, 36.8]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.16, 35.02], [25.54, 34.78]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.9, 38.24], [27.05, 38.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36.4, 39.56], [36.86, 39.3]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.57, 37.24], [45.17, 36.73]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.4, 35.88], [27.51, 35.69]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.43, 39.89], [39.67, 39.69]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.66, 39.66], [39.98, 39.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.76, 40.38], [28.06, 40.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.9, 41.94], [46.73, 41.43]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[26.14, 35.43], [26.44, 35]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[46.34, 38.59], [47.23, 37.84]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[45.48, 36], [45.97, 35.6]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.64, 40.03], [39.89, 39.94]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[38.53, 39.79], [39, 39.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.8, 37.14], [28.95, 36.96]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.7, 37.18], [28.8, 37.1]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[39.2, 44.34], [39.93, 43.32]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.6, 39.13], [28.76, 39.03]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[44.1, 38.76], [44.44, 38.53]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[36.76, 44.91], [38.01, 44.45]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[20, 38.83], [20.96, 37.7]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.5, 36.64], [28.79, 36.4]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[25.36, 34.47], [25.5, 34.34]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.89, 35.87], [24.1, 35.67]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.67, 36.13], [29.72, 36.01]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.34, 36.06], [27.47, 35.9]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.98, 36.1], [29.16, 35.98]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.22, 36.63], [28.32, 36.45]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[24.01, 40.16], [24.09, 40.07]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[23.68, 39.68], [23.81, 39.54]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[33.53, 38.35], [34.39, 37.73]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[41, 39.1], [41.21, 38.97]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[27.81, 39.73], [27.94, 39.62]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[37.76, 38.06], [38, 37.91]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[34.25, 41.28], [34.44, 41.02]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[29.99, 36.82], [30.13, 36.61]],
                },
              },
              {
                range: {
                  type: 'envelope',
                  coordinates: [[28.82, 36.23], [28.92, 36.1]],
                },
              },
            ],
          },
        ],
        materialize: {
          identifier: 'datamart.upload',
        },
      },
      join_columns: [],
      union_columns: [],
    },
  ],
};
