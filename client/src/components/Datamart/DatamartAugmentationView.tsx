import * as React from 'react';
import * as Icon from 'react-feather';
import * as d3m from 'd3m/dist';
import { searchDatamart, augmentData } from '../../data/Api';
import PersistentComponent from '../PersistentComponent';
import { connectPropsToDatastore } from 'src/data/store';
import { HelpText } from '../HelpText/HelpText';
import Loading from '../Loading/Loading';
import { GreenSpan } from '../styled';

enum SearchState {
  UNKNOWN,
  SEARCH_REQUESTING,
  SEARCH_SUCCESS,
  SEARCH_FAILED,
  AUGMENT_REQUESTING,
  AUGMENT_FAILED,
  AUGMENT_SUCCESS,
}

interface DataAugmentationViewState {
  searchState: SearchState;
  searchHits?: d3m.DatamartResult[];
}

interface DatamartHitProps {
  hit: d3m.DatamartResult;
  augment: (hit: d3m.DatamartResult) => void;
  clientConfig: d3m.ClientConfig;
}

interface DatamartHitState {
  hidden: boolean;
}

function formatSize(size: number) {
  const units = ['bytes', 'kb', 'mb', 'gb', 'tb'];
  for (let i = 0; i < units.length; i++ ) {
    const unitMultiplier = Math.pow(1024, i);
    const unitMaximum = unitMultiplier * 1024;
    if (size < unitMaximum) {
      return `${(size / unitMultiplier).toFixed(1)} ${units[i]}`;
    }
  }
  return size;
}

class DatamartHit extends React.PureComponent<DatamartHitProps, DatamartHitState> {

  constructor(props: DatamartHitProps) {
    super(props);
    this.state = {
      hidden: true,
    };
  }

  splitColumns(columns: { name: string }[] ) {
    const visibleColumns: string[] = [];
    const hiddenColumns: string[] = [];
    const maxLength = 100;
    let characters = 0;
    columns.forEach(c => {
      if (characters + c.name.length > maxLength) {
        hiddenColumns.push(c.name);
      } else {
        visibleColumns.push(c.name);
        characters += c.name.length + 5; // add 5 extra to account for extra space of badges
      }
    });
    return { visibleColumns, hiddenColumns };
  }

  render() {
    const { hit } = this.props;
    const { visibleColumns, hiddenColumns} = this.splitColumns(hit.metadata.columns);
    const augInfo = hit.augmentation ? {
      type: hit.augmentation.type,
      leftColumn: hit.augmentation.left_columns_names[0][0],
      rightColumn: hit.metadata.columns[hit.augmentation.right_columns[0][0]].name,
    } : undefined;
    return (
      <div className="card mb-4 shadow-sm">
        <div className="card-body d-flex flex-column">
          <span className="text-primary" style={{fontSize: '1.1rem', fontFamily: 'Source Sans Pro'}}>
            {hit.metadata.name}
          </span>
          <span className="small text-muted">
            {formatSize(hit.metadata.size)}
          </span>
          <div className="row">
            <div className="col-md-8 mt-2">
              <span className="text-muted">
                {hit.metadata.description}
              </span>
              <div className="mt-2">
                Attributes:&nbsp;
                {visibleColumns.map((cname) => (
                  <span
                    key={`${hit.id}-${cname}`}
                    className="badge badge-pill badge-secondary mr-1"
                  >
                    {cname}
                  </span>
                ))}
                {
                  !this.state.hidden &&
                  hiddenColumns.map((cname) => (
                    <span
                      key={`${hit.id}-${cname}`}
                      className="badge badge-pill badge-secondary mr-1"
                    >
                      {cname}
                    </span>
                  ))
                }
                {
                  hiddenColumns.length > 0 &&
                  <a
                    className="text-muted small" style={{cursor: 'pointer', textDecoration: 'underline'}}
                    onClick={() => this.setState({hidden: !this.state.hidden})}
                  >
                    {this.state.hidden ? `Show ${hiddenColumns.length} more...` : 'Hide'}
                  </a>
                }
              </div>
              <div className="mt-2">
                <button
                  onClick={() => this.props.augment(hit)}
                  className="btn btn-sm btn-primary"
                >
                  <Icon.PlusSquare className="feather" />&nbsp;
                  { (augInfo && augInfo.type === 'join') && 'Add Features' }
                  { (augInfo && augInfo.type === 'union') && 'Add Instances'}
                </button>
                {/* <a
                  href={`${this.props.clientConfig.env_vars.DATAMART_URL}/dataset/${hit.id}`}
                  className="btn btn-sm btn-outline-primary ml-2"
                  target="_blank"
                >
                  <Icon.Info className="feather" /> View Details
                </a> */}
              </div>
            </div>
            <div className="col-md-4 mt-2">
              {
                // if there is data augmentation information available
                augInfo &&
                <>
                  {
                    augInfo.type === 'join' &&
                    <HelpText>
                      We can <GreenSpan>add features</GreenSpan> to your
                      dataset by joining instances on common values of the
                      following attributes:<br/>
                      &#8226;  <span className="badge badge-pill badge-secondary">{augInfo.leftColumn}</span>
                      &nbsp;from <em>your current dataset</em><br/>
                      &#8226;  <span className="badge badge-pill badge-secondary">{augInfo.rightColumn}</span>
                      &nbsp;from <em>this search result</em>.
                    </HelpText>
                  }
                  {
                    augInfo.type === 'union' &&
                    <HelpText>
                    We can <GreenSpan>add instances</GreenSpan> to your
                    dataset by merging datasets and <b>keeping only</b>&nbsp;
                    the following common attributes in the dataset:<br/>
                    {
                      hit.augmentation &&
                      hit.augmentation.left_columns_names.map((lc, i) => {
                        return <>
                          &#8226;  <span className="badge badge-pill badge-secondary">
                            {lc}
                          </span>, which matches
                          <span className="badge badge-pill badge-secondary">
                            {hit.augmentation!.right_columns_names[i]}
                          </span><br />
                        </>;
                      })
                    }
                    </HelpText>
                  }
                </>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

interface DataAugmentationViewProps {
  datasets: d3m.Dataset[];
  problem: d3m.Problem;
  dataset: d3m.Dataset;
  clientConfig: d3m.ClientConfig;
  onAugment: (datasetID: string) => void;
}

class DataAugmentationView extends PersistentComponent<DataAugmentationViewProps, DataAugmentationViewState> {
  constructor(props: DataAugmentationViewProps) {
    super(props);
    this.state = {
      searchState: SearchState.UNKNOWN,
    };
  }

  augment = (hit: d3m.DatamartResult) => {
    this.setState({searchState: SearchState.AUGMENT_REQUESTING});
    return augmentData({
      datasetID: this.props.dataset.about.datasetID,
      result: hit,
    })
    .then(r => {
      this.setState({searchState: SearchState.AUGMENT_SUCCESS});
      this.props.onAugment(r.datasetID);
    })
    .catch(() => {
      this.setState({searchState: SearchState.AUGMENT_FAILED});
    });
  };

  componentDidMount = () => {
    if (this.state.searchState === SearchState.UNKNOWN) {
      this.search();
    }
  }

  search(query?: string) {
    this.setState({searchState: SearchState.SEARCH_REQUESTING});
    searchDatamart({
      query: query,
      datasetID: this.props.dataset.about.datasetID,
    })
    .then((res) => {
      console.log(res);
      this.setState({
        searchHits: res.results,
        searchState: SearchState.SEARCH_SUCCESS,
      });
    })
    .catch(() => {
      this.setState({
        searchHits: undefined,
        searchState: SearchState.SEARCH_FAILED,
      });
    });
  }

  renderSearchResults() {
    switch (this.state.searchState) {
      case SearchState.SEARCH_REQUESTING: {
        return (
          <div className="col-md-12" style={{ textAlign: 'center' }}>
            <Loading message="Searching..." />
          </div>
        );
      }
      case SearchState.SEARCH_FAILED: {
        return (
          <div className="col-md-12" style={{ textAlign: 'center' }}>
            <Icon.XCircle className="feather" />&nbsp;
            Search failed. Please try again later.
          </div>
        );
      }
      case SearchState.SEARCH_SUCCESS: {
        const { searchHits } = this.state;
        if (!(searchHits && searchHits.length > 0)) {
          return (
            <div className="col-md-12" style={{ textAlign: 'center' }}>
              <Icon.AlertCircle className="feather" />&nbsp;
              Sorry, no datasets found for you query.
            </div>
          );
        }
        // TODO: Implement proper results pagination
        const page = 1;
        const k = 20;
        const currentHits = searchHits.slice((page - 1) * k, page * k);
        return (
          currentHits
          .filter((hit, idx) => {
            // TODO: We are temporarily filtering out 'union' results
            // until we get better search results from datamart
            return hit.augmentation && hit.augmentation.type === 'join';
          })
          .map((hit, idx) => (
            <div className="col-md-12" key={idx}>
              <DatamartHit
                clientConfig={this.props.clientConfig}
                hit={hit}
                augment={this.augment}
              />
            </div>
          ))
        );
      }
      case SearchState.AUGMENT_REQUESTING: {
        return (
          <div className="col-md-12" style={{ textAlign: 'center' }}>
            <Loading message="Augmenting dataset..." />
          </div>
        );
      }
      case SearchState.AUGMENT_FAILED: {
        return (
          <div className="col-md-12" style={{ textAlign: 'center' }}>
            <Icon.XCircle className="feather" />&nbsp;
            Failed to augment your dataset with selected search result.
            Please try again later or with another search result.
          </div>
        );
      }
      case SearchState.AUGMENT_SUCCESS: {
        return (
          <div className="col-md-12" style={{ textAlign: 'center' }}>
            <Icon.Check className="feather" />&nbsp;
            Data augmentation completed successfully.
          </div>
        );
      }
      default: {
        // search is triggered automatically when the data augmentation
        // tag is opened, so this shouldn't show up for long time
        return <div />;
      }
    }
  }

  render() {
    return (
      <div>
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text" id="basic-addon1">
              <Icon.Search className="feather" />
            </span>
          </div>
          <input
            onKeyPress={e => {
              if (e.key === 'Enter') {
                const value = (e.target as HTMLInputElement).value;
                this.search(value);
              }
            }}
            type="text"
            className="form-control"
            placeholder="Search..."
            aria-label="Search..."
          />
        </div>
        <div
          className={'text-muted small'}
          style={{marginTop: '-15px', marginBottom: '15px', textAlign: 'right'}}>
          Powered by Auctus Data Search
        </div>
        <div className="row" id="results">
          {this.renderSearchResults()}
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<DataAugmentationViewProps>((store) => ({
  clientConfig: store.clientConfig,
  selectDataset: store.selectDataset,
  datasets: store.datasets.value,
  dataset: store.selectedDataset,
  problem: store.selectedProblem,
}))(DataAugmentationView);
