import * as React from 'react';
import * as Icon from 'react-feather';
import { select, event as d3Event } from 'd3-selection';
import 'd3-transition'; // must import d3-transition otherwise selection.transition does not work properly
import { axisLeft } from 'd3-axis';
import { line } from 'd3-shape';
import { scaleLinear, ScaleLinear } from 'd3-scale';
import * as d3m from 'd3m/dist';
import { getTransform, areSegmentsIntersected } from 'src/utils';
import { LIGHT_NYU_PURPLE } from 'src/common';
import { FlatButton } from '../../styled';
import './SolutionParallelCoordinates.css';
import { connectPropsToDatastore } from 'src/data/store';
import { formatValue, normalize } from 'src/data/utils';

const DEFAULT_MARGINS = { left: 100, right: 100, top: 10, bottom: 40 };
const DEFAULT_HEIGHT_PX = 200;
const DOMAIN_MARGIN_PCT = .1; // domain margin percentage at both sides of the axes
const LABEL_OFFSET_PX = 20;

interface Metric {
  name: string;
  shouldAxisBeInverted: boolean;
  isZeroMinMetric: boolean;
  isZeroOneMetric: boolean;
  label: string;
  accessor: (solution: d3m.Solution) => number;
}

interface SolutionParallelCoordinatesProps {
  solutions: d3m.Solution[];
  hoveredSolution?: string; // id
  margins?: Margins;
  isFavoriteSolution: (id: string) => boolean;
  toggleFavoriteSolution: (id: string) => void;
  removeSolution: (id: string) => void;
}

interface SolutionParallelCoordinatesState {
  metrics: Metric[];
  svgWidth: number;
  svgHeight: number;
  selectedSolutions: Set<string>; // selected solution id's
  brushPoints: Point[];
}

class SolutionParallelCoordinates extends React.PureComponent<SolutionParallelCoordinatesProps,
  SolutionParallelCoordinatesState> {
  private xScale: ScaleLinear<number, number> = scaleLinear();
  private yScales: ScaleLinear<number, number>[] = [];

  private margins: Margins = DEFAULT_MARGINS;
  private svgRef: React.RefObject<SVGSVGElement>;
  private isBrushing = false;
  private listenersSet = false;

  constructor(props: SolutionParallelCoordinatesProps) {
    super(props);
    this.svgRef = React.createRef();
    this.state = {
      svgWidth: 500,
      svgHeight: 200,
      metrics: [],
      brushPoints: [],
      selectedSolutions: new Set(),
    };
  }

  componentWillReceiveProps(nextProps: SolutionParallelCoordinatesProps) {
    this.prepare(nextProps);
  }

  componentDidMount() {
    this.prepare(this.props);
    window.addEventListener('resize', () => this.updateSize(this.margins));
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.updateSize(this.margins));
  }

  prepare(props: SolutionParallelCoordinatesProps) {
    this.computeMetrics(props.solutions);
    this.updateSize(props.margins);
  }

  updateSize = (margins?: Margins) => {
    if (this.svgRef.current === null) {
      return;
    }
    this.margins = Object.assign(DEFAULT_MARGINS, margins);
    const container = this.svgRef.current;
    const bbox = (container.parentElement as HTMLElement).getBoundingClientRect();
    this.setState({
      svgWidth: bbox.width,
      svgHeight: bbox.height,
    });
  }

  computeScales = (solutions: d3m.Solution[]) => {
    this.xScale = scaleLinear()
      .domain([0, this.state.metrics.length - 1])
      .range([this.margins.left, this.state.svgWidth - this.margins.right]);

    this.yScales = this.state.metrics.map(metric => {
      const scores = solutions.map(solution => metric.accessor(solution)).sort((a, b) => a - b);
      const range = metric.shouldAxisBeInverted ?
        [this.margins.top, this.state.svgHeight - this.margins.bottom] :
        [this.state.svgHeight - this.margins.bottom, this.margins.top];
      let [scoreMin, scoreMax] = [scores[0], scores[scores.length - 1]];

      // Add some margin to both sides of the axes.
      // If the margin results in the extremums falling out of the metric's range (lowerbound 0, or upperbound 1),
      // then force the extremums to the metric's lower and upper bounds.
      const scoreSpan = scoreMax - scoreMin;
      scoreMin -= scoreSpan * DOMAIN_MARGIN_PCT;
      scoreMax += scoreSpan * DOMAIN_MARGIN_PCT;
      if (metric.isZeroMinMetric && scoreMin < 0) {
        scoreMin = 0;
      }
      if (metric.isZeroOneMetric) {
        scoreMin = scoreMin < 0 ? 0 : scoreMin;
        scoreMax = scoreMax > 1 ? 1 : scoreMax;
      }
      return scaleLinear()
        .domain([scoreMin, scoreMax])
        .range(range);
    });
  }

  computeMetrics = (solutions: d3m.Solution[]) => {
    if (!solutions.length) {
      this.setState({ metrics: [] });
      return;
    }
    let metrics: Metric[] = [ ];
    if (solutions.length) {
      const firstSolutionScores = solutions[0].scores!.scores!;
      const metricNames = Object.keys(firstSolutionScores);
      metrics = metrics.concat(metricNames.map((metricName) => ({
        name: metricName,
        label: d3m.translate.ta2MetricName(d3m.ta2.PerformanceMetric[metricName]),
        isZeroMinMetric: d3m.metrics.isZeroMinMetric(d3m.ta2.PerformanceMetric[metricName]),
        isZeroOneMetric: d3m.metrics.isZeroOneMetric(d3m.ta2.PerformanceMetric[metricName]),
        shouldAxisBeInverted: d3m.metrics.isErrorMetric(d3m.ta2.PerformanceMetric[metricName]),
        accessor: (s: d3m.Solution) => s.scores!.scores![metricName].score,
      })));
    }
    this.setState({ metrics });
  }

  renderAxes = () => {
    const exitAxes = select(this.svgRef.current).select('#axes')
      .selectAll<SVGGElement, Metric>('g.axis')
      .data(this.state.metrics, metric => metric.name)
      .exit();
    exitAxes.transition().style('opacity', 0).remove();
    this.state.metrics.forEach((metric, metricIndex) => {
      this.renderAxis(metricIndex, metric);
    });
  }

  renderAxis = (axisIndex: number, metric: Metric) => {
    const yScale = this.yScales[axisIndex];
    let [domainMin, domainMax] = yScale.domain();
    const id = 'axis-' + normalize(metric.name);
    const axis = axisLeft(yScale)
      // when domainMin === domainMax the d3 formatting of the value loses precision and is misleading
      .tickValues(domainMin === domainMax ? [] : [domainMin, domainMax])
      .tickFormat(v => formatValue(+v));
    const svgAxes = select(this.svgRef.current).select('#axes');
    let g = svgAxes.select<SVGGElement>('#' + id);
    const gTransform = getTransform([this.xScale(axisIndex), 0]);
    if (g.empty()) {
      g = svgAxes.append<SVGGElement>('g').datum(metric.name)
        .attr('id', id)
        .classed('axis', true)
        .attr('transform', gTransform);
    }
    g.call(axis);
    const updatedG = g.transition();
    updatedG
      .style('opacity', 1)
      .attr('transform', gTransform);

    let label = g.select<SVGTextElement>('.label');
    const labelTransform = getTransform([0, this.state.svgHeight - LABEL_OFFSET_PX]);
    if (label.empty()) {
      label = g.append('text')
        .classed('label', true)
        .style('text-anchor', 'middle')
        .attr('transform', labelTransform);
    }
    const updatedLabel = label.transition();
    updatedLabel
      .style('opacity', 1)
      .attr('transform', labelTransform)
      .text(metric.label + (metric.shouldAxisBeInverted ? '▼' : ''));
  }

  renderLines() {
    const l = line<number>()
      .x((v, axisIndex) => this.xScale(axisIndex))
      .y((v, axisIndex) => this.yScales[axisIndex](v));

    let lines = select(this.svgRef.current).select('#lines')
      .selectAll<SVGPathElement, d3m.Solution>('path')
      .data(this.props.solutions, sol => sol.id);

    lines.exit().transition().style('opacity', 0).remove();

    lines = lines.enter().append<SVGPathElement>('path')
      .attr('id', sol => sol.id)
      .merge(lines);

    const isHighlighted = (sol: d3m.Solution) => {
      return sol.id === this.props.hoveredSolution ||
        this.state.selectedSolutions.has(sol.id);
    };

    const updatedLines = lines.transition();
    updatedLines
      .attr('d', sol =>
        l(this.state.metrics.map(metric => metric.accessor(sol))))
      .style('stroke', sol => isHighlighted(sol) ? LIGHT_NYU_PURPLE : 'black')
      .style('stroke-width', sol => isHighlighted(sol) ? '3px' : '1.5px')
      .style('opacity', sol => isHighlighted(sol) ? 1 : .3);
  }

  renderBrushLasso(brushPoints: Point[]) {
    const container = select(this.svgRef.current).select('#brush');
    const l = line<Point>()
      .x(d => d.x)
      .y(d => d.y);
    container.select('.lasso').remove();
    container.append('path')
      .classed('lasso', true)
      .attr('d', l(brushPoints) as string);
  }

  setListeners() {
    this.listenersSet = true;

    select(this.svgRef.current)
      .on('mousedown', () => this.startBrush())
      .on('mousemove', () => this.brush())
      .on('mouseup', () => this.stopBrush())
      .on('mouseleave', () => this.stopBrush());
  }

  startBrush = () => {
    this.isBrushing = true;
    this.setState({ brushPoints: [] });
  }

  brush() {
    if (!this.isBrushing) {
      return;
    }
    this.setState({
      brushPoints: this.state.brushPoints.concat({
        x: d3Event.offsetX,
        y: d3Event.offsetY,
      }),
    });
  }

  stopBrush() {
    this.isBrushing = false;
    this.computeSelected(this.state.brushPoints);
    this.setState({ brushPoints:  [] });
  }

  computeSelected(brushPoints: Point[]) {
    if (!brushPoints.length) {
      return;
    }
    const [start, end] = [brushPoints[0], brushPoints[brushPoints.length - 1]];
    if (start.x === end.x && start.y === end.y) {
      return;
    }
    const selected = new Set<string>();
    for (const solution of this.props.solutions) {
      const points = this.state.metrics.map((metric, index) => ({
        x: this.xScale(index),
        y: this.yScales[index](metric.accessor(solution)),
      }));
      let hit = false;
      for (let axisIndex = 0; axisIndex < this.state.metrics.length - 1 && !hit; axisIndex++) {
        for (let i = 0; i < brushPoints.length - 1 && !hit; i++) {
          if (areSegmentsIntersected(points[axisIndex], points[axisIndex + 1], brushPoints[i], brushPoints[i + 1])) {
            hit = true;
            selected.add(solution.id);
          }
        }
      }
    }
    this.setState({ selectedSolutions: selected });
  }

  get areAllSelectedFavorite() {
    if (!this.state.selectedSolutions.size) {
      return false;
    }
    let solutionID;
    for (let it = this.state.selectedSolutions.values(); solutionID = it.next().value;) {
      if (!this.props.isFavoriteSolution(solutionID)) {
        return false;
      }
    }
    return true;
  }

  /**
   * If not all selected solutions are favorite, then favor all of them.
   * If all solutions are favorite, then unfavor all of them.
   */
  updateFavorite() {
    const solutionIDs = Array.from(this.state.selectedSolutions);
    const allFavorite = this.areAllSelectedFavorite; // NOTE: getter may change when we toggle
    solutionIDs.forEach(solutionID => {
      if (this.props.isFavoriteSolution(solutionID) === allFavorite) {
        this.props.toggleFavoriteSolution(solutionID);
      }
    });
  }

  removeSelected() {
    const solutionIDs = Array.from(this.state.selectedSolutions);
    solutionIDs.forEach(solutionID => this.props.removeSolution(solutionID));
  }

  componentDidUpdate() {
    this.computeScales(this.props.solutions);
    if (this.svgRef.current) {
      if (!this.listenersSet) {
        this.setListeners();
      }
      this.renderAxes();
      this.renderLines();
      this.renderBrushLasso(this.state.brushPoints);
    }
  }

  render() {
    return (
      <div className="solution-parallel-coordinates">
        <div style={{ float: 'right' }}>
          <FlatButton
            disabled={!this.state.selectedSolutions.size}
            onClick={() => this.updateFavorite()}
          >
            <Icon.Star className="icon" color="#555" fill={this.areAllSelectedFavorite ? '#555' : 'none'}/>
          </FlatButton>
          <FlatButton
            disabled={!this.state.selectedSolutions.size}
            onClick={() => this.removeSelected()}
          >
            <Icon.Trash className="icon" color="#555"/>
          </FlatButton>
        </div>
        <div style={{ height: DEFAULT_HEIGHT_PX }}>
          <svg
            ref={this.svgRef}
            style={{ width: '100%', height: DEFAULT_HEIGHT_PX }}
          >
            <g id="brush"/>
            <g id="lines"/>
            <g id="axes"/>
          </svg>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<SolutionParallelCoordinatesProps>(store => ({
  isFavoriteSolution: store.isFavoriteSolution,
  toggleFavoriteSolution: store.toggleFavoriteSolution,
  removeSolution: store.removeSolution,
}))(SolutionParallelCoordinates);
