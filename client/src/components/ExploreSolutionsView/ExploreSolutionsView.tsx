import * as React from 'react';
import * as Icon from 'react-feather';
import { PageHeader } from '../PageHeader/PageHeader';
import Spinner from '../Loading/Spinner';
import { Row, Col } from 'react-bootstrap';
import { HelpText } from '../HelpText/HelpText';
import { Link, Redirect } from 'react-router-dom';
import { connectPropsToDatastore } from 'src/data/store';
import SolutionGraph from './SolutionGraph/SolutionGraph';
import * as d3m from 'd3m/dist';
import SolutionTable from '../SolutionTable/SolutionTable';
import SolutionParallelCoordinates from './SolutionParallelCoordinates/SolutionParallelCoordinates';
import { Card } from '../Card/Card';

interface ExploreSolutionsViewProps {
  selectedProblem: d3m.Problem;

  searchStatus: d3m.SolutionSearchState;
  stopSearch: (searchID: string) => void;
  solutions: d3m.Solution[];
}

interface ExploreSolutionsViewState {
  hoveredSolution: string; // solution id
  selectedSolution?: string;

  searchStopped: boolean;
}

class ExploreSolutionsView extends React.PureComponent<ExploreSolutionsViewProps, ExploreSolutionsViewState> {
  constructor(props: ExploreSolutionsViewProps) {
    super(props);
    this.state = {
      hoveredSolution: '',
      searchStopped: false,
    };
  }

  getSelectedSolution() {
    if (this.state.selectedSolution) {
      return this.props.solutions.find(s => s.id === this.state.selectedSolution);
    }
    return undefined;
  }

  render() {
    let {searchStatus} = this.props;
    if (!searchStatus) {
      console.error('No search status set.', searchStatus);
      return <Redirect to="/" />;
    }
    const selectedSolution = this.getSelectedSolution();
    const progressState = searchStatus.progress.state.toUpperCase();
    const isRunning =
      progressState !== d3m.ta2.ProgressStateNameMap[d3m.ta2.ProgressState.COMPLETED] &&
      progressState !== d3m.ta2.ProgressStateNameMap[d3m.ta2.ProgressState.ERRORED];
    const isStopping = this.state.searchStopped && isRunning;
    const canStopSearch = searchStatus.searchID ? (isRunning && !isStopping) : false;
    return (
      <div>
        <PageHeader title="Explore Solutions">
          <Link to="explain-solutions">
            <button className="btn btn-sm btn-outline-primary">
              <Icon.ChevronsRight className="feather" />
              Explain Solutions
            </button>
          </Link>
        </PageHeader>
        <Row>
          <Col md={12}>
            {
              isRunning
              &&
              <Row style={{fontSize: '20px'}}>
                <Col md={12} style={{color: '#63508B'}}>
                  <Spinner color="#63508B" /> Searching for solutions...
                  { isStopping && ' (waiting for it to stop)' }
                </Col>
              </Row>
            }
            <Row>
              <Col md={6}>
              <strong>Search Status:</strong> {searchStatus.progress.status}
              </Col>
              <Col md={6} style={{textAlign: 'right'}}>
                <button
                  className="btn btn-sm btn-outline-primary mb-1"
                  disabled={!canStopSearch}
                  onClick={() => {
                    if (searchStatus && searchStatus.searchID) {
                      this.props.stopSearch(searchStatus.searchID);
                      this.setState({ searchStopped: true });
                    }
                  }}>
                  <Icon.StopCircle className="feather mr-1" />
                  Stop Search
                </button>
              </Col>
            </Row>

            <HelpText text="Select the best solutions for further explanations." />
            <SolutionTable
              showScoreDistribution={true}
              onHoverSolution={id => this.setState({ hoveredSolution: id })}
              onUnhoverSolution={id => this.setState({ hoveredSolution: '' })}
              onSelectedSolutionChange={id => this.setState({ selectedSolution: id })}
            />
          </Col>
        </Row>
        { // display paralell coordinates only if more than one metric was selected
          this.props.selectedProblem.inputs.performanceMetrics.length > 1 &&
          <Row className="mt-3">
            <Col md={12}>
              <Card title="Metric Parallel Coordinates" className="mt-2">
                <SolutionParallelCoordinates
                  solutions={this.props.solutions.filter(s =>
                    s.scores &&
                    s.scores.status === d3m.ScoringStatus.COMPLETED &&
                    s.scores.scores &&
                    Object.keys(s.scores.scores).length > 0)}
                  hoveredSolution={this.state.hoveredSolution}
                />
              </Card>
            </Col>
          </Row>
        }
        <Row>
          {selectedSolution && <SolutionGraph solution={selectedSolution} />}
        </Row>
        <div className="mb-3"/>
      </div>
    );
  }
}

export default connectPropsToDatastore(store => ({
  selectedProblem: store.selectedProblem,
  solutions: store.solutions,
  searchStatus: store.solutionSearchStatus,
  stopSearch: store.stopSearch,
}))(ExploreSolutionsView);
