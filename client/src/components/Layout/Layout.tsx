import * as React from 'react';
import * as Icon from 'react-feather';
import { Link } from 'react-router-dom';
import './layout.css';
import { cacheSolutionEvaluations, clearCachedSolutionEvaluations } from '../../data/Api';
import { downloadState, loadStateFromFile } from '../../data/store';
import logoVISUS from '../../images/VISUS_LOGO.png';
import { NavWorkflowSteps, NavMobile, NavWorkflow } from './NavFlow';

/**
 * Top navigation bar displayed on regular screen sizes.
 */

const NavTop = () => (
  <div>
    <nav
      className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap
      p-0 shadow navbar-purple navbar-ta3-top navbar-left"
    >
      <div className="navbar-brand col-sm-3 col-md-2 text-oswald">
        <img src={logoVISUS} height="30" width="30" style={{ marginRight: 5}} />
        Visus
      </div>
    </nav>
    <nav className="navbar fixed-top flex-md-nowrap p-0 navbar-purple navbar-ta3-top navbar-right">
      <ul className="navbar-nav px-3">
        <li className="nav-item text-nowrap d-flex ml-1">
          {
            process.env.NODE_ENV === 'development' ?
            <>
              <div className="nav-link" onClick={() => cacheSolutionEvaluations()}>
                <Icon.Save className="feather" /> Save Evaluation
              </div>
              <div className="nav-link" onClick={() => clearCachedSolutionEvaluations()}>
                <Icon.Trash2 className="feather" /> Clear Evaluation
              </div>
            </> : null
          }
          <div className="nav-link" onClick={() => downloadState()}>
            <Icon.Save className="feather" /> Save State
          </div>
          <div className="nav-link" onClick={() => loadStateFromFile()}>
            <Icon.Upload className="feather" /> Load State
          </div>
          <Link className="nav-link" to="/">
            <Icon.LogOut className="feather" /> Exit
          </Link>
        </li>
      </ul>
    </nav>
  </div>
);

/*
 * Main layout component. This wraps all navigation bars.
 */

interface LayoutProps {
  children?: JSX.Element;
}

class Layout extends React.PureComponent<LayoutProps> {
  render() {
    const workflowSteps: NavWorkflowSteps [] = [
      {toStep: '/select-dataset', labelStep: 'Select Dataset', iconComponent: Icon.Database},
      {toStep: '/select-task', labelStep: 'Select Task', iconComponent: Icon.CheckSquare},
      {toStep: '/define-problem', labelStep: 'Define Problem', iconComponent: Icon.Edit},
      {toStep: '/explore-solutions', labelStep: 'Explore Solutions', iconComponent: Icon.List},
      {toStep: '/explain-solutions', labelStep: 'Explain Solutions', iconComponent: Icon.HelpCircle},
    ];
    return (
      <div>
        <NavMobile workflow={workflowSteps} systemName={'Visus'}/>
        <NavTop />
        <div className="container-fluid">
          <div className="row">
            <NavWorkflow workflow={workflowSteps}/>
            <main role="main" className="col-md-12 main-content">
              {this.props.children}
            </main>
          </div>
        </div>
      </div>
    );
  }
}

export { Layout };
