import * as React from 'react';
import * as Icon from 'react-feather';
import { Route, Link } from 'react-router-dom';
import './layout.css';

/**
 * Main navigation on the left side-bard, which allows navigation between the
 * steps of the workflow.
 */

interface NavWorkflowLinkProps {
  to: string;
  label: string;
  activeOnlyWhenExact?: boolean;
  iconComponent: React.ComponentType<Icon.Props>;
}

class NavWorkflowLink extends React.Component<NavWorkflowLinkProps> {
  render() {
    const {
      to,
      label,
      iconComponent: IconComponent,
      activeOnlyWhenExact = true,
    } = this.props;
    return (
      <Route
        path={to}
        exact={activeOnlyWhenExact}
        children={({ match }) => (
          <Link to={to} className={match ? 'nav-link active' : 'nav-link '}>
            <div className="workflow-circle">
              <IconComponent className="feather" />
            </div>
            <span>{label}</span>
          </Link>
        )}
      />
    );
  }
}

interface NavWorkflowSteps {
  toStep: string;
  labelStep: string;
  iconComponent: React.ComponentType<Icon.Props>;
}

interface NavWorkflowProps {
  workflow: NavWorkflowSteps[];
}

class NavWorkflow extends React.Component<NavWorkflowProps> {
  render() {
    const { workflow} = this.props;
    const height =  workflow.length * 40;
    return (
      <nav className="d-none d-md-block bg-light sidebar navbar-left">
        <div className="sidebar-sticky">
          <ul className="nav flex-column workflow-nav">
          {
            workflow.map((d, index) => (
              <li key={ 'NavWorkflow_' + index} className="nav-item">
                <div className="workflow-line" style={{height: height}}/>
                <NavWorkflowLink
                  to={d.toStep}
                  label={d.labelStep}
                  iconComponent={d.iconComponent}
                />
              </li>
            ))
          }
          </ul>
        </div>
      </nav>
    );
  }
}

/**
 * Alternative navigation bar that is displayed only on mobile devices
 * (small screens sizes).
 */

interface NavMobileLinkProps {
  to: string;
  label: string;
}

class NavMobileLink extends React.PureComponent<NavMobileLinkProps> {
  render() {
    return (
      <Route
        path={this.props.to}
        exact={true}
        children={({ match }) => (
          <li className={'nav-item' + (match ? ' active' : '')}>
            <Link className="nav-link" to={this.props.to}>
              {this.props.label}
            </Link>
          </li>
        )}
      />
    );
  }
}

interface NavMobileProps {
  workflow: NavWorkflowSteps[];
  systemName: string;
}

interface NavMobileState {
  toggleActive: boolean;
}

class NavMobile extends React.Component<NavMobileProps, NavMobileState> {
  constructor(props: NavMobileProps) {
    super(props);
    this.state = { toggleActive: false };
  }

  onToggle() {
    this.setState({ toggleActive: !this.state.toggleActive });
  }

  render() {
    return (
      <nav className="navbar navbar-mobile navbar-dark bg-dark navbar-purple">
        <div className="navbar-brand text-oswald">{this.props.systemName}</div>
        <button className="navbar-toggler" onClick={() => this.onToggle()}>
          <span className="navbar-toggler-icon" />
        </button>
        <div
          className={
            'collapse navbar-collapse' +
            (this.state.toggleActive ? ' show' : '')
          }
        >
          <ul className="navbar-nav mr-auto">
          {
            this.props.workflow.map((d, index) => (
              <NavMobileLink key={'NavMobileLink_' + index} to={d.toStep} label={d.labelStep} />
            ))
          }
          </ul>
        </div>
      </nav>
    );
  }
}

export { NavWorkflowSteps, NavMobile, NavWorkflow };
