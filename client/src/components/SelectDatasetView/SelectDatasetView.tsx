import * as React from 'react';
import * as Icon from 'react-feather';
import Loadable from '../../data/Loadable';
import { Link } from 'react-router-dom';
import * as d3m from 'd3m/dist';
import './select-dataset-view.css';
import { connectPropsToDatastore } from 'src/data/store';
import { PageHeader } from '../PageHeader/PageHeader';
import { CardShadow } from '../Card/Card';
import { HelpText } from '../HelpText/HelpText';
import ReactMarkdown from 'react-markdown';

interface DatasetLinkProps {
  dataset: d3m.Dataset;
  selectDataset: (dataset: d3m.Dataset) => void;
}

class DatasetLink extends React.PureComponent<DatasetLinkProps> {
  render() {
    const { about, dataResources } = this.props.dataset;
    let description = about.description ? about.description : '';
    return (
        <CardShadow height="250px">
          <div className="mb-2" style={{height: '165px'}}>
            <h6 className="font-weight-bold mb-2" data-cy="datasetID">{about.datasetID}</h6>
            <div className="mb-2">
              {dataResources.map((dr, i) => (
                <span
                  key={'data-resource_' + about.datasetID + i}
                    className="badge badge-pill badge-primary mr-1"
                >
                  {dr.resType}
                </span>
              ))}
            </div>
            <div className="text-merriweather text-muted small" style={{height: '85px', overflow: 'scroll'}}>
            <ReactMarkdown className="code-non-language" source={description}/>
            </div>
          </div>
          <Link to={'/select-task'} className={'dataset-link'} onClick={() => {
            this.props.selectDataset(this.props.dataset);
          }}>
          <button className="btn btn-outline-primary">
            <Icon.ChevronsRight className="feather" /> Select
          </button>
          </Link>
        </CardShadow>
    );
  }
}

interface SelectDatasetViewProps {
  datasets: Loadable<d3m.Dataset[]>;
  selectDataset: (dataset: d3m.Dataset) => void;
  resetValue: (key: string) => void;
}

export class SelectDatasetView extends React.PureComponent<SelectDatasetViewProps> {

  render() {
    const { datasets, selectDataset, resetValue } = this.props;
    return (
      <div>
        <PageHeader title="Select Dataset" />

        <HelpText>
          Please load a tabular dataset or select an existing dataset
          you want to use for your problem.<br />
          Your dataset would include a series of
          <span style={{color: '#12a912'}}><b> data instances </b></span>
          (e.g., Dog A, Dog B, Dog C, ...) which contain the same
          <span style={{color: '#12a912'}}><b> features </b></span>
          (i.e., the properties of a dog such a weight, height, breed, age).
        </HelpText>

        <div className="mt-3 mb-3">
          <div className="row">
            { datasets.error ?
              <div className="alert alert-danger" role="alert">
                <span><b>Error:</b> {datasets.error.originalError && datasets.error.message}</span>
                {' '} - <a href="#" onClick={() => resetValue('datasets')}>Try again</a>
             </div>
            :
            <>
            {/* Disabled until we implement importing of CSV/XLS files (issue #125) */}
            {/* <div className="col-lg-4 mb-4">
              <Link to={'/select-dataset'} className={'dataset-link'}>
                <CardShadow height="250px">
                  <CardButton>
                    <h6>Open your own data</h6>
                  </CardButton>
                </CardShadow>
              </Link>
            </div> */}

            {datasets.value!.map((dataset, i) => (
              <div key={'dataset-' + i}  className="col-lg-4 mb-4">
              <DatasetLink dataset={dataset} selectDataset={selectDataset}  />
              </div>
            ))}
            </>
            }
          </div>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<SelectDatasetViewProps>(store => ({
  datasets: store.datasets,
  selectDataset: store.selectDataset,
  resetValue: store.resetValue,
}))(SelectDatasetView);
