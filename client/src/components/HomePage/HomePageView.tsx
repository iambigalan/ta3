import * as React from 'react';
import * as Icon from 'react-feather';
import { Link } from 'react-router-dom';
import logoNYU from '../../images/NYU_LOGO.png';
import logoVISUS from '../../images/VISUS_LOGO.png';
import logoDARPA from '../../images/DARPA_LOGO.png';

export class HomePageView extends React.PureComponent {
  componentWillMount() {
    // Applying a class here seems odd, but it is the only way to apply a style
    // to the body tag which is outside of the scope managed by React.
    document.body.className = 'datasets-view';
  }

  componentWillUnmount() {
    document.body.className = '';
  }

  render() {
    return (
    <div>
        <div className="bg-purple" style={{marginLeft: 0, width: '100%', textAlign: 'center', padding: 100}}>
            <div>
                <img src={logoNYU}  height="32" width="82" style={{ margin: 10, position: 'absolute', top: 0,
                left: 0}} />
                <img src={logoDARPA}  height="32" width="72" style={{ margin: 10, position: 'absolute', top: 0,
                right: '0%'}} />
                <img src={logoVISUS}  height="110" width="110" style={{ margin: 20}} />
                <br />
                <h1 className="mb-4 text-white text-oswald">Visus</h1>
                <h5 className="mb-3 text-white text-merriweather">Welcome!</h5>
                <p className="mb-0 text-white small">
                  What would you like to know? The price of a 780 ft<sup>2</sup>, 1 bedroom
                  apartment in your neighborhood?
                  Your dog's life expectancy? Weather on next Monday?
                </p>
                <p className="mb-0 text-white small">
                  Visus helps you to answer your questions using
                  existing datasets
                  and state-of-the-art machine learning techniques.
                </p>
                <p className="mb-0 text-white small">
                  Visus presents step-by-step guidance which enables
                  you to build your own Machine Learning model without
                  knowing how machine learning works.
                </p>
            </div>
        </div>
        <Link to={'/select-dataset'}>
        <div
          className="d-flex align-items-center"
          style={{display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 30, marginBottom: 30}}
        >
          <button className="btn btn-outline-primary" data-cy="use-visus">
            <Icon.ChevronsRight className="feather" /> USE VISUS
          </button>
        </div>
        </Link>
    </div>
    );
  }
}

export default {HomePageView};
