import * as React from 'react';
import {
  RuleMatrixApp, RuleList, Streams, Support, SupportMat,
  createStreams, createConditionalStreams, isConditionalStreams, labelColor, ConditionalStreams,
} from 'rule-matrix-js';
import * as d3m from 'd3m/dist';
import { connectPropsToDatastore } from 'src/data/store';
import { getRuleMatrix } from '../../../data/Api';
import Loading from '../../Loading/Loading';

const FETCH_INTERVAL_MS = 1000;

interface RuleMatrixProps {
  fittedSolutionID: string;
  resources: d3m.DataResource[];
  selectedProblem: d3m.Problem;
}

interface RuleMatrixState {
  model?: RuleList;
  streams?: Streams | ConditionalStreams;
  support?: Support | SupportMat;
  working?: boolean;
  error?: boolean;
}

const styles = {
  flowWidth: 50,
  // mode: 'matrix',
  rectWidth: 45,
  rectHeight: 27,
  color: labelColor,
  width: 1000,
  height: 1000,
};

class RuleMatrix extends React.PureComponent<RuleMatrixProps, RuleMatrixState> {
  unmounted: boolean;
  data: d3m.RuleMatrixData | undefined;
  dataFetchTimeout: NodeJS.Timeout | null = null;

  constructor(props: RuleMatrixProps) {
    super(props);
    this.state = {
      model: undefined,
      streams: undefined,
      support: undefined,
      working: undefined,
    };
    this.unmounted = true;
  }

  getRuleMatrix = async () => {
    if (this.unmounted) {
      return;
    }
    if (this.dataFetchTimeout !== null) {
      clearTimeout(this.dataFetchTimeout);
      this.dataFetchTimeout = null;
    }
    const data = await getRuleMatrix(this.props.fittedSolutionID, this.ruleMatrixConfig);
    if (this.unmounted) {
      // TODO(bowen): The component is rendered twice when the visualization type is switched.
      // The first mounted RuleMatrix can be unmounted when the first data request returns.
      return;
    }
    if (data.continue) {
      this.dataFetchTimeout = setTimeout(this.getRuleMatrix, FETCH_INTERVAL_MS);
      this.setState({ working: true, error: false });
      return;
    } else if (data.error || !data.model || !data.streams || !data.support) {
      console.error('there was an error generating rule matrix');
      this.setState({ working: false, error: true });
      return;
    }
    const model = new RuleList(data.model);
    const streams = isConditionalStreams(data.streams)
      ? createConditionalStreams(data.streams)
      : createStreams(data.streams);
    const support = data.support;
    model.support(support);
    this.setState({ model, streams, support, working: false, error: false });
  }

  componentDidMount() {
    this.unmounted = false;
    this.getRuleMatrix();
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  get ruleMatrixConfig(): d3m.RuleMatrixConfig[] {
    return this.props.resources.map((resource, index) => {
      const config: d3m.RuleMatrixConfig = {
        resId: resource.resID,
        featureIndices: [],
        targetIndices: [],
      };
      const columns = resource.columns as d3m.Column[];
      // NOTE(bowen): Generates the features/target config as RuleMatrixConfig.
      // This is directly read from datasetDoc.json and neither the UI nor the TA2 currently can change it.
      if (columns) {
        for (let j = 0; j < columns.length; ++j) {
          if (columns[j].colName !== 'd3mIndex') {
            if (columns[j].role.indexOf(d3m.ColumnRole.attribute) !== -1) {
              config.featureIndices.push(j);
            } else if (columns[j].role.indexOf(d3m.ColumnRole.suggestedTarget) !== -1) {
              config.targetIndices.push(j);
            }
          }
        }
      }
      return config;
    });
  }

  render() {
    const taskType = this.props.selectedProblem.about.taskType;
    const { model, streams, support, working, error } = this.state;
    if (!taskType) {
      return null;
    }
    let content = null;
    if (working === true) {
      content = <Loading message="Wait a minute, processing rule matrix..."/>;
    }
    if (error === true) {
      content = <div>Failed to generate the Rule Matrix visualization.</div>;
    }
    if (model) {
      content = <div style={{ overflow: 'scroll' }}>
        <RuleMatrixApp
          model={model}
          streams={streams}
          support={support}
          styles={styles}
          input={null}
        />
      </div>;
    }

    return (
      <div>
        <h5>Rule Matrix</h5>
        {content}
      </div>
    );
  }
}

export default connectPropsToDatastore<RuleMatrixProps>(store => ({
  selectedProblem: store.selectedProblem,
}))(RuleMatrix);
