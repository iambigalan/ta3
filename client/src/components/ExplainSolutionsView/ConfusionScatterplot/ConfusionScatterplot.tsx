import * as React from 'react';
import { scaleLinear, ScaleLinear } from 'd3-scale';
import * as _ from 'lodash';
import { Axis, axisLeft, axisBottom } from 'd3-axis';
import { select } from 'd3-selection';
import { getTransform } from '../../../utils';
import './ConfusionScatterplot.css';

const UPSCALE = 2;
const DOT_SIZE_PX = 4;
const DOT_OPACITY = 0.3;
const DEFAULT_SIZE_PX = 300;
const DOT_COLOR = 'darkgray';
const LINE_COLOR = 'lightgray';
const DEFAULT_AXIS_TICKS = 5;
const LABEL_OFFSET_PX = 5;

const DEFAULT_MARGINS = { left: 40, right: 10, top: 10, bottom: 20 };

// Default distance between consecutive html element under the scatterplot div
// TODO: check where is this value coming from?
const ELEMENT_DISTANCE_PX = 15;

export interface ConfusionScatterplotData {
  points: [number, number][];
}

interface ConfusionScatterplotProps {
  data: ConfusionScatterplotData;
  size?: number;
}

interface ConfusionScatterplotState {
  ctx: CanvasRenderingContext2D | null;
  size: number;
}

export default class ConfusionScatterplot extends React.PureComponent<ConfusionScatterplotProps,
  ConfusionScatterplotState> {
  xScale: ScaleLinear<number, number>;
  yScale: ScaleLinear<number, number>;
  margins: Margins = DEFAULT_MARGINS;
  svgRef: React.RefObject<SVGSVGElement>;

  constructor(props: ConfusionScatterplotProps) {
    super(props);
    this.svgRef = React.createRef();
    this.state = {
      ctx: null,
      size: this.props.size || DEFAULT_SIZE_PX,
    };
  }

  computeScales() {
    const points = this.props.data.points;
    const xValues = points.map(p => +p[0]);
    const yValues = points.map(p => +p[1]);
    const [ xmin, xmax ] = [_.min(xValues), _.max(xValues)] as [number, number];
    const [ ymin, ymax ] = [_.min(yValues), _.max(yValues)] as [number, number];
    const domain = [Math.min(xmin, ymin), Math.max(xmax, ymax)];
    this.xScale = scaleLinear()
      .domain(domain)
      .range([this.margins.left, this.state.size - this.margins.right]);
    this.yScale = scaleLinear()
      .domain(domain)
      .range([this.state.size - this.margins.bottom, this.margins.top]);
  }

  drawLines(ctx: CanvasRenderingContext2D) {
    const [ xmin, xmax ] = this.xScale.range();
    const [ ymin, ymax ] = this.yScale.range();
    ctx.save();
    ctx.strokeStyle = LINE_COLOR;
    ctx.lineWidth = DOT_SIZE_PX * 0.25;
    ctx.lineJoin = 'round';

    ctx.beginPath();
    ctx.moveTo(xmin, ymin);
    ctx.lineTo(xmax, ymax);
    ctx.stroke();

    ctx.restore();
  }

  drawPoints(ctx: CanvasRenderingContext2D) {
    const draw = (list: [ number, number ][]): void => {
      list.forEach((p) => {
        const [ x, y ] = [ this.xScale(p[0]), this.yScale(p[1]) ];
        ctx.save();
        ctx.fillStyle = DOT_COLOR;
        ctx.translate(x, y);
        ctx.globalAlpha = DOT_OPACITY;
        ctx.beginPath();
        ctx.arc(0, 0, DOT_SIZE_PX, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.beginPath();
        ctx.arc(0, 0, 0.75 * DOT_SIZE_PX, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.beginPath();
        ctx.arc(0, 0, 0.5 * DOT_SIZE_PX, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.globalAlpha = 1.0;
        ctx.beginPath();
        ctx.arc(0, 0, 0.25 * DOT_SIZE_PX, 0, 2 * Math.PI, false);
        ctx.fill();
        ctx.restore();
      });
    };

    draw(this.props.data.points);
  }

  setRef = (ref: HTMLCanvasElement) => {
    this.setState({
      ctx: ref && ref.getContext('2d', {
        alpha: false,
      }),
    });
  }

  renderAxis = (svg: SVGElement, scale: ScaleLinear<number, number>, options: {
      orient: string,
      transform?: string,
      label: string,
      labelTransform: string,
    }) => {
    let axis: Axis<number>;
    const { orient, transform, label, labelTransform } = options;
    if (orient === 'left') {
      axis = axisLeft<number>(scale);
    } else {
      axis = axisBottom<number>(scale);
    }
    axis.ticks(DEFAULT_AXIS_TICKS);
    let svgAxes = select(svg).select<SVGGElement>('g');
    if (svgAxes.empty()) {
      svgAxes = select(svg).append<SVGGElement>('g');
    } else {
      svgAxes.selectAll('*').remove();
    }
    svgAxes
      .attr('transform', transform || '')
      .call(axis);

    if (options.label) {
      let svgLabel = svgAxes.select<SVGTextElement>('.label');
      if (svgLabel.empty()) {
        svgLabel = svgAxes.append('text')
          .classed('label', true);
      }
      svgLabel
        .text(label)
        .attr('transform', labelTransform);
    }
  };

  renderScatterplot(ctx: CanvasRenderingContext2D) {
    const size = this.state.size;
    ctx.save();
    ctx.scale(UPSCALE, UPSCALE);

    ctx.save();
    ctx.clearRect(0, 0, size, size);
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, size, size);
    ctx.restore();

    ctx.save();
    this.drawLines(ctx);
    ctx.restore();

    ctx.save();
    this.drawPoints(ctx);
    ctx.restore();

    ctx.restore();
  }

  renderAxes() {
    this.renderAxis(
      select(this.svgRef.current).select('#x-axis').node() as SVGGElement,
      this.xScale,
      {
        orient: 'bottom',
        transform: getTransform([0, this.state.size - this.margins.bottom]),
        label: 'Predicted',
        labelTransform: getTransform([this.state.size - this.margins.right, -LABEL_OFFSET_PX]),
      },
    );
    this.renderAxis(
      select(this.svgRef.current).select('#y-axis').node()  as SVGGElement,
      this.yScale,
      {
        orient: 'left',
        transform: getTransform([this.margins.left, 0]),
        label: 'Label',
        labelTransform: getTransform([LABEL_OFFSET_PX, this.margins.top], 1, 90),
      },
    );
  }

  render() {
    const { size } = this.state;
    if (this.state.ctx !== null) {
      this.computeScales();
      this.renderScatterplot(this.state.ctx);
      this.renderAxes();
    }
    return (
      <div className="confusion-scatterplot">
        <canvas
          width={UPSCALE * size}
          height={UPSCALE * size}
          style={{
            width: size,
            height: size,
          }}
          ref={this.setRef}
        />
        <svg
          style={{
            width: size,
            height: size,
            position: 'absolute',
            top: 0,
            left: ELEMENT_DISTANCE_PX,
          }}
          ref={this.svgRef}
        >
          <g id="x-axis" transform="translate(10, 0)"/>
          <g id="y-axis" transform="translate(10, 0)"/>
        </svg>
      </div>
    );
  }
}
