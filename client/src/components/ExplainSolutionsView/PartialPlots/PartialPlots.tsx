import React, { PureComponent } from 'react';
import { ComposedChart, XAxis, YAxis, CartesianGrid, Line, Area, Label, Bar } from 'recharts';
import * as d3m from 'd3m/dist';
import { connectPropsToDatastore } from 'src/data/store';
import Loading from 'src/components/Loading/Loading';
import PersistentComponent from 'src/components/PersistentComponent';
import styled from 'styled-components';
import { cachedSuspense, getPartialPlots } from 'src/data/Api';

interface PartialPlotsProps {
  fittedSolutionID: string;
  problem?: d3m.Problem;
  data?: d3m.PartialPlotData;
}

const LegendBox = styled.div`
  height: 10px;
  width: 10px;
  background-color:${(props: {color: string}) => props.color};
  display: inline-block;
  margin-left: 10px;
`;

const ChartContainer = styled.div`
  & .recharts-line-dots {
    display: none;
  }
`;

export class PartialPlotsView extends PersistentComponent<PartialPlotsProps> {
  constructor(props: PartialPlotsProps) {
    super(props);
    this.state = {};
  }
  render() {
    const { data, problem } = this.props;
    if (!data) {
      return <div>No Data </div>;
    }

    const partials = data.partials.map(p => ({
      ...p,
      values: p.values.map(v => {
        const hist = p.histogram.find(h => h.value === v.value)!;
        if (!hist || !hist.count) {
          return {...v, std: 0, base: 0, trueBand: 0, trueMean: 0};
        }
        return {
          ...v,
          std: [v.mean - v.std, v.mean + v.std],
          base: [data.baseStats.mean - data.baseStats.std, data.baseStats.mean + data.baseStats.std],
          trueBand: [hist.mean - (hist.std || 0), hist.mean + (hist.std || 0)],
          trueMean: hist.mean,
        };
      }).filter(v => v.trueMean || v.std),
    }));

    return (
      <div>
        <div className="d-flex" style={{justifyContent: 'center'}}>
          <div><LegendBox color="#8884d8" /> Predicted </div>
          <div><LegendBox color="#EEBBBB"/> Ground Truth</div>
        </div>
      <ChartContainer style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'space-evenly'}}>
        {partials.map(p => (
          <div key={p.column.colName} style={{ border: 'solid 1px #ccc', margin: '10px', padding: '10px' }}>
            <div style={{textAlign: 'center', fontWeight: 'bold'}}>{p.column.colName}</div>
            <ComposedChart width={300} height={200} margin={{ top: 25, right: 5, bottom: 5, left: 5 }} data={p.values}>
              <YAxis dataKey="mean">
                <Label
                  value={`${problem!.inputs.data[0].targets[0].colName }`}
                  x={0} y={125}
                  textAnchor="middle"
                  transform="rotate(270, 100,125) translate(80, -70)" />
              </YAxis>
              <CartesianGrid stroke="#eee" strokeDasharray="5 5" />
              <Line dataKey="mean" stroke="#8884d8" animationDuration={0} />
              <Line dataKey="trueMean" stroke="#EEBBBB"  animationDuration={0} />
              <Area dataKey="std" stroke="#0088FE" fillOpacity="0.1" strokeOpacity="0.2"  animationDuration={0} />
              <Area dataKey="trueBand" fill="#FFCCCC" stroke="#FFCCCC" strokeOpacity="0.2"
                    animationDuration={0} fillOpacity="0.2"  />
            </ComposedChart>
            <ComposedChart
              width={300}
              height={100}
              margin={{ top: 0, right: 5, bottom: 20, left: 5 }}
              data={p.histogram}>
              <YAxis dataKey="count">
                <Label value="count" x={0} y={0} textAnchor="middle" transform="rotate(270, 25, 35)" />
              </YAxis>
              <Bar dataKey="count" fill="#ccc" />
              <XAxis
                dataKey="value"
                tickFormatter={v =>
                  p.column.colType === d3m.ColumnType.dateTime ? new Date(v).toISOString().slice(0, 10) : v
                }
              />
            </ComposedChart>
          </div>
        ))}
      </ChartContainer>
      </div>
    );
  }
}

const partialPlots = (fittedSolutionID?: string, selectedProblem?: d3m.Problem) => {
  if (!selectedProblem || fittedSolutionID === undefined) {
    return undefined;
  }
  return cachedSuspense<d3m.PartialPlotData>({
    cacheID: `partialPlot-${fittedSolutionID}`,
    loader: () => {
      return getPartialPlots(fittedSolutionID, selectedProblem);
    },
  }).value;
};

const PartialPlots = connectPropsToDatastore<PartialPlotsProps>((store, props) => ({
  data: partialPlots(props.fittedSolutionID, store.selectedProblem),
  problem: store.selectedProblem,
}))(PartialPlotsView);

export default class PartialPlotsContainer extends PureComponent<PartialPlotsProps> {
  render() {
    return (
      <React.Suspense fallback={<Loading message="Computing Partial Dependence Plots..." />}>
        <PartialPlots fittedSolutionID={this.props.fittedSolutionID} />
      </React.Suspense>
    );
  }
}
