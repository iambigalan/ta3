import * as React from 'react';
import * as Icon from 'react-feather';
import { PageHeader } from '../PageHeader/PageHeader';
import { HelpText } from '../HelpText/HelpText';
import * as d3m from 'd3m/dist';

import {
  evaluateSolution,
  getConfusionMatrix,
  getConfusionScatterplot,
} from 'src/data/Api';
import ConfusionMatrix from './ConfusionMatrix/ConfusionMatrix';
import ConfusionScatterplot from './ConfusionScatterplot/ConfusionScatterplot';
import RuleMatrix from './RuleMatrix/RuleMatrix';
import SolutionTable from '../SolutionTable/SolutionTable';
import { Row, Col } from 'react-bootstrap';
import Select from 'react-select';
import { connectPropsToDatastore } from 'src/data/store';
import { Card } from '../Card/Card';
import PartialPlots from './PartialPlots/PartialPlots';
import Loading from '../Loading/Loading';
import { startCase } from 'lodash';

enum ExplainSolutionsVisualziationType {
  UNSET = 'unset',
  NONE = 'none',
  CONFUSION_MATRIX = 'confusion-matrix',
  CONFUSION_SCATTERPLOT = 'confusion-scatterplot',
  RULE_MATRIX = 'rule-matrix',
  PARTIAL_PLOTS = 'partial-plots',
}

interface ExplainSolutionsViewProps {
  selectedProblem: d3m.Problem;
  selectedDataset: d3m.Dataset;
  favoriteSolutions: d3m.Solution[];
  exportSolutions: (solutions: d3m.Solution[], callback?: (message: string, done?: boolean) => void) => void;
}

interface ExplainSolutionsViewState {
  selectedSolution?: string; // solution id
  selectedVisualizationOption: { value: ExplainSolutionsVisualziationType, label: string };

  confusionMatrixData?: d3m.ConfusionMatrixData;
  confusionScatterplotData?: d3m.ConfusionScatterplotData;

  isEvaluatingSolution: boolean;
  fittedSolutionID?: string; // currently selected fitted solution

  message: string; // message displayed to the user
  isExporting: boolean;
  isThereErrorFitting: boolean;
}

class ExplainSolutionsView extends React.PureComponent<ExplainSolutionsViewProps, ExplainSolutionsViewState> {
  constructor(props: ExplainSolutionsViewProps) {
    super(props);
    this.state = {
      selectedVisualizationOption: this.visualizationOptions[0],
      isEvaluatingSolution: false,
      message: '',
      isExporting: false,
      isThereErrorFitting: false,
    };
  }

  onVisualizationTypeChange = (selected: { value: ExplainSolutionsVisualziationType, label: string }) => {
    const shouldEvaluate = selected.value !== ExplainSolutionsVisualziationType.PARTIAL_PLOTS;
    this.setState(
      { selectedVisualizationOption: selected, isEvaluatingSolution: shouldEvaluate },
      () => {
        if (shouldEvaluate) {
          if (this.state.selectedSolution) {
            this.loadVisualization(this.state.selectedSolution);
          }
        }
      },
    );
  }

  get visualizationOptions() {
    const options = [];
    const taskType = this.props.selectedProblem.about.taskType;
    options.push({ value: ExplainSolutionsVisualziationType.NONE, label: 'Select A Visualization' }); // fallback
    if (taskType === d3m.TaskType.classification) {
      options.push({ value: ExplainSolutionsVisualziationType.CONFUSION_MATRIX, label: 'Confusion Matrix' });
      options.push({ value: ExplainSolutionsVisualziationType.RULE_MATRIX, label: 'Rule Matrix' });
    } else if (taskType === d3m.TaskType.regression) {
      options.push({ value: ExplainSolutionsVisualziationType.PARTIAL_PLOTS, label: 'Partial Dependence Plots' });
      options.push({ value: ExplainSolutionsVisualziationType.CONFUSION_SCATTERPLOT, label: 'Confusion Scatterplot' });
    }
    return options;
  }

  renderVisualization() {

    const taskType = this.props.selectedProblem.about.taskType;
    if (taskType === d3m.TaskType.vertexClassification ||
        taskType === d3m.TaskType.vertexNomination ||
        taskType === d3m.TaskType.objectDetection ||
        taskType === d3m.TaskType.timeSeriesForecasting) {
      return <div>There are no visualizations available
        for <mark className="badge-pill badge-secondary">{startCase(taskType)}</mark> problem.</div>;

    } else if (this.state.isThereErrorFitting) {
      return <div>Failed to generate predictions for the selected model.</div>;

    } else {
      if (!this.state.selectedSolution) {
        return <div>Please select a solution to visualize.</div>;
      }
      if (this.state.isEvaluatingSolution) {
        return <Loading message="Evaluating selected solution..." />;
      }
    }

    let visualization;
    switch (this.state.selectedVisualizationOption.value) {
      case ExplainSolutionsVisualziationType.CONFUSION_MATRIX:
        visualization = this.state.confusionMatrixData ?
          <ConfusionMatrix data={this.state.confusionMatrixData}/> :
          <Loading message="Computing confusion matrix..." />;
        break;
      case ExplainSolutionsVisualziationType.CONFUSION_SCATTERPLOT:
        visualization = this.state.confusionScatterplotData ?
          <ConfusionScatterplot data={this.state.confusionScatterplotData}/> :
          <Loading message="Computing confusion scatterplot..." />;
        break;
      case ExplainSolutionsVisualziationType.RULE_MATRIX:
        visualization = <RuleMatrix
          fittedSolutionID={this.state.fittedSolutionID}
          resources={this.props.selectedDataset.dataResources}
        />;
        break;
      case ExplainSolutionsVisualziationType.PARTIAL_PLOTS:
        visualization = <PartialPlots
          fittedSolutionID={this.state.fittedSolutionID || ''}
        />;
        break;
      default:
        visualization = null;
    }
    return <>
      <Row className="mb-3">
        <Col sm={12} md={6} lg={4}>
          <Select
            options={this.visualizationOptions}
            value={this.state.selectedVisualizationOption}
            onChange={this.onVisualizationTypeChange}
          />
        </Col>
      </Row>
      <Row><Col md={12}>{ visualization }</Col></Row>
    </>;
  }

  loadVisualization = async (solutionID: string) => {
    // Request evaluation of the solution using TA3-side train/test split.
    this.setState({
      isEvaluatingSolution: true,
      fittedSolutionID: undefined,
    });
    const { fittedSolutionID, error } = await evaluateSolution(solutionID, this.props.selectedProblem);
    this.setState({
      isEvaluatingSolution: false,
      fittedSolutionID,
      isThereErrorFitting: error,
    });

    const visualizationType = this.state.selectedVisualizationOption.value;
    if (visualizationType === ExplainSolutionsVisualziationType.UNSET ||
      this.visualizationOptions.find(option => option.value === visualizationType) === undefined) {
      // If the currently selected visualization type is no longer supported,
      // use the first available visualization type.
      this.setState({ selectedVisualizationOption: this.visualizationOptions[0] });
    }
    this.getVisualizationData(fittedSolutionID);
  }

  getVisualizationData = async (fittedSolutionID: string) => {
    switch (this.state.selectedVisualizationOption.value) {
      case ExplainSolutionsVisualziationType.CONFUSION_MATRIX:
        this.setState({
          confusionMatrixData: await getConfusionMatrix(fittedSolutionID),
        });
        break;
      case ExplainSolutionsVisualziationType.CONFUSION_SCATTERPLOT:
        this.setState({
          confusionScatterplotData: await getConfusionScatterplot(fittedSolutionID),
        });
        break;
      case ExplainSolutionsVisualziationType.RULE_MATRIX:
        // nothing, rule matrix will handle itself
        break;
      default:
    }
  }

  selectSolution = (solutionID: string | undefined) => {
    if (solutionID === this.state.selectedSolution) {
      return;
    }
    this.setState({
      selectedSolution: solutionID,
      isThereErrorFitting: false,
    });
    // clear visualization data to wait for async computation
    this.clearVisualizationData();
    if (solutionID === undefined) {
      return;
    }
    this.loadVisualization(solutionID);
  }

  clearVisualizationData = () => {
    this.setState({
      confusionMatrixData: undefined,
      confusionScatterplotData: undefined,
    });
  }

  render() {
    return (
      <>
        <PageHeader title="Explain Solutions">
          <button className="btn btn-sm btn-outline-primary"
            disabled={!this.props.favoriteSolutions.length || this.state.isExporting}
            onClick={() => this.props.exportSolutions(
              this.props.favoriteSolutions,
              (message, done) => {
                const isExporting = !done;
                this.setState({ message, isExporting });
              },
            )}
          >
            <Icon.ChevronsRight className="feather" />
            Export Favorite Solutions & Finish
          </button>
        </PageHeader>
        <Row>
          <Col md={12}>
            <HelpText
              text="Select solutions to see explanatory visualizations
              bellow. Use the arrows to re-rank the best solutions."
            />
            {
              this.state.message !== '' ?
                <div className="alert alert-primary">
                  {
                    this.state.isExporting ?
                    <Loading message={this.state.message}/> :
                    <div>{ this.state.message }</div>
                  }
                </div> : null
            }
            <SolutionTable
              onSelectedSolutionChange={id => this.selectSolution(id)}
            />
          </Col>
          <Col md={12}>
            <Card title="Explanation Method" className="mt-4">
              { this.renderVisualization() }
            </Card>
          </Col>
        </Row>
      </>
    );
  }
}

export default connectPropsToDatastore(store => ({
  selectedProblem: store.selectedProblem,
  selectedDataset: store.selectedDataset,
  favoriteSolutions: store.favoriteSolutions,
  exportSolutions: store.exportSolutions,
  solutions: store.solutions,
  searchStatus: store.solutionSearchStatus,
}))(ExplainSolutionsView);
