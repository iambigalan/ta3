import * as React from 'react';
import styled from 'styled-components';
import { formatValue } from '../../../data/utils';

const Table = styled.table`
  border-collapse: collapse;
  vertical-align: middle;
`;

const DummyRow = styled.tr`
  height: 0;
  max-height: 0;
  line-height: 0;
  overflow: hidden;
`;

const DummyCell = styled.td`
  height: 0;
  max-height: 0;
  line-height: 0;
  overflow: hidden;
  padding: 5px;
  text-align: right;
  user-select: none;
  color: white;
`;

const Row = styled.tr`
`;

const Cell = styled.td`
  user-select: none;
  padding: 5px;
  text-align: right;
  border-width: 1px;
  border-color: black;
  cursor: pointer;
  color: ${({ magnitude }: CellThemeProps): string => getFontColor(magnitude)};
  background-color: ${({ magnitude }: CellThemeProps): string => getBackgroundColor(magnitude)};
  border-style:${({ isT, isR, isB, isL }: CellThemeProps): string => {
    return isT || isR || isB || isL ? 'solid' : 'none';
  }};
`;

const TotalCell = styled.td`
  user-select: none;
  padding: 5px;
  text-align: right;
  border-width: 2px;
  border-color: black;
  border-style: solid;
  cursor: pointer;
`;

interface CellThemeProps {
  isT?: boolean;
  isR?: boolean;
  isB?: boolean;
  isL?: boolean;
  magnitude?: number;
}

function getBackgroundColor(magnitude?: number): string {
  if (magnitude === undefined) {
    return 'white';
  }
  const sym = `0${(255 - Math.floor(magnitude * 255)).toString(16)}`.slice(-2);
  return `#${sym}${sym}${sym}`;
}

function getFontColor(magnitude?: number): string {
  return magnitude !== undefined && magnitude > 0.5 ? 'white' : 'black';
}

interface ConfusionTableProps {
  classDefs: string[];
  predSums: { [index: string]: number };
  labelSums: { [index: string]: number };
  conf: { [index: string]: { [index: string]: number } };
  totalSum: number;
  bestCell: number;
}

export default class ConfusionTable extends React.PureComponent<ConfusionTableProps, {}> {
  render() {
    const {
      classDefs, predSums, labelSums, conf, totalSum, bestCell,
    }: ConfusionTableProps = this.props;
    const showNum = classDefs.length <= 10;
    const pad = classDefs.reduce((p, v) => p.length < `${v}`.length ? `${v}` : p, '');

    return (
      <Table>
        <tbody>
          <Row>
            <Cell>&nbsp;</Cell>
            {
              classDefs.map((p) => (<Cell key={p}>{p}</Cell>))
            }
            <Cell>← Pred.</Cell>
          </Row>
          {
            classDefs.map((l, lix) => (
            <Row key={l}>
              <Cell>{l}</Cell>
              {
                classDefs.map((p, pix) => (
                  <Cell
                    key={p}
                    title={`Predicted Label: ${p}\nActual Label: ${l}\nCount: ${formatValue(conf[p][l])}`}
                    isT={lix === 0}
                    isB={lix === classDefs.length - 1}
                    isL={pix === 0}
                    isR={pix === classDefs.length - 1}
                    magnitude={conf[p][l] / bestCell}
                  >
                    {showNum ? formatValue(conf[p][l]) : null}
                  </Cell>
                ))
              }
              <Cell title={`Label: ${l} Count: ${formatValue(labelSums[l])}`}>
                {showNum ? formatValue(labelSums[l]) : null}
              </Cell>
            </Row>
            ))
          }
          <Row>
            <Cell>↑ Label</Cell>
            {
              classDefs.map((p) => (
                <Cell key={p} title={`Predicted Label: ${p} Count: ${formatValue(predSums[p])}`}>
                  {showNum ? formatValue(predSums[p]) : null}
                </Cell>
              ))
            }
            <TotalCell title={`Count: ${formatValue(totalSum)}`}>
              {showNum ? formatValue(totalSum) : null}
            </TotalCell>
          </Row>
          <DummyRow>
            <DummyCell>&nbsp;</DummyCell>
            {classDefs.map((p) => (<DummyCell key={p}>{showNum ? formatValue(totalSum) : pad}</DummyCell>))}
            <DummyCell>{showNum ? formatValue(totalSum) : pad}</DummyCell>
          </DummyRow>
        </tbody>
      </Table>
    );
  }
}
