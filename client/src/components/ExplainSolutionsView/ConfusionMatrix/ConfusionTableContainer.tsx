import * as React from 'react';
import styled from 'styled-components';
import ConfusionTable from './ConfusionTable';

import { formatValue } from '../../../data/utils';

const TdAlignRight = styled.td`
  text-align: right;
`;

interface ConfusionTableContainerProps {
  conf: { [index: string]: { [index: string]: number } };
  classDefs: string[];
}

interface ConfusionTableContainerState {
  labelSums: { [index: string]: number };
  predSums: { [index: string]: number };
  totalSum: number;
  acc: number;
  precRecs: [ string, number, number ][];
  bestCell: number;
}

export default class ConfusionTableContainer extends React.PureComponent<ConfusionTableContainerProps,
  ConfusionTableContainerState> {
  constructor(props: ConfusionTableContainerProps) {
    super(props);
    this.state = {
      predSums: {},
      labelSums: {},
      totalSum: -1,
      acc: 0,
      precRecs: [],
      bestCell: 0,
    };
  }

  componentWillMount() {
    this.propsToState(
        {
          conf: {},
          classDefs: [],
        },
        this.props,
    );
  }

  componentWillReceiveProps(nextProps: ConfusionTableContainerProps) {
    this.propsToState(this.props, nextProps);
  }

  propsToState(props: ConfusionTableContainerProps, nextProps: ConfusionTableContainerProps) {
    const { conf, classDefs } = nextProps;
    if (props.conf === conf && props.classDefs === classDefs) {
      return;
    }
    if (classDefs.some((c) => `${c}` === `${null}`)) {
      console.warn('null as class might cause problems!');
    }
    const predSums = {};
    const labelSums = {};
    let totalSum = 0;
    let bestCell = 1;
    classDefs.forEach((c) => {
      const ps = classDefs.reduce((p, v) => p + +conf[c][v], 0);
      totalSum += ps;
      predSums[c] = ps;
      labelSums[c] = classDefs.reduce((p, v) => p + +conf[v][c], 0);
      classDefs.forEach((v) => {
        bestCell = Math.max(bestCell, +conf[v][c]);
      });
    });
    const diag = classDefs.reduce((p, v) => p + +conf[v][v], 0);
    const acc = diag / Math.max(1, totalSum) * 100.0;
    const precRecs = classDefs.map((v) => {
      const prec = +conf[v][v] / Math.max(1, predSums[v]) * 100.0;
      const rec = +conf[v][v] / Math.max(1, labelSums[v]) * 100.0;
      const res: [ string, number, number ] = [ v, prec, rec ]; // NOTE(joschi): TS type checker is fragile!
      return res;
    });
    this.setState({
      predSums,
      labelSums,
      totalSum,
      acc,
      precRecs,
      bestCell,
    });
  }

  render() {
    const {
      classDefs, conf,
    } = this.props;
    const {
      predSums, labelSums, totalSum, acc, precRecs, bestCell,
    } = this.state;
    return (
      <div>
        <h5>Confusion Matrix</h5>
        <ConfusionTable
          classDefs={classDefs}
          predSums={predSums}
          labelSums={labelSums}
          conf={conf}
          totalSum={totalSum}
          bestCell={bestCell}
        />
        <table className="table" style={{width: 'auto'}}>
          <thead>
            <tr>
              <th>&nbsp;</th>
              <th>&nbsp;Precision</th>
              <th>&nbsp;Recall</th>
            </tr>
          </thead>
          <tbody>
          {
            precRecs.map((p) => (
              <tr key={p[0]}>
                <TdAlignRight>&nbsp;{formatValue(p[0])}</TdAlignRight>
                <TdAlignRight>&nbsp;{formatValue(p[1])}%</TdAlignRight>
                <TdAlignRight>&nbsp;{formatValue(p[2])}%</TdAlignRight>
              </tr>
            ))
          }
          </tbody>
        </table>
        <div>Accuracy: {formatValue(acc)}%</div>
      </div>
    );
  }
}
