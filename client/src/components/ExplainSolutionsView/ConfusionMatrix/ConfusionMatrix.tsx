import * as React from 'react';
import ConfusionTableContainer from './ConfusionTableContainer';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';

interface ConfusionMatrixProps {
  data: d3m.ConfusionMatrixData;
}

class ConfusionMatrix extends React.PureComponent<ConfusionMatrixProps, {}> {
  constructor(props: ConfusionMatrixProps) {
    super(props);
  }

  render() {
    const { conf, classDefs } = this.props.data;
    return <ConfusionTableContainer conf={conf} classDefs={classDefs}/>;
  }
}

export default connectPropsToDatastore<ConfusionMatrixProps>(store => ({
}))(ConfusionMatrix);
