import * as React from 'react';
import * as Icon from 'react-feather';
import * as d3m from 'd3m/dist';
import Loadable from '../data/Loadable';
import { CardShadow, CardButton, CardAttrField, CardAttrValue } from './Card/Card';
import { Link, Redirect } from 'react-router-dom';
import { PageHeader } from './PageHeader/PageHeader';
import { startCase } from 'lodash';
import { connectPropsToDatastore } from 'src/data/store';
import styled from 'styled-components';
import { GreenSpan } from './styled';
import { HelpText } from './HelpText/HelpText';

const CardAttrField110px = styled(CardAttrField)`
  font-weight: bold;
  width: 72px;
`;

interface SelectTaskProps {
  dataset: d3m.Dataset;
  availableProblems: Loadable<d3m.Problem[]>;
  setDefineProblemStatus: (defineProblemStatus: boolean) => void;
  setExistProblem: (problem: d3m.Problem) => void;
  setInitialNewProblem: () => void;
}

class SelectTaskView extends React.PureComponent<SelectTaskProps> {
  render() {
    const { dataset, availableProblems, setExistProblem,
      setInitialNewProblem: setiInitialNewProblem, setDefineProblemStatus} = this.props;
    if (!dataset) {
      return <Redirect to="/" />;
    }
    return (
      <div>
        <PageHeader title="Select Task" />

        <h5 className="mb-3">
          Now your are ready to define a problem
        </h5>

        <HelpText>
          You chose <mark className="badge-pill badge-secondary">
          {dataset.about.datasetID}</mark>.&nbsp;
          {
            availableProblems.value && availableProblems.value.length > 0 ?
            <>
              It seems like you used this dataset before (we found&nbsp;
              {availableProblems.value.length} problem(s) for this dataset).<br/>
              Now you can either <GreenSpan>define your problem from scratch </GreenSpan>
              or <GreenSpan>use an existing problem setting </GreenSpan>
              that your machine learning model will solve.
            </>
            :
            <>
            It seems like you never used this dataset before.<br/>
            You will need to <GreenSpan>define your problem from scratch</GreenSpan>.
            </>
          }
        </HelpText>

        <div className="mt-3 mb-3">
          <div className="row">
            <div className="col-lg-4 mb-3 pb-5">
            <Link className={'dataset-link'} to="define-problem" onClick={ () => {
                setDefineProblemStatus(false);
                setiInitialNewProblem();
              }
            }>
              <CardShadow>
                <CardButton>
                  <h6 data-cy="btn-define-problem">
                    Define a problem <br/> from scratch
                  </h6>
                </CardButton>
              </CardShadow>
            </Link>
            </div>
            {availableProblems.value && availableProblems.value.map((problem, i) => (
              <div key={i + '-problem'} className="col-lg-4 mb-3">
                  <CardShadow>
                    <div className="mb-2" style={{height: '165px', overflow: 'scroll'}}>
                      <h6><b>Setting {i + 1}: {startCase(problem.about.taskType)}</b></h6>
                      <div className="row mb-2 mt-3">
                        <CardAttrField110px>ID:</CardAttrField110px>
                        <CardAttrValue>{problem.about.problemID}</CardAttrValue>
                      </div>
                      <div className="row mb-2">
                        <CardAttrField110px>Target:</CardAttrField110px>
                        <CardAttrValue>{startCase(problem.inputs.data[0].targets[0].colName)}</CardAttrValue>
                      </div>
                      <div className="row mb-2">
                        <CardAttrField110px>Name:</CardAttrField110px>
                        <CardAttrValue>{problem.about.problemName}</CardAttrValue>
                      </div>
                    </div>
                    <Link
                      className={'dataset-link'} to="define-problem" onClick={() => {
                        setExistProblem(problem);
                        setDefineProblemStatus(true);
                      }}
                    >
                      <button className="btn btn-outline-primary">
                        <Icon.ChevronsRight className="feather" /> Use this settings
                      </button>
                    </Link>
                  </CardShadow>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<SelectTaskProps>(store => ({
  dataset: store.selectedDataset,
  availableProblems: store.problems,
  setDefineProblemStatus: store.setDefineProblemStatus,
  setExistProblem: store.setExistProblem,
  setInitialNewProblem: store.setInitialNewProblem,
}))(SelectTaskView);
