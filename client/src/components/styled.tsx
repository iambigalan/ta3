import styled from 'styled-components';
import { Button } from 'react-bootstrap';

export const FlatButton = styled(Button)`
  font-size: 1em;
  line-height: 1em;
  padding: .1em;
  margin: .2em;
  background-color: transparent;

  .icon { width: 1em }

  > * {
    vertical-align: middle;
  }

  > span:last-child {
    margin-left: .25em;
  }

  &[disabled] {
    opacity: .25;
  }

  &:hover:not([disabled]) {
    background-color: #eee;
  }
`;

export const IconButton = styled(Button)`
  color: #555;
  line-height: 1em;
  background: none;
  padding: 0;
  &:hover {
    opacity: .5;
  }
  &:focus {
    box-shadow: none;
  }
`;

export const GreenSpan = styled.span`
  color: #12a912;
  font-weight: bold;
`;
