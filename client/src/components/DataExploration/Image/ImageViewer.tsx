import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import './ImageViewer.css';

interface ImageViewerProps {
  data: d3m.ResourceFileData;
  getURLToFile: (filePath: string) => string;
}

class ImageViewer extends React.PureComponent<ImageViewerProps, {}> {
  render() {
    const imageData = this.props.data;
    return (
      <div className="image-viewer">
        <div className="mb-1">
          <b>{ imageData.name }</b>
        </div>
        <img
          className="img-thumbnail"
          src={this.props.getURLToFile(imageData.src)}
        />
      </div>
    );
  }
}

export default connectPropsToDatastore<ImageViewerProps>(store => ({
  getURLToFile: store.getURLToFile,
}))(ImageViewer);
