
import * as React from 'react';

import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';

import { Tab, Tabs } from '../../Tabs/Tabs';
import TextSummary from './TextSummary';
import TextList from './TextList';

interface TextViewProps {
  resource: d3m.DataResource;
  data: d3m.ResourceFileData[];
  summary: d3m.ColumnProfile[];
}

enum TextVisualizations {
  Instance,
  Summary,
}

interface TextViewState {
  show: TextVisualizations;
}

class TextView extends React.Component<TextViewProps, TextViewState> {
  constructor(props: TextViewProps) {
    super(props);
    this.state = {
      show: TextVisualizations.Summary,
    };
  }

  renderView() {
    const { data, summary } = this.props;
    switch (this.state.show) {
      case TextVisualizations.Summary:
        return <TextSummary data={summary}/>;
      case TextVisualizations.Instance:
        return <TextList data={data}/>;
      default:
        return <div>Not Imlemented</div>;
    }
  }

  changeVisualization(visualization: TextVisualizations) {
    this.setState({show: visualization});
  }

  render() {
    const { show } = this.state;
    return (
      <div>
        <Tabs>
          <Tab selected={show === TextVisualizations.Summary}
            onClick={() => this.changeVisualization(TextVisualizations.Summary)}>Summary</Tab>
          <Tab selected={show === TextVisualizations.Instance}
            onClick={() => this.changeVisualization(TextVisualizations.Instance)}>Instances</Tab>
        </Tabs>
        {this.renderView()}
      </div>
    );
  }
}

export default connectPropsToDatastore<TextViewProps>((store, ownProps) => ({
  data: store.getResourceSamples(ownProps.resource!),
  summary: store.getColumnsStats(ownProps.resource!),
}))(TextView);
