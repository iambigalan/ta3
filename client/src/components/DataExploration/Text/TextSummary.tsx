import React from 'react';
import * as d3m from 'd3m/dist';
import { scaleLinear } from 'd3';
interface TextSummaryProps {
    data: d3m.ColumnProfile[];
    numWords?: number;
    numColumns?: number;
    heightTextContainer?: number;
    widthTextContainer?: number;
}

class TextSummary extends React.PureComponent<TextSummaryProps> {
    render() {
        const { data, numWords, heightTextContainer, widthTextContainer } = this.props;
        const words =  numWords ? data[0].values.filter((word, idx) => (idx < numWords)) : data[0].values;
        const numColumns = this.props.numColumns ? this.props.numColumns : 4;
        const numRows = Math.ceil(words.length / numColumns);
        const width = widthTextContainer ? widthTextContainer : 500;
        const height = heightTextContainer ? heightTextContainer : 300;
        const rowHeight = (height - 10) / numRows;
        const colWidth = width / numColumns;

        const barScale = scaleLinear()
            .range([0, colWidth - 10])
            .domain([0, Math.max(...words.map(d => d.count))]);

        return <svg
            width={width}
            height={height}
            style={{ border: 'solid 0px #ccc', cursor: 'pointer' }}>
            <g transform="translate(5, 5)">
                {words.map((word, idx) => <g
                    key={word.name.toString()}
                    transform={`translate(${Math.floor(idx / numRows) * colWidth},${(idx % numRows) * rowHeight})`}>
                    <rect
                        y={4}
                        height={rowHeight - 4}
                        width={barScale(word.count)}
                        fill="steelblue"
                        opacity="0.2"
                    />
                    <text
                        dy={rowHeight / 2}
                        dx={2}
                        style={{fontSize: '14px'}}
                        alignmentBaseline="central">
                        {word.name.toString()}
                    </text>
                </g>)}
            </g>
        </svg>;
    }
}

export default TextSummary;
