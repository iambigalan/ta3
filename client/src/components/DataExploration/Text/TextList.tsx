import React from 'react';
import * as d3m from 'd3m/dist';
import DataSelector from '../DataSelector';
import TextPlot from './TextPlot';

interface TextListProps {
  data: d3m.ResourceFileData[];
}

interface TextListState {
  selected: string[];
}

const DEFAULT_TEXT_COUNT = 3;
class TextList extends React.PureComponent<TextListProps, TextListState> {
  constructor(props: TextListProps) {
    super(props);
    this.state = {
      selected: this.props.data.slice(0, DEFAULT_TEXT_COUNT).map(d => d.name),
    };
  }
  onSelectedChange = (selected: string[]) => {
    this.setState({ selected });
  };
  render() {
    const selected = new Set(this.state.selected);
    const data = this.props.data.filter(d => selected.has(d.name));
    return (
      <>
        <DataSelector
          data={this.props.data}
          selected={this.state.selected}
          onSelectedChange={this.onSelectedChange}
        />
        {
          data.map((d, index) => (
            <div className="m-1" key={index}>
              <TextPlot key={d.name} data={d}/>
            </div>
          ))
        }
      </>
    );
  }
}

export default TextList;
