
import * as React from 'react';
import Select from 'react-select';
import { capitalize } from 'src/data/utils';

interface TimeSeriesSelectorProps {
  data: Array<{ name: string }>;
  selected: string[];
  onSelectedChange: (selected: string[]) => void;
}

class DataSelector extends React.PureComponent<TimeSeriesSelectorProps, {}> {
  onSelectedChange = (value: Array<{ label: string, value: string }>) => {
    this.props.onSelectedChange(value.map(v => v.value));
  }

  render() {
    const data = this.props.data;
    const options = data.map(d => ({ value: d.name, label: capitalize(d.name) }));
    const valueSet = new Set(this.props.selected);
    const defaultValue = options.filter(option => valueSet.has(option.value));
    return (
      <div>
        <Select
          options={options}
          defaultValue={defaultValue}
          isMulti={true}
          onChange={this.onSelectedChange}
        />
      </div>
    );
  }
}

export default DataSelector;
