import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import VideoViewer from './VideoViewer';
import DataSelector from '../DataSelector';
import { Row, Col } from 'react-bootstrap';

const DEFAULT_VIDEO_COUNT = 1;

interface VideoViewProps {
  resource: d3m.DataResource;
  data: d3m.ResourceFileData[];
}

interface VideoViewState {
  selected: string[];
}

class VideoView extends React.Component<VideoViewProps, VideoViewState> {
  constructor(props: VideoViewProps) {
    super(props);
    this.state = {
      selected: this.props.data.slice(0, DEFAULT_VIDEO_COUNT).map(d => d.name),
    };
  }

  onSelectedChange = (selected: string[]) => {
    this.setState({ selected });
  }

  render() {
    const selected = new Set(this.state.selected);
    const data = this.props.data.filter(d => selected.has(d.name));
    return (
      <>
        <div className="mb-3">
          <DataSelector
            data={this.props.data}
            selected={this.state.selected}
            onSelectedChange={this.onSelectedChange}
          />
        </div>
        <Row>
          {
            data.map(d => (
              <Col md={12} key={d.name}>
                <VideoViewer data={d}/>
              </Col>
            ))
          }
        </Row>
      </>
    );
  }
}

export default connectPropsToDatastore<VideoViewProps>(store => ({
}))(VideoView);
