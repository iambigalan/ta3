
import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';

interface VideoViewerProps {
  data: d3m.ResourceFileData;
}

class VideoViewer extends React.PureComponent<VideoViewerProps, {}> {
  render() {
    const videoData = this.props.data;
    return (
      <div>
        <hr/>
        <div className="mb-2">
          <b>{ videoData.name }</b>
        </div>
        <div>
          <video controls={true} style={{ width: '100%' }}>
            <source src={videoData.src}/>
            Video not supported by the browser.
          </video>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<VideoViewerProps>(store => ({
}))(VideoViewer);
