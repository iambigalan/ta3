import * as React from 'react';
import * as Icon from 'react-feather';
import * as d3m from 'd3m/dist';

interface NotSupportedViewProps {
  resource: d3m.DataResource;
}

class NotSupportedView extends React.PureComponent<
  NotSupportedViewProps
> {
  render() {
    const { resType } = this.props.resource;
    return (
      <p>
        <Icon.AlertCircle className="feather"/> Resources of type{' '}
        <i>{resType}</i> are currently not supported.
      </p>
    );
  }
}

export default NotSupportedView;
