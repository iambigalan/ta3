
import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as Icon from 'react-feather';
import * as WaveSurfer from 'wavesurfer.js';
import { Button } from 'react-bootstrap';
import styled from 'styled-components';
import * as d3m from 'd3m/dist';

const ControlButton = styled(Button)`
  padding: 0 .05em;
  margin: 0 .1em;

  &:hover {
    color: #57068c;
  }
`;

const UPDATE_TIME_INTERVAL_MS = 20;

interface AudioViewerProps {
  data: d3m.ResourceFileData;
  getURLToFile: (url: string) => string;
}

interface AudioViewerState {
  isPlaying: boolean;
  currentTime: number;
}

class AudioViewer extends React.PureComponent<AudioViewerProps, AudioViewerState> {
  private surfer: WaveSurfer;
  private surferRef: HTMLDivElement;
  private updateTimeInterval: NodeJS.Timeout | null = null;

  constructor(props: AudioViewerProps) {
    super(props);
    this.state = {
      isPlaying: false,
      currentTime: -1, // differ from zero to allow the first updateTime call
    };
  }

  componentDidMount() {
    this.surfer = WaveSurfer.create({
      container: this.surferRef,
    });
    this.surfer.load(this.props.getURLToFile(this.props.data.src));
    this.surfer.on('ready', () => this.updateTime());
    this.surfer.on('seek', () => this.updateTime());
  }

  componentWillUnmount() {
    this.surfer.destroy();
  }

  play() {
    this.setState({ isPlaying: true });
    this.surfer.play();
    this.clearUpdateTimeInterval();
    this.updateTimeInterval = setInterval(() => this.updateTime(), UPDATE_TIME_INTERVAL_MS);
  }

  pause() {
    this.setState({ isPlaying: false });
    this.surfer.playPause();
    this.clearUpdateTimeInterval();
  }

  stop() {
    this.setState({ isPlaying: false });
    this.surfer.stop();
    this.clearUpdateTimeInterval();
  }

  clearUpdateTimeInterval() {
    if (this.updateTimeInterval !== null) {
      clearInterval(this.updateTimeInterval);
    }
  }

  /**
   * Formats a floating point second value into hh:mm:ss.
   */
  formatTime(t: number): string {
    const ms = ('00' + Math.floor(t * 1000)).slice(-3);
    t = Math.floor(t);
    const h = Math.floor(t / 3600);
    const m = ('0' + Math.floor(t / 60) % 60).slice(-2);
    const s = ('0' + t % 60).slice(-2);
    return (h > 0 ? h + ':' : '') + m + ':' + s + ':' + ms;
  }

  updateTime() {
    this.setState({
      currentTime: this.surfer.getCurrentTime(),
    });
  }

  renderTime() {
    if (!this.surfer) {
      return null;
    }
    return this.formatTime(this.state.currentTime) + ' / ' + this.formatTime(this.surfer.getDuration());
  }

  render() {
    const audioData = this.props.data;
    return (
      <div>
        <hr/>
        <div className="mb-2">
          <b className="mr-1">{ audioData.name }</b>
          <ControlButton onClick={() => this.state.isPlaying ? this.pause() : this.play()}>
            {
              this.state.isPlaying ? <Icon.PauseCircle className="feather"/>  :
              <Icon.PlayCircle className="feather"/>
            }
          </ControlButton>
          <ControlButton onClick={() => this.stop()}>
            <Icon.StopCircle className="feather"/>
          </ControlButton>
          <span className="ml-2">{ this.renderTime() }</span>
        </div>
        <div className="audioViewerWaveContainer">
          <div ref={e => this.surferRef = e as HTMLDivElement}/>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<AudioViewerProps>(store => ({
  getURLToFile: store.getURLToFile,
}))(AudioViewer);
