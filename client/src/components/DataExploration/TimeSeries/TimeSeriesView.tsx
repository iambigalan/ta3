
import * as React from 'react';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import TimeSeriesPlot from './TimeSeriesPlot';
import DataSelector from '../DataSelector';

const DEFAULT_TIMESERIES_COUNT = 5;

interface TimeSeriesViewProps {
  resource: d3m.DataResource;
  data: d3m.ResourceFileData[];
}

interface TimeSeriesViewState {
  selectedSeries: string[]; // names of the selected time series
}

class TimeSeriesView extends React.Component<TimeSeriesViewProps, TimeSeriesViewState> {
  constructor(props: TimeSeriesViewProps) {
    super(props);
    this.state = {
      selectedSeries: this.props.data.slice(0, DEFAULT_TIMESERIES_COUNT).map(d => d.name),
    };
  }

  onSelectedSeriesChange = (selected: string[]) => {
    this.setState({
      selectedSeries: selected,
    });
  }

  render() {
    const selected = new Set(this.state.selectedSeries);
    const data = this.props.data.filter(d => selected.has(d.name));
    return (
      <>
        <div className="mb-3">
          <DataSelector
            data={this.props.data}
            selected={this.state.selectedSeries}
            onSelectedChange={this.onSelectedSeriesChange}
          />
        </div>
        {
          data.map((d, index) => (
            <div className="m-1" key={index}>
              <TimeSeriesPlot key={d.name} data={d}/>
            </div>
          ))
        }
      </>
    );
  }
}

export default connectPropsToDatastore<TimeSeriesViewProps>((store, ownProps) => ({
  data: store.dataForResource(ownProps.resource!) as d3m.ResourceFileData[],
}))(TimeSeriesView);
