import * as React from 'react';
import * as Icon from 'react-feather';
import { startCase } from 'lodash';
import DataResourceIcons from './DataResourceIcons';

interface DataResourceHeaderProps {
  id: string;
  resourceType: string;
  active: boolean;
  clickFn: () => void;
}

class DataResourceHeader extends React.PureComponent<DataResourceHeaderProps> {
  /*
   * Data can be of following types: boolean, integer, real, string, categorical, dateTime, realVector, json, geojson
   * For more details see:
   * https://gitlab.datadrivendiscovery.org/MIT-LL/d3m_data_supply/blob/shared/documentation/datasetSchema.md
   */
  getIconForResourceType() {
    return DataResourceIcons[this.props.resourceType];
  }

  currentClass() {
    if (this.props.active) {
      return 'nav-link flex-sm-fill text-sm-center active';
    } else {
      return 'nav-link flex-sm-fill text-sm-center';
    }
  }

  currentLinkStyle() {
    if (this.props.active) {
      return {
        color: '#fff',
        cursor: 'pointer',
      };
    } else {
      return {
        color: 'var(--primary)',
        cursor: 'pointer',
      };
    }
  }

  getEyeIcon() {
    if (this.props.active) {
      return <Icon.Eye className="feather" />;
    } else {
      return <Icon.EyeOff className="feather icon-muted" />;
    }
  }

  render() {
    return (
      <li className="nav-item">
        <a
          className={this.currentClass()}
          style={this.currentLinkStyle()}
          onClick={this.props.clickFn}
        >
          {this.getIconForResourceType()}{' '}
          {startCase(this.props.id) + ': ' + startCase(this.props.resourceType) + ' '}
          {this.getEyeIcon()}
        </a>
      </li>
    );
  }
}

export default DataResourceHeader;
