import * as React from 'react';
import * as d3m from 'd3m/dist';
import { Redirect } from 'react-router-dom';
import { CardAttrField, CardAttrValue } from '../Card/Card';
import { connectPropsToDatastore } from 'src/data/store';
// import { startCase } from 'lodash';

interface FormFieldProps {
    label: string;
    forId?: string;
    labelPadding?: string;
}

class FormField extends React.PureComponent<FormFieldProps> {
    render() {
        const { label, forId, labelPadding } = this.props;
        return (
        <div className="row mb-2">
            <CardAttrField width="160px" padding={labelPadding}>
            <label className="col-form-label" htmlFor={forId}>
                {label}
            </label>
            </CardAttrField>
            <CardAttrValue>
            <div style={{padding: '7px 0'}}>
            {this.props.children}
            </div>
            </CardAttrValue>
        </div>
        );
    }
}

interface CheckboxFieldProps {
    metric: string;
    metricLabel: string;
    checked: boolean;
    onChange: (status: boolean) => void;
}

class MetricCheckbox extends React.PureComponent<CheckboxFieldProps> {
    render() {
      const { metric, metricLabel, checked, onChange } = this.props;
      const metricInputId = 'chck-bx-metric-' + metric;
      return (
        <div className="form-check form-check">
          <input
            className="form-check-input"
            type="checkbox"
            onChange={(e) => onChange(e.target.checked)}
            id={metricInputId}
            checked={checked}
          />
          <label className="form-check-label" htmlFor={metricInputId}>
            {metricLabel}
          </label>
        </div>
      );
    }
  }

enum TimeUnits {
  Minutes = 1,
  Hours = 60,
}

interface ConfigureSearchViewProps {
    problemDefinition: d3m.ProblemDefinition;
    setProblemDefinition: (problemDefinition: d3m.ProblemDefinition) => void;
    selectedProblem?: d3m.Problem; // Configure search
    setProblemMetrics: (metrics: d3m.Metrics[]) => void; // Configure search
    selectedConfig: string;
    setSelectedConfig: (selectedConfig: string) => void;
    defaultProblemDefinition: d3m.ProblemDefinition;
}

class ConfigureSearchView extends React.Component<ConfigureSearchViewProps> {
    constructor(props: ConfigureSearchViewProps) {
        super(props);
    }

    render() {
        const {
          problemDefinition,
          setProblemDefinition,
          selectedConfig,
          setSelectedConfig,
          defaultProblemDefinition,
        } = this.props;

        /* Configure search*/
        const { maxTimeUnit, maxTime } = problemDefinition;
        const availableMetrics = problemDefinition.type &&
        d3m.metrics.getMetricsForTask(problemDefinition.type as d3m.TaskType,
                                      problemDefinition.subType as d3m.TaskSubType);
        if (!availableMetrics) {
          console.error('No metrics available.', availableMetrics);
          return <Redirect to="/" />; // TODO: what we should do in this case?
        }

        let metrics = (selectedConfig === '1') ?
                new Set(defaultProblemDefinition.metrics!.map(m => m.metric))
                :
                new Set(problemDefinition.metrics!.map(m => m.metric));

        const firstMetric = String(metrics.values().next().value);
        // Getting the metric's name in a proper format (eg. f1Macro will be F1 Macro)
        const metricInProperFormat = availableMetrics.filter(m => m.metric === firstMetric).map(m => m.label);

        return (
          <div className="define-problem">

            {/* Default Configuration Mode*/}
            <label className="col-md-12 style-paragraph" key={'1'} style={{padding: 0}} >
                <input type="radio" className="mr-md-1"
                    value={'1'}
                    onChange={e => {
                        setSelectedConfig(e.target.value);
                    }}
                    checked={selectedConfig === '1' ? true : false}
                />
                {'Default Configuration'}
            </label>
            {
            selectedConfig === '1'
            ?
            <>
                <div className="sub-steps-define-problem">
                    <ul className="text-muted">
                    <li>
                    {
                    (maxTime && maxTimeUnit) ?
                    <> The search for solutions will
                        run for <b> {+maxTime} {TimeUnits[maxTimeUnit].toLowerCase()}</b> or
                        <b> until you stop it</b>. </>
                    :
                    <>The search for solutions will run indefinitely until it is stopped by you
                    or until there are no more solutions to be created.</>
                    }
                    </li>
                    <li>
                    { (metricInProperFormat) ?
                        <> <b>{metricInProperFormat}</b> will be used as the evaluation metric.</>
                        :
                        <>There is no any evaluation metric available for this search.</>
                    }
                    </li>
                    </ul>
                </div>
            </>
            :
            <>
            </>
            }

            {/* Custom Configuration Mode*/}
            <label className="col-md-12 style-paragraph" key={'2'} style={{padding: 0}} >
                <input type="radio" className="mr-md-1"
                    value={'2'}
                    onChange={e => {
                        setSelectedConfig(e.target.value);
                    }}
                    checked={selectedConfig === '2' ? true : false}
                />
                {'Custom Configuration'}
            </label>
            {
            selectedConfig === '2'
            ?
            <>
              <form className="sub-steps-define-problem">
                <FormField label={'Maximum Time:'} forId="settings-max-time-value" labelPadding="10px 15px">
                <div className="d-flex align-items-left">
                    <input
                    type="number"
                    className="form-control"
                    id="settings-max-time-value"
                    value={maxTime}
                    min="0"
                    onChange={e => {
                        setProblemDefinition({
                        ...problemDefinition,
                        maxTime: e.target.value,
                        });
                    }}
                    // onBlur={(e) => this.setState({maxTime: e.target.value || '0' }) } // TODO: Why is it zero?
                    style={{ maxWidth: '100px' }}
                    />
                    <select
                    className="custom-select mr-sm-2 ml-2"
                    id="settings-max-time-unit"
                    style={{ maxWidth: '100px' }}
                    value={maxTimeUnit}
                    onChange={e => {
                        setProblemDefinition({
                        ...problemDefinition,
                        maxTimeUnit: +e.target.value,
                        });
                    }}
                    >
                    {[TimeUnits.Minutes, TimeUnits.Hours]
                        .map(unit => <option key={unit} value={unit}>{TimeUnits[unit]}</option>)}
                    </select>
                </div>
                <span className="small text-muted">
                { (maxTime && maxTimeUnit) ?
                    `The search for solutions will run for ${+maxTime} ${TimeUnits[maxTimeUnit].toLowerCase()}.`
                    :
                    'The search for solutions will run indefinitely until it is stopped by you ' +
                    'or until there are no more solutions to be created.'
                }
                </span>
                </FormField>
                {/* <FormField label={'Maximum Solutions:'} forId="settings-max-pipelines" labelPadding="10px 15px">
                <input
                    type="number"
                    className="form-control"
                    id="settings-max-pipelines"
                    value={maxSolutions}
                    min="0"
                    onChange={e => {
                    setProblemDefinition({
                        ...problemDefinition,
                        maxSolutions: e.target.value,
                    });
                    }}
                    style={{ maxWidth: '100px' }}
                />
                <span className="small text-muted">
                { (maxSolutions) ?
                    'The search for solutions will automatically be stopped after ' +
                    `${+maxSolutions} ${+maxSolutions === 1 ? 'solution is' : 'solutions are'} created.`
                    :
                    'The search for solutions will run until the time limit or you stop it.'
                }
                </span>
                </FormField> */}

                <FormField label={'Evaluation Metrics:'} labelPadding="10px 15px">
                    {availableMetrics && availableMetrics.map((metric, i) => (
                    <MetricCheckbox
                        key={i + '-checkbox-' + metric}
                        metric={metric.metric}
                        checked={metrics.has(metric.metric)}
                        metricLabel={metric.label}
                        onChange={(status: boolean) => {
                        const newSelectedMetrics = new Set(metrics);
                        if (status) {
                            newSelectedMetrics.add(metric.metric);
                        } else {
                            newSelectedMetrics.delete(metric.metric);
                        }
                        this.props.setProblemMetrics(
                            Array.from(newSelectedMetrics).map(m => ({metric: d3m.MetricValue[m]})),
                        );
                        }}
                    />
                    ))}
                </FormField>
              </form>
            </>
            :
            <>
            </>
            }
          </div>
        );
    }
}

export default connectPropsToDatastore<ConfigureSearchViewProps>(store => ({
  problemDefinition: store.problemDefinition,
  setProblemDefinition: store.setProblemDefinition,
  selectedProblem: store.selectedProblem, // Configure search
  setProblemMetrics: store.setProblemMetrics, // Configure search
  setSelectedConfig: store.setSelectedConfig, // Configure search
  selectedConfig: store.getSelectedConfig, // Configure search
  defaultProblemDefinition: store.getDefaultProblemDefinition,
}))(ConfigureSearchView);
