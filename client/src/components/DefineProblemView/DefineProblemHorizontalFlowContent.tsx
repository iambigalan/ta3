import * as React from 'react';
import * as d3m from 'd3m/dist';
import './define-problem-view.css';
import { startCase } from 'lodash';
import { connectPropsToDatastore } from 'src/data/store';
import ConfigureSearchView from './ConfigureSearchView';

/**
 * Content for each Define Problem Sub Step (target, type, and subtype). For each sub step, all the
 * possible target, type and subtype available to define a problem are displayed using radio buttons.
 * Radio buttons allow the user to choose only one of a predefined set of targets, types or subtypes.
 */

interface DefineProblemHorizontalFlowContentProps {
  step: number;
  columns: d3m.Feature[];
  problemDefinition: d3m.ProblemDefinition;
  setProblemDefinition: (problemDefinition: d3m.ProblemDefinition) => void;
}

function getFeatureID(c: d3m.Feature): string {
  return `${c.resID}:${c.colName}`;
}

class DefineProblemHorizontalFlowContent extends React.Component<DefineProblemHorizontalFlowContentProps> {
  constructor(props: DefineProblemHorizontalFlowContentProps) {
    super(props);
    // TODO: The main data resource is always named "learningData". We might want to
    // make it always show up first, before other remaining secondary resources.
    // this.state = {
    // };
  }

  render() {
    const {
      columns,
      problemDefinition,
      setProblemDefinition,
    } = this.props;
    const taskType = problemDefinition.type;
    const taskSubType = problemDefinition.subType;
    const target = problemDefinition.target;

    switch (this.props.step) {
      case 1:
        return (
          <div>
          <p className={'style-paragraph small'}>
            Which attribute would you like your model to predict?
          </p>
          <form>
            <div className="sub-steps-define-problem">
            {
              columns.map((column, i) => {
                const label = getFeatureID(column);
                return (
                  <label key={i} className="col-lg-4" style={{padding: 0}}>
                    <input
                      className="mr-md-1"
                      type="radio"
                      value={label}
                      onChange={e => {
                        setProblemDefinition({
                          ...problemDefinition,
                          target: e.target.value,
                        });
                      }}
                      checked={target === label ? true : false}
                    />
                    {label}
                  </label>
                );
              })
            }
            </div>
          </form>
          </div>
        );
      case 2:
        return (
          <div className="define-problem">
            {/* Defining problem type */}
            <p className={'style-paragraph small'}>
              Among the problem <b>types</b> bellow, which one
              best characterizes your problem?
            </p>
            <form>
              <div className="sub-steps-define-problem">
              {
                Object.keys(d3m.TaskType).map(k =>
                <label className="col-md-4" key={k} style={{padding: 0}}>
                    <input type="radio" className="mr-md-1"
                      value={k}
                      onChange={e => {
                        setProblemDefinition({
                          ...problemDefinition,
                          type: e.target.value,
                        });
                      }}
                      checked={taskType === k ? true : false}
                    />
                    {startCase(k)}
                </label>)}
              </div>
            </form>
            {

              /* Defining problem subtype */
            taskType && this.props.problemDefinition.type !== '' && d3m.problemType[taskType].length > 0
            ?
            <>
            <p className={'style-paragraph small'}>
              Among the problem <b>sub-types</b> bellow, which one
              best characterizes your problem?
            </p>
            <form>
            <div className="sub-steps-define-problem">
              {
                Object.keys(d3m.TaskSubType).filter(subtype => d3m.problemType[taskType].includes(subtype)).map(k =>
                <label className="col-md-4" key={k} style={{padding: 0}}>
                    <input type="radio" className="mr-md-1"
                      value={k}
                      onChange={e => {
                        setProblemDefinition({
                          ...problemDefinition,
                          subType: e.target.value,
                        });
                      }}
                      checked={taskSubType === k ? true : false}
                    />
                    {startCase(k)}
                </label>)}
              </div>
            </form>
            </>
            :
            <>
            </>
            }
          </div>
        );
      case 3:
          return ( <ConfigureSearchView/> );
      default:
        return <div>Unknown Tab</div>;
      }
  }
}

export default connectPropsToDatastore<DefineProblemHorizontalFlowContentProps>(store => ({
  columns: store.datasetFeatures,
  problemDefinition: store.problemDefinition,
  setProblemDefinition: store.setProblemDefinition,
}))(DefineProblemHorizontalFlowContent);
