import * as React from 'react';
import * as Icon from 'react-feather';

const DataResourceIcons = {
  table: <Icon.AlignJustify className="feather" />,
  video: <Icon.Film className="feather" />,
  image: <Icon.Image className="feather" />,
  audio: <Icon.Volume2 className="feather" />,
  graph: <Icon.Share2 className="feather" />,
  text: <Icon.FileText className="feather" />,
  timeseries: <Icon.Activity className="feather" />,
  edgeList: <Icon.Share2 className="feather" />,
  speech: <Icon.Volume2 className="feather" />,
};

export default DataResourceIcons;
