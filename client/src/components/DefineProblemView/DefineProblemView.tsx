import * as React from 'react';
import * as d3m from 'd3m/dist';
import { PageHeader } from '../PageHeader/PageHeader';
import DataResourceHeader from './DataResourceHeader';
import NotSupportedView from '../DataExploration/NotSupportedView';
import './define-problem-view.css';
import DataResourceIcons from './DataResourceIcons';
import { capitalize } from 'src/data/utils';
import { startCase } from 'lodash';
import * as mockData from '../../mockData';
import * as Views from '../DataExploration';
import { connectPropsToDatastore } from 'src/data/store';
import DataAugmentationView from '../Datamart/DatamartAugmentationView';
import { Tab, Tabs } from '../Tabs/Tabs';
import DefineProblemStepper from './DefineProblemStepper';
import { Link } from 'react-router-dom';
import * as Icon from 'react-feather';
import { HelpText } from '../HelpText/HelpText';
import { GreenSpan } from '../styled';

interface DefineProblemViewProps {
  selectedDataset: d3m.Dataset;
  selectedProblem: d3m.Problem;
  dataset: d3m.Dataset;
  defineProblemStatus: boolean;
  defineProblemObject: d3m.ProblemDefinition;
  changeDataset: (datasetID: string) => void;
  getFeatureID: (c: d3m.Feature) => string;
  selectDataset: (dataset: d3m.Dataset) => void;
  setDefineProblemStatus: (defineProblemStatus: boolean) => void;
  searchSolutions: (params: d3m.SolutionSearchRequest) => void;

}

enum DefineProblemTab {
  DATA_PROFILE,
  DATA_AUGMENTATION,
}

interface DefineProblemViewState {
  activeResources: { [resId: string]: boolean };
  selectedTab: DefineProblemTab;
}

class DefineProblemView extends React.Component<
  DefineProblemViewProps,
  DefineProblemViewState
  > {
  constructor(props: DefineProblemViewProps) {
    super(props);
    let resourceState: { [resId: string]: boolean } = {};
    // TODO: The main data resource is always named "learningData". We might want to
    // make it always show up first, before other remaining secondary resources.
    for (let i = 0; i < props.selectedDataset.dataResources.length; i++) {
      // Make all supported resource types active by default.
      const resource = props.selectedDataset.dataResources[i];
      const type = resource.resType;
      resourceState[resource.resID] = type !== 'graph' && type !== 'edgeList' && type !== 'speech';
    }
    this.state = {
      activeResources: resourceState,
      selectedTab: DefineProblemTab.DATA_PROFILE,
    };
  }

  resourceState(resID: string): boolean {
    return this.state.activeResources[resID];
  }

  getActiveResources(): JSX.Element[] {

    const activeResources = this.props.selectedDataset.dataResources.filter(
      resource => this.state.activeResources[resource.resID],
    );

    const resources = activeResources.map(resource => {
      //
      // TODO: Add support for other resource types
      //
      let element: JSX.Element = <NotSupportedView resource={resource} />;
      if (resource.resType === 'table') {
        element = <Views.TableView
          resource={resource}
        />;
      } else if (resource.resType === 'video') {
        element = <Views.VideoView
          resource={resource}
          data={mockData.videoData}
        />;
      } else if (resource.resType === 'image') {
        element = <Views.ImageView
          resource={resource}
        />;
      } else if (resource.resType === 'audio') {
        element = <Views.AudioView
          resource={resource}
        />;
      } else if (resource.resType === 'timeseries') {
        element = <Views.TimeSeriesView
          resource={resource}
        />;
      } else if (resource.resType === 'text') {
        element = <Views.TextView
          resource={resource}
        />;
      }
      return {
        id: resource.resID,
        type: resource.resType,
        element,
      };
    });
    if (!resources.length) {
      return [(
        <div className="list-group-item card-header" key="_none">
          No resources selected. Please select a data resource.
        </div>
      )];
    }
    return resources.map(resource => (
      <div className="list-group-item" key={resource.id}>
        <div className="mb-2 text-oswald">
          {DataResourceIcons[resource.type]} {startCase(resource.id)}: {capitalize(resource.type)}
        </div>
        {resource.element}
      </div>
    ));
  }

  toggleResource(resID: string): void {
    let newState: { [resId: string]: boolean } = Object.assign({}, this.state.activeResources);
    for (let i = 0; i < this.props.selectedDataset.dataResources.length; i++) {
      if (this.props.selectedDataset.dataResources[i].resID === resID) {
        newState[resID] = !newState[resID];
        this.setState({ activeResources: newState });
      }
    }
  }

  renderTab() {
    const { selectedTab } = this.state;
    const { selectedDataset } = this.props;
    switch (selectedTab) {
      case DefineProblemTab.DATA_PROFILE:
        return <div><div className="d-flex justify-content-begin flex-wrap flex-md-nowrap align-items-center">
          <ul className="nav nav-pills nav-resources flex-column flex-sm-row">
            {selectedDataset.dataResources.map((resource, i) => (
              <DataResourceHeader
                key={i + '_data-resource-header'}
                active={this.resourceState(resource.resID)}
                id={resource.resID}
                resourceType={resource.resType}
                clickFn={() => {
                  this.toggleResource(resource.resID);
                }}
              />
            ))}
          </ul>
        </div>
          <div className="card mt-2 mb-2">
            <div className="list-group list-group-flush">
              {this.getActiveResources()}
            </div>
          </div>
        </div>;
      case DefineProblemTab.DATA_AUGMENTATION:
        return <DataAugmentationView onAugment={(datasetID: string) => {
          this.props.changeDataset(datasetID);
          this.setState({selectedTab: DefineProblemTab.DATA_PROFILE});
        }} />;
      default:
        return <div>Unknown Tab</div>;
    }
  }

  startSearch = () => {
    const { selectedProblem } = this.props;
    const { maxTime, maxTimeUnit, maxSolutions } = this.props.defineProblemObject;
    const maxSolutionsUp = maxSolutions ? maxSolutions : ''; // maxSolutions could be empty.
    if (maxTime && maxTimeUnit) {
      const timeLimit = +maxTime * maxTimeUnit;
      this.props.searchSolutions({
        priority: 1,
        timeLimit: timeLimit,
        maxSolutions: +maxSolutionsUp,
        problem: selectedProblem!,
      });
    }

  }

  render() {
    const { target, type, subType } = this.props.defineProblemObject;
    return (
      <div className="define-problem">
        <PageHeader title="Define Problem">
          <Link to="explore-solutions">
            <button className="btn btn-sm btn-outline-primary"
                    disabled={!this.props.defineProblemStatus}
                    onClick={this.startSearch}>
              <Icon.ChevronsRight className="feather" />
              Start Solutions Search
            </button>
          </Link>
        </PageHeader>
        <h5  className="mb-3">
          What would you like to know?
        </h5>
        <HelpText>
          {
            this.props.defineProblemStatus ?
            <>
              You defined a&nbsp;
              {
                subType &&
                <>
                  <mark className="badge-pill badge-secondary">
                    {subType}
                  </mark>
                  &nbsp;
                </>
              }
              <mark className="badge-pill badge-secondary">
                {type}
              </mark>
              &nbsp;problem, where the goal is to predict the attribute&nbsp;
              <mark className="badge-pill badge-secondary">
                {target}
              </mark>
              .&nbsp;<br/>
              Now you can <GreenSpan>explore your data</GreenSpan>,&nbsp;
              <GreenSpan>edit the defined problem setting</GreenSpan>,&nbsp;
              or <GreenSpan>go straight to start solutions search</GreenSpan>,&nbsp;
              where Visus will automatically generate different solutions for the current problem.
            </>
            :
            <>
            It seems like you want to define your problem from scratch.
            Please <GreenSpan>complete the steps bellow</GreenSpan> to do it.
            </>
          }
        </HelpText>
        <br />
        <DefineProblemStepper />
        <Tabs>
          <Tab
            selected={this.state.selectedTab === DefineProblemTab.DATA_PROFILE}
            onClick={() => this.setState({ selectedTab: DefineProblemTab.DATA_PROFILE })}>
            <Icon.BarChart className="feather mr-1" />
            Data Profiling
          </Tab>
          <Tab
            selected={this.state.selectedTab === DefineProblemTab.DATA_AUGMENTATION}
            onClick={() => this.setState({ selectedTab: DefineProblemTab.DATA_AUGMENTATION })}>
            <Icon.Search className="feather mr-1" />
            Data Augmentation
          </Tab>
        </Tabs>
        <div>
          {this.renderTab()}
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<DefineProblemViewProps>(store => ({
  selectDataset: store.selectDataset,
  selectedProblem: store.selectedProblem,
  getFeatureID: store.getFeatureID,
  selectedDataset: store.selectedDataset,
  changeDataset: store.changeDataset,
  setDefineProblemStatus: store.setDefineProblemStatus,
  defineProblemStatus: store.getDefineProblemStatus,
  defineProblemObject: store.problemDefinition,
  searchSolutions: store.searchSolutions,
}))(DefineProblemView);
