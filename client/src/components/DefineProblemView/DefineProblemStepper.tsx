import * as React from 'react';
import * as Icon from 'react-feather';
import DefineProblemHorizontalFlow from './DefineProblemHorizontalFlow';
import { connectPropsToDatastore } from 'src/data/store';

/*
 * Define Problem Stepper component.
 */

interface DefineProblemStepperProps {
  defineProblemStatus: boolean;
}

interface NavWorkflowSteps {
  toStep: number;
  labelStep: string;
  iconComponent: React.ComponentType<Icon.Props>;
}

enum Steps {
WHAT_TO_PREDICT = 1,
HOW_TO_PREDICT,
ADVANCED_CONFIGURATION,
}

class DefineProblemStepper extends React.PureComponent<DefineProblemStepperProps> {
  render() {
    const workflowSteps: NavWorkflowSteps [] = [
      {toStep: Steps.WHAT_TO_PREDICT, labelStep: 'What to predict', iconComponent: Icon.Target},
      {toStep: Steps.HOW_TO_PREDICT, labelStep: 'How to predict', iconComponent: Icon.HelpCircle},
      {toStep: Steps.ADVANCED_CONFIGURATION, labelStep: 'Advanced Configuration', iconComponent: Icon.Settings},
    ];
    return (
      <div
        className="card card-attributes mb-3"
        style={{
          boxShadow: '1px 1px 5px grey',
          minWidth: 250,
          padding: 0,
        }}
      >
        <div className="card-body">
          <DefineProblemHorizontalFlow workflow={workflowSteps}/>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<DefineProblemStepperProps>(store => ({
  defineProblemStatus: store.getDefineProblemStatus,
}))(DefineProblemStepper);
