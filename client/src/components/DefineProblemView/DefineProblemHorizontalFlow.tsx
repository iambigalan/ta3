import * as React from 'react';
import * as Icon from 'react-feather';
import { startCase } from 'lodash';
import DefineProblemHorizontalFlowContent from './DefineProblemHorizontalFlowContent';
import { connectPropsToDatastore } from 'src/data/store';
import * as d3m from 'd3m/dist';
import * as api from '../../data/Api';

/*
 * Define Problem main navigation through step-by-step layout. It allows navigation between the
 * three required steps to define a problem (define: target, type, and subtype).
 */

interface IconIProps {
  iconComponent: React.ComponentType<Icon.Props>;
}

class IconTest extends React.Component<IconIProps> {
  render() {
    const { iconComponent: IconComponent } = this.props;
    return (
      <IconComponent className="feather" style={{width: 28, height: 28, strokeWidth: 2}} />
    );
  }
}

interface NavWorkflowSteps {
  toStep: number;
  labelStep: string;
  iconComponent: React.ComponentType<Icon.Props>;
}

interface DefineProblemHorizontalFlowProps {
  target: d3m.InputsTarget;
  workflow: NavWorkflowSteps[];
  defineProblemStatus: boolean;
  setDefineProblemStatus: (defineProblemStatus: boolean) => void;
  problemDefinition: d3m.ProblemDefinition;
  setProblemDefinition: (problemDefinition: d3m.ProblemDefinition) => void;
  createNewProblem: (problemDefinition: d3m.ProblemDefinition) => void;
  setDefaultProblemDefinition: () => void;
  selectedConfig: string;
  defaultProblemDefinition: d3m.ProblemDefinition;
}

interface NavWorkflowState {
  selectedStep: number;
}

class DefineProblemHorizontalFlow extends React.Component<DefineProblemHorizontalFlowProps, NavWorkflowState> {

  constructor(props: DefineProblemHorizontalFlowProps) {
    super(props);
    this.state = {
      /**
       * if the define problem status is true that means that the problem was already defined.
       * Then the selectedStep should be updated to 3 (last step).
       */
      selectedStep: this.props.defineProblemStatus ? 3 : 1,
    };
  }

  selectContentStep() {
    const { selectedStep } = this.state;
    switch (selectedStep) {
      case 1:
        return <DefineProblemHorizontalFlowContent step={1} />;
      case 2:
        return <DefineProblemHorizontalFlowContent step={2} />;
      case 3:
        return <DefineProblemHorizontalFlowContent step={3} />;
      default:
        return <div>Unknown Tab</div>;
      }
  }

  /***
   * Selected value by the user. This value could be either
   * the target, type of problem or sub-type of problem.
   * It will be updated automatically after user selection.
   */
  selectedValueStep(currentStep: number, selectedStep: number) {
    const { target, type } = this.props.problemDefinition;
    let isThereMetrics = this.props.defaultProblemDefinition.metrics &&
                         Object.keys(this.props.defaultProblemDefinition.metrics!).length  > 0 ? true : false;
    switch (currentStep) {
      case 1:
        return (!target) ? undefined : target;
      case 2:
        return ((type && target) || this.props.defineProblemStatus) ?
          startCase(type) : undefined;
      case 3:
        return ((type && target && this.props.selectedConfig === '1' && isThereMetrics )) ?
        'Default' : (type && target && isThereMetrics ) ? 'Custom' : undefined;
      default:
        return undefined;
      }
  }

  selectButtonsStep() {
    const { selectedStep } = this.state;
    const {
      setDefineProblemStatus,
      problemDefinition,
      setProblemDefinition,
      createNewProblem,
      setDefaultProblemDefinition,
    } = this.props;
    switch (selectedStep) {
      case 1:
        return  <div>
                  <button
                    data-cy="next"
                    className="btn btn-primary float-right"
                    onClick={() => {
                      this.setState({ selectedStep: selectedStep + 1 });
                      api.log('SELECT_TARGET');
                    }}
                    disabled={this.props.problemDefinition.target !== '' ? false : true}
                  >
                    <Icon.ChevronsRight className="feather"/> Next
                  </button>
                </div>;
      case 2:
          if (this.props.defineProblemStatus) {
            return <></>;
          } else {
            return (
              <div>
                <button
                  className="btn btn-primary float-right"
                  data-cy="next"
                  onClick={() => {
                    this.setState({ selectedStep: selectedStep + 1 });
                    setDefaultProblemDefinition();
                    api.log('SELECT_PROBLEM_TYPE');
                  }}
                  disabled={this.props.problemDefinition.type !== '' ? false : true}>
                  <Icon.ChevronsRight className="feather"/> Next
                </button>
                <button
                  className="btn btn-outline-primary float-right mr-3"
                  onClick={() => this.setState({ selectedStep: selectedStep - 1 })}>
                  <Icon.ChevronsLeft className="feather"/> Back
                </button>
              </div>
            );
          }
      case 3:
        if (this.props.defineProblemStatus) {
          return <></>;
        } else {
          return (
            <div>
              <button
                className="btn btn-primary float-right"
                data-cy="done"
                onClick={() => {
                  setDefineProblemStatus(true);
                  setProblemDefinition(problemDefinition);
                  createNewProblem(problemDefinition);
                  api.log('SELECT_PROBLEM_SUBTYPE');
                }}
                disabled={this.props.problemDefinition.type !== '' ? false : true}
              >
                <Icon.ChevronsRight className="feather" />Done
              </button>
              <button
                className="btn btn-outline-primary float-right mr-3"
                onClick={() => this.setState({ selectedStep: selectedStep - 1 })}
              >
                <Icon.ChevronsLeft className="feather"/> Back
              </button>
            </div>
          );
        }
      default:
        return <div>Unknown Tab</div>;
      }
  }

  render() {
    const { workflow, setDefineProblemStatus } = this.props;
    return (
      <div>
        <div className="d-flex">
          <div className="w-100">
            <nav className="d-flex" style={{marginLeft: 0, textAlign: 'center', width: '100%'}}>
              <ul className="nav row" style={{marginLeft: 0, textAlign: 'center', width: '100%'}}>
              {
                workflow.map((d, index) => {
                  const selectedStep = this.state.selectedStep;
                  const selectedValueStep = this.selectedValueStep(d.toStep, selectedStep);
                  return (
                    <div
                      key={ 'NavWorkflow_' + index}
                      className="col-md-4 align-items-center"
                      style={ d.toStep ===  selectedStep && !this.props.defineProblemStatus ?
                        {borderBottom: '3px solid #63508b', height: '81px'} :
                        {height: '81px'}
                      }
                      >
                      <li  className="nav-item">
                        <a
                          className={d.toStep <=  selectedStep ? 'nav-link-text' : 'active'}
                          style={ d.toStep <=  selectedStep ?
                            {color: '#63508b'} :
                            {color: 'silver'}
                          }
                        >
                          <IconTest iconComponent={d.iconComponent} />
                          <br />
                          <span>{d.labelStep}</span>
                          {
                            selectedValueStep &&
                            <p>
                              <small>
                                <mark style={ d.toStep <=  selectedStep ? {color: 'black'} : {color: 'silver'}}
                                      className="badge-pill badge-secondary"
                                >
                                  {selectedValueStep}
                                </mark>
                              </small>
                            </p>
                          }
                        </a>
                      </li>
                    </div>
                  );
                })
              }
              </ul>
            </nav>
            {
              this.props.defineProblemStatus ?
              <></>
              :
              <div className="card mt-2 mb-2" style={{marginBottom: 20}}>
                {this.selectContentStep()}
              </div>
            }
            {this.selectButtonsStep()}
          </div>
          <div className="flex-shrink-1 align-self-center">
          {
            this.props.defineProblemStatus &&
            <div className="">
              <button
                className="btn btn-sm btn-outline-primary float-right"
                onClick={() => setDefineProblemStatus(false)}
              >
                <Icon.Edit className="feather" /> Edit
              </button>
            </div>
          }
          </div>
        </div>
      </div>
    );
  }
}

export default connectPropsToDatastore<DefineProblemHorizontalFlowProps>((store, ownProps) => ({
  setDefineProblemStatus: store.setDefineProblemStatus,
  problemDefinition: store.problemDefinition,
  setProblemDefinition: store.setProblemDefinition,
  defineProblemStatus: store.getDefineProblemStatus,
  createNewProblem: store.createNewProblem,
  setDefaultProblemDefinition: store.setDefaultProblemDefinition,
  selectedConfig: store.getSelectedConfig, // Configure search
  defaultProblemDefinition: store.getDefaultProblemDefinition,
}))(DefineProblemHorizontalFlow);
