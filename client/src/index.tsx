import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
//
// We disabled service worker due to issues with authentication behing proxy.
// For more details, see issue https://gitlab.com/ViDA-NYU/d3m/ta3/issues/85
//
// import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root') as HTMLElement);
// registerServiceWorker();
