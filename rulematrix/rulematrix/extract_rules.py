"""
Sample config files
{
    "rulesPath": "./rule_list.mdl",
    "samplePath": "./sample.csv",
    "datasetPath": "../server/data/training_datasets/LL0/LL0_186_braziltourism/LL0_186_braziltourism_dataset",
    "problemDataset": [
        {
            "resId":"0",
            "featureIndices":[1,2,3,4,6,7,8],
            "targetIndices":[9]
        }
    ]
}
"""

import os

import pandas as pd
import numpy as np
from rulematrix.utils.io_utils import json2dict
from rulematrix.models import RuleList
from rulematrix.data_processing import get_discretizer


def train_rule_list(x, y, is_categorical, name, verbose=False, **kwargs):

    discretizer = get_discretizer(x, y, continuous_features=np.logical_not(is_categorical))

    rule_list = RuleList(name=name, discretizer=discretizer, **kwargs)
    rule_list.train(x, y)

    if verbose:
        rule_list.evaluate(x, y, 'train')
        rule_list.describe()

    return rule_list


def extract_and_save(rule_path, dataset_path, sample_path, feature_indices, target_indices, res_id="0", nan_handle=None, verbose=False, **kwargs):
    dataset_doc = json2dict(os.path.join(dataset_path, 'datasetDoc.json'))

    resources = { resource['resID']: resource for resource in dataset_doc['dataResources'] }

    resource_path = resources[res_id]['resPath']
    columns = resources[res_id]['columns']

    idx2feature_info = {int(col['colIndex']): col for col in columns}

    feature_names = [idx2feature_info[idx]['colName'] for idx in feature_indices]
    is_categorical = [idx2feature_info[idx]['colType'] == 'categorical' for idx in feature_indices]

    if len(target_indices) > 1:
        raise "The current rule matrix algorithm only supports one target feature!"
    target_info = idx2feature_info[target_indices[0]]
    if target_info['colType'] != 'categorical':
        print("Warning! The current rule matrix only supports categorical output (classification)!")
    target_name = target_info['colName']

    data_file = os.path.join(os.path.join(dataset_path, resource_path))

    df = pd.read_csv(data_file)

    # Convert maybe categorical data into numerics
    for feature_name, is_cat in zip(feature_names, is_categorical):
        if is_cat:
            df[feature_name] = df[feature_name].astype('category')
    df[target_name] = df[target_name].astype('category')

    df_num = df.copy()
    cat_columns = df_num.select_dtypes(['category']).columns
    df_num[cat_columns] = df_num[cat_columns].apply(lambda x: x.cat.codes)
    X = df_num[feature_names].values

    Y = df_num[target_name].values.astype(np.int)

    # Handle Nan Values
    if nan_handle is not None:
        assert callable(nan_handle)
        X = nan_handle(X)
        Y = nan_handle(Y, target=True)
    else:
        X = np.nan_to_num(X)
        Y = np.nan_to_num(Y)

    rule_list = train_rule_list(X, Y, is_categorical, 'rule-list', verbose, **kwargs)
    rule_list.save(rule_path)


def main(args=None):
    import sys
    import argparse
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(description='Command Line Tool for sampling data')
    parser.add_argument('--rule_path', '-m', dest='rule_path', action='store',
                        help='The path to store the rule list model. E.g. ./rule_list.mdl')
    parser.add_argument('--sample_path', '-p', dest='sample_path', action='store',
                        help='The path to store the sampled data. E.g. sample.csv')
    parser.add_argument('--dataset', '-d', dest='data_path', action='store',
                        help='The path of the dataset. E.g. LL0_8_liver_disorders_dataset')
    parser.add_argument('--features', '-f', dest='features', action='store',
                        help='The feature indices used for input. E.g. -f 1,2,3,4')
    parser.add_argument('--target', '-t', dest='target', action='store', type=int,
                        help='The target indice used for output. E.g. -t 5')
    parser.add_argument('--sample_rate', '-r', dest='sample_rate', action='store', default=4., type=float,
                        help='The rate of the sampling, default to be 4')
    parser.add_argument('--config', '-c', dest='config', action='store',
                        help='The configuration file to use.')
    parser.add_argument('--verbose', '-v', dest='verbose', action='store_const', const=True, default=False,
                        help='Whether to print more logs or info.')
    args = parser.parse_args(args)

    if args.config is not None:
        config = json2dict(args.config)
        rule_path = config['rulesPath']
        sample_path = config['samplePath']
        dataset_path = config['datasetPath']

        problem_info = config['problemDataset'][0]
        if len(config['problemDataset']) > 1:
            raise "Error: currently only 1 resource is supported! The first resource is used"

        extract_and_save(rule_path, dataset_path, sample_path, problem_info['featureIndices'],
                         problem_info['targetIndices'], problem_info['resId'], verbose=args.verbose)

    else:
        feature_idx = [int(f) for f in args.features.split(',')]
        extract_and_save(args.rule_path, args.data_path, args.sample_path, feature_idx, [args.target], '0', verbose=args.verbose)


if __name__ == '__main__':
    main()
