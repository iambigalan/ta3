#!/bin/env python3
import os
import csv
import sys
import json
import shutil
import tempfile
import extract_rules

from rulematrix.models import load_model, SurrogateMixin, RuleList
from rulematrix.server import model2json, get_stream, filter_data, compute_streams, rl2json
from rulematrix.utils.io_utils import get_path, json2dict, NumpyEncoder
from sklearn.metrics import confusion_matrix
from json import dumps
import numpy as np
import pandas as pd


def load_data(cfg, nan_handle=None):
    dataset_doc = json2dict(os.path.join(cfg["datasetPath"], "datasetDoc.json"))

    # Only use one resource dataset now
    problem_dataset = cfg["problemDataset"][0]
    res_id = problem_dataset["resId"]
    feature_indices = problem_dataset["featureIndices"]
    target_indices = problem_dataset["targetIndices"]

    resources = { resource["resID"]: resource for resource in dataset_doc["dataResources"] }

    resource_path = resources[res_id]["resPath"]
    columns = resources[res_id]["columns"]

    idx2feature_info = {int(col["colIndex"]): col for col in columns}

    feature_names = [idx2feature_info[idx]["colName"] for idx in feature_indices]
    is_categorical = [idx2feature_info[idx]["colType"] == "categorical" for idx in feature_indices]

    if len(target_indices) > 1:
        raise "The current rule matrix algorithm only supports one target feature!"
    target_info = idx2feature_info[target_indices[0]]
    if target_info["colType"] != "categorical":
        print("Warning! The current rule matrix only supports categorical output (classification)!")
    target_name = target_info["colName"]

    data_file = os.path.join(os.path.join(cfg["datasetPath"], resource_path))

    df = pd.read_csv(data_file)

    # Convert maybe categorical data into numerics
    for feature_name, is_cat in zip(feature_names, is_categorical):
        if is_cat:
            df[feature_name] = df[feature_name].astype("category")
    df[target_name] = df[target_name].astype("category")

    df_num = df.copy()
    cat_columns = df_num.select_dtypes(["category"]).columns
    df_num[cat_columns] = df_num[cat_columns].apply(lambda x: x.cat.codes)
    X = df_num[feature_names].values

    Y = df_num[target_name].values.astype(np.int)

    # Handle Nan Values
    if nan_handle is not None:
        assert callable(nan_handle)
        X = nan_handle(X)
        Y = nan_handle(Y, target=True)
    else:
        X = np.nan_to_num(X)
        Y = np.nan_to_num(Y)

    categories = [list(df[col].cat.categories) if is_cat else None for col, is_cat in zip(feature_names, is_categorical)]

    mins = np.min(X, axis=0)
    maxs = np.max(X, axis=0)
    ranges = np.vstack([mins, maxs]).T

    return {
        "data": X,
        "target": Y,
        "categories": categories,
        "is_categorical": is_categorical,
        "ranges": ranges,
        "data_name": dataset_doc["about"]["datasetName"],
        "feature_names": feature_names,
        "label_names": list(df[target_name].cat.categories)
    }


def apply_filters(data_dict, filters=None):
    if filters is None:
        return data_dict

    X, Y = filter_data(data_dict["is_categorical"], data_dict["data"], data_dict["target"], filters)
    new_data_dict = data_dict.copy()
    new_data_dict.update({"data": X, "target": Y})
    return new_data_dict


def prepare_streams(model, data_dict, conditional=True, bins=20):
    ranges = data_dict["ranges"]
    categories = data_dict["categories"]
    streams = compute_streams(model, data_dict["data"], data_dict["target"], ranges, categories, conditional, bins)
    return dumps(streams, cls=NumpyEncoder)


def prepare_model_json(model, data_dict):
    # data = get_dataset(get_model_data(model))

    meta = {
        'featureNames': data_dict['feature_names'],
        'labelNames': data_dict['label_names'],
        'isCategorical': data_dict["is_categorical"],
        'categories': data_dict["categories"],
        # 'continuous': [True] * x.shape[1],
        'ranges': data_dict["ranges"],
    }

    ret_dict = {'meta': meta, 'dataset': data_dict["data_name"], 'name': model.name}
    ret_dict.update(rl2json(model))

    if isinstance(model, SurrogateMixin):
        ret_dict['target'] = model.target.name

    return dumps(ret_dict, cls=NumpyEncoder)


def compute_support_matrix(model, x, y, y_pred) -> np.ndarray:
    # n_rules x n_instances
    assert isinstance(model, RuleList)
    decision_supports = model.decision_support(x, transform=True)
    n_classes = model.n_classes
    matrices = np.empty((len(decision_supports), n_classes, n_classes), dtype=np.float)
    for i, decision_support in enumerate(decision_supports):
        _y = y[decision_support]
        _y_pred = y_pred[decision_support]
        if len(_y):
            mat = confusion_matrix(_y, _y_pred, list(range(n_classes)))
        else:
            mat = np.zeros((n_classes, n_classes), dtype=np.float)
        matrices[i, :, :] = mat
    return matrices


def prepare_jsons(cfg, prediction_file, filters=None):
    rule_path = cfg["rulesPath"]
    rule_list = load_model(rule_path)

    data_dict = load_data(cfg)
    data_dict = apply_filters(data_dict, filters)

    model_json = prepare_model_json(rule_list, data_dict)
    with open(get_path("tmp", "model.json"), "w") as fp:
        fp.write(model_json)

    stream_json = prepare_streams(rule_list, data_dict)
    with open(get_path("tmp", "stream.json"), "w") as fp:
        fp.write(stream_json)

    # with open(prediction_file, "r") as p_in:
    df = pd.read_csv(prediction_file)
    target_names = [name for name in df.columns if name != "d3mIndex"]
    if len(target_names) != 1:
        raise ValueError("prediction file has more targets")
    y_pred = np.empty(data_dict["target"].shape, dtype=np.int)
    label2idx = {label: i for i, label in enumerate(data_dict['label_names'])}
    for i, label in enumerate(df[target_names[0]]):
        y_pred[i] = label2idx[label]

    supports = compute_support_matrix(rule_list, data_dict["data"], data_dict["target"], y_pred)
    with open(get_path("tmp", "support_mat.json"), "w") as fp:
        fp.write(dumps(supports, cls=NumpyEncoder))


def prepare(cfg, prediction_file):
    if len(cfg["problemDataset"]) != 1:
        raise ValueError("unsupported problem dataset! more than one resource")
    pd = cfg["problemDataset"][0]
    tixs = pd["targetIndices"]
    if len(tixs) != 1:
        raise ValueError("unsupported problem dataset! more than one target")
    tix_s = tixs[0]
    with tempfile.TemporaryFile(mode="w+") as fp:
        out = csv.writer(fp)
        with open(cfg["samplePath"], "r") as s_in:
            with open(prediction_file, "r") as p_in:
                ix_s = None
                ix_p = None
                tix_p = None
                for (r_s, r_p) in zip(csv.reader(s_in), csv.reader(p_in)):
                    if ix_s is None:
                        ix_s = r_s.index("d3mIndex")
                        ix_p = r_p.index("d3mIndex")
                        targets = [ hix for (hix, hname) in enumerate(r_p) if hname != "d3mIndex" ]
                        if len(targets) != 1:
                            raise ValueError("prediction file has more targets")
                        tix_p = targets[0]
                        # also write header
                    if r_s[ix_s] != r_p[ix_p]:
                        raise ValueError("inconsistent index: '{0}' != '{1}'".format(r_s[ix_s], r_p[ix_p]))
                    r_s[tix_s] = r_p[tix_p]
                    out.writerow(r_s)
        fp.flush()
        fp.seek(0)
        with open(cfg["samplePath"], "w") as s_out:
            shutil.copyfileobj(fp, s_out)


if __name__ == "__main__":
    cargs = sys.argv[1:]
    prediction_file = cargs.pop(0) # we don"t want to pass this argument
    sample_prediction_file = cargs.pop(0)
    args = cargs[:]
    config = None
    while args:
        arg = args.pop(0)
        if arg == "--config":
            config = args.pop(0)
    if config is None:
        raise ValueError("must specify config file!")
    with open(config, "r") as f_c:
        cfg = json.load(f_c)
    prepare(cfg, sample_prediction_file)
    print("executing: extract_rules.main({0})".format(cargs))
    extract_rules.main(cargs)
    prepare_jsons(cfg, prediction_file)
