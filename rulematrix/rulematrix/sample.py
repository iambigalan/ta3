import os

import pandas as pd
import numpy as np

from rulematrix.utils.io_utils import json2dict
from rulematrix.models import RuleSurrogate, create_constraints, create_sampler


def do_sample(X, is_categorical=None, is_continuous=None, ranges=None, sample_rate=4.0):
    n_features = X.shape[1]
    if is_categorical is None:
        is_categorical = np.zeros((n_features,), dtype=np.bool)
    if is_continuous is None:
        is_continuous = np.ones((n_features,), dtype=np.bool)
    if ranges is None:
        ranges = [None] * n_features

    constraints = create_constraints(is_categorical, is_continuous, ranges)
    data_distribution = create_sampler(X, constraints, cov_factor=1.0)
    n_samples = int(X.shape[0] * sample_rate)
    return data_distribution(n_samples)


def sample(dataset_path, feature_indices, sample_path, res_id="0", sample_rate=4.0, nan_handle=None):
    dataset_doc = json2dict(os.path.join(dataset_path, 'datasetDoc.json'))

    resources = { resource['resID']: resource for resource in dataset_doc['dataResources'] }

    resource_path = resources[res_id]['resPath']
    columns = resources[res_id]['columns']

    idx2feature_info = {int(col['colIndex']): col for col in columns}

    all_feature_names = [col['colName'] for col in columns]
    feature_names = [idx2feature_info[idx]['colName'] for idx in feature_indices]
    is_categorical = [idx2feature_info[idx]['colType'] == 'categorical' for idx in feature_indices]
    is_continuous = [idx2feature_info[idx]['colType'] == 'real' for idx in feature_indices]
    integer_features = [idx2feature_info[idx]['colName'] for idx in feature_indices if idx2feature_info[idx]['colType'] == 'integer']

    data_file = os.path.join(os.path.join(dataset_path, resource_path))

    df = pd.read_csv(data_file)

    # Convert maybe categorical data into numerics
    for feature_name, is_cat in zip(feature_names, is_categorical):
        if is_cat:
            df[feature_name] = df[feature_name].astype('category')

    df_num = df.copy()
    cat_columns = df_num.select_dtypes(['category']).columns
    df_num[cat_columns] = df_num[cat_columns].apply(lambda x: x.cat.codes)
    X = df_num[feature_names].values

    # Handle Nan Values
    if nan_handle is not None:
        assert callable(nan_handle)
        X = nan_handle(X)
    else:
        X = np.nan_to_num(X)

    X_sampled = do_sample(X, is_categorical, is_continuous, None, sample_rate)

    # Store the sampled data to a file
    n_sampled = X_sampled.shape[0]
    X_back = np.full((n_sampled, len(columns)), np.nan, dtype=np.float)
    X_back[:, feature_indices] = X_sampled
    X_back[:, 0] = np.arange(n_sampled)
    df_back = pd.DataFrame(X_back, columns=all_feature_names)
    # Map categorical data back
    for feature_name, is_cat in zip(feature_names, is_categorical):
        # print(feature_name)
        if is_cat:
            df_back[feature_name] = df_back[feature_name].apply(lambda x: df[feature_name].cat.categories[int(x)])
    df_back['d3mIndex'] = df_back['d3mIndex'].astype(np.int)
    # Fix integer types
    df_back[integer_features] = df_back[integer_features].apply(lambda x: x.astype(np.int))
    # Save to file
    df_back.to_csv(sample_path, index=False)


def main(args=None):
    import sys
    import argparse
    if args is None:
        args = sys.argv[1:]

    parser = argparse.ArgumentParser(description='Command Line Tool for sampling data')
    parser.add_argument('--sample_path', '-p', dest='sample_path', action='store',
                        help='The path to store the sampled data. E.g. sample.csv')
    parser.add_argument('--dataset', '-d', dest='data_path', action='store',
                        help='The path of the dataset. E.g. LL0_8_liver_disorders_dataset')
    parser.add_argument('--features', '-f', dest='features', action='store',
                        help='The feature indices used for input. E.g. -f 1,2,3,4')

    parser.add_argument('--sample_rate', '-r', dest='sample_rate', action='store', default=4., type=float,
                        help='The rate of the sampling, default to be 4')
    parser.add_argument('--config', '-c', dest='config', action='store',
                        help='The configuration file to use.')
    args = parser.parse_args(args)

    if args.config is not None:
        config = json2dict(args.config)
        sample_path = config['samplePath']
        dataset_path = config['datasetPath']

        problem_info = config['problemDataset'][0]
        if len(config['problemDataset']) > 1:
            raise "Error: currently only 1 resource is supported! The first resource is used"

        sample(dataset_path, problem_info['featureIndices'], sample_path, problem_info['resId'], args.sample_rate)

    else:
        feature_idx = [int(f) for f in args.features.split(',')]
        sample(args.data_path, feature_idx, args.sample_path, "0", args.sample_rate)

if __name__ == '__main__':
    main()
