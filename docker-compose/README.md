## docker-compose files

This directory contains docker-compose files to run Visus.
Currently, all system configurations are done via environment variables.

### Running the full system

The `docker-compose.yml` file provides the specification of all services
required to run Visus, which includes the Visus client and backend server (TA3)
and an AutoML system (TA2) responsible to generate machine learning pipelines.

To run the full system, make sure to correctly set the images in docker-compose file to the desired images, and then run:

```
docker-compose up
```

### Local development

The `docker-compose-ta2-dev.yml` provides a way to run only the AutoML
system (TA2) configured to output files to a local directory, which is
ideal for local development. You can run local 
develoment versions of the `server` and `client` modules using npm (see the
main README.md file for more instructions) that connect to a TA2 system running
on Docker (started by this docker-compose file).
To start the TA2 dev, run:

```
D3MINPUTDIR=/datasets/ D3MOUTPUTDIR=/output/ docker-compose -f docker-compose-ta2-dev.yml up
```
and make sure to set the values of the variables `D3MINPUTDIR` and `D3MOUTPUTDIR` to
the same path that the local development installations use.

**Note**: You can create a `.env` file in the same directory where
the `docker-compose-ta2-dev.yml` is locate to configure the environment
variables. For example create an `.env` file with:
```
D3MINPUTDIR=/datasets/
D3MOUTPUTDIR=/output/
```
And then, simply run:
```
docker-compose -f docker-compose-ta2-dev.yml up
```

For more details, see https://docs.docker.com/compose/environment-variables/.